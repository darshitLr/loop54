<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://wp1.co/
 * @since             1.0.0
 * @package           Bought_Together_For_Woocommerce
 *
 * @wordpress-plugin
 * Plugin Name: Bought Together for WooCommerce
 * Plugin URI: https://wp1.co/wp/bought-together/
 * Description: The Bought Together feature drives customers to add more items to their cart means more sales for you and increase in revenue.
 * Version: 1.0.8
 * Author: WP1
 * Author URI: http://wp1.co/
 * Developer: WP1
 * Developer URI: http://wp1.co/
 * Text Domain: bought-together-for-woocommerce
 * Domain Path: /languages
 *
 * Woo: 6399904:5382dee957609369fd971535b639ba9e
 * WC requires at least: 3.3.0
 * WC tested up to: 5.1.0
 *
 * Copyright: © 2009-2015 WooCommerce.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! defined( 'BOUGHT_TOGETHER_FOR_WOOCOMMERCE_PLUGIN_DIR' ) ) {
	define( 'BOUGHT_TOGETHER_FOR_WOOCOMMERCE_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'BOUGHT_TOGETHER_FOR_WOOCOMMERCE_PLUGIN_URL' ) ) {
	define( 'BOUGHT_TOGETHER_FOR_WOOCOMMERCE_PLUGIN_URL', plugins_url() . '/bought-together-for-woocommerce' );
}
if ( ! defined( 'BOUGHT_TOGETHER_FOR_WOOCOMMERCE_PLUGIN_FILE' ) ) {
	define( 'BOUGHT_TOGETHER_FOR_WOOCOMMERCE_PLUGIN_FILE', __FILE__ );
}
if ( ! defined( 'BOUGHT_TOGETHER_FOR_WOOCOMMERCE_PLUGIN_BASENAME' ) ) {
	define( 'BOUGHT_TOGETHER_FOR_WOOCOMMERCE_PLUGIN_BASENAME', plugin_basename( BOUGHT_TOGETHER_FOR_WOOCOMMERCE_PLUGIN_FILE ) );
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'BOUGHT_TOGETHER_FOR_WOOCOMMERCE_VERSION', '1.0.8' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-bought-together-for-woocommerce-activator.php
 */
function activate_bought_together_for_woocommerce() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bought-together-for-woocommerce-activator.php';
	$activator = new Bought_Together_For_Woocommerce_Activator();
	$activator->activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-bought-together-for-woocommerce-deactivator.php
 */
function deactivate_bought_together_for_woocommerce() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bought-together-for-woocommerce-deactivator.php';
	$deactivator = new Bought_Together_For_Woocommerce_Deactivator();
	$deactivator->deactivate();
}

register_activation_hook( __FILE__, 'activate_bought_together_for_woocommerce' );
register_deactivation_hook( __FILE__, 'deactivate_bought_together_for_woocommerce' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-bought-together-for-woocommerce.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_bought_together_for_woocommerce() {

	$plugin = new Bought_Together_For_Woocommerce();
	$plugin->run();

}
run_bought_together_for_woocommerce();
