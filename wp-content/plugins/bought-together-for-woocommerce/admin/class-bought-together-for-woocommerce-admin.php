<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://wp1.co/
 * @since      1.0.0
 *
 * @package    Bought_Together_For_Woocommerce
 * @subpackage Bought_Together_For_Woocommerce/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Bought_Together_For_Woocommerce
 * @subpackage Bought_Together_For_Woocommerce/admin
 */
class Bought_Together_For_Woocommerce_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bought_Together_For_Woocommerce_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bought_Together_For_Woocommerce_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bought-together-for-woocommerce-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bought_Together_For_Woocommerce_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bought_Together_For_Woocommerce_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/bought-together-for-woocommerce-admin.js', array( 'jquery' ), $this->version, false );

	}

	/*public function bought_together_plugin_menus() {
		add_submenu_page( 'woocommerce', 'Bought Together', 'Bought Together', 'manage_options', 'wc-settings&tab=bought_together_for_woocommerce', array( $this ) );
	}*/

	public function bought_together_plugin_action_links( $links ) {
		$action_links = array(
			'settings' => '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=bought_together_for_woocommerce' ) . '" aria-label="' . esc_attr__( 'Bought Together For Woocommerce', 'woocommerce' ) . '">' . esc_html__( 'Settings', 'woocommerce' ) . '</a>',
		);
		return array_merge( $action_links, $links );
	}

	
	public function bought_together_admin_ajaxurl() { ?>
		<script type="text/javascript">
		var btfwc_admin_ajaxurl = '<?php echo esc_url( admin_url('admin-ajax.php') ); ?>';
		</script>
		<?php
	}


	public function bought_together_add_custom_product_data_tab( $product_data_tabs ) {
		$product_data_tabs['wc-bought-together'] = array(
			'label' => __( 'Bought Together', 'bought-together' ),
			'target' => 'bought_together_product_data',
		);
		return $product_data_tabs;
	}
	
	
	public function bought_together_add_custom_product_data_fields() {
		global $woocommerce, $post, $product; 
		$bt_meta = is_array(get_post_meta($post->ID, 'bought_together_ids', true)) ? get_post_meta($post->ID, 'bought_together_ids', true) : array();
		$bt_title = get_post_meta($post->ID, 'bought_together_title', true);
		$bt_display_option = get_post_meta($post->ID, 'bought_together_display_option', true);
		$bt_variation_id = get_post_meta($post->ID, 'bought_together_variation_id', true);
		?>
		<div id="bought_together_product_data" class="panel woocommerce_options_panel">
			<div id="bought_together_tab_message"></div>
			<p class="form-field">
				<label for="bought_together_title">Bought Together Title</label>
				<input type="text" class="short" name="bought_together_title" id="bought_together_title" value="<?php echo esc_attr( $bt_title ); ?>" placeholder="Frequently Bought Together">
				<span class="woocommerce-help-tip" data-tip="Enter title to display above Bought together products in product detail page. e.g. Frequently Bought Together."></span>
			</p>
			<p class="form-field">
				<label for="bought_together_display_option">Display Option</label>
				<select class="short" name="bought_together_display_option" id="bought_together_display_option">
					<?php
					$array = array('row' => 'Row', 'table' => 'Table');
					foreach ( $array as $ak => $av ) { 
						?>
						<option value="<?php echo esc_attr( $ak ); ?>" <?php echo ( esc_attr( $bt_display_option ) == $ak ) ? 'selected' : ''; ?>><?php echo esc_attr($av); ?></option> 
						<?php
					}
					?>
				</select>
				<span class="woocommerce-help-tip" data-tip="<b>Table</b> - Items will be displayed in table format.<br><b>Row</b> - Image above with plus icon and item list below with checkboxes."></span>
			</p>
			
			<?php if ( is_variable_product_by_id( $post->ID ) ) { ?>
				<p class="form-field">
					<label for="bought_together_variation_id">Select variation to buy together</label>
					<select class="short" name="bought_together_variation_id" id="bought_together_variation_id">
						<?php
						$var_prd = wc_get_product($post->ID);
						$product_variations = $var_prd->get_available_variations();
						$cur_variation_id = !empty( $bt_variation_id ) ? $bt_variation_id : get_default_variation_id_by_product_id($post->ID);
						foreach ( $product_variations as $pvar ) {
							$name = get_the_title($pvar['variation_id']) . ' (' . get_post_field( 'post_name', $pvar['variation_id'] ) . ')';
							?>
							<option value="<?php echo intval( $pvar['variation_id'] ); ?>" <?php echo ( intval( $cur_variation_id ) == intval( $pvar['variation_id'] ) ) ? 'selected' : ''; ?>><?php echo esc_attr( $name ); ?></option>
							<?php
						}
						?>
					</select>
					<span class="woocommerce-help-tip" data-tip="<b>Table</b> - Items will be displayed in table format.<br><b>Row</b> - Image above with plus icon and item list below with checkboxes."></span>
				</p>
			<?php } ?>

			<p class="form-field">
				<label for="bought_together_ids">Bought Together Products</label>
				<?php 
				/*
				<input type="text" class="short" id="bought_together_search" placeholder="Filter products by name">
				<script>
				jQuery('#bought_together_search').on('input', function() {
					var curText = jQuery(this).val();
					if( curText.length == 0 ){
						jQuery('.bt-form-field div').show();
					}else{
						jQuery('.bt-form-field div').hide();
						jQuery('.bt-form-field div[data-search-key*="'+curText+'"]').show();
					}
				});
				</script>
				<?php */ 
				?>
				<select class="wc-product-search" multiple="multiple" style="width: 50%;" id="bought_together_ids" name="bought_together_ids[]" data-placeholder="Search for a product..." data-action="bought_together_json_search_items" data-exclude="<?php echo intval( $post->ID ); ?>">
					<?php
					if ( is_array($bt_meta) && count($bt_meta) > 0 ) {
						foreach ( $bt_meta as $bt ) { 
							echo '<option value="' . intval($bt) . '" selected="selected">' . esc_attr( get_the_title($bt) ) . ' (' . esc_attr( get_post_field( 'post_name', $bt ) ) . ')</option>';
						}
					}
					?>
				</select>
				<span class="woocommerce-help-tip" data-tip="Bought together are products which you recommend to buy together with currently viewed product."></span>
			</p>
			<?php 
			/*
			<div class="bt-form-field">
				<?php
				$all_ids = get_posts( array(
					'post_type' => 'product',
					'numberposts' => -1,
					'post_status' => 'publish',
					'fields' => 'ids',
					'orderby' => 'post_title',
					'order' => 'ASC',
					'exclude' => array( $post->ID )
				) );
				$prd_types = array('external', 'grouped');
				foreach ( $all_ids as $id ) {
					$prd = wc_get_product( $id );
					if ( ! in_array($prd->get_type(), $prd_types) ) {
						if ( $prd->is_type( 'variable' ) ) {
							$available_variations = $prd->get_available_variations();
							foreach ( $available_variations as $avar ) {
								$var_attribs = bougth_together_get_item_attributes( $avar['variation_id'] );
								if (false !== $var_attribs) {
									$name = get_the_title($avar['variation_id']) . ' (' . get_post_field( 'post_name', $avar['variation_id'] ) . ')';
									echo '<div data-search-key="' . esc_attr( $name ) . '"><label><input type="checkbox" name="bought_together_ids[]" value="' . intval( $avar['variation_id'] ) . '" ' . ( in_array($avar['variation_id'], $bt_meta) ? 'checked' : '' ) . '> &nbsp; ' . esc_attr( $name ) . '</label></div>';
								}
							}
						} else {
							$name = get_the_title($id) . ' (' . get_post_field( 'post_name', $id ) . ')';
							echo '<div data-search-key="' . esc_attr( $name ) . '"><label><input type="checkbox" name="bought_together_ids[]" value="' . intval( $id ) . '" ' . ( in_array($id, $bt_meta) ? 'checked' : '' ) . '> &nbsp; ' . esc_attr( $name ) . '</label></div>';
						}
					}
				}
				?>
			</div>*/ 
			?>
			<div class="bt-submit-button">
				<input type="hidden" name="_bought_together_post_id" value="<?php echo intval($post->ID); ?>">
				<input type="hidden" name="_bought_together_nonce" value="<?php echo esc_attr( wp_create_nonce( 'save-bought-together-' . intval($post->ID) ) ); ?>">
				<button type="button" class="button save_bought_together_product_data button-primary">Save Bought Together Products</button>
			</div>
		</div>
		<?php
	}


	/**
	 * Search bought together items ajax
	 */
	public function bought_together_json_search_items() {
		global $wpdb;
		$search_term = isset($_REQUEST['term']) ? sanitize_text_field($_REQUEST['term']) : '';
		$data = array();
		
		if ( isset($_REQUEST['security']) && wp_verify_nonce( sanitize_text_field($_REQUEST['security']), 'search-products') ) { 
			$all_ids = get_posts( array(
				's' => $search_term,
				'post_type' => 'product',
				'numberposts' => 1000,
				'post_status' => 'publish',
				'fields' => 'ids',
				'orderby' => 'post_title',
				'order' => 'ASC',
				'exclude' => array( 303 ),
				'tax_query' => array(
					array(
						'taxonomy' => 'product_type',
						'field'    => 'slug',
						'terms'    => array('external', 'grouped'),
						'operator' => 'NOT IN',
					),
				),
			) );
			$data = array();
			foreach ( $all_ids as $id ) {
				$prd = wc_get_product( $id );
				if ( $prd->is_type( 'variable' ) ) {
					$available_variations = $prd->get_available_variations();
					foreach ( $available_variations as $avar ) {
						$var_attribs = bougth_together_get_item_attributes( $avar['variation_id'] );
						if (false !== $var_attribs) {
							$name = get_the_title($avar['variation_id']) . ' (' . get_post_field( 'post_name', $avar['variation_id'] ) . ')';
							$data[ $avar['variation_id'] ] = esc_attr( $name );
						}
					}
				} else {
					$name = get_the_title($id) . ' (' . get_post_field( 'post_name', $id ) . ')';
					$data[ $id ] = esc_attr( $name );
				}
			}
		}

		wp_send_json($data);
		wp_die(); 

	}

	
	
	public function bought_together_save_product_data_items() {
		$data = array();
		$content = '';
		$status_code = '100';
		
		if ( isset($_POST['_bought_together_nonce']) && isset($_POST['_bought_together_post_id']) && wp_verify_nonce( sanitize_text_field($_POST['_bought_together_nonce']), 'save-bought-together-' . intval($_POST['_bought_together_post_id']) ) ) {
			if ( isset( $_POST['bought_together_title'] ) ) {
				update_post_meta( intval($_POST['_bought_together_post_id']), 'bought_together_title', sanitize_text_field($_POST['bought_together_title']) );
			}
			if ( isset( $_POST['bought_together_display_option'] ) ) {
				update_post_meta( intval($_POST['_bought_together_post_id']), 'bought_together_display_option', sanitize_text_field($_POST['bought_together_display_option']) );
			}
			if ( isset( $_POST['bought_together_variation_id'] ) ) {
				update_post_meta( intval($_POST['_bought_together_post_id']), 'bought_together_variation_id', sanitize_text_field($_POST['bought_together_variation_id']) );
			}			
			if ( isset( $_POST['bought_together_ids'] ) ) {
				$bought_together_ids = array_map( 'sanitize_text_field', wp_unslash($_POST['bought_together_ids']) );
				update_post_meta( intval($_POST['_bought_together_post_id']), 'bought_together_ids', $bought_together_ids );
				$status_code = 201; // data updated
				$content = 'Bought together product data updated.';
			} else {
				delete_post_meta( intval($_POST['_bought_together_post_id']), 'bought_together_ids' );
				$status_code = 201; // data updated
				$content = 'Bought together product data updated.';
			}
		} else {
			$status_code = 401; // unauthorized
			$content = 'Unauthorized access.';
		} 
	
		$data = array(
			'message' => $content,
			'status_code' => $status_code,
		);
	
		wp_send_json($data);
		wp_die(); 
	
	}
	

	public function bought_together_save_custom_fields( $id, $post ) {
		if ( isset($_POST['_bought_together_nonce']) && isset($_POST['_bought_together_post_id']) && wp_verify_nonce( sanitize_text_field($_POST['_bought_together_nonce']), 'save-bought-together-' . intval($_POST['_bought_together_post_id']) ) ) {
			$title = isset($_POST['bought_together_title']) ? sanitize_text_field($_POST['bought_together_title']) : '';
			update_post_meta( $id, 'bought_together_title', $title );

			$display_option = isset($_POST['bought_together_display_option']) ? sanitize_text_field($_POST['bought_together_display_option']) : '';
			update_post_meta( $id, 'bought_together_display_option', $display_option );

			if ( isset($_POST['bought_together_ids']) ) {
				$bought_together_ids = array_map( 'sanitize_text_field', wp_unslash($_POST['bought_together_ids']) );
				update_post_meta( $id, 'bought_together_ids', $bought_together_ids );
			} else {
				delete_post_meta( $id, 'bought_together_ids' );
			}
		}
	}


}
