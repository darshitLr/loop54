jQuery( function() {
	//'use strict';

	jQuery(document).on('click', '.save_bought_together_product_data', function(){
		var allInputs = jQuery('#bought_together_product_data :input').serialize() + "&action=bought_together_save_product_data_items";
		jQuery.ajax({
			type: 'post',
			url: btfwc_admin_ajaxurl,
			data: allInputs,
			beforeSend: function () {
				jQuery('#bought_together_product_data').addClass('bt-processing');
			},
			complete: function () {
				jQuery('#bought_together_product_data').removeClass('bt-processing');
			},
			success: function (response) {
				if( parseInt(response.status_code) == 201 ){
					var response_html = '<div class="notice notice-success is-dismissible"><p>'+response.message+'</p></div>';
				}else{
					var response_html = '<div class="error notice is-dismissible"><p>'+response.message+'</p></div>';
				}
				jQuery('#bought_together_tab_message').html(response_html).slideDown();
				// remove message after 5 seconds
				setTimeout(function(){
					jQuery('#bought_together_tab_message').slideUp().html('');
				}, 5000);
			}
		});
	});

});
