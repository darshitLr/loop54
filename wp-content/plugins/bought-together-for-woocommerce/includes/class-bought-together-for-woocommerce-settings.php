<?php
// phpcs:ignoreFile

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Bought_Together_For_Woocommerce_Settings {
	public static function init() {
		add_filter( 'woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50 );
		add_action( 'woocommerce_settings_tabs_bought_together_for_woocommerce', __CLASS__ . '::settings_tab' );
		add_action( 'woocommerce_update_options_bought_together_for_woocommerce', __CLASS__ . '::update_settings' );
	}

	public static function add_settings_tab( $settings_tabs ) {
		$settings_tabs['bought_together_for_woocommerce'] = __( 'Bought Together', 'bought-together-for-woocommerce' );
		return $settings_tabs;
	}

	public static function settings_tab() {
		woocommerce_admin_fields( self::get_settings() );
	}

	public static function update_settings() {
		woocommerce_update_options( self::get_settings() );
	}

	public static function get_settings() {
		$settings = array(
			'section_title' => array(
				'name'     => __( 'Bought Together for WooCommerce Settings', 'bought-together-for-woocommerce' ),
				'type'     => 'title',
				'desc'     => 'This is where your Bought Together for WooCommerce setting is located.',
				'id'       => 'bought_together_for_woocommerce_section_title',
			),
			/*'bt_enable_rtl_radio' => array(
				'name' => __( 'Enable RTL', 'bought-together-for-woocommerce' ),
				'type' => 'radio',
				'desc' => __( 'Select <b>Yes</b> if you want to display bought together items right to left (default: No).', 'bought-together-for-woocommerce' ),
				'id'   => 'bought_together_for_woocommerce_enable_rtl_button',
				'desc_tip' => true,
				'options' => array(
					'yes'   => __( 'Yes', 'bought-together-for-woocommerce' ),
					'no'    => __( 'No', 'bought-together-for-woocommerce' ),
				),
				'default' => 'no',
			),*/
			'bt_disable_bought_together' => array(
				'name' => __( 'Disable Bought Together', 'bought-together-for-woocommerce' ),
				'type' => 'radio',
				'desc' => __( 'Check Yes if you would like to disable Bought Together feature on all products.', 'bought-together-for-woocommerce' ),
				'id'   => 'bought_together_for_woocommerce_disable_bought_together',
				'desc_tip' => true,
				'options' => array(
					'yes'   => __( 'Yes', 'bought-together-for-woocommerce' ),
					'no'    => __( 'No', 'bought-together-for-woocommerce' ),
				),
				'default' => 'no',
			),
			'bt_display_format' => array(
				'name' => __( 'Display Option', 'bought-together-for-woocommerce' ),
				'type' => 'radio',
				'desc' => __( '<b>Table</b> - Items will be displayed in table format.<br><b>Row</b> - Image above with plus icon and item list below with checkboxes.', 'bought-together-for-woocommerce' ),
				'id'   => 'bought_together_for_woocommerce_display_format',
				'desc_tip' => true,
				'options' => array(
					'row'   => __( 'Row', 'bought-together-for-woocommerce' ),
					'table'    => __( 'Table', 'bought-together-for-woocommerce' ),
				),
				'default' => 'row',
			),
			'bt_display_location' => array(
				'name' => __( 'Display Location', 'bought-together-for-woocommerce' ),
				'type' => 'radio',
				'desc' => __( 'Select location to display bought together products', 'bought-together-for-woocommerce' ),
				'id'   => 'bought_together_for_woocommerce_display_location',
				'desc_tip' => true,
				'options' => array(
					'woocommerce_after_single_product_summary'   => __( 'After Single Product Summary', 'bought-together-for-woocommerce' ),
					'woocommerce_product_meta_start'    => __( 'Before Product Meta Start', 'bought-together-for-woocommerce' ),
					'woocommerce_product_meta_end'    => __( 'After Product Meta End', 'bought-together-for-woocommerce' ),
					'woocommerce_after_single_product'    => __( 'After Single Product Information', 'bought-together-for-woocommerce' ),
				),
				'default' => 'woocommerce_after_single_product_summary',
			),
			/*'primary_color' => array(
				'name' => __( 'Primary Color', 'bought-together-for-woocommerce' ),
				'type' => 'color',
				'desc' => __( 'Select primary color. It applies to buttons and links.', 'bought-together-for-woocommerce' ),
				'desc_tip' => true,
				'id'   => 'bought_together_for_woocommerce_primary_color',
				'default' => '#ff0000',
			),*/
			'bt_default_title_text' => array(
				'name' => __( 'Default Title', 'bought-together-for-woocommerce' ),
				'type' => 'text',
				'desc' => __( 'Enter text you want display as title above bought together product selection.', 'bought-together-for-woocommerce' ),
				'desc_tip' => true,
				'id'   => 'bought_together_for_woocommerce_default_title_text',
				'default' => 'Frequently Bought Together',
			),
			'bt_table_total_text' => array(
				'name' => __( 'Table total label text', 'bought-together-for-woocommerce' ),
				'type' => 'text',
				'desc' => __( 'Enter text you want display in table total price row.', 'bought-together-for-woocommerce' ),
				'desc_tip' => true,
				'id'   => 'bought_together_for_woocommerce_table_total_text',
				'default' => 'Total for selected item(s)',
			),
			'bt_button_one_text' => array(
				'name' => __( 'Buton text for single (1) item', 'bought-together-for-woocommerce' ),
				'type' => 'text',
				'desc' => __( 'Enter button text you want display when only one item is selected.', 'bought-together-for-woocommerce' ),
				'desc_tip' => true,
				'id'   => 'bought_together_for_woocommerce_button_one_text',
				'default' => 'Add to cart',
			),
			'bt_button_two_text' => array(
				'name' => __( 'Buton text for two (2) items', 'bought-together-for-woocommerce' ),
				'type' => 'text',
				'desc' => __( 'Enter button text you want display when two items are selected.', 'bought-together-for-woocommerce' ),
				'desc_tip' => true,
				'id'   => 'bought_together_for_woocommerce_button_two_text',
				'default' => 'Add both to cart',
			),
			'bt_button_multi_text' => array(
				'name' => __( 'Buton text for multiple items', 'bought-together-for-woocommerce' ),
				'type' => 'text',
				'desc' => __( 'Enter button text you want display when more than items are selected. <b>%%bt_count%%</b> will be replaced with number of items selected.', 'bought-together-for-woocommerce' ),
				'desc_tip' => true,
				'id'   => 'bought_together_for_woocommerce_button_multi_text',
				'default' => 'Add all %%bt_count%% to cart',
			),/*
			'bt_empty_message' => array(
				'name' => __( 'Empty selection message', 'bought-together-for-woocommerce' ),
				'type' => 'textarea',
				'desc' => __( 'Enter message to display when user deslect all items.', 'bought-together-for-woocommerce' ),
				'desc_tip' => true,
				'id'   => 'bought_together_for_woocommerce_bt_empty_message',
				'default' => 'Items not selected',
			),*/
			'section_end' => array(
				'type' => 'sectionend',
				'id' => 'bought_together_for_woocommerce_section_end',
			),
		);
		return apply_filters( 'wc_bought_together_for_woocommerce_settings', $settings );
	}

}
Bought_Together_For_Woocommerce_Settings::init();
