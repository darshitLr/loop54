<?php

/**
 * Fired during plugin activation
 *
 * @link       https://wp1.co/
 * @since      1.0.0
 *
 * @package    Bought_Together_For_Woocommerce
 * @subpackage Bought_Together_For_Woocommerce/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Bought_Together_For_Woocommerce
 * @subpackage Bought_Together_For_Woocommerce/includes
 */
class Bought_Together_For_Woocommerce_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public function activate() {
		if ( ! get_option( 'bought_together_for_woocommerce_enable_rtl_button' ) ) {
			update_option( 'bought_together_for_woocommerce_enable_rtl_button', 'no' );
		}
		if ( ! get_option( 'bought_together_for_woocommerce_display_format' ) ) {
			update_option( 'bought_together_for_woocommerce_display_format', 'row' );
		}
		if ( ! get_option( 'bought_together_for_woocommerce_display_location' ) ) {
			update_option( 'bought_together_for_woocommerce_display_location', 'woocommerce_after_single_product_summary' );
		}
		if ( ! get_option( 'bought_together_for_woocommerce_default_title_text' ) ) {
			update_option( 'bought_together_for_woocommerce_default_title_text', __( 'Frequently Bought Together', 'bought-together-for-woocommerce' ) );
		}
		if ( ! get_option( 'bought_together_for_woocommerce_table_total_text' ) ) {
			update_option( 'bought_together_for_woocommerce_table_total_text', __( 'Total for selected item(s)', 'bought-together-for-woocommerce' ) );
		}
		if ( ! get_option( 'bought_together_for_woocommerce_button_one_text' ) ) {
			update_option( 'bought_together_for_woocommerce_button_one_text', __( 'Add to cart', 'bought-together-for-woocommerce' ) );
		}
		if ( ! get_option( 'bought_together_for_woocommerce_button_two_text' ) ) {
			update_option( 'bought_together_for_woocommerce_button_two_text', __( 'Add both to cart', 'bought-together-for-woocommerce' ) );
		}
		if ( ! get_option( 'bought_together_for_woocommerce_button_multi_text' ) ) {
			update_option( 'bought_together_for_woocommerce_button_multi_text', __( 'Add all %%bt_count%% to cart', 'bought-together-for-woocommerce' ) );
		}
	}

}
