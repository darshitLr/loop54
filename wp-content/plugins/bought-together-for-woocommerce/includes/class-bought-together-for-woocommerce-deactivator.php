<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://wp1.co/
 * @since      1.0.0
 *
 * @package    Bought_Together_For_Woocommerce
 * @subpackage Bought_Together_For_Woocommerce/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Bought_Together_For_Woocommerce
 * @subpackage Bought_Together_For_Woocommerce/includes
 */
class Bought_Together_For_Woocommerce_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public function deactivate() {

	}

}
