<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://wp1.co/
 * @since      1.0.0
 *
 * @package    Bought_Together_For_Woocommerce
 * @subpackage Bought_Together_For_Woocommerce/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Bought_Together_For_Woocommerce
 * @subpackage Bought_Together_For_Woocommerce/includes
 */
class Bought_Together_For_Woocommerce {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @var      Bought_Together_For_Woocommerce_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'BOUGHT_TOGETHER_FOR_WOOCOMMERCE_VERSION' ) ) {
			$this->version = BOUGHT_TOGETHER_FOR_WOOCOMMERCE_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'bought-together-for-woocommerce';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

		require_once BOUGHT_TOGETHER_FOR_WOOCOMMERCE_PLUGIN_DIR . 'includes/class-bought-together-for-woocommerce-settings.php';

		require_once BOUGHT_TOGETHER_FOR_WOOCOMMERCE_PLUGIN_DIR . 'includes/bought-together-for-woocommerce-functions.php';

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Bought_Together_For_Woocommerce_Loader. Orchestrates the hooks of the plugin.
	 * - Bought_Together_For_Woocommerce_I18n. Defines internationalization functionality.
	 * - Bought_Together_For_Woocommerce_Admin. Defines all hooks for the admin area.
	 * - Bought_Together_For_Woocommerce_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bought-together-for-woocommerce-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bought-together-for-woocommerce-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-bought-together-for-woocommerce-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-bought-together-for-woocommerce-public.php';

		$this->loader = new Bought_Together_For_Woocommerce_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Bought_Together_For_Woocommerce_I18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 */
	private function set_locale() {

		$plugin_i18n = new Bought_Together_For_Woocommerce_I18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Bought_Together_For_Woocommerce_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_filter( 'plugin_action_links_' . plugin_basename(plugin_dir_path(__DIR__) . $this->plugin_name . '.php'), $plugin_admin, 'bought_together_plugin_action_links' );

		$this->loader->add_action( 'admin_head', $plugin_admin, 'bought_together_admin_ajaxurl' );
		$this->loader->add_action( 'woocommerce_product_data_tabs', $plugin_admin, 'bought_together_add_custom_product_data_tab', 99, 1 );
		$this->loader->add_action( 'woocommerce_product_data_panels', $plugin_admin, 'bought_together_add_custom_product_data_fields' );
		$this->loader->add_action( 'wp_ajax_bought_together_save_product_data_items', $plugin_admin, 'bought_together_save_product_data_items' );
		$this->loader->add_action( 'woocommerce_process_product_meta', $plugin_admin, 'bought_together_save_custom_fields', 10, 2 );

		$this->loader->add_action('wp_ajax_bought_together_json_search_items', $plugin_admin, 'bought_together_json_search_items');

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 */
	private function define_public_hooks() {

		$plugin_public = new Bought_Together_For_Woocommerce_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$this->loader->add_action( 'wp_head', $plugin_public, 'bought_together_ajaxurl' );

		$bt_display_location = get_option( 'bought_together_for_woocommerce_display_location', 'woocommerce_after_single_product_summary' );
		$this->loader->add_action( $bt_display_location, $plugin_public, 'bought_together_render_add_to_cart_table', 10 );

		$this->loader->add_action( 'wp_ajax_bought_together_get_table_data', $plugin_public, 'bought_together_get_table_data' );
		$this->loader->add_action( 'wp_ajax_nopriv_bought_together_get_table_data', $plugin_public, 'bought_together_get_table_data' );

		$this->loader->add_action( 'wp_ajax_bought_together_add_to_cart', $plugin_public, 'bought_together_add_to_cart' );
		$this->loader->add_action( 'wp_ajax_nopriv_bought_together_add_to_cart', $plugin_public, 'bought_together_add_to_cart' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Bought_Together_For_Woocommerce_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
