<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function bought_together_get_table_html( $pid, $child = 0, $update = false ) {

	$bt_display_format = get_option( 'bought_together_for_woocommerce_display_format', 'row' );
	$bt_default_title_text = __( get_option( 'bought_together_for_woocommerce_default_title_text', 'Frequently Bought Together'), 'bought-together-for-woocommerce' );
	$bt_table_total_text = __( get_option( 'bought_together_for_woocommerce_table_total_text', 'Total for selected item(s)'), 'bought-together-for-woocommerce' );
	$bt_button_one_text = __( get_option( 'bought_together_for_woocommerce_button_one_text', 'Add to cart' ), 'bought-together-for-woocommerce');
	$bt_button_two_text = __( get_option( 'bought_together_for_woocommerce_button_two_text', 'Add both to cart' ), 'bought-together-for-woocommerce');
	$bt_button_multi_text = __( get_option( 'bought_together_for_woocommerce_button_multi_text', 'Add all %%bt_count%% to cart' ), 'bought-together-for-woocommerce' );

	$bt_meta = get_post_meta($pid, 'bought_together_ids', true);
	$bt_display_option = get_post_meta($pid, 'bought_together_display_option', true);
	$bt_variation_id = get_post_meta($pid, 'bought_together_variation_id', true);
	$bt_title = empty( get_post_meta($pid, 'bought_together_title', true) ) ? $bt_default_title_text : get_post_meta($pid, 'bought_together_title', true);
	$child_products = explode(',', $child);

	$bt_display_format = !empty( $bt_display_option ) ? $bt_display_option : $bt_display_format;

	$images = array();
	$output = '<div class="bought_together_products">';
	$bt_title_html = '<h3>' . $bt_title . '</h3>';
	$output .= $bt_title_html;
	$output .= '<table class="bought_together_tbl" cellspacing="0">
		<tbody>';
		
	$check_count = 0;
	$sum = 0;
	$item_list_formatted = array();
	if ( is_array($bt_meta) && count($bt_meta) > 0 ) {
		if ( is_variable_product_by_id( $pid ) && $bt_variation_id > 0 ) {
			$pid = $bt_variation_id;
		}
		array_unshift($bt_meta, $pid);
		$allowed_product_types = array('variation', 'simple');
		foreach ( $bt_meta as $prd ) {
			$_product   = wc_get_product($prd);

			$product_permalink = get_permalink( $prd ); 
			if ( in_array( $_product->get_type(), $allowed_product_types ) ) {

				$parent_id = wp_get_post_parent_id($prd);

				$attributes = '';
				$attr_json = '';
				$enabled = true;
				if ( $parent_id > 0 ) {
					$attributes_html = array();
					$variation = new WC_Product_Variation( $prd );
					$var_attribs = bougth_together_get_item_attributes( $prd );
					$enabled = ( false !== $var_attribs ) ? true : false; 
					if (false !== $var_attribs) {
						foreach ( $var_attribs as $attr_key => $attr_val ) {
							$attributes_html[] = '<small><strong>' . $attr_key . ':</strong> ' . $attr_val . '</small>';
						}
						if ( count($attributes_html) > 0 ) {
							$attributes = '(' . implode(', ', $attributes_html) . ')';
							$attr_json = urlencode( json_encode($var_attribs) );
						}
					}
				}
				if ( false !== $enabled ) {

					if ( false == $update || in_array($_product->get_id(), $child_products) ) {
						$images[] = $_product->get_image('woocommerce_gallery_thumbnail');
						$sum += ( 'incl' == get_option( 'woocommerce_tax_display_shop', 'excl' ) ) ?  $_product->get_price_including_tax() : $_product->get_price();
						$check_count++;
					}

					$disabled_current = ''; //( $_product->get_id() == $pid ) ? 'disabled' : '' ;
					$product_name = ( $_product->get_id() == $pid ) ? '<strong>' . __('This item', 'bought-together-for-woocommerce') . ':</strong> ' . $_product->get_name() . ' ' . $attributes : '<a href="' . get_permalink( $_product->get_id() ) . '">' . $_product->get_name() . ' ' . $attributes . '</a>';
					$prd_checked = ( false == $update ) ? 'checked' : ( ( in_array($_product->get_id(), $child_products) ) ? 'checked' : '' ) ;
					$product_input_checkbox = '<input type="checkbox" data-prd-id="' . $_product->get_id() . '" data-attribute="' . $attr_json . '" ' . $disabled_current . ' ' . $prd_checked . ' data-price="' . $_product->get_price() . '">';
					$output .= '<tr>
								<td>' . $product_input_checkbox . 
								$_product->get_image('woocommerce_gallery_thumbnail') . $product_name . 
								'</td>
								<td class="bt-price">' . $_product->get_price_html() . '</td>
							</tr>';

					$item_list_formatted[] = $product_input_checkbox . ' &nbsp; ' . $product_name . ' <strong>' . $_product->get_price_html() . '</strong>' ;
				}
			}
		}

		$output .= '<tr class="bt-total-sum-row">
					<td><strong>' . $bt_table_total_text . '</strong></td>
					<td class="bt-price"><strong>' . wc_price( $sum ) . '</strong></td>
				</tr>';
	}

	$add_all_button_text = ( 1 == $check_count ) ? $bt_button_one_text : ( ( 2 == $check_count ) ? $bt_button_two_text : str_replace('%%bt_count%%', $check_count, $bt_button_multi_text) ); // bought_together_number_to_text($check_count) text
	$add_all_button = '<button type="button" id="bought-together-add-to-cart" data-nonce="' . esc_attr( wp_create_nonce( 'add-bt-to-cart' ) ) . '" class="button alt">' . $add_all_button_text . ' </button>';

	$output .= '</tbody></table>';
	if ( $check_count > 0 ) {
		$output .= '<p class="text-right">' . $add_all_button . '</p>';
	}
	$output .= '</div>';

	if ( 'row' == $bt_display_format ) {
		$output = '';
		$output .= '<div style="clear:both;"></div>' . $bt_title_html;
		if ( $check_count > 0 ) {
			$output .= '<div class="bt-items-container">';
			$output .= '<div>' . implode('</div><div class="bt-plus-icon"><div>+</div></div><div>', $images) . '</div>';
			$output .= '<div class="bt-add-all-button"><span>' . __('Total', 'bought-together-for-woocommerce') . ': <strong>' . wc_price( $sum ) . '</strong></span><span>' . $add_all_button . '</span></div>' ;
			$output .= '</div>';
		}
		$output .= '<div class="bt-list-items"><label class="bt-list-item">' . implode('</label><label class="bt-list-item-">', $item_list_formatted) . '</label></div><div style="clear:both;"></div>';
	}
	return $output;
}

function bougth_together_get_item_attributes( $variation_id ) {

	$parent_id = wp_get_post_parent_id($variation_id);

	$_product = wc_get_product($parent_id);
	$default_attributes = $_product->get_default_attributes();

	$attrs = array();
	foreach ( $default_attributes as $ak => $av ) {
		$attrs[$ak][ 'key' ] = wc_attribute_label($ak);
		$attrs[$ak][ 'value' ] = $av;
	}

	$variation = new WC_Product_Variation( $variation_id );
	$attribs = $variation->get_variation_attributes();

	$attributes = array();
	foreach ( $attribs as $attr_key => $attr_val ) {
		$attr_key = str_replace('attribute_', '', $attr_key);
		$key = wc_attribute_label( $attr_key, $variation );
		$attributes[ $key ] = !empty($attr_val) ? $attr_val : ( !empty($attrs[$attr_key]['value']) ? $attrs[$attr_key]['value'] : '' );
	}

	if (count(array_filter($attributes)) == count($attributes)) { 
		return $attributes;
	} else {
		return false;
	}

}


function bought_together_kses_allowed_html() {
	global $allowedposttags;
	$rev_tags = $allowedposttags;
	$rev_tags['input'] = array(
		'type' => 'checkbox',
		'data-prd-id' => true,
		'data-attribute' => true,
		'disabled' => true,
		'checked' => array(
			'true', 'false'
		),
		'data-price' => true
	); 
	return $rev_tags;
}

function bought_together_number_to_text( $number) {
	$no = floor($number);
	$hundred = null;
	$digits_1 = strlen($no);
	$i = 0;
	$str = array();
	$words = array('0' => '', '1' => 'one', '2' => 'two',
	'3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
	'7' => 'seven', '8' => 'eight', '9' => 'nine',
	'10' => 'ten', '11' => 'eleven', '12' => 'twelve',
	'13' => 'thirteen', '14' => 'fourteen',
	'15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
	'18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
	'30' => 'thirty', '40' => 'forty', '50' => 'fifty',
	'60' => 'sixty', '70' => 'seventy',
	'80' => 'eighty', '90' => 'ninety');
	$digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
	while ($i < $digits_1) {
		$divider = ( 2 == $i ) ? 10 : 100;
		$number = floor($no % $divider);
		$no = floor($no / $divider);
		$i += ( 10 == $divider ) ? 1 : 2;
		if ($number) {
			
			//$plural = ( ( $counter = count($str) ) && $number > 9 ) ? 's' : null;
			$counter = count($str);
			$plural = ( !empty($counter) && $number > 9 ) ? 's' : null;

			$hundred = ( 1 == $counter  && $str[0] ) ? ' and ' : null;
			$str [] = ( $number < 21 ) ? $words[$number] .
			' ' . $digits[$counter] . $plural . ' ' . $hundred
			:
			$words[floor($number / 10) * 10]
			. ' ' . $words[$number % 10] . ' '
			. $digits[$counter] . $plural . ' ' . $hundred;
		} else {
			$str[] = null;
		}
	}
	$str = array_reverse($str);
	$result = implode('', $str);
	return $result;
}



function get_default_variation_id_by_product_id( $prd_id = 0 ) {
	$_product = wc_get_product( $prd_id );
	$variation_id = 0;
	if ( $_product->get_type() == 'variable' ) {
		$default_attributes = $_product->get_default_attributes();
		foreach ($_product->get_available_variations() as $variation_values ) {
			foreach ($variation_values['attributes'] as $key => $attribute_value ) {
				$attribute_name = str_replace( 'attribute_', '', $key );
				$default_value = $_product->get_variation_default_attribute($attribute_name);
				if ( $default_value == $attribute_value ) {
					$is_default_variation = true;
				} else {
					$is_default_variation = false;
					break; // Stop this loop to start next main lopp
				}
			}
			if ( $is_default_variation ) {
				$variation_id = $variation_values['variation_id'];
				break; // Stop the main loop
			}
		}
	}
	return $variation_id;
}


function is_variable_product_by_id( $prd_id = 0 ) {
	$_product = wc_get_product( $prd_id );
	if ( $_product->get_type() == 'variable' ) {
		return true;
	}
	return false;
}




//[bought_together_table product_id=123456]
add_shortcode( 'bought_together_table', 'bought_together_table_product_page_shortcode' );
function bought_together_table_product_page_shortcode( $atts ) {
	$a = shortcode_atts( array(
		'product_id' => 0,
	), $atts );
	if ( $a['product_id'] > 0 ) {
		$pid = $a['product_id'];
	} else {
		global $post;
		$pid = $post->ID;
	}

	$html = bought_together_get_product_page_table_html($pid);

	$allowed_kses = bought_together_kses_allowed_html();
	return wp_kses($html, $allowed_kses );
}


function bought_together_get_product_page_table_html( $pid) {
	$bt_disable_bought_together = get_option( 'bought_together_for_woocommerce_disable_bought_together', 'no' );

	$table_html = '';
	if ( 'yes' != $bt_disable_bought_together ) {
		$bt_meta = get_post_meta($pid, 'bought_together_ids', true);
		if ( is_array($bt_meta) && count($bt_meta) > 0 ) {
			$bt_enable_rtl = ( 'yes' == get_option( 'bought_together_for_woocommerce_enable_rtl_button', 'no' ) ) ? 'bought-together-rtl' : 'bought-together-ltr';
			$table_html  = '<div style="clear:both"></div>';
			$table_html .= '<div id="bought-together-products-data" class="' . $bt_enable_rtl . '" data-nonce="' . esc_attr( wp_create_nonce( 'get-bt-table' ) ) . '" data-id="' . intval( $pid ) . '">';
			$table_html .= bought_together_get_table_html( $pid );
			$table_html .= '</div>';
			$table_html .= '<div style="clear:both"></div>';
		}
	} 
	return $table_html;
}
