jQuery(function() {
	//'use strict';

	jQuery(document).on('click', '#bought-together-add-to-cart', function(){
		var this_nonce = jQuery(this).attr('data-nonce');
		var prdChked = [];
		var JSONObj = new Object();
		jQuery('#bought-together-products-data input[type="checkbox"]:checked').each( function(){
			prdChked.push( jQuery(this).attr('data-prd-id') );
			JSONObj[jQuery(this).attr('data-prd-id')] = jQuery(this).attr('data-attribute');
		});
		//console.log( JSON.stringify(JSONObj) );
		var allPrdChecked = prdChked.toString();
		//console.log( allPrdChecked );
		jQuery.ajax({
			type: 'post',
			url: btfwc_ajaxurl,
			data: {
				action: 'bought_together_add_to_cart',
				_nonce: this_nonce,
				_prd_checked: JSONObj,
			},
			beforeSend: function () {
				jQuery('#bought-together-products-data').addClass('processing');
			},
			complete: function () {
				jQuery('#bought-together-products-data').removeClass('processing');
				jQuery( document.body ).trigger( 'wc_fragment_refresh' );
			},
			success: function (response) {
				var response_json = JSON.parse( response );
				//console.log( response_json );
				jQuery('#bought-together-products-data').html(response_json.content);
				if( '' != response_json.redirect ){
					//jQuery('#bought-together-products-data').append('<p>Wait...!!! You are redirecting to cart.</p>');
					setTimeout(function(){ 
						location.href = response_json.redirect; 
					}, 1000);
				}
			},
		});
	});


	jQuery(document).on('change', '#bought-together-products-data input[type="checkbox"]', function(){
		var prdChkd = [];
		jQuery('#bought-together-products-data input[type="checkbox"]:checked').each( function(){
			prdChkd.push( jQuery(this).attr('data-prd-id') );
		});
		var prdChecked = prdChkd.toString();
		jQuery('#bought-together-products-data').refreshBoughtTogetherTableData(prdChecked, true);
	});


	jQuery.fn.refreshBoughtTogetherTableData = function( pChecked, updateState = false ){
		var this_nonce = jQuery('#bought-together-products-data').attr('data-nonce');
		var this_pid = jQuery('#bought-together-products-data').attr('data-id');
		
		jQuery.ajax({
			type: 'post',
			url: btfwc_ajaxurl,
			data: {
				action: 'bought_together_get_table_data',
				_pid: this_pid,
				_nonce: this_nonce,
				_prd_checked: pChecked,
				_update: updateState
			},
			beforeSend: function () {
				jQuery('#bought-together-products-data').addClass('processing');
			},
			complete: function () {
				jQuery('#bought-together-products-data').removeClass('processing');
			},
			success: function (response) {
				jQuery('#bought-together-products-data').html(response);
			},
		});
	}

});
