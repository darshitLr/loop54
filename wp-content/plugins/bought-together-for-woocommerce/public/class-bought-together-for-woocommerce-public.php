<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://wp1.co/
 * @since      1.0.0
 *
 * @package    Bought_Together_For_Woocommerce
 * @subpackage Bought_Together_For_Woocommerce/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Bought_Together_For_Woocommerce
 * @subpackage Bought_Together_For_Woocommerce/public
 */
class Bought_Together_For_Woocommerce_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bought_Together_For_Woocommerce_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bought_Together_For_Woocommerce_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bought-together-for-woocommerce-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bought_Together_For_Woocommerce_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bought_Together_For_Woocommerce_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/bought-together-for-woocommerce-public.js', array( 'jquery' ), $this->version, false );

	}


	public function bought_together_ajaxurl() { ?>
		<script type="text/javascript">
		var btfwc_ajaxurl = '<?php echo esc_url( admin_url('admin-ajax.php') ); ?>';
		</script>
		<?php
	}


	public function bought_together_render_add_to_cart_table() {
		global $post;
		$pid = $post->ID;

		$bt_disable_bought_together = get_option( 'bought_together_for_woocommerce_disable_bought_together', 'no' );

		$table_html = '';
		if ( 'yes' != $bt_disable_bought_together ) {
			$bt_meta = get_post_meta($pid, 'bought_together_ids', true);
			if ( is_array($bt_meta) && count($bt_meta) > 0 ) {
				$bt_enable_rtl = ( 'yes' == get_option( 'bought_together_for_woocommerce_enable_rtl_button', 'no' ) ) ? 'bought-together-rtl' : 'bought-together-ltr';
				$table_html  = '<div style="clear:both"></div>';
				$table_html .= '<div id="bought-together-products-data" class="' . $bt_enable_rtl . '" data-nonce="' . esc_attr( wp_create_nonce( 'get-bt-table' ) ) . '" data-id="' . intval( $pid ) . '">';
				$table_html .= bought_together_get_table_html( $pid );
				$table_html .= '</div>';
				$table_html .= '<div style="clear:both"></div>';
			}
			
			$allowed_kses = bought_together_kses_allowed_html();
			echo wp_kses($table_html, $allowed_kses );
		}
	}


	public function bought_together_get_table_data() {
		if ( isset($_POST['_nonce']) && wp_verify_nonce( sanitize_text_field($_POST['_nonce']), 'get-bt-table' ) ) {
			$_pid = isset($_POST['_pid']) ? intval($_POST['_pid']) : 0;
			$_prd_checked = isset( $_POST['_prd_checked'] ) ? sanitize_text_field( $_POST['_prd_checked'] ) : array();
			$_update = isset($_POST['_update']) ? (bool) $_POST['_update'] : false;
			$content = bought_together_get_table_html( $_pid, $_prd_checked, $_update );
		} else {
			$content = '<div class="woocommerce-info">Something went wrong! Try again.</div>';
		}
		$allowed_kses = bought_together_kses_allowed_html();
		echo wp_kses($content, $allowed_kses );
		wp_die(); 
	}
	
	
	public function bought_together_add_to_cart() {
		global $woocommerce;
		$content = '';
		if ( isset($_POST['_nonce']) && wp_verify_nonce( sanitize_text_field($_POST['_nonce']), 'add-bt-to-cart' ) ) {
			$_prd_checked = isset( $_POST['_prd_checked'] ) ? array_map( 'sanitize_text_field', wp_unslash($_POST['_prd_checked']) ) : array();
			$count = 0;
			if ( count($_prd_checked) > 0 ) {
				foreach ( $_prd_checked as $prd => $prdval ) {
					$count++;
					$product_status = get_post_status($prd);
					$parent_id = wp_get_post_parent_id($prd);
					$product_id = ( $parent_id > 0 ) ? $parent_id: $prd;
					$variation_id = ( $parent_id > 0 ) ? $prd: 0;

					$attributes = empty($prdval) ? array() : json_decode( urldecode($prdval), true );
					$woocommerce->cart->add_to_cart($product_id, 1, $variation_id, $attributes);
				}
				/* translators: %d: number of products */
				$content = '<div class="woocommerce-info">' . sprintf( __( '%d product(s) added to cart.', 'bought-together-for-woocommerce' ), 
				$count ) . '</div>';
			}
		} else {
			$content = '<div class="woocommerce-info">Something went wrong! Try again.</div>';
		}
	
		if ( 'yes' === get_option( 'woocommerce_cart_redirect_after_add' ) ) {
			$cart_url = wc_get_cart_url();
			$return_content = array( 'content' => wp_kses_post($content), 'redirect' => $cart_url );
		} else {
			$return_content = array( 'content' => wp_kses_post($content), 'redirect' => '' );
		}
		echo json_encode($return_content);
		wp_die(); 
	}
	

}
