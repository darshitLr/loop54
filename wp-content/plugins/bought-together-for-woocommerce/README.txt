=== Bought Together for WooCommerce ===
Contributors: wponeco
Tags: bought together, frequently bought together, bundle, group
Requires at least: 3.3.0
Tested up to: 5.1.0
Stable tag: 4.3
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

The Bought Together feature drives customers to add more items to their cart means more sales for you and increase in revenue.

== Description ==

 You can create a bundle based on previous purchases or relevance. When a customer visits a product page, a bundle of recommended products is displayed. With a single click on "Add to cart" button, all products in bundle are added to the Cart. Customer can add all of the Frequently Bought Together products to their cart at once, or select the specific ones they want to bundle together in one order. For example: customer who is looking for laptop might also be interested in wireless mouse, laptop case, pen drive. 

The Bought Together for WooCommerce plugin brings the smart feature in a most convenient way, so you can configure Frequently Bought Together product bunch within a seconds. The Frequently Bought Together feature increases engagement with consumers. Its easier for customers to add multiple products in a single click which means more sales for you and increase in revenue.

== Features ==

* Configure the layout of Frequently Bought Together section for each product (Row or Table).
* Configure unlimited number of recommended products for each product in your store.
* Configure the title of Frequently Bought Together section.
* Configure Add to Cart button text.
* Configure the Frequently Bought Together widget position.
* Enable / Disable Frequently Bought Together feature.

== Installation ==

How to install the plugin and get it working.

1. Download the `bought-together-for-woocommerce.zip` file from your WooCommerce account.
2. Go to: WordPress Admin > Plugins > Add New and Upload Plugin with the file you downloaded with Choose File.
3. Install Now and Activate the extension.

More information at: Installing and Managing Plugins (https://wordpress.org/support/article/managing-plugins/).

Once installed and activated, you can see Bought Together settings under WooCommerce and Bought Together tab in Product Data section in product add/edit screen.

== Frequently Asked Questions ==

= How can I display Bought Together feature in product page? =

You can select multiple products tocreate a bundle, which can be displaed in 2 formats:
1. Row - Image above with plus icon and list of items below with checkbox.
2. Table - Bought together items displayed in table format.

== How it works? ==

Customer can add all of the Frequently Bought Together products to their cart at once, or select the specific ones they want to bundle together in one order.

== How does it help to increase revenue? ==

The Frequently Bought Together feature increases engagement and stickiness with consumers. Its easier for customers to add multiple products in a single click which means more sales for you and increase in revenue.

== Screenshots ==

1. https://woocommerce.com/wp-content/uploads/2020/08/bt-frontend-product-row.png
2. https://woocommerce.com/wp-content/uploads/2020/08/bt-frontend-product-table.png
3. https://woocommerce.com/wp-content/uploads/2020/08/bt-admin-product-edit.png
4. https://woocommerce.com/wp-content/uploads/2020/08/bt-admin-settings.png

== Changelog ==

= 1.0.0 =
* Inital release

= 1.0.1 =
* Translation and POT template file issues fixed.

= 1.0.2 =
* Compatibility checked for latest WooCommerce.

= 1.0.3 =
* Redirect after add to cart, if option selected.
* Variable product with no attribute issue fixed.
* Compatibility checked for latest WooCommerce.

= 1.0.4 =
* Compatibility checked for latest WooCommerce.

= 1.0.5 =
* Shortcode `[bought_together_table]` added to display Bought Together items on product page.

= 1.0.6 =
* Admin bought together product search changed to ajax product search instead of displaying all products at once.

= 1.0.7 =
* Product page bought together row items display issue fixed.

= 1.0.8 =
* Responsive layout issues fixed.
* Shortcode added to display bought together items on any page or post [bought_together_table product_id=123456]
