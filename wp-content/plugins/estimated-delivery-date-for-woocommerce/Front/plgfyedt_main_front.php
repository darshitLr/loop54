<?php
add_action('woocommerce_before_add_to_cart_button', 'plgfyedt_create_table_or_msg');
function plgfyedt_create_table_or_msg() {
	$all_rules=get_option('plgfyedt_all_rules_in_db');
	if ('' == $all_rules) {
		$all_rules=array();
	}
	$current_product_id= get_the_ID();
	$product=wc_get_product(get_the_ID());
	$break=false;
	foreach ($all_rules['isactive'] as $key => $value) {
		if ('true' == $value) {
			if ('Products' == $all_rules['plgfqdp_applied_onc'][$key]) {
				if (isset($all_rules['applied_on_ids'][$key]) && 0 < count($all_rules['applied_on_ids'][$key])) {
					if (in_array($current_product_id, $all_rules['applied_on_ids'][$key])) {
						
						$break=true;
					}
				}
			} else if ('Category' == $all_rules['plgfqdp_applied_onc'][$key]) {
				if (isset($all_rules['applied_on_ids'][$key]) && 0 < count($all_rules['applied_on_ids'][$key])) {
					$cat_ids = wp_get_post_terms($current_product_id, 'product_cat', array('fields'=>'ids'));
					foreach ($cat_ids as $key1 => $value1) {							
						if (isset($all_rules['plgfqdp_applied_onc'][$key]) && in_array($value1, $all_rules['applied_on_ids'][$key])) {
							
							$break=true;
						}
					}
				}
			} else if ('whole' == $all_rules['plgfqdp_applied_onc'][$key]) {
				$break=true;
			}
		}
		if ($break) {
			$messageto_be_shown=$all_rules['to_b_displayd'][$key];
			$messageto_be_shown= str_replace('[days]', $all_rules['estimated_start'][$key] . '-' . $all_rules['estimated_end'][$key], $messageto_be_shown);
			?>
			<input value="<?php echo filter_var($all_rules['estimated_start'][$key]); ?>-<?php echo filter_var($all_rules['estimated_end'][$key]); ?>" type="hidden" name="howmanydays" id="howmanydays">
			<input value="<?php echo filter_var($all_rules['to_b_displayd'][$key]); ?>" type="hidden" name="days_msg_cart" id="days_msg_cart">
			<input value="<?php echo filter_var($all_rules['estimated_start'][$key] . '-' . $all_rules['estimated_end'][$key]); ?>" type="hidden" name="days_msggg_carttt" id="days_msggg_carttt">

			<?php
			if ('variable' == $product->get_type()) {
				$variations=$product->get_available_variations();
				$variations_id = wp_list_pluck( $variations, 'variation_id' );
				foreach ($variations_id as $keyforprogress => $valueforprogress) {
					$current_product_id=$valueforprogress;
					?>

					<div style="display: none;" class="main_div_parent_plfyedt" id="main_div_parent_plfyedt<?php echo filter_var($current_product_id); ?>">
						<?php
						$plgfydc_all_data=get_option('save_gnrl_settings_plgfyedt');

						if ('2' == $plgfydc_all_data['msg_template']) {
							?>
							<div style="color: #<?php echo filter_var($plgfydc_all_data['clr']); ?>;width: 98%;background-color: #<?php echo filter_var($plgfydc_all_data['bg_clr']); ?>;border: 1px solid #<?php echo filter_var($plgfydc_all_data['brdr_color']); ?>;border-radius: 4px;padding: 16px;">
								<?php echo filter_var($messageto_be_shown); ?>
							</div>
							<?php
						} else if ('1' == $plgfydc_all_data['msg_template']) {
							?>
							<div style="color: #<?php echo filter_var($plgfydc_all_data['clr']); ?>;width: 98%;background-color: #<?php echo filter_var($plgfydc_all_data['bg_clr']); ?>;border-left: 5px solid #<?php echo filter_var($plgfydc_all_data['brdr_color']); ?>;padding: 16px;">
								<?php echo filter_var($messageto_be_shown); ?>
							</div>
							<?php
						} else if ('3' == $plgfydc_all_data['msg_template']) {
							?>
							<div style="color: #<?php echo filter_var($plgfydc_all_data['clr']); ?>;width: 98%;background-color: #<?php echo filter_var($plgfydc_all_data['bg_clr']); ?>;border-radius: 4px;padding: 16px;box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);">
								<?php echo filter_var($messageto_be_shown); ?>
							</div>
							<?php

						}
						?>


						<br>




						<?php
						if (isset($all_rules['scenarios'][$key][0]) && count($all_rules['scenarios'][$key][0]) > 0) {

							?>
							
							<label style="font-size:20px;">Want to change expected delivery time?</label><br>
							<table >
								<tr>



									<th>Expected Days</th>
									<th>Price Per Item</th>
								</tr>
								<?php
								foreach ($all_rules['scenarios'][$key][0] as $key_inner => $value_inner) {
									?>
									<tr>

										<td>
											<?php
											$returned_price=get_discounted_altered_price($current_product_id, $all_rules['scenarios'][$key][2][$key_inner], $all_rules['scenarios'][$key][3][$key_inner]); 
											?>
											<input value="<?php echo filter_var($returned_price); ?>" type="radio" name="unique_name">
											<label id="lbl_plgfy">
												<?php echo filter_var($value_inner . '-' . $all_rules['scenarios'][$key][1][$key_inner]); ?>
											</label>

										</td>
										<td>
											<?php
											$returned_price=get_discounted_altered_price($current_product_id, $all_rules['scenarios'][$key][2][$key_inner], $all_rules['scenarios'][$key][3][$key_inner]); 
											echo filter_var(wc_price($returned_price));
											echo filter_var(get_discounted_string($all_rules['scenarios'][$key][2][$key_inner], $all_rules['scenarios'][$key][3][$key_inner]));
											?>
										</td>
									</tr>
									<?php

								}
								?>
								<tr>

									<td>
										<?php
										$new_price=get_post_meta($current_product_id, '_sale_price', true);
										if ('' == $new_price || 0 == $new_price) {
											$new_price= get_post_meta($current_product_id, '_regular_price', true);
										}	
										?>
										<input id="defaultt" value="<?php echo filter_var($new_price); ?>" checked type="radio" name="unique_name">
										<label id="lbl_plgfy">
											<?php echo filter_var($all_rules['estimated_start'][$key]); ?>-<?php echo filter_var($all_rules['estimated_end'][$key]); ?>
										</label>

									</td>
									<td>
										<?php
										$new_price=get_post_meta($current_product_id, '_sale_price', true);
										if ('' == $new_price || 0 == $new_price) {
											$new_price= get_post_meta($current_product_id, '_regular_price', true);
										}							
										echo filter_var(wc_price($new_price));
										echo filter_var(' (Normal Price)');
										?>
									</td>
								</tr>
							</table>
							<?php
						}
						?>
					</div>
					<?php
				}
			} else {
				?>
				<div class="main_div_parent_plfyedt">
					<?php
					$plgfydc_all_data=get_option('save_gnrl_settings_plgfyedt');
					$messageto_be_shown=$all_rules['to_b_displayd'][$key];
					$messageto_be_shown= str_replace('[days]', $all_rules['estimated_start'][$key] . '-' . $all_rules['estimated_end'][$key], $messageto_be_shown);

					if ('2' == $plgfydc_all_data['msg_template']) {
						?>
						<div style="color: #<?php echo filter_var($plgfydc_all_data['clr']); ?>;width: 98%;background-color: #<?php echo filter_var($plgfydc_all_data['bg_clr']); ?>;border: 1px solid #<?php echo filter_var($plgfydc_all_data['brdr_color']); ?>;border-radius: 4px;padding: 16px;">
							<?php echo filter_var($messageto_be_shown); ?>
						</div>
						<?php
					} else if ('1' == $plgfydc_all_data['msg_template']) {
						?>
						<div style="color: #<?php echo filter_var($plgfydc_all_data['clr']); ?>;width: 98%;background-color: #<?php echo filter_var($plgfydc_all_data['bg_clr']); ?>;border-left: 5px solid #<?php echo filter_var($plgfydc_all_data['brdr_color']); ?>;padding: 16px;">
							<?php echo filter_var($messageto_be_shown); ?>
						</div>
						<?php
					} else if ('3' == $plgfydc_all_data['msg_template']) {
						?>
						<div style="color: #<?php echo filter_var($plgfydc_all_data['clr']); ?>;width: 98%;background-color: #<?php echo filter_var($plgfydc_all_data['bg_clr']); ?>;border-radius: 4px;padding: 16px;box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);">
							<?php echo filter_var($messageto_be_shown); ?>
						</div>
						<?php

					}
					?>
					


					
					<br>
					
					
					



					<?php
					if (isset($all_rules['scenarios'][$key][0]) && count($all_rules['scenarios'][$key][0]) > 0) {
						?>
						
						<label style="font-size:20px;">Want to change expected delivery time?</label><br>
						<table >
							<tr>



								<th>Expected Days</th>
								<th>Price Per Item</th>
							</tr>
							<?php
							foreach ($all_rules['scenarios'][$key][0] as $key_inner => $value_inner) {
								?>
								<tr>

									<td>
										<?php
										$returned_price=get_discounted_altered_price($current_product_id, $all_rules['scenarios'][$key][2][$key_inner], $all_rules['scenarios'][$key][3][$key_inner]); 
										?>
										<input value="<?php echo filter_var($returned_price); ?>" type="radio" name="unique_name">
										<label id="lbl_plgfy">
											<?php echo filter_var($value_inner . '-' . $all_rules['scenarios'][$key][1][$key_inner]); ?>
										</label>

									</td>
									<td>
										<?php
										$returned_price=get_discounted_altered_price($current_product_id, $all_rules['scenarios'][$key][2][$key_inner], $all_rules['scenarios'][$key][3][$key_inner]); 
										echo filter_var(wc_price($returned_price));
										echo filter_var(get_discounted_string($all_rules['scenarios'][$key][2][$key_inner], $all_rules['scenarios'][$key][3][$key_inner]));
										?>
									</td>
								</tr>
								<?php

							}
							?>
							<tr>

								<td>
									<?php
									$new_price=get_post_meta($current_product_id, '_sale_price', true);
									if ('' == $new_price || 0 == $new_price) {
										$new_price= get_post_meta($current_product_id, '_regular_price', true);
									}	
									?>
									<input id="defaultt" value="<?php echo filter_var($new_price); ?>" checked type="radio" name="unique_name">
									<label id="lbl_plgfy">
										<?php echo filter_var($all_rules['estimated_start'][$key]); ?>-<?php echo filter_var($all_rules['estimated_end'][$key]); ?>
									</label>

								</td>
								<td>
									<?php
									$new_price=get_post_meta($current_product_id, '_sale_price', true);
									if ('' == $new_price || 0 == $new_price) {
										$new_price= get_post_meta($current_product_id, '_regular_price', true);
									}							
									echo filter_var(wc_price($new_price));
									echo filter_var(' (Normal Price)');
									?>
								</td>
							</tr>
						</table>
						<?php
					} else {
						?>
						
						
						<table style="display:none;">
							<tr>



								<th>Expected Days</th>
								<th>Price Per Item</th>
							</tr>
							
							<tr>

								<td>
									<?php
									$new_price=get_post_meta($current_product_id, '_sale_price', true);
									if ('' == $new_price || 0 == $new_price) {
										$new_price= get_post_meta($current_product_id, '_regular_price', true);
									}	
									?>
									<input id="defaultt" value="<?php echo filter_var($new_price); ?>" checked type="radio" name="unique_name">
									<label id="lbl_plgfy">
										<?php echo filter_var($all_rules['estimated_start'][$key]); ?>-<?php echo filter_var($all_rules['estimated_end'][$key]); ?>
									</label>

								</td>
								<td>
									<?php
									$new_price=get_post_meta($current_product_id, '_sale_price', true);
									if ('' == $new_price || 0 == $new_price) {
										$new_price= get_post_meta($current_product_id, '_regular_price', true);
									}							
									echo filter_var(wc_price($new_price));
									echo filter_var(' (Normal Price)');
									?>
								</td>
							</tr>
						</table>
						<?php

					}
					?>
				</div>
				<?php

				
			}
			break;
		}
	}
	?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#howmanydays').val(jQuery(this).next().html());
			jQuery('body').on('click', '.reset_variations' , function(){
				jQuery('.main_div_parent_plfyedt').each(function(){
					jQuery(this).hide();
				});
			});
			jQuery( ".single_variation_wrap" ).on( "show_variation", function ( event, variation ) {
				jQuery('.main_div_parent_plfyedt').each(function(){
					jQuery(this).hide();
				});
				jQuery('#main_div_parent_plfyedt'+variation.variation_id).show();
			});


			jQuery('#howmanydays').val(jQuery('#defaultt').next().html());
			jQuery('input[name="unique_name"]').on('click', function(){
				if (jQuery(this).parent().find('#lbl_plgfy').length >0) {
					jQuery('#days_msggg_carttt').val(jQuery(this).parent().find('#lbl_plgfy')[0].innerText);
				}
				
				
				jQuery('#howmanydays').val(jQuery(this).next().html());
			});
		});

		
	</script>
	<?php
	
}

function get_discounted_string( $discount_type, $discount_amount ) {
	$string=' (';
	if ('-' == $discount_type) {
		$string = $string . wc_price($discount_amount) . ' discount';
		
	} else if ('-%' == $discount_type) {		
		$string = $string . ( $discount_amount ) . '% discount';
	} else if ('+' == $discount_type) {
		$string = $string . wc_price($discount_amount) . ' extra charges';
		
	} else if ('+%' == $discount_type) {
		$string = $string . ( $discount_amount ) . '% extra charges';
		
	}
	$string = $string . ')';
	return $string;

}
function get_discounted_altered_price( $current_product_id, $discount_type, $discount_amount ) {
	$new_price=get_post_meta($current_product_id, '_sale_price', true);
	if ('' == $new_price || 0 == $new_price) {
		$new_price= get_post_meta($current_product_id, '_regular_price', true);
	}
	$old_price=$new_price;

	if ('-' == $discount_type) {
		$new_price=$old_price-$discount_amount;
		if (0>$new_price) {
			$new_price=0;
		}
		return $new_price;
	} else if ('-%' == $discount_type) {

		$new_price=$old_price/100;
		$new_price=$new_price*$discount_amount;
		$new_price=$old_price-$new_price;
		if (0>$new_price) {
			$new_price=0;
		}

		return $new_price;

	} else if ('+' == $discount_type) {
		$new_price=$old_price+$discount_amount;
		if (0>$new_price) {
			$new_price=0;
		}
		return $new_price;
	} else if ('+%' == $discount_type) {
		$new_price=$old_price/100;
		$new_price=$new_price*$discount_amount;
		$new_price=$old_price+$new_price;
		if (0>$new_price) {
			$new_price=0;
		}

		return $new_price;
	}
	return $new_price;
}


add_filter( 'woocommerce_add_cart_item_data', 'plgfyedt_add_custom_field_item_data', 10, 4 );
function plgfyedt_add_custom_field_item_data( $cart_item_data, $product_id, $variation_id, $quantity ) {
	if (isset($_REQUEST['unique_name'])) {
		$uniquename=sanitize_text_field($_REQUEST['unique_name']);
		$cart_item_data['ammended_price'] =	$uniquename;
	}
	if (isset($_REQUEST['howmanydays'])) {
		$howmanydays=sanitize_text_field($_REQUEST['howmanydays']);
		$cart_item_data['howmanydays'] =	$howmanydays;
	}
	
	if (isset($_REQUEST['days_msggg_carttt'])) {
		$days_msggg_carttt=sanitize_text_field($_REQUEST['days_msggg_carttt']);
		if (isset($_REQUEST['days_msg_cart'])) {
			$days_msg_cart=sanitize_text_field($_REQUEST['days_msg_cart']);

		}


		$final_msg= str_replace('[days]', $days_msggg_carttt, $days_msg_cart);
		$cart_item_data['final_msg'] =	$final_msg;
	}
	

	return $cart_item_data;

}

add_action( 'woocommerce_before_calculate_totals', 'plgfyedt_before_calculate_totals', 10, 1 );
function plgfyedt_before_calculate_totals( $cart_obj ) {



	foreach ( $cart_obj->get_cart() as $key=>$value ) {	


		if ( isset( $value['ammended_price'] ) ) {
			$price = $value['ammended_price'];
			$value['data']->set_price( ( $price ) );
		}	
		


	}

}
add_filter( 'woocommerce_cart_item_price', 'plgfyqdp_woocommerce_cart_item_price_filter', 10, 3 );	
function plgfyqdp_woocommerce_cart_item_price_filter( $price, $cart_item, $cart_item_key ) {

	if ( isset( $cart_item['ammended_price'] ) ) {
		$price = $cart_item['ammended_price'];
		return wc_price($price);
	}
	return $price;
}
add_filter( 'woocommerce_cart_item_name', 'plgfyedt_cart_item_name', 10, 3 );

function plgfyedt_cart_item_name( $name, $cart_item, $cart_item_key ) {
	$plgfydc_all_data=get_option('save_gnrl_settings_plgfyedt');
	$allow_cart=false;
	$allow_checkout=false;
	if (isset($plgfydc_all_data['plfyedt_expected_on_cart']) && 'true' == $plgfydc_all_data['plfyedt_expected_on_cart']) {
		
		$allow_cart=true;
		
	}
	if (isset($plgfydc_all_data['plfyedt_expected_on_checkout']) && 'true' == $plgfydc_all_data['plfyedt_expected_on_checkout']) {
		
		$allow_checkout=true;
		
	}
	if ($allow_checkout && $allow_cart) {
		if (!is_cart() && !is_checkout()) {
			return $name;
		}
	} else if ($allow_cart) {
		if (!is_cart()) {
			return $name;
		}
	} else if ($allow_checkout) {
		if (!is_checkout()) {
			return $name;
		}
	} else {
		return $name;
	}

	if ( isset( $cart_item['howmanydays'] ) ) {
		if (isset($cart_item['final_msg'])) {
			$final_msg = $cart_item['final_msg'];
		}
		$name = $name . '<br>' . $final_msg;

	} else {
		$all_rules=get_option('plgfyedt_all_rules_in_db');
		if ('' == $all_rules) {
			$all_rules=array();
		}
		$pprooduucctt= $cart_item['data'];
		$current_product_id=$pprooduucctt->get_id();
		$break=false;
		foreach ($all_rules['isactive'] as $key => $value) {
			if ('true' == $value) {
				if ('Products' == $all_rules['plgfqdp_applied_onc'][$key]) {

					if (isset($all_rules['applied_on_ids'][$key]) && 0 < count($all_rules['applied_on_ids'][$key])) {
						if (in_array($current_product_id, $all_rules['applied_on_ids'][$key])) {

							$break=true;
						}
					}
				} else if ('Category' == $all_rules['plgfqdp_applied_onc'][$key]) {
					if (isset($all_rules['applied_on_ids'][$key]) && 0 < count($all_rules['applied_on_ids'][$key])) {
						$cat_ids = wp_get_post_terms($current_product_id, 'product_cat', array('fields'=>'ids'));
						foreach ($cat_ids as $key1 => $value1) {							
							if (isset($all_rules['plgfqdp_applied_onc'][$key]) && in_array($value1, $all_rules['applied_on_ids'][$key])) {

								$break=true;
							}
						}
					}
				} else if ('whole' == $all_rules['plgfqdp_applied_onc'][$key]) {
					$break=true;
				}
			}
			if ($break) {
				$messageto_be_shown=$all_rules['to_b_displayd'][$key];
				$messageto_be_shown= str_replace('[days]', $all_rules['estimated_start'][$key] . '-' . $all_rules['estimated_end'][$key], $messageto_be_shown);
				$name = $name . '<br>' . $messageto_be_shown;
				
				break;
			}
		}
		
	}
	return $name;
}

add_action( 'woocommerce_add_order_item_meta', 'plgfyedt_woocommerce_add_order_item_meta', 10, 3 );

function plgfyedt_woocommerce_add_order_item_meta( $item_id, $values, $cart_item_key) {


	$product_idd_plgfy= WC()->cart->get_cart();
	


	if (isset($values['howmanydays']) && '' != $values['howmanydays']) {
		$final_msg='';
		if (isset($values['final_msg'])) {
			$final_msg = $values['final_msg'];
		}
		if ($final_msg && '' == $final_msg ) {
			
			wc_update_order_item_meta($item_id, 'Expected Delivery Time ' , $values['howmanydays']);
		} else {
			wc_update_order_item_meta($item_id, $final_msg , ':');
		}

		

	} else {
		$all_rules=get_option('plgfyedt_all_rules_in_db');
		if ('' == $all_rules) {
			$all_rules=array();
		}
		$product_idd_plgfy= $product_idd_plgfy[$cart_item_key]['product_id'];
		$current_product_id=$product_idd_plgfy;
		$break=false;
		foreach ($all_rules['isactive'] as $key => $value) {
			if ('true' == $value) {
				if ('Products' == $all_rules['plgfqdp_applied_onc'][$key]) {

					if (isset($all_rules['applied_on_ids'][$key]) && 0 < count($all_rules['applied_on_ids'][$key])) {
						if (in_array($current_product_id, $all_rules['applied_on_ids'][$key])) {

							$break=true;
						}
					}
				} else if ('Category' == $all_rules['plgfqdp_applied_onc'][$key]) {
					if (isset($all_rules['applied_on_ids'][$key]) && 0 < count($all_rules['applied_on_ids'][$key])) {
						$cat_ids = wp_get_post_terms($current_product_id, 'product_cat', array('fields'=>'ids'));
						foreach ($cat_ids as $key1 => $value1) {							
							if (isset($all_rules['plgfqdp_applied_onc'][$key]) && in_array($value1, $all_rules['applied_on_ids'][$key])) {

								$break=true;
							}
						}
					}
				} else if ('whole' == $all_rules['plgfqdp_applied_onc'][$key]) {
					$break=true;
				}
			}
			if ($break) {
				$messageto_be_shown=$all_rules['to_b_displayd'][$key];
				$messageto_be_shown= str_replace('[days]', $all_rules['estimated_start'][$key] . '-' . $all_rules['estimated_end'][$key], $messageto_be_shown);
				wc_update_order_item_meta($item_id, $messageto_be_shown , ':');
				
				
				break;
			}
		}
	}



}
add_action('woocommerce_after_shop_loop_item', 'plgfyedt_shoppage_msg');
function plgfyedt_shoppage_msg () {
	$gnrlrulesstng=get_option('save_gnrl_settings_plgfyedt');
	if ('true' != $gnrlrulesstng['plfyedt_expected_on_shop']) {
		return;
	}
	$all_rules=get_option('plgfyedt_all_rules_in_db');
	if ('' == $all_rules) {
		$all_rules=array();
	}
	$current_product_id= get_the_ID();
	$product=wc_get_product(get_the_ID());
	$break=false;
	foreach ($all_rules['isactive'] as $key => $value) {
		if ('true' == $value) {
			if ('Products' == $all_rules['plgfqdp_applied_onc'][$key]) {
				if (isset($all_rules['applied_on_ids'][$key]) && 0 < count($all_rules['applied_on_ids'][$key])) {
					if (in_array($current_product_id, $all_rules['applied_on_ids'][$key])) {
						
						$break=true;
					}
				}
			} else if ('Category' == $all_rules['plgfqdp_applied_onc'][$key]) {
				if (isset($all_rules['applied_on_ids'][$key]) && 0 < count($all_rules['applied_on_ids'][$key])) {
					$cat_ids = wp_get_post_terms($current_product_id, 'product_cat', array('fields'=>'ids'));
					foreach ($cat_ids as $key1 => $value1) {							
						if (isset($all_rules['plgfqdp_applied_onc'][$key]) && in_array($value1, $all_rules['applied_on_ids'][$key])) {
							
							$break=true;
						}
					}
				}
			} else if ('whole' == $all_rules['plgfqdp_applied_onc'][$key]) {
				$break=true;
			}
		}
		if ($break) {
			$messageto_be_shown=$all_rules['to_b_displayd'][$key];
			$messageto_be_shown= str_replace('[days]', $all_rules['estimated_start'][$key] . '-' . $all_rules['estimated_end'][$key], $messageto_be_shown);
			?>
			<input value="<?php echo filter_var($all_rules['estimated_start'][$key]); ?>-<?php echo filter_var($all_rules['estimated_end'][$key]); ?>" type="hidden" name="howmanydays" id="howmanydays">
			<input value="<?php echo filter_var($all_rules['to_b_displayd'][$key]); ?>" type="hidden" name="days_msg_cart" id="days_msg_cart">
			<input value="<?php echo filter_var($all_rules['estimated_start'][$key] . '-' . $all_rules['estimated_end'][$key]); ?>" type="hidden" name="days_msggg_carttt" id="days_msggg_carttt">
		
				<div class="main_div_parent_plfyedt" style="margin-bottom:1.5%;"> 
					<?php
					$plgfydc_all_data=get_option('save_gnrl_settings_plgfyedt');

					if ('2' == $plgfydc_all_data['msg_template']) {
						?>
						<div style="color: #<?php echo filter_var($plgfydc_all_data['clr']); ?>;width: 98%;background-color: #<?php echo filter_var($plgfydc_all_data['bg_clr']); ?>;border: 1px solid #<?php echo filter_var($plgfydc_all_data['brdr_color']); ?>;border-radius: 4px;padding: 4px;">
							<?php echo filter_var($messageto_be_shown); ?>
						</div>
						<?php
					} else if ('1' == $plgfydc_all_data['msg_template']) {
						?>
						<div style="color: #<?php echo filter_var($plgfydc_all_data['clr']); ?>;width: 98%;background-color: #<?php echo filter_var($plgfydc_all_data['bg_clr']); ?>;border-left: 5px solid #<?php echo filter_var($plgfydc_all_data['brdr_color']); ?>;padding: 4px;">
							<?php echo filter_var($messageto_be_shown); ?>
						</div>
						<?php
					} else if ('3' == $plgfydc_all_data['msg_template']) {
						?>
						<div style="color: #<?php echo filter_var($plgfydc_all_data['clr']); ?>;width: 98%;background-color: #<?php echo filter_var($plgfydc_all_data['bg_clr']); ?>;border-radius: 4px;padding: 4px;box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);">
							<?php echo filter_var($messageto_be_shown); ?>
						</div>
						<?php

					}
					?>
				</div>
				<?php			
				break;
		}
	}

}
