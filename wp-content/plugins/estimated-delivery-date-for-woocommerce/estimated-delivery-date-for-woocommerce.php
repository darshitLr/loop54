<?php
/**
 * Plugin Name: Estimated Delivery Date for WooCommerce
 * Plugin URI: https://woocommerce.com/products/estimated-delivery-date-time-for-woocommerce/
 * Author: Plugify
 * Author URI: https://woocommerce.com/vendor/plugify/
 * Version: 1.0.3
 * Developed By: Plugify Team
 * Description: Display estimated delivery date on shop, product, cart, and checkout page. Create scenarios to provide urgent delivery with extra charges.
 * Woo: 8371555:26f3bcd49ebb2fde5907bd386ffc6f42
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Requires at least: 4.4
 * Tested up to: 5.3.2
 * WC requires at least: 3.0
 * WC tested up to: 3.8.0
 */
if ( ! defined( 'ABSPATH' ) ) { 
	exit; // Exit if accessed directly
}
/**
 * Check if WooCommerce is active
 * if wooCommerce is not active Direct Checkout Pro For Woocommerce will not work.
 **/
if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	function my_admin_notice() {		
		deactivate_plugins(__FILE__);
		$error_message = __('This plugin requires <a href="http://wordpress.org/extend/plugins/woocommerce/">WooCommerce</a> plugin to be installed and active!', 'woocommerce');
		echo esc_attr( $error_message );
		wp_die();
	}
	add_action( 'admin_notices', 'my_admin_notice' );
}
error_reporting(0);
class Plugify_EDT_Main_Class {	
	public function __construct() {
		
		if (is_admin()) {
			include ('Admin/plgfyedt_main_admin.php');			
		} else {
			include ('Front/plgfyedt_main_front.php');
		}		
		add_action( 'wp_ajax_plgfyedt_save_all_rules', array( $this, 'plgfyedt_save_all_rules' ) );
		
		add_action( 'wp_ajax_resettooption', array( $this, 'plgfyedt_resettooption' ));
		add_action( 'wp_ajax_save_gnrl_settings_plgfyedt', array( $this, 'plfyedt_save_gnrl_settings_plgfyedt' ));
	
	}
	public function plgfyedt_resettooption() {
		$plgfydc_all_data=array(
			'plfyedt_expected_on_shop'=>'false',
			'plfyedt_expected_on_cart'=>'true',
			'plfyedt_expected_on_checkout'=>'true',
			'msg_template'=>'2',
			'bg_clr'=>'e5cfe8',
			'clr'=>'000000',
			'brdr_color'=>'d6b4e0'

		);
		if (isset($_REQUEST['index']) && '1' ==$_REQUEST['index']) {
			$plgfydc_all_data=array(
				'plfyedt_expected_on_shop'=>'false',
				'plfyedt_expected_on_cart'=>'true',
				'plfyedt_expected_on_checkout'=>'true',
				'msg_template'=>'1',
				'bg_clr'=>'cfeac8',
				'clr'=>'000000',
				'brdr_color'=>'008000'

			);
		} else if (isset($_REQUEST['index']) && '2' ==$_REQUEST['index']) {
			$plgfydc_all_data=array(
				'plfyedt_expected_on_shop'=>'false',
				'plfyedt_expected_on_cart'=>'true',
				'plfyedt_expected_on_checkout'=>'true',
				'msg_template'=>'2',
				'bg_clr'=>'e5cfe8',
				'clr'=>'000000',
				'brdr_color'=>'d6b4e0'

			);

		} else if (isset($_REQUEST['index']) && '3' ==$_REQUEST['index']) {
			$plgfydc_all_data=array(
				'plfyedt_expected_on_shop'=>'false',
				'plfyedt_expected_on_cart'=>'true',
				'plfyedt_expected_on_checkout'=>'true',
				'msg_template'=>'3',
				'bg_clr'=>'4effff',
				'clr'=>'000000',
				'brdr_color'=>'d6b4e0'

			);

		}
		
		update_option('save_gnrl_settings_plgfyedt', $plgfydc_all_data);
		wp_die();
		
	}	
	public function plfyedt_save_gnrl_settings_plgfyedt() {
		
	
		update_option('save_gnrl_settings_plgfyedt', $_REQUEST);
		wp_die();
		
	}
	public function plgfyedt_save_all_rules() {
		update_option('plgfyedt_all_rules_in_db', $_REQUEST);
		wp_die();
	}
	
} 
new Plugify_EDT_Main_Class();
