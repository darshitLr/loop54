
jQuery(document).ready(function () {

	"use strict";
	var acc = document.getElementsByClassName("accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
		acc[i].addEventListener("click", function() {
			this.classList.toggle("active");
			var panel = this.nextElementSibling;
			
			if (panel.style.display === "block") {
				
				
				jQuery(this).find('i').removeClass('fa-chevron-circle-up');
				jQuery(this).find('i').addClass('fa-chevron-circle-down');
				panel.style.display = "none";
			} else {
				jQuery(this).find('i').addClass('fa-chevron-circle-up');
				jQuery(this).find('i').removeClass('fa-chevron-circle-down');
				panel.style.display = "block";
			}
		});
	}
	
	jQuery('.plgfqdp_products').select2();
	jQuery('.plgfqdp_selectcat').select2();
	jQuery('body').on('click', '.checkkk' , function(e){
		// e.preventDefault();
		// var classNameShow='show';
		// var classNameContainer='.container';
		// var containers=jQuery(classNameContainer);
		// var currentAccordion=jQuery(this);
		// var currentContainer=jQuery(this).next(classNameContainer);
		// var openedAccordions=accordions.filter(function(){return jQuery(this).hasClass(classNameShow);});
		// jQuery(this).prev().removeClass('fa-chevron-circle-up');
		// jQuery(this).prev().addClass('fa-chevron-circle-down');
		// containers.not(currentContainer).stop(true).hide({


		// 	complete:function(){
		// 		openedAccordions.removeClass(classNameShow);
		// 		openedAccordions.find('i').removeClass('fa-chevron-circle-up');
		// 		openedAccordions.find('i').addClass('fa-chevron-circle-down');
		// 	}
		// });
		
	});
	jQuery('body').on('click', '.save_All_rules_btn' , function(){
		var isactive=[];
		var plgfyedt_rule_name=[];
		var plgfqdp_applied_onc=[];
		var applied_on_ids=[];
		var estimated_start=[];
		var estimated_end=[];
		var to_b_displayd=[];
		var scenarios=[];
		jQuery('.accordion').each(function(){
			isactive.push(jQuery(this).find('input[type="checkbox"]').prop('checked'));
			plgfyedt_rule_name.push(jQuery(this).next().find('#plgfyedt_rule_name').val());
			plgfqdp_applied_onc.push(jQuery(this).next().find('#plgfqdp_applied_onc').val());
			if ('Products' == jQuery(this).next().find('#plgfqdp_applied_onc').val()) {
				applied_on_ids.push(jQuery(this).next().find('#plgfqdp_productsc').val());
			} else if ('Category' == jQuery(this).next().find('#plgfqdp_applied_onc').val()) {
				applied_on_ids.push(jQuery(this).next().find('#plgfqdp_selectcatc').val());
			} else {
				applied_on_ids.push('whole');
			}
			estimated_start.push(jQuery(this).next().find('#estimated_start').val());
			estimated_end.push(jQuery(this).next().find('#estimated_end').val());
			to_b_displayd.push(jQuery(this).next().find('#msg_t_be').val());
			var extra_start=[];
			var extra_end=[];
			var extra_type=[];
			var extra_amount=[];
			var thisrulescenario=[];
			jQuery(this).next().find('.extra_start').each(function(){
				extra_start.push(jQuery(this).val());
			});
			jQuery(this).next().find('.extra_end').each(function(){
				extra_end.push(jQuery(this).val());
			});
			jQuery(this).next().find('.extra_type').each(function(){
				extra_type.push(jQuery(this).val());
			});
			jQuery(this).next().find('.extra_amount').each(function(){
				extra_amount.push(jQuery(this).val());
			});
			thisrulescenario.push(extra_start);
			thisrulescenario.push(extra_end);
			thisrulescenario.push(extra_type);
			thisrulescenario.push(extra_amount);
			scenarios.push(thisrulescenario);
		});


		jQuery.ajax({
			url : plgfyqdpData.admin_url,

			type : 'post',
			data : {
				action : 'plgfyedt_save_all_rules',      
				isactive : isactive,       
				plgfyedt_rule_name : plgfyedt_rule_name,       
				plgfqdp_applied_onc : plgfqdp_applied_onc,       
				applied_on_ids : applied_on_ids,       
				estimated_start : estimated_start,       
				estimated_end : estimated_end,       
				to_b_displayd : to_b_displayd,       
				scenarios : scenarios,       

			},
			success : function( response ) {
				jQuery('.plugifyyy').remove();
				jQuery('#plgfydc_savediv11').after('<div class="notice notice-success is-dismissible plugifyyy" ><p id="plgfydc_saveeditmsg">Done</p><button type="button" class="notice-dismiss hidedivv"><span class="screen-reader-text">Dismiss this notice.</span></button></div>')

				jQuery('#plgfydc_saveeditmsg').html('Rules have been saved successfully!');;
				jQuery("html, body").animate({ scrollTop: 0 }, "slow");
				// location.reload();


			}

		});

		
	});
	jQuery('body').on('click', '.plgfyedt_del_current_row' , function(){
		jQuery(this).parent().remove();
	});	
	jQuery('body').on('click', '.plgfyedt_del_current_rule' , function(){
		if(!confirm('Are you sure to delete this rule..?')) {
			return;
		}
		
		jQuery(this).parent().parent().prev().remove();
		jQuery(this).parent().parent().remove();
	});
	jQuery('body').on('click', '.add_main_rule_btn' , function() {
		
		jQuery(this).after('<button class="accordion" id="frstaccord" type="button" style="margin-top:1%;">New Rule<i style="float: right;" class="fa fa-chevron-circle-down" aria-hidden="true"></i>	<span style="float:right;margin-right: 5%;" class="checkkk">		<strong>Active</strong>		<label class="switch">			<input type="checkbox" id="plgfyedt_active"			>			<span class="slider"></span>		</label>	</span></button><div class="panel container " style="width:auto !important;height:auto !important;">	<br>		<strong>Rule Name</strong>	<input type="text" id="plgfyedt_rule_name" value="New Rule" style="margin-left:4%;">	<table class="last_tbl_gnrl_stng " > 		<tr>			<th>				Applied On			</th>			<th>				Select Products/Categories			</th>		</tr>		<tr>			<td style="width: 30%;">				<select id="plgfqdp_applied_onc" style="width: 90%;" class="class_plgfqdp_applied_onc">					<option value="Products">Products</option>					<option value="Category">Category</option>			<option value="whole">Apply To Whole Shop</option>	</select>			</td>			<td style="width: 70%;">				<div id="plgfqdp_products1c" >					<select name="plgfqdp_products[]" multiple="multiple"  style="max-width: 90%;width: 90%;font-size: 12px;" class=" plgfqdp_products" id="plgfqdp_productsc"   >	'+jQuery('#prod_html').val()+'</select><br>					<i>(These rule will be applicable to selected products)</i>				</div>				<div id="plgfqdp_selectcat1c" style="display: none;">					<select  name="plgfqdp_selectcat[]" style="max-width: 90%;width: 90%;font-size: 12px;" id="plgfqdp_selectcatc" class="plgfqdp_selectcat"   multiple="multiple[]">'+jQuery('#cat_html').val()+'				</select><br>				<i>(These rule will be applicable to selected categories)</i>			</div>		</td>	</tr>		</table><br><br><div>	<strong>Estimated Time</strong>	<input type="number" id="estimated_start" style="width:7%;margin-left:4%;">   to   <input style="width:7%;" type="number" id="estimated_end">   days</div>	<div style="background:#f1f1f1;padding: 10px;border-radius: 4px;margin-top: 1%;width: max-content;"><i style="font-size:12px;">Note: Use [days] shortcode to display in your custom message e.g. This product will be delivered in <b>[days]</b> days.</i></div><br><div>			<strong>Message to be displayed</strong>			<input  placeholder="Enter your custom message"  type="text" id="msg_t_be" style="margin-left: 4%;width: 70%;"> 		</div>		<br><div style="width:100%;">	<button class="button-secondary add_scenario_click_btn" type="button" style="border-color: green;color: green;">		<i class="fa fa-fw fa-plus"></i>	Add Scenario(s)</button></div><div  class="overflow_classs">	<input type="hidden" id="tobeafter"></div><br><br><div>	<span class="plgfyedt_del_current_rule" style="color: red;float: right;padding: 6px 8px 6px 8px;cursor: pointer;    border: 1px solid red;    border-radius: 4px;"><i style="color:red;" class="fa fa-trash" aria-hidden="true"></i> Delete Rule		</span></div></div>');	jQuery('.plgfqdp_products').select2();	jQuery('.plgfqdp_selectcat').select2();

		var acc = document.getElementsByClassName("accordion");
		var i;


		acc[0].addEventListener("click", function() {
			this.classList.toggle("active");
			var panel = this.nextElementSibling;
			if (panel.style.display === "block") {
				
				
				jQuery(this).find('i').removeClass('fa-chevron-circle-up');
				jQuery(this).find('i').addClass('fa-chevron-circle-down');
				panel.style.display = "none";
			} else {
				jQuery(this).find('i').addClass('fa-chevron-circle-up');
				jQuery(this).find('i').removeClass('fa-chevron-circle-down');
				panel.style.display = "block";
			}
		});
		
		
	});
	jQuery('body').on('click', '.add_scenario_click_btn' , function(){
		jQuery(this).parent().next().find('#tobeafter').after('<div style="background:#eee;margin-top: 1%;border-left: 4px solid green;padding: 10px;">	<input type="number" class="extra_start"  style="width:7%;"><label style="margin-left:4%;">to</label><input type="number" class="extra_end"  style="width:7%;margin-left:4%;"><label style="margin-left:4%;">days</label>	<select style="margin-left:4%;" class="extra_type">		<option value="frst">Select Charges Type</option>		<option value="+">+</option>		<option value="+%">+%</option>		<option value="-">-</option>		<option value="-%">-%</option>	</select>	<input type="number" class="extra_amount"  style="width:7%;margin-left: 4%;"><span class="plgfyedt_del_current_row" style="float: right;padding: 6px 8px 6px 8px;cursor: pointer;    border: 1px solid red;    border-radius: 4px;"><i style="color:red;" class="fa fa-trash" aria-hidden="true"></i>		</span>	</div>');
	});
	jQuery('body').on('change', '.class_plgfqdp_applied_onc' , function(){


		if (jQuery(this).val() == 'Products') {

			jQuery(this).parent().parent().find('#plgfqdp_products1c').show();
			jQuery(this).parent().parent().find('#plgfqdp_selectcat1c').hide();
			jQuery(this).parent().parent().find('#plgfqdp_selectcat1c').parent().show();
			jQuery(this).parent().parent().prev().children(':last').show();
			jQuery(this).parent().parent().parent().parent().css('width','100%');

		}else if ('Category'== jQuery(this).val()) {
			jQuery(this).parent().parent().find('#plgfqdp_products1c').hide();
			jQuery(this).parent().parent().find('#plgfqdp_selectcat1c').show();
			jQuery(this).parent().parent().find('#plgfqdp_selectcat1c').parent().show();
			jQuery(this).parent().parent().prev().children(':last').show();
			jQuery(this).parent().parent().parent().parent().css('width','100%');
		} else {
			jQuery(this).parent().parent().find('#plgfqdp_products1c').hide();
			jQuery(this).parent().parent().find('#plgfqdp_selectcat1c').hide();
			jQuery(this).parent().parent().find('#plgfqdp_selectcat1c').parent().hide();
			jQuery(this).parent().parent().prev().children(':last').hide();
			jQuery(this).parent().parent().parent().parent().css('width','31%');

		}
	});
});
