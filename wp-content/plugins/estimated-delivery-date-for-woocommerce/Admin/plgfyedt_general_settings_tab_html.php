<?php
$plgfydc_all_data=get_option('save_gnrl_settings_plgfyedt');
if ('' == $plgfydc_all_data) {
	$plgfydc_all_data=array(
		'plfyedt_expected_on_shop'=>'false',
		'plfyedt_expected_on_cart'=>'true',
		'plfyedt_expected_on_checkout'=>'true',
		'msg_template'=>'2',
		'bg_clr'=>'e5cfe8',
		'clr'=>'000000',
		'brdr_color'=>'d6b4e0'

	);
}

?>

<div id="main_settings_div">
	<input type="hidden" id="plgfydc_savediv">
	<br>
	<div style="width:100%;border-radius:5px;padding:10px;border: 4px solid #ae7b3b;display: inline-flex;" id="parent_div">
		<div style="width: 35%;background-color: #dcdcde;" id="left_div">
			<div>
				<img src="<?php echo filter_var(plugins_url()); ?>/estimated-delivery-date-for-woocommerce/Admin/6 (2).png" style="width: 70%; margin-left: 15%;   ">
				
				<h1 style="font-family: ver;font-size: 22px;font-weight:600;margin-top: -8%;text-align: center;" >Estimated Delivery Time For Woocommerce</h1>

			</div>
		</div>
		<div style="width: 65%;margin-left: 2%;" id="right_div">
			<h1 style="font-family: ver;font-size: 28px;background-color: #dcdcde;padding: 1px 10px;border-radius: 2px;">General Settings</h1>
			<hr>
			<table class="plgfydc_tbl_main" style="width: 100%; ">
				<tr>
					<td>
						<strong >
							Show Expected Time On Shop 
						</strong>
					</td>
					<td>
						<label class="switch">
							<input type="checkbox" id="plfyedt_expected_on_shop"
							<?php
							if ('true' == $plgfydc_all_data['plfyedt_expected_on_shop']) {
								echo filter_var('checked');
							}
							?>
							>
							<span class="slider"></span>
						</label>
					</td>
				</tr>
				<tr>
					<td>
						<strong >
							Show Expected Time On Cart 
						</strong>
					</td>
					<td>
						<label class="switch">
							<input type="checkbox" id="plfyedt_expected_on_cart"
							<?php
							if ('true' == $plgfydc_all_data['plfyedt_expected_on_cart']) {
								echo filter_var('checked');
							}
							?>
							>
							<span class="slider"></span>
						</label>
					</td>
				</tr>
				<tr>
					<td>
						<strong >
							Show Expected Time On Checkout 
						</strong>
					</td>
					<td>
						<label class="switch">
							<input type="checkbox" id="plfyedt_expected_on_checkout"
							<?php
							if ('true' == $plgfydc_all_data['plfyedt_expected_on_checkout']) {
								echo filter_var('checked');
							}
							?>
							>
							<span class="slider"></span>
						</label>
					</td>
				</tr>
			</table>
			<hr>
			<strong style="font-family: ver;font-size: 16px;">'Expected Delivery Time' Message Settings</strong>
			<hr>
			<table  class="plgfydc_tbl_main" style="width: 100%; ">
				<tr>
					<td>
						<strong>
							Choose Template
						</strong>
					</td>
					<td>
						<label class="border-bottun" >
							<input value="1" type="radio" name="msg_template"
							<?php
							if ('1' == $plgfydc_all_data['msg_template']) {
								echo filter_var('checked');
							}
							?>
							>
							<div class="imgclass" id="imgclass1" style="width: 100%;background-color: #cfeac8;border-left: 5px solid green;padding: 16px;">
								This product will be delivered within 3-4 days
							</div>
						</label> <i style="color:green;" id="hideallticks" class="hideallticks fa fa-check" aria-hidden="true"></i><br>

						<label class="border-bottun" style="margin-top:3%;">
							<input value="2" type="radio" name="msg_template"
							<?php
							if ('2' == $plgfydc_all_data['msg_template']) {
								echo filter_var('checked');
							}
							?>
							>
							<div class="imgclass" id="imgclass2" style="width: 100%;background-color: #e5cfe8;border: 1px solid #d6b4e0;border-radius: 4px;padding: 16px;">
								This product will be delivered within 3-4 days
							</div>
						</label> <i style="color:green;" id="hideallticks" class="hideallticks fa fa-check" aria-hidden="true"></i><br>
						



						
						
						
						<label class="border-bottun" style="margin-top:3%;">
							<input value="3" type="radio" name="msg_template"
							<?php
							if ('3' == $plgfydc_all_data['msg_template']) {
								echo filter_var('checked');
							}
							?>
							>
							<div class="imgclass" id="imgclass3" style="width: 100%;background-color: #4effff;border-radius: 4px;padding: 16px;box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);">
								This product will be delivered within 3-4 days
							</div>
						</label> <i style="color:green;" id="hideallticks" class=" hideallticks fa fa-check" aria-hidden="true"></i>
						<br>
					</td>
				</tr>
				
				<tr>
					<td>
						<strong>
							Background Color
						</strong>
					</td>
					<td>
						<input class="jscolor" value="<?php echo filter_var($plgfydc_all_data['bg_clr']); ?>" type="text" id="bg_clr" style="width: 79%;">
					</td>
				</tr>
				<tr>
					<td>
						<strong>
							Text Color
						</strong>
					</td>
					<td>
						<input class="jscolor" value="<?php echo filter_var($plgfydc_all_data['clr']); ?>" type="text" id="clr" style="width: 79%;">
					</td>
				</tr>
				<tr>
					<td>
						<strong>
							Border Color
						</strong>
					</td>
					<td>
						<input class="jscolor" value="<?php echo filter_var($plgfydc_all_data['brdr_color']); ?>" type="text" id="brdr_color" style="width: 79%;">
					</td>
				</tr>
			<!-- 	<tr>
					<td>
						<strong>
							Reset Options
						</strong>
					</td>
					<td>
						<button value="1" type="button" class="button-primary resetto1 resettoall" style="width: 79%;background: #ae7b3b; border-color: #ae7b3b;background-color: #ae7b3b;">Reset According To First Template</button><br><br>
						<button value="2" type="button" class="button-primary resetto2 resettoall" style="width: 79%;background: #ae7b3b; border-color: #ae7b3b;background-color: #ae7b3b;">Reset According To Second Template</button><br><br>
						<button value="3" type="button" class="button-primary resetto3 resettoall" style="width: 79%;background: #ae7b3b; border-color: #ae7b3b;background-color: #ae7b3b;">Reset According To Third Template</button><br><br>
					</td>
				</tr> -->

				
				<tr>
					<td colspan="2">
						<button type="button" class="button-primary plgfydc_save_gnrl_set" style="background: #ae7b3b; border-color: #ae7b3b;background-color: #ae7b3b;float: right;right: 5px;">Save Settings</button>
					</td>
					
				</tr>

			</table>
		</div>

	</div>
	<br>
	<br>
</div>


<style type="text/css">
@media only screen and (max-width: 600px) {
	#left_div {
		width: 100% !important;
	}
	#right_div{
		width: 100% !important;
		margin-left: unset !important;
		margin-top: 2%;

	}
	#right_div input[type="text"]{
		width: 90% !important;
	}
	#right_div input[type="number"]{
		width: 90% !important;
	}
	.remove_margin_for_mbl{
		margin-left: unset !important;
	}


	input[name="plgfydc_method"]{
		margin: unset !important;
	}
	.changeit_plgfydc{
		display: -webkit-inline-box !important;
	}
	#parent_div{
		display: block !important;
		width: unset !important;
		margin-left: unset !important;
	}
}
.woocommerce-save-button{
	display: none !important;
}

.hideallticks{
	display: none;
}

[type=radio] { 
	position: absolute;
	opacity: 0;
	width: 0;
	height: 0;
}

/* IMAGE STYLES */
[type=radio] + .imgclass  {
	cursor: pointer;
}

/* CHECKED STYLES */
[type=radio]:checked + .imgclass {
	outline: 2px solid green;

/*	border: solid 3px green;
transition: border-width 0.5s linear;*/
}
.imgclass{
	font-weight: 200 !important;
}

</style>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#imgclass1').css('border-left', '5px solid #'+jQuery('#brdr_color').val());
		jQuery('#imgclass2').css('border', '1px solid #'+jQuery('#brdr_color').val());
		jQuery('.imgclass').css('color', '#'+jQuery('#clr').val());
		jQuery('.imgclass').css('background-color', '#'+jQuery('#bg_clr').val());	

		
		jQuery('#brdr_color').on('change', function(){
			jQuery('#imgclass1').css('border-left', '5px solid #'+jQuery(this).val());
			jQuery('#imgclass2').css('border', '1px solid #'+jQuery(this).val());


		});
		jQuery('#clr').on('change', function(){
			jQuery('.imgclass').css('color', '#'+jQuery(this).val());



		});
		jQuery('#bg_clr').on('change', function(){
			jQuery('.imgclass').css('background-color', '#'+jQuery(this).val());	

		});
		jQuery('.resettoall').on('click', function(){
			jQuery.ajax ({
				url: '<?php echo filter_var(admin_url('admin-ajax.php')); ?>',
				type:'POST',
				data:{
					action : 'resettooption',
					index : jQuery(this).val(),
					
					
				},
				success:function(response) {

					jQuery('.plugifyyy').remove();
					jQuery('#plgfydc_savediv').after('<div class="notice notice-success is-dismissible plugifyyy" ><p id="plgfydc_saveeditmsg">Done</p><button type="button" class="notice-dismiss hidedivv"><span class="screen-reader-text">Dismiss this notice.</span></button></div>')

					jQuery('#plgfydc_saveeditmsg').html('Settings have been saved successfully!');;
					jQuery("html, body").animate({ scrollTop: 0 }, "slow");
					setTimeout(function(){
						location.reload();
					},2000);

				}

			});
		});
		jQuery('.plgfydc_save_gnrl_set').on('click', function(){
			var plgfydc_hide_def_cart=jQuery('#plgfydc_hide_def_cart').prop('checked');
			var plgfydc_method=jQuery('[name="plgfydc_method"]:checked').val();
			var plgfydc_btn_txt=jQuery('#plgfydc_btn_txt').val();
			var plgfydc_txt_clr=jQuery('#plgfydc_txt_clr').val();
			var plgfydc_bg_clr=jQuery('#plgfydc_bg_clr').val();

			var plgfydc_padding_l=jQuery('#plgfydc_padding_l').val();
			var plgfydc_padding_r=jQuery('#plgfydc_padding_r').val();
			var plgfydc_padding_t=jQuery('#plgfydc_padding_t').val();
			var plgfydc_padding_b=jQuery('#plgfydc_padding_b').val();

			var plgfydc_margin_l=jQuery('#plgfydc_margin_l').val();
			var plgfydc_margin_r=jQuery('#plgfydc_margin_r').val();
			var plgfydc_margin_t=jQuery('#plgfydc_margin_t').val();
			var plgfydc_margin_b=jQuery('#plgfydc_margin_b').val();
			var plgfydc_brdr_rdius=jQuery('#plgfydc_brdr_rdius').val();

			jQuery.ajax ({
				url: '<?php echo filter_var(admin_url('admin-ajax.php')); ?>',
				type:'POST',
				data:{
					action : 'save_gnrl_settings_plgfyedt',
					plfyedt_expected_on_shop:jQuery('#plfyedt_expected_on_shop').prop('checked'),
					plfyedt_expected_on_cart:jQuery('#plfyedt_expected_on_cart').prop('checked'),
					plfyedt_expected_on_checkout:jQuery('#plfyedt_expected_on_checkout').prop('checked'),
					msg_template:jQuery('input[name="msg_template"]:checked').val(),
					bg_clr:jQuery('#bg_clr').val(),
					clr:jQuery('#clr').val(),
					brdr_color:jQuery('#brdr_color').val(),
					
					
				},
				success:function(response) {

					jQuery('.plugifyyy').remove();
					jQuery('#plgfydc_savediv').after('<div class="notice notice-success is-dismissible plugifyyy" ><p id="plgfydc_saveeditmsg">Done</p><button type="button" class="notice-dismiss hidedivv"><span class="screen-reader-text">Dismiss this notice.</span></button></div>')

					jQuery('#plgfydc_saveeditmsg').html('Settings have been saved successfully!');;
					jQuery("html, body").animate({ scrollTop: 0 }, "slow");

				}

			});


		});
		jQuery('body').on('click', '.hidedivv' , function(){
			jQuery('.plugifyyy').remove();
		});
		jQuery('input[name="msg_template"]').each(function(){
			if (jQuery(this).prop('checked')){
				jQuery('.hideallticks').hide()
				jQuery(this).parent().next().show();

			}
		})
		jQuery('input[name="msg_template"]').on('click', function(){

			jQuery('.hideallticks').hide()
			jQuery(this).parent().next().show();
		});
	});
</script>
