<?php
class Plugify_Edt_Admin_Main_Class {	
	public function __construct() {
		add_action('woocommerce_settings_plgfyedt', array($this, 'plgfyedt_callback_against_mainsetting_content'));		
		add_filter('woocommerce_settings_tabs_array', array($this, 'plgfyedt_filter_woocommerce_settings_tabs'), 50);	

		add_action('init', array( $this, 'plgfyedt_scripts_on_load'));
	}
	
	public function plgfyedt_filter_woocommerce_settings_tabs ( $tabs ) {
		$tabs['plgfyedt'] = __('Estimated Delivery Time', 'plgfyedt');		
		return $tabs;
	}	
	public function plgfyedt_callback_against_mainsetting_content () {
		?>
		<section class="home-content-top">
			<div class="container-fluid">
				<div class="clearfix"></div>
				<div class="tabbable-panel margin-tops4 ">
					<div class="tabbable-line">
						<ul class="nav nav-tabs tabtop tabsetting">
							<li class="active"> <a href="#tab_default_1" data-toggle="tab"><i class="fa fa-cogs" aria-hidden="true"></i> <?php echo esc_html__('General Settings', 'plgfyedt'); ?></a>
							</li>
							<li> <a href="#tab_default_2" data-toggle="tab"><i class="fa fa-briefcase" aria-hidden="true"></i> <?php echo esc_html__('All Rules', 'plgfyedt'); ?></a> 
							</li>

						</ul>
						<div class="tab-content margin-tops">
							<div class="tab-pane active fade in" id="tab_default_1">
								<div class="col-md-12 plgfyqdp_main">
									<?php include('plgfyedt_general_settings_tab_html.php'); ?>
								</div>
							</div>
							<div class="tab-pane fade" id="tab_default_2">
								<div class="col-md-12 plgfyqdp_main">
									<?php include('plgfyedt_rules.php'); ?>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</section>
		<?php
		
	}	

	public function plgfyedt_scripts_on_load () {
		
		if ( isset ($_GET['page']) && 'wc-settings' == $_GET['page'] && isset ( $_GET['tab'] ) && 'plgfyedt' == $_GET['tab']  ) {
			wp_enqueue_script('jquery'); 
			wp_enqueue_style('animatecssanimate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css', '1.0', 'all');
			
			wp_enqueue_style('date_picker_css_plgfyqdp', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', false, '1.0', 'all');
			wp_register_script('datepicker_plgfyqdp_alpha', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js', '', '1.0');
			wp_enqueue_script('datepicker_plgfyqdp_alpha'); 
			wp_enqueue_script('colorpickearjs', plugins_url('js/jscolor.js', __FILE__), false, '1.0', 'all');			
			wp_enqueue_style('daemo_acsss', plugins_url('css/plgfyqdp_admin_style.css', __FILE__), false, '1.0', 'all');					
			wp_enqueue_script('my_custom_script_plgfyqdp', plugins_url('js/admin_main_ajax_jas.js', __FILE__) , false, '1.0', 'all' );
			$plgfyqdpData = array(
				'admin_url' => admin_url('admin-ajax.php'),
				
			);
			wp_localize_script('my_custom_script_plgfyqdp', 'plgfyqdpData', $plgfyqdpData);			
			wp_enqueue_script( 'select2', plugins_url( 'js/select2.min.js', __FILE__ ), false, '1.0', 'all');
			wp_enqueue_style( 'select2', plugins_url( 'js/select2.min.css', __FILE__ ), false, '1.0', 'all' );
			
		}	}
}
new Plugify_Edt_Admin_Main_Class();
