<?php

$plgfqdp_args = array(
	'numberposts' => -1,
	'taxonomy' => 'product_cat',
);
$plgfqdp_terms = get_terms($plgfqdp_args);
$cat_html='';
if ( $plgfqdp_terms ) {   
	foreach ( $plgfqdp_terms as $plgfqdp_term1 ) {
		$cat_html= $cat_html . '<option value="' . $plgfqdp_term1->term_id . '">' . $plgfqdp_term1->name . '</option>';
	}          
}
$plgfqdp_args = array(
	'posts_per_page' => '-1',
	'post_status'           => 'publish',
	'post_type'      =>   array('product')
);
$plgfqdp_the_query = new WP_Query( $plgfqdp_args );
$prod_html='';

while ( $plgfqdp_the_query -> have_posts() ) {
	$plgfqdp_the_query -> the_post(); 	
	$prod_html = $prod_html . '<option value="' . get_the_ID() . '">' . get_the_title() . '</option>';		
}
?>
<input type="hidden" value='<?php echo filter_var($prod_html); ?>' id="prod_html">
<input type="hidden" value='<?php echo filter_var($cat_html); ?>' id="cat_html">
<div id="main_settings_div">
<input type="hidden" id="plgfydc_savediv11">
	<style>
	.accordion {
		background-color: #eee;
		color: #444;
		cursor: pointer;
		padding: 10px;
		width: 100%;
		border: none;
		text-align: left;
		outline: none;
		font-size: 15px;
		transition: 0.4s;
	}

	.active, .accordion:hover {
		background-color: #ccc; 
	}

	.panel {
		padding: 0 18px;
		display: none;
		background-color: white;
		overflow: hidden;
	}
</style>


<h2>All Rules</h2>
<button class="button-primary add_main_rule_btn" type="button" style="background-color: #ae7b3b;border-color:#ae7b3b;float: right;">
	<i class="fa fa-fw fa-plus"></i>
Add Rule(s)</button>
<?php
if ('' == get_option('plgfyedt_all_rules_in_db')) {


	?>
	<button class="accordion" type="button" id="frstaccord" style="margin-top:1%;">Dummy Rule

		<i style="float: right;" class="fa fa-chevron-circle-down" aria-hidden="true"></i>
		<span style="float:right;margin-right: 5%;z-index: 999999999;" class="checkkk">
			<strong>Active</strong>
			<label class="switch">
				<input type="checkbox" id="plgfyedt_active"

				>
				<span class="slider"></span>
			</label>
		</span>
	</button>
	<div class="panel container " style="width: auto !important;height:auto !important;">
		<br>

		<strong>Rule Name</strong>
		<input type="text" value="Dummy Rule" id="plgfyedt_rule_name" style="margin-left:4%;">
		<table class="last_tbl_gnrl_stng " > 
			<tr>
				<th>
					Applied On
				</th>
				<th>
					Select Products/Categories
				</th>
			</tr>
			<tr>
				<td style="width: 30%;">
					<select id="plgfqdp_applied_onc" style="width: 90%;" class="class_plgfqdp_applied_onc">

						<option value="Products">Products</option>
						<option value="Category">Category</option>
						<option value="whole">Apply To Whole Shop</option>
					</select>
				</td>
				<td style="width: 70%;">
					<div id="plgfqdp_products1c" >

						<select name="plgfqdp_products[]" multiple="multiple"  style="max-width: 90%;width: 90%;font-size: 12px;" class=" plgfqdp_products" id="plgfqdp_productsc"   >

							<?php
							echo filter_var($prod_html);
							?>
						</select><br>
						<i>(These rule will be applicable to selected products)</i>
					</div>
					<div id="plgfqdp_selectcat1c" style="display: none;">

						<select  name="plgfqdp_selectcat[]" style="max-width: 90%;width: 90%;font-size: 12px;" id="plgfqdp_selectcatc" class="plgfqdp_selectcat"   multiple='multiple[]'>
							<?php
							echo filter_var($cat_html);
							?>
						</select><br>
						<i>(These rule will be applicable to selected categories)</i>
					</div>
				</td>
			</tr>				
		</table>
		<br>
		<br>
		<div>
			<strong>Estimated Time</strong>
			<input type="number" id="estimated_start" style="width:7%;margin-left:4%;">   to   <input style="width:7%;" type="number" id="estimated_end">   days

		</div>
		<div style="background:#f1f1f1;padding: 10px;border-radius: 4px;margin-top: 1%;width: max-content;"><i style="font-size:12px;">Note: Use [days] shortcode to display in your custom message e.g. This product will be delivered in <b>[days]</b> days.</i></div>
		<br>
		
		<div>
			<strong>Message to be displayed</strong>
			<input type="text" id="msg_t_be"  placeholder="Enter your custom message" style="margin-left: 4%;width: 70%;"> 
		</div>
		<div style="width:100%;">
			<button class="button-secondary add_scenario_click_btn" type="button" style="border-color: green;color: green;">
				<i class="fa fa-fw fa-plus"></i>
			Add Scenario(s)</button>
		</div>
		<div class="overflow_classs">
			<input type="hidden" id="tobeafter">

		</div>
		<br>
		<br>
		<div>
			<span class="plgfyedt_del_current_rule" style="color: red;float: right;padding: 6px 8px 6px 8px;cursor: pointer;    border: 1px solid red;    border-radius: 4px;"><i style="color:red;" class="fa fa-trash" aria-hidden="true"></i> Delete Rule		</span>
		</div>
		<br>
		<br>
		<br>
	</div>
	<?php
} else {
	$all_rules=get_option('plgfyedt_all_rules_in_db');
	foreach ($all_rules['isactive'] as $key => $value) {
		
		?>
		<button class="accordion" type="button" id="frstaccord" style="margin-top:1%;"><?php echo filter_var($all_rules['plgfyedt_rule_name'][$key]); ?>

		<i style="float: right;" class="fa fa-chevron-circle-down" aria-hidden="true"></i>
		<span style="float:right;margin-right: 5%;z-index: 999999999;" class="checkkk">
			<strong>Active</strong>
			<label class="switch">
				<input type="checkbox" id="plgfyedt_active"
				<?php
				if ('true' == $all_rules['isactive'][$key]) {
					echo filter_var('checked');
				}
				?>
				>
				<span class="slider"></span>
			</label>
		</span>
	</button>
	<div class="panel container " style="width: auto !important;height:auto !important;">
		<br>

		<strong>Rule Name</strong>
		<input type="text" value="<?php echo filter_var($all_rules['plgfyedt_rule_name'][$key]); ?>" id="plgfyedt_rule_name" style="margin-left:4%;">
		<table class="last_tbl_gnrl_stng " > 
			<tr>
				<th>
					Applied On
				</th>
				<th>
					Select Products/Categories
				</th>
			</tr>
			<tr>
				<td style="width: 30%;">
					<select id="plgfqdp_applied_onc" style="width: 90%;" class="class_plgfqdp_applied_onc">

						<option value="Products"
						<?php
						if ('Products' == $all_rules['plgfqdp_applied_onc'][$key]) {
							echo filter_var('selected="selected"');
						}
						?>
						>Products</option>
						<option value="Category"
						<?php
						if ('Category' == $all_rules['plgfqdp_applied_onc'][$key]) {
							echo filter_var('selected="selected"');
						}
						?>
						>Category</option>
						<option value="whole"
						<?php
						if ('whole' == $all_rules['plgfqdp_applied_onc'][$key]) {
							echo filter_var('selected="selected"');
						}
						?>
						>Apply To Whole Shop</option>
					</select>
				</td>
				<td style="width: 70%;">
					<div id="plgfqdp_products1c" 
					<?php
					if ('Products' != $all_rules['plgfqdp_applied_onc'][$key]) {
						echo filter_var(' style="display: none;" ');
					}
					?>
					>

					<select name="plgfqdp_products[]" multiple="multiple"  style="max-width: 90%;width: 90%;font-size: 12px;" class=" plgfqdp_products" id="plgfqdp_productsc"   >

						<?php
						while ( $plgfqdp_the_query -> have_posts() ) {
							$plgfqdp_the_query -> the_post(); 	
							?>
							<option value="<?php echo filter_var(get_the_ID()); ?>"
								<?php
								if ( 'Products' == $all_rules['plgfqdp_applied_onc'][$key] && in_array(get_the_ID(), $all_rules['applied_on_ids'][$key])) {
									echo filter_var('selected');
								}
								?>
								><?php echo filter_var(get_the_title()); ?></option>
								<?php
								
						}
						?>
						</select><br>
						<i>(These rule will be applicable to selected products)</i>
					</div>
					<div id="plgfqdp_selectcat1c" 
					<?php
					if ('Category' != $all_rules['plgfqdp_applied_onc'][$key]) {
						echo filter_var(' style="display: none;" ');
					}
					?>
					>

					<select  name="plgfqdp_selectcat[]" style="max-width: 90%;width: 90%;font-size: 12px;" id="plgfqdp_selectcatc" class="plgfqdp_selectcat"   multiple='multiple[]'>
						<?php
						if ( $plgfqdp_terms ) {   
							foreach ( $plgfqdp_terms as $plgfqdp_term1 ) {
								?>
								<option value="<?php echo filter_var($plgfqdp_term1->term_id); ?>"
									<?php
									if ( 'Category' == $all_rules['plgfqdp_applied_onc'][$key] && in_array($plgfqdp_term1->term_id, $all_rules['applied_on_ids'][$key])) {
										echo filter_var('selected');
									}
									?>
									><?php echo filter_var($plgfqdp_term1->name); ?></option>
									<?php

							}          
						}
						?>
						</select><br>
						<i>(These rule will be applicable to selected categories)</i>
					</div>
				</td>
			</tr>				
		</table>
		<br>
		<br>
		<div>
			<strong>Estimated Time</strong>
			<input value="<?php echo filter_var($all_rules['estimated_start'][$key]); ?>" type="number" id="estimated_start" style="width:7%;margin-left:4%;">   to   <input style="width:7%;" value="<?php echo filter_var($all_rules['estimated_end'][$key]); ?>" type="number" id="estimated_end">   days
		</div>
			<div style="background:#f1f1f1;padding: 10px;border-radius: 4px;margin-top: 1%;width: max-content;"><i style="font-size:12px;">Note: Use [days] shortcode to display in your custom message e.g. This product will be delivered in <b>[days]</b> days.</i></div>
		<br>
		<div>
			<strong>Message to be displayed</strong>
			<input  placeholder="Enter your custom message"  value="<?php echo filter_var($all_rules['to_b_displayd'][$key]); ?>" type="text" id="msg_t_be" style="width: 70%;margin-left: 4%;"> 
		</div>
		<br>
		<div style="width:100%;">
			<button class="button-secondary add_scenario_click_btn" type="button" style="border-color: green;color: green;">
				<i class="fa fa-fw fa-plus"></i>
			Add Scenario(s)</button>
		</div>
		<div class="overflow_classs">
			<input type="hidden" id="tobeafter">
			<?php
			foreach ($all_rules['scenarios'][$key][0] as $key_inner => $value_inner) {
				
				?>
				<div style="background:#eee;margin-top: 1%;border-left: 4px solid green;padding: 10px;">	
					<input value="<?php echo filter_var($value_inner); ?>" type="number" class="extra_start"  style="width:7%;">
					<label style="margin-left:4%;">to</label>
					<input value="<?php echo filter_var($all_rules['scenarios'][$key][1][$key_inner]); ?>"  type="number" class="extra_end"  style="width:7%;margin-left:4%;">
					<label style="margin-left:4%;">days</label>	
					<select style="margin-left:4%;" class="extra_type">	
						<option
						<?php
						if ('frst' == $all_rules['scenarios'][$key][2][$key_inner]) {
							echo filter_var('selected');
						}
						?>
						>Select Charges Type</option>	
						<option
						<?php
						if ('+' == $all_rules['scenarios'][$key][2][$key_inner]) {
							echo filter_var('selected');
						}
						?>
						>+</option>	
						<option
						<?php
						if ('+%' == $all_rules['scenarios'][$key][2][$key_inner]) {
							echo filter_var('selected');
						}
						?>
						>+%</option>	
						<option
						<?php
						if ('-' == $all_rules['scenarios'][$key][2][$key_inner]) {
							echo filter_var('selected');
						}
						?>
						>-</option>	
						<option
						<?php
						if ('-%' == $all_rules['scenarios'][$key][2][$key_inner]) {
							echo filter_var('selected');
						}
						?>
						>-%</option>
					</select>
					<input value="<?php echo filter_var($all_rules['scenarios'][$key][3][$key_inner]); ?>" type="number" class="extra_amount"  style="width:7%;margin-left: 4%;">
					<span class="plgfyedt_del_current_row" style="float: right;padding: 6px 8px 6px 8px;cursor: pointer;    border: 1px solid red;    border-radius: 4px;"><i style="color:red;" class="fa fa-trash" aria-hidden="true"></i>		</span>
				</div>
				<?php
			}
			?>

		</div>
		<br>
		<br>
		<div>
			<span class="plgfyedt_del_current_rule" style="color: red;float: right;padding: 6px 8px 6px 8px;cursor: pointer;    border: 1px solid red;    border-radius: 4px;"><i style="color:red;" class="fa fa-trash" aria-hidden="true"></i> Delete Rule		</span>
		</div>
		<br>
		<br>
		
	</div>
		<?php
	}
}
?>


<button class="button-primary save_All_rules_btn" type="button" style="width:10%;margin-top: 2%;" >
	
Save</button>


<style type="text/css">
.last_tbl_gnrl_stng {
	margin-top: 2%;
	font-family: Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

.last_tbl_gnrl_stng td, .last_tbl_gnrl_stng th {
	border: 1px solid #f1f1f1;
	padding: 4px;

}
.overflow_classs{
	overflow: auto;
	/*height: 410px !important;*/
}
.panel .container {
	height: auto !important;
	width: auto !important;
}
.container {
	height: auto !important;
	width: auto !important;
}
</style>




</div>
