<?php
/**
 * Plugin Name: WooCommerce Delivery Times
 * Plugin URI: https://redlight.se/
 * Description: Delivery time for each product
 * Version: 1.0.0
 * Author: Redlight Media
 * Author URI: https://redlight.se/
 * Developer: Christopher Hedqvist
 * Developer URI: https://redlight.se/
 * Text Domain: wcdt
 * Domain Path: /languages
 * WC requires at least: 3.0.0
 * WC tested up to: 3.5.2
 *
 * Copyright: © 2018 Redlight.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Required minimums and constants
 */
define( 'REDLIGHT_WCDT_PLUGIN_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'REDLIGHT_WCDT_PLUGIN_URL', untrailingslashit( plugin_dir_url( __FILE__ ) ) );
define( 'REDLIGHT_WCDT_MAIN_FILE', __FILE__ );
define( 'REDLIGHT_WCDT_STORE_URL', 'https://redlight.se' );
define( 'REDLIGHT_WCDT_ITEM_NAME', 'wcdt' );
define( 'REDLIGHT_WCDT_ITEM_ID', '' );
define( 'REDLIGHT_WCDT_VERSION', '1.0.0' );

if ( ! class_exists( 'WooCommerce_Delivery_Times' ) ) {
    /**
	 * Class WooCommerce_Delivery_Times
	 */
    class WooCommerce_Delivery_Times{
        /**
		 * The reference the *Singleton* instance of this class.
		 *
		 * @var $instance
		 */
        protected static $instance;
        
        /**
		 * Returns the *Singleton* instance of this class.
		 *
		 * @return self::$instance The *Singleton* instance.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
			}

			return self::$instance;
        }

         /**
		 * Protected constructor to prevent creating a new instance of the
		 * *Singleton* via the `new` operator from outside of this class.
		 */
		protected function __construct() {
            add_action( 'plugins_loaded', array( $this, 'init' ) );
        }

        /**
		 * Init the plugin after plugins_loaded so environment variables are set.
		 */
		public function init() {
            if ( ! class_exists( 'WooCommerce_Delivery_Times' ) ) {
				return;
            }

            include_once REDLIGHT_WCDT_PLUGIN_PATH . '/includes/class-woocommerce-delivery-times-woocommerce.php';

            load_plugin_textdomain(
                'wcdt',
                false,
                plugin_basename( __DIR__ ) . '/languages'
            );

        }
        


    }
    WooCommerce_Delivery_Times::get_instance();
}