<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WooCommerce_Delivery_Times_WooCommerce {
	/**
	 * Static class instance.
	 *
	 * @var null|WooCommerce_Delivery_Times_WooCommerce
	 */
	public static $instance = null;

	/**
	 * WooCommerce_Delivery_Times_WooCommerce constructor.
	 */
	private function __construct() {
        //Product
        add_action( 'woocommerce_product_options_shipping', array( $this, 'product_delivery_time_field' ) );
		add_action( 'woocommerce_product_options_shipping', array( $this, 'product_delivery_info_field' ) );
		add_action( 'woocommerce_product_options_stock_status', array( $this, 'product_shelf_location_field' ) );
        add_action( 'woocommerce_process_product_meta', array( $this, 'save_product_delivery_time_field' ) );
		add_action( 'woocommerce_process_product_meta', array( $this, 'save_product_delivery_info_field' ) );
		add_action( 'woocommerce_process_product_meta', array( $this, 'save_product_shelf_location' ) );

        add_action( 'woocommerce_single_product_summary', array( $this, 'display_product_delivery_time_field'), 25 );
        
        //Variable
        // Shelf Location
		add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'variation_product_shelf_location_field' ), 10, 3 );
		add_action( 'woocommerce_save_product_variation', array( $this, 'save_variation_save_product_shelf_location' ), 10, 2 );
        
	}

	/**
	 * Get a singelton instance of the class.
	 *
	 * @return WooCommerce_Delivery_Times_WooCommerce
	 */
	public static function get_instance(): WooCommerce_Delivery_Times_WooCommerce {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
    }
	/**
     * @global \WP_Post $post
     */
    public function product_shelf_location_field() {
        
        global $post;

        woocommerce_wp_text_input( 
            array(    
                'id'          => '_shelf_location',
                'label'       => __('Warehouse Shelf location', 'wcdt'),
				'desc_tip'    => 'true',
				'wrapper_class'	=>'show_if_simple',
                'description' => __('Enter Shelf Location', 'wcdt'),
                'value'       => get_post_meta( $post->ID, '_shelf_location', true ),
            )
        );

    }
	/**
     * Save Unifaun Tariff Code
     *
     * @param  $post_id
     * @return      void
     */
    public function save_product_shelf_location( $post_id ) {

        $shelf_location = $_POST['_shelf_location'];

        if( isset( $shelf_location ) ) {
            update_post_meta( $post_id, '_shelf_location', wp_kses_post(( $shelf_location ) ));
        }

        $shelf_location = get_post_meta( $post_id, '_shelf_location', true );

        if( empty( $shelf_location ) ) {
            delete_post_meta( $post_id, '_shelf_location', '' );
        }

    }
	/**
     * Add Unifaun Tariff Code for variations
     *
     * @param int $loop
     * @param $variation_data
     * @param \WC_Product_Variation $variation
     *
     * @return      void
     */
    public function variation_product_shelf_location_field( $loop, $variation_data, $variation ) {

        woocommerce_wp_text_input( 
            array(    
                'id' => '_shelf_location[' . $variation->ID . ']',
                'label' => __('Shelf Location', 'wcdt'),
                'wrapper_class' => 'form-row form-row-first',
                'desc_tip' => 'true',
                'description' => __('Enter Shelf Location', 'wcdt'),
                'value'       => get_post_meta( $variation->ID, '_shelf_location', true ),
            )
        );

    }
	/**
     * Save Unifaun Tariff Description for variations
     *
     * @param int $post_id
     *
     * @return      void
     */
    public function save_variation_save_product_shelf_location( $post_id ) {
        
        $data = $_POST['_shelf_location'][ $post_id ];

        if( isset( $data ) ) {
            update_post_meta( $post_id, '_shelf_location', esc_attr( $data ) );
        }

        $shelf_location = get_post_meta( $post_id,'_shelf_location', true );

        if ( empty( $shelf_location ) ) {
            delete_post_meta( $post_id, '_shelf_location', '' );
        }

	}

    /**
     * @global \WP_Post $post
     */
    public function product_delivery_time_field() {
        
        global $post;
        
        woocommerce_wp_select( 
            array(    
                'id'            => '_delivery_time',
                'label'         => __('Delivery time', 'wcdt'),
				'desc_tip'      => 'true',
				'wrapper_class'	=>'', //show_if_simple
                'description'   => __('', 'wcdt'),
                'options'       => apply_filters( 'redlight_wcdt_product_delivery_options',
                    array(
                        '' => __( 'Dont display', 'wcdt' ),
                        '1-2d' => __( 'Delivery within 1-2 days', 'wcdt' ),
                        '2-5d' => __( 'Delivery within 2-5 days', 'wcdt' ),
                        'backorder-1-2-w' => __( 'Backorder, delivery within 1-2 weeks', 'wcdt' ),
                        'backorder-2-4-w' => __( 'Backorder, delivery within 2-4 weeks', 'wcdt' ),
                    ),$post
                )
            )
        );

    }
	/**
     * @global \WP_Post $post
     */
    public function product_delivery_info_field() {
        
        global $post;
        
        woocommerce_wp_textarea_input( 
            array(    
                'id'            => '_delivery_info',
                'label'         => __('Delivery info', 'wcdt'),
				'desc_tip'      => 'true',
				'wrapper_class'	=>'', //show_if_simple
                'description'   => __('', 'wcdt')
            )
        );

    }
	/**
     * Save Delivery info
     *
     * @param  $post_id
     * @return      void
     */
    public function save_product_delivery_info_field( $post_id ) {
        
        $delivery_info = $_POST['_delivery_info'];

        if( isset( $delivery_info ) ) {
            update_post_meta( $post_id, '_delivery_info', wp_kses_post(( $delivery_info ) ));
        }

        $delivery_info = get_post_meta( $post_id, '_delivery_info', true );

        if( empty( $delivery_info ) ) {
            delete_post_meta( $post_id, '_delivery_info', '' );
        }

    }

    /**
     * Save Delivery time
     *
     * @param  $post_id
     * @return      void
     */
    public function save_product_delivery_time_field( $post_id ) {
        
        $delivery_time = $_POST['_delivery_time'];

        if( isset( $delivery_time ) ) {
            update_post_meta( $post_id, '_delivery_time', wp_kses_post(( $delivery_time ) ));
        }

        $delivery_time = get_post_meta( $post_id, '_delivery_time', true );

        if( empty( $delivery_time ) ) {
            delete_post_meta( $post_id, '_delivery_time', '' );
        }

    }

    public function display_product_delivery_time_field(){
        global $product;
        $delivery_time = get_post_meta($product->id,'_delivery_time', true);
        if(!empty($delivery_time)){
            echo '<div class="delivery-time">';
            switch ($delivery_time) {
                case "1-2d":
                    _e( 'Delivery within 1-2 days', 'wcdt' );
                    break;
                case "2-5d":
                    _e( 'Delivery within 2-5 days', 'wcdt' );
                    break;
                case "backorder-1-2-w":
                    _e( 'Backorder, delivery within 1-2 weeks', 'wcdt' );
                    break;
                case "backorder-2-4-w":
                    _e( 'Backorder, delivery within 2-4 weeks', 'wcdt' );
                    break;
                default:

            }
            echo '</div>';
        }
    }

}

WooCommerce_Delivery_Times_WooCommerce::get_instance();
