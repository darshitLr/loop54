��          \       �       �   $   �   $   �      �      �           -     F  �  S          4     Q     ]     |     �  	   �   Backorder, delivery within 1-2 weeks Backorder, delivery within 2-4 weeks Delivery time Delivery time for each product Delivery within 1-2 days Delivery within 2-5 days Dont display Project-Id-Version: WooCommerce Delivery Times
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-04-03 07:34+0000
PO-Revision-Date: 2020-04-03 07:36+0000
Last-Translator: bb-2w2 <christopher.hedqvist@redlight.se>
Language-Team: Svenska
Language: sv_SE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.1; wp-5.3.2 Beställningsvara 1-2 veckor Beställningsvara 2-4 veckor Leveranstid Leveranstid för varje produkt Skickas inom 1-2 dagar Skickas inom 2-5 dagar Visa inte 