<?php
/**
 * Plugin Name: WooCommerce Orderscanner Packstation
 * Plugin URI: https://redlight.se/
 * Description: Extends WooCommerce. Provides an orderscanner/packstation.
 * Version: 1.0.1
 * Author: Redlight Media
 * Author URI: https://redlight.se/
 * Developer: Christopher Hedqvist
 * Developer URI: https://redlight.se/
 * Copyright: © 2018 Redlight.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: woocommerce-orderscanner-packstations
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Required minimums and constants
 */
define( 'REDLIGHT_OP_MIN_PHP_VER', '7.0.0' );
define( 'REDLIGHT_OP_MIN_WC_VER', '3.0.0' );
define( 'REDLIGHT_OP_MAIN_FILE', __FILE__ );
define( 'REDLIGHT_OP_PLUGIN_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'REDLIGHT_OP_PLUGIN_URL', untrailingslashit( plugin_dir_url( __FILE__ ) ) );
define( 'REDLIGHT_OP_STORE_URL', 'https://redlight.se' );
define( 'REDLIGHT_OP_ITEM_NAME', 'woocommerce-orderscanner-packstation' );
define( 'REDLIGHT_OP_ITEM_ID', '39334' );
define( 'REDLIGHT_OP_VERSION', '1.0.1' );
define( 'REDLIGHT_OP_TEXTDOMAIN', 'woocommerce-orderscanner-packstation' );

define( 'REDLIGHT_OP_STATION_OPTION_KEY', 'redlight_op_stations');
define( 'REDLIGHT_OP_SETTING_PREFIX', 'redlight_op_setting_');


call_user_func( function () {

	require_once plugin_dir_path( __FILE__ ) . '/includes/class-wc-orderscanner-packstation-plugin.php';

	$main = new WC_Orderscanner_Packstation_Plugin( );

	register_activation_hook( __FILE__, [ $main, 'activate' ] );

	register_deactivation_hook( __FILE__, [ $main, 'deactivate' ] );

	register_uninstall_hook( __FILE__, [ $main, 'uninstall' ] );

	$main->run();
} );