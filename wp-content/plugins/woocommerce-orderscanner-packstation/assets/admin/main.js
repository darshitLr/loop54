jQuery(document).ready(function($){

    var orderSearchForm = $('[data-op-orders-search-form]');
    var page = $('.op-wrapper').first().data('page');
    var barcodeScanner = new BarcodeScanner();
    var timeout;
    var productItem = $('[data-order-product-item]');
    var thumbnail = $('.wc-order-item-thumbnail');

    /* 
     * Actions
     */
    $('#decrease').on( 'click', ( event )=> {
        var self = $(event.currentTarget)
        var input = self.parent().parent().find('input')
        var oldValue = input.val()
        if (oldValue > 1) {
          var newValue = parseFloat(oldValue) - 1;
        } else {
          newValue = 1;
        }
        self.blur()
        input.val(newValue)
        input.trigger("change")
      })
  
      $('#increase').on( 'click', ( event )=> {
        var self = $(event.currentTarget)
        var input = self.parent().parent().find('input')
        var oldValue = input.val()
        var newValue = parseFloat(oldValue) +1;
        self.blur()
        input.val(newValue)
        input.trigger("change")

      })

      $('#parcels').on( 'change', ( event )=> {
        console.log('SOMETHING HAPPEND')
        var self = $(event.currentTarget)
        var value = self.val()

        var orderId = self.data('order-id');
        var data = {
			'action': 'op_order_parcels',
			'order_id': orderId,
			'parcels': value,
        };
        
        jQuery.ajax({
            type:'POST',
            url:ajaxurl,
            data: data,
            success:function(data){
                if(data.success){
                    console.log('YAY')
                }else{
                    console.log('NAY')
                    jQuery.magnificPopup.open({
                        items: {
                            src: '<div class="op-popup"><p class="op-popup__text">'+data.data+'</p></div>',
                            type: 'inline',
                            closeBtnInside: false,
                        },
                    });
        
                    timeout = setTimeout(function(){
                        $.magnificPopup.close()
                    }, 2000)
                    
                }
                jQuery('.op-preloader').removeClass('op-preloader--active');
            }
        });

      })

    barcodeScanner.on('scanned', function(code){
        
        var triggerEl = $('[data-barcode-trigger*="' + code + '"]:not([data-barcode-was-triggered])').first();

        if(triggerEl.length < 1){
            triggerEl = $('[data-barcode-trigger*="' + code.replace(/^0+|0+$/g,'') + '" i]:not([data-barcode-was-triggered])').first()
        }

        //triggerEl = $(triggerEl)
        if(triggerEl.is('a') && triggerEl.attr('href') !== '#'){
            console.log("trying to click " + code, triggerEl);
            window.location = triggerEl.attr('href');

            if(!triggerEl.is('[data-barcode-allow-multiple-scans]')){
                triggerEl.attr('data-barcode-was-triggered', true)
            }

            return false
        } else if(triggerEl.length > 0){
            console.log("trying to click " + code, triggerEl);
            triggerEl.trigger('click').trigger('tap').trigger('scanned');

            if(!triggerEl.is('[data-barcode-allow-multiple-scans]')){
                triggerEl.attr('data-barcode-was-triggered', true)
            }

            if(triggerEl.is('[data-barcode-scroll-to-element]')){
                $([document.documentElement, document.body]).animate({
                    scrollTop: triggerEl.offset().top - 52
                }, 500);
            }
            
        } else {
            if(page === 'product-scan' ){
                $.magnificPopup.open({
                    items: {
                        src: '<div class="op-popup"><p class="op-popup__text">'+redlight_op.cannot_find_code+code+'</p></div>',
                        type: 'inline',
                        closeBtnInside: false,
                    },
                });

                timeout = setTimeout(function(){
                    $.magnificPopup.close()
                }, 2000)
            }

        }
    });

    orderSearchForm.on('submit', function(event){
        event.preventDefault();
        $.ajax({
            type:'POST',
            url:ajaxurl,
            data:$(this).serialize(),
            success:function(data){
                if(!data.success){
                    $.magnificPopup.open({
                        items: {
                            src: '<div class="op-popup"><p class="op-popup__text">'+data.data+'</p></div>',
                            type: 'inline',
                            closeBtnInside: false
                        },
                    });
                    timeout = setTimeout(function(){
                        $.magnificPopup.close()
                    }, 2000)
                }else{
                    if(data.data.find_several){

                        $.magnificPopup.open({
                            items: {
                                src: '<div class="op-popup modal-several-order-results"><p class="op-popup__text">'+redlight_op.found_several_orders_message+'</p><div class="op-popup__items"></div></div>',
                                type: 'inline',
                                closeBtnInside: false
                            },
                        });


                        data.data.orders.forEach(function(e){
                            var item = $('<a></a>');
                            item.attr('href',e.link);
                            item.text(e.order_name);
                            item.addClass('op-popup__item');

                            if(e.packed_status.end_pack){
                                item.attr('data-order-packed', 1);
                                item.attr('data-packed-confirm-message', e.packed_confirm_message);
                                item.attr('href',e.link + '&override');
                            }

                            $('.op-popup__items').append(item);
                        });

                    }else if(data.data.packed_status.end_pack){
                        confirmAction(data.data.packed_confirm_message, function(){
                            window.location.href = data.data.link + '&override';
                        }, function () {

                        });
                    }else{
                        window.location.href = data.data.link;
                    }
                }
            }
        });
    });

    thumbnail.on('mouseup', function(event){
        // stop the event from bubbling.
        event.stopPropagation();
        }).magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: false,
            fixedContentPos: true,
            mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
            image: {
                verticalFit: true,
            },
            zoom: {
                enabled: true,
                easing: 'ease-in-out', // CSS transition easing function
                duration: 300 // don't foget to change the duration also in CSS
            }
        });

    var longpress = 1500;
    // holds the start time
    var start;

    productItem.on( 'mousedown', function( e ) {
        start = new Date().getTime();
    } );

    productItem.on( 'mouseleave', function( e ) {
        start = 0;
    } );

    productItem.on( 'mouseup', function( e ) {
        var self = $(this);
        //console.log(self,'mouseup / click')
        if ( new Date().getTime() >= ( start + longpress )  ) {
            resetScanProduct(self);
        } else {
            scan(self);   
        }
    } );
    productItem.on( 'tap', function( e ) {
        var self = $(this);

        scanProduct(self);
    } );
    productItem.on( 'taphold', function( e ) {
        var self = $(this);

        resetScanProduct(self);
    } );

    $(document).on('click', '[data-order-packed=1]', function (e) {
        e.preventDefault();
        var $self = $(this);
        confirmAction($(this).data('packed-confirm-message'), function(){
            document.location.href = $self.attr('href') + '&override';
        }, function () {

        });
    });

    $('[data-change-station-name-link]').on('click', function () {
        $(this).parent().next('.packing-station-item__change_name').toggleClass('packing-station-item__change_name--active');
    });
});

function scan(element){
    //Check if element has barcode
    var code = element.data('barcode-trigger');
    //Else
    if(code.length > 0){
        code.toString().split('').forEach(function(el){
            jQuery(document).trigger(
                jQuery.Event( 'keypress', { keyCode: el.charCodeAt(0), which: el.charCodeAt(0) } )
            );
        });
    
        jQuery(document).trigger(
            jQuery.Event( 'keypress', { keyCode:13, which:13} )
        );
    }else{
        scanProduct(element)
    }
}

function confirmAction(message, fnSuccess, fnFail){
    jQuery.magnificPopup.open({
        items: {
            src: '<div class="op-popup"><p class="op-popup__text">'+message+'</p><button class="button textleft op-confirm-button button-primary" data-op-modal-confirm>OK</button><button class="button textleft op-cancel-button" data-op-modal-cancel>Cancel</button></div>',
            type: 'inline',
            closeBtnInside: true
        }
    });

    jQuery('[data-op-modal-confirm]').on('click', function(){
        fnSuccess();
        jQuery.magnificPopup.instance.close();
    });

    jQuery('[data-op-modal-cancel]').on('click', function(){
        fnFail();
        jQuery.magnificPopup.instance.close();
    });
}

function scanProduct(element){
    var orderContainer    = jQuery('[data-order-products-container]');
    var scanProductAction = orderContainer.data('scan-product-action');
    var orderId           = orderContainer.data('order-id');
    var fullScannedClass  = 'op-order-product-item--scanned_full';
    var partScannedClass  = 'op-order-product-item--scanned_part';
    var productQuantity   = parseInt(element.data('quantity'));
    var productScanned    = parseInt(element.attr('data-scanned'));
    var scannedTimes      = element.find('[data-scanned-times]');
    var highQuantity      = parseInt(orderContainer.data('high-quantity-product'));
    var oitemId           = parseInt(element.data('order-item-id'));
    var self              = this;
    var isScanAll         = element.data('is-scan-all');

    this.send = function(){
        jQuery('.op-preloader').addClass('op-preloader--active');

        jQuery.ajax({
            type:'POST',
            url:ajaxurl,
            data:{data: element.data(), order_id: orderId, action: scanProductAction, is_scan_all: isScanAll ? 1 : 0 },
            success:function(data){
                if(data.success){

                    if(data.data.end_pack){
                        window.location.href = redlight_op.packing_complete_url;
                    }

                    if((productScanned + 1) === productQuantity || isScanAll){
                        element.removeClass(partScannedClass);
                        element.addClass(fullScannedClass);
                    }else{
                        element.addClass(partScannedClass);
                    }
                    //console.log(data.data.products);
                    var scanned = Object.values(data.data.products).find(function(item){
                        return item.order_item_id == oitemId;
                    }).scanned;

                    scannedTimes.html(scanned);
                    element.attr('data-scanned', scanned);
                }else{
                    jQuery.magnificPopup.open({
                        items: {
                            src: '<div class="op-popup"><p class="op-popup__text">'+data.data+'</p></div>',
                            type: 'inline',
                            closeBtnInside: true
                        },
                    });
                }
                jQuery('.op-preloader').removeClass('op-preloader--active');
            },
        });
    }

    if(productScanned < productQuantity){

        if(productQuantity - productScanned >= highQuantity){
            if(isScanAll === ''){
                confirmAction(redlight_op.confirm_high_quantity_message + ' (' + (productQuantity - productScanned) + ')', function(){
                    isScanAll = true;
                    self.send();
                }, function(){
                    isScanAll = false;
                    self.send();
                });
            }else{
                self.send();
                isScanAll = isScanAll === 1;
            }

            element.data('is-scan-all', isScanAll ? 1 : 0);
        }else{
            self.send();
        }
    }
}

function resetScanProduct(element) {
    console.log(element,"resetscan");
    var orderContainer         = jQuery('[data-order-products-container]');
    var resetScanProductAction = orderContainer.data('reset-scan-product-action');
    var orderId                = orderContainer.data('order-id');
    var fullScannedClass       = 'op-order-product-item--scanned_full';
    var partScannedClass       = 'op-order-product-item--scanned_part';
    var scannedTimes           = element.find('[data-scanned-times]');


    if(!element.data('done')){
        jQuery('.op-preloader').addClass('op-preloader--active');

        jQuery.ajax({
            type:'POST',
            url:ajaxurl,
            data:{data: element.data(), order_id: orderId, action: resetScanProductAction},
            success:function(data){
                if(data.success){
                    element.removeClass(fullScannedClass);
                    element.removeClass(partScannedClass);
                    scannedTimes.html(0);
                    element.attr('data-scanned', 0);
                }else{
                    jQuery.magnificPopup.open({
                        items: {
                            src: '<div class="op-popup"><p class="op-popup__text">'+data.data+'</p></div>',
                            type: 'inline',
                            closeBtnInside: false,
                        },
                    });
        
                    timeout = setTimeout(function(){
                        $.magnificPopup.close()
                    }, 2000)
                    
                }
                jQuery('.op-preloader').removeClass('op-preloader--active');
            }
        });
    }
}