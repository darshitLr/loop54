function BarcodeScanner(){
    var privates = {};
    var publics = {};
    publics.events = {};
    privates.interval = false;
    privates.buffer = "";
    privates.timeout = 5000;

    privates.init = function(){
        privates.setup();
    };

    privates.setup = function(){

        jQuery(document).keypress(function(e){

            if(!privates.interval){
                privates.buffer = String.fromCharCode(e.which)
            } else {
                if(e.which === 13) {
                    privates.buffer = privates.buffer.replace(/[^A-Za-z0-9_-]/g, "");

                    if(privates.buffer.length > 1){
                        publics.trigger('scanned', privates.buffer);
                    }

                    privates.buffer = "";
                } else {
                    privates.buffer += String.fromCharCode(e.which);
                }
            }

            clearInterval(privates.interval);

            privates.interval = setTimeout(function(){
                privates.interval = false;
            }, privates.timeout);

        })
    };

    publics.on = function (event, cb) {
        publics.events[event] = publics.events[event] || [];
        publics.events[event].push(cb)
    };

    publics.trigger = function (event, data) {
        if (!publics.events[event]) { return false }

        jQuery.each(publics.events[event], function (key, cb) {
            if (typeof cb === 'function') {
                cb(data)
            }
        })
    };

    privates.init();

    return publics;
}
