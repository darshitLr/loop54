<?php if ( ! defined( 'WPINC' ) ) die; ?>

<?php if(!empty($current_user_station)):?>
<?php do_action( 'orderscanner_packstation_order_before_wrapper', $order, $user, $current_user_station ); ?>
    <div class="wrapper op-wrapper" data-page="product-scan">
    <?php do_action( 'orderscanner_packstation_order_after_wrapper', $order, $user, $current_user_station ); ?>
        <?php if($is_packed): ?>
            <div class="postbox op-postbox-done">
                <h3><?php _e('This order has been already packed.', 'woocommerce-orderscanner-packstations'); ?>
                    <a href="<?php echo $repack_link; ?>"><?php _e('You can repack it!', 'woocommerce-orderscanner-packstations'); ?></a></h3>
                <p class="op-postbox-done__text">
                    <b>
                        <?php echo __('Packing began', 'woocommerce-orderscanner-packstations'); ?>:
                    </b>
                        <?php
                            printf(
                                _x( '%s ago', '%s = human-readable time difference', 'woocommerce' ),
                                human_time_diff( $packed_status['begin_pack'], current_time( 'timestamp', true ) )
                            );
                        ?>
                </p>
                <p class="op-postbox-done__text">
                    <b>
                        <?php echo __('Packing ended', 'woocommerce-orderscanner-packstations'); ?>:
                    </b>
                    <?php
                        printf(
                            _x( '%s ago', '%s = human-readable time difference', 'woocommerce' ),
                            human_time_diff( $packed_status['end_pack'], current_time( 'timestamp', true ) )
                        );
                    ?>
                </p>
                <p class="op-postbox-done__text">
                    <b>
                        <?php echo __('Packer', 'woocommerce-orderscanner-packstations'); ?>:
                    </b>
                    <?php
                        $packer = new WP_User($packed_status['packer']);

                        echo $packer->first_name . ' ' . $packer->last_name;
                    ?>
                </p>
                <p class="op-postbox-done__text">
                    <b>
                        <?php echo __('Packstation', 'woocommerce-orderscanner-packstations'); ?>:
                    </b>
                    <?php
                        echo $controller->packing_station->getStationByHash($packed_status['station'])['name'];
                    ?>
                </p>
            </div>
        <?php endif; ?>

        <div class="postbox op-postbox">
            <div class="inside">
                <div class="op-order-actions">
                    <?php do_action( 'orderscanner_packstation_order_actions', $order,$user,$current_user_station ); ?>
                    <a href="<?php echo $mark_all_packed_link; ?>"><button <?php disabled($is_packed); ?> class="button op-order-actions__button"><?php _e('Mark all item as packed', 'woocommerce-orderscanner-packstations'); ?></button></a>
                    <?php if(strtolower(wc_get_order_status_name($order->get_status()))  != strtolower(wc_get_order_status_name($mark_as))): ?>
                        <a href="<?php echo $mark_as_link; ?>"><button <?php disabled($is_packed);?>  class="button op-order-actions__button"><?php echo __('Mark as', 'woocommerce-orderscanner-packstations') . ' ' . wc_get_order_status_name($mark_as); ?></button></a>
                    <?php else: ?>
                        <button class="button" disabled><?php echo __('Order', 'woocommerce-orderscanner-packstations') . ' ' . wc_get_order_status_name($mark_as); ?></button>
                    <?php endif; ?>
                </div>

                <div class="panel-wrap">
                    <h2 class="op-postbox__title"><?php echo $current_user_station['name']; ?> <strong><?php echo __('Order', 'woocommerce-orderscanner-packstations') . ' #' . $order->get_order_number() ?></strong> </h2>
                    <p class="op-postbox__description"><?php echo __('Logged in as', 'woocommerce-orderscanner-packstations') . ' ' . $user->user_firstname . ' ' . $user->user_lastname . ' (' . $user->nickname . ')'; ?></p>
                </div>
                <div class="op-order-parcels">
                    <p class="control"><button id="decrease" class="change-amount">-</button></p>
                    <p class="control"><input id="parcels" name="parcels" class="parcels-input" type="number" min="1" value="1" data-order-id="<?php echo esc_html( $order->get_id() ) ?>"></p>
                    <p class="control"><button id="increase" class="change-amount">+</button></p>
                </div>

            </div>
        </div>

        <div class="postbox op-order-details op-postbox">
            <div class="inside">
                <div class="panel-wrap">
                    <h2 class="op-postbox__title"><?php echo __('Order', 'woocommerce-orderscanner-packstations') . ' #' . $order->get_order_number() ?></h2>
                    <p class="op-postbox__description">
                        <?php echo $orderDetails; ?>
                    </p>
                </div>
            </div>
        </div>

        <div id="woocommerce-order-items" data-order-products-container data-high-quantity-product="<?php echo $high_quantity_product; ?>" class="postbox" data-scan-product-action="<?php echo $scan_product_action; ?>" data-order-id="<?php echo $order->get_id(); ?>" data-reset-scan-product-action="<?php echo $reset_scan_product_action; ?>">
            <div class="op-preloader">
                <div class="spinner op-spinner"></div>
            </div>

            <div class="inside">
                <div class="woocommerce_order_items_wrapper">
                    <table cellpadding="0" cellspacing="0" class="woocommerce_order_items op-order-products">
                        <thead>
                            <tr>
                                <th class="item" colspan="2"><?php _e('Item', 'woocommerce-orderscanner-packstations'); ?></th>
                                <th class="item op-order-products__scanned"><?php _e('Scanned', 'woocommerce-orderscanner-packstations'); ?></th>
                                <th class="item op-order-products__qty"><?php _e('Quantity', 'woocommerce-orderscanner-packstations'); ?></th>
                            </tr>
                        </thead>
                        <tbody id="order_line_items">
                            <?php 
                            foreach ($order->get_items() as $item_id => $item){
                                do_action( 'orderscanner_packstation_order_before_item_html', $item_id, $item, $order );

                                if($item instanceof WC_Order_Item_Product){
                                    $wc_product = $item->get_product();
                                    if( $wc_product->is_type('bundle') || $wc_product->is_virtual() ){
                                        //Skip bundle since it only contains other products
                                        //Skip virtual products, since they should not be packed.
                                        continue;
                                    }
                                    include REDLIGHT_OP_PLUGIN_PATH . '/views/admin/html-order-item.php'; 
                                }
                                do_action( 'orderscanner_packstation_order_after_item_html', $item_id, $item, $order );
                            }
                            do_action( 'orderscanner_packstation_order_items_after_line_items', $order->get_id() );
                            ?>
                         </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php else: ?>
    <h1><?php _e('Please, select your station', 'woocommerce-orderscanner-packstations' ); ?></h1>
<?php endif; ?>