<?php if ( ! defined( 'WPINC' ) ) die; ?>

<div class="wrapper op-wrapper" data-page="order-scan">
    <h1><?php _e('Orders', 'woocommerce-orderscanner-packstations'); ?></h1>
    <div class="op-orders-form-wrapper">

        <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" class="search-box op-packstation-form op-packstation-form--left" data-op-orders-search-form>
            <label class="screen-reader-text" for="op-order-code-input"><?php _e('Search order', 'woocommerce-orderscanner-packstations') ?>:</label>
            <input type="search" required class="op-packstation-form__search" name="code" id="op-order-code-input" autofocus> 
            <input type="submit" id="search-submit" class="button op-packstation-form__button" value="<?php _e('Search order', 'woocommerce-orderscanner-packstations') ?>">

            <input type="hidden" name="action" value="<?php echo $order_search_action; ?>">
        </form>

        <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" class="search-box op-packstation-form op-packstation-form--left">
            <label for="op-select-station"><?php _e('Select station', 'woocommerce-orderscanner-packstations'); ?></label>
            <select class="op-packstation-form__select" id="op-select-station" onchange="window.location.href = this.value">
                <option value=""><?php _e('Not selected', 'woocommerce-orderscanner-packstations')?></option>
                <?php foreach ($stations as $station):?>

                    <?php
                        $set_link = wp_nonce_url(admin_url('admin-post.php?hash='.$station['hash'].'&action=' . $set_station_action), $set_station_action);
                    ?>
                    <option value="<?php echo $set_link; ?>" <?php selected( $current_user_station, $station['hash'] ); ?> ><?php echo $station['name']; ?></option>
                <?php endforeach; ?>
            </select>
        </form>

    </div>

    <?php

        if($display_order_list){
            if(!empty($current_user_station)){
                $table->prepare_items();
                $table->display();
           } else {
               ?>
                   <h1><?php _e('Please, select your station', 'woocommerce-orderscanner-packstations'); ?></h1>
               <?php
           }
        }elseif(empty($current_user_station)){
            ?>
            <h1><?php _e('Please, select your station', 'woocommerce-orderscanner-packstations'); ?></h1>
            <?php
       }else {
            ?>
                <h1><?php _e('Scan order to continue', 'woocommerce-orderscanner-packstations'); ?></h1>
            <?php
        }

    ?>

</div>
