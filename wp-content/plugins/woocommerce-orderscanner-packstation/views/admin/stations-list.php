<?php if ( ! defined( 'WPINC' ) ) die; ?>

<div class="wrapper op-wrapper">

    <h1><?php _e('Manage stations', 'woocommerce-orderscanner-packstations'); ?></h1>

    <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" class="op-packstation-form">
        <input type="text" class="op-packstation-form__station_name" name="station_name">
        <input type="submit" class="button op-packstation-form__button" value="<?php _e('Add new', 'woocommerce-orderscanner-packstations'); ?>">

        <?php wp_nonce_field('op_add_new_station')?>
        <input type="hidden" name="action" value="<?php echo $add_station_action; ?>">
    </form>

    <div id="woocommerce-order-items">
        <div class="inside">
            <div class="woocommerce_order_items_wrapper">
                <table cellpadding="0" cellspacing="0" class="woocommerce_order_items op-order-products">
                    <thead>
                    <tr>
                        <th class="item"><?php _e('Station', 'woocommerce-orderscanner-packstations'); ?></th>
                        <th class="item op-order-products__scanned"><?php _e('Date created', 'woocommerce-orderscanner-packstations'); ?></th>
                        <th class="item op-order-products__qty"><?php _e('Created by', 'woocommerce-orderscanner-packstations'); ?></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($stations)): ?>

                            <?php foreach ($stations as $station): ?>

                                <?php
                                    $delete_link = wp_nonce_url(admin_url('admin-post.php?hash='.$station['hash'].'&action=' . $delete_station_action), $delete_station_action);
                                ?>

                                <tr class="item packing-station-item">
                                    <td class="name">
                                        <div class="packing-station-item__name">
                                            <?php echo $station['name']; ?>
                                            <a href="#" class="packing-station-item__change-name-link" data-change-station-name-link><?php _e('Change', 'woocommerce-orderscanner-packstations'); ?></a>
                                        </div>
                                        <div class="packing-station-item__change_name">
                                            <?php $change_name_link = wp_nonce_url(admin_url('admin-post.php?hash='.$station['hash'].'&action=' . $change_station_name_action), $change_station_name_action);?>
                                            <form action="<?php echo $change_name_link; ?>" method="post">
                                                <input type="text" value="<?php echo $station['name']; ?>" name="name">
                                                <input type="hidden" value="<?php echo $station['hash'];?>" name="hash">

                                                <?php wp_nonce_field($change_station_name_action); ?>

                                                <button class="button"><?php _e('Save', 'woocommerce-orderscanner-packstations'); ?></button>
                                            </form>
                                        </div>
                                    </td>

                                    <td class="quantity">
                                        <?php
                                            echo sprintf(
                                                _x( '%s ago', '%s = human-readable time difference', 'woocommerce' ),
                                                human_time_diff( $station['date_created'], current_time( 'timestamp', true ) )
                                            );
                                        ?>
                                    </td>

                                    <td class="quantity scanned-times">
                                        <?php $user = new WP_User($station['user_created']); ?>
                                        <?php echo $user->user_firstname . ' ' . $user->user_lastname . ' ' . '('. $user->nickname .')' ?>
                                    </td>
                                    <td width="1%">
                                        <?php $confirm_message = __('Do you really want to delete this station?', 'woocommerce-orderscanner-packstations'); ?>
                                        <button onclick="if(confirm('<?php echo $confirm_message; ?>')) window.location.href = '<?php echo $delete_link;?>'"class="button"><?php _e('Delete', 'woocommerce-orderscanner-packstations'); ?></button>
                                    </td>
                                </tr>
                            <?php endforeach;?>

                        <?php else: ?>
                            <tr>
                                <td>
                                    <span><?php _e('Stations do not exist. Create first one', 'woocommerce-orderscanner-packstations'); ?></span>
                                </td>
                            </tr>
                        <?php endif; ?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>

</div>