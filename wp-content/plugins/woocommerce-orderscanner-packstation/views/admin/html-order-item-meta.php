<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$hidden_order_itemmeta = apply_filters(
	'woocommerce_hidden_order_itemmeta', array(
		'_qty',
		'_tax_class',
		'_product_id',
		'_variation_id',
		'_line_subtotal',
		'_line_subtotal_tax',
		'_line_total',
		'_line_tax',
		'method_id',
		'cost',
		'_reduced_stock',
	)
);

$packstation_product_comment = get_post_meta($item->get_product_id(), '_packstation_product_comment', true);
if($wc_product->get_type() == 'variation'){
    $packstation_variation_comment = $wc_product->get_meta('_packstation_variation_comment',true);
}
?>
<?php if(!empty($wc_product->get_sku())):?>
    <br>
    <span><b><?php _e('SKU', 'woocommerce-orderscanner-packstations')?></b>: <?php echo $wc_product->get_sku(); ?></span>
<?php endif; ?>
<?php if(!empty($wc_product->get_meta($barcode_meta,true))):?>
    | <span><b><?php _e('Barcode', 'woocommerce-orderscanner-packstations')?></b>: <?php echo $wc_product->get_meta($barcode_meta,true); ?></span>
<?php endif; ?>
<?php if(!empty($packstation_product_comment)):?>
    <span class="packstation-comment product"><b><?php _e('Product comment', 'woocommerce-orderscanner-packstations')?></b>: <?php echo $packstation_product_comment; ?></span>
<?php endif; ?>
<?php if(!empty($packstation_variation_comment)):?>
    <span class="packstation-comment variation"><b><?php _e('Variant comment', 'woocommerce-orderscanner-packstations')?></b>: <?php echo $packstation_variation_comment; ?></span>
<?php endif; ?>
<?php if ( $meta_data = $item->get_formatted_meta_data( '' ) ) : ?>
    <table cellspacing="0" class="display_meta">
        <?php
        foreach ( $meta_data as $meta_id => $meta ) :
            if ( in_array( $meta->key, $hidden_order_itemmeta, true ) ) {
                continue;
            }
            ?>
            <tr>
                <th><?php echo wp_kses_post( $meta->display_key ); ?>:</th>
                <td><?php echo wp_kses_post( force_balance_tags( $meta->display_value ) ); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
