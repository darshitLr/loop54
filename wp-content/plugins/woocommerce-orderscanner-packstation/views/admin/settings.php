<?php if ( ! defined( 'WPINC' ) ) die; ?>

<div class="wrapper op-settings-wrapper">
    <div class="postbox op-postbox">
        <div class="inside">
            <?php if($is_network): ?>
                <form action="<?php echo admin_url('admin-post.php'); ?>" method="post">
                    <?php
                        do_settings_sections( 'packing_station_setting' );
                        submit_button( __( 'Save', 'woocommerce-orderscanner-packstations' ) );
                    ?>
                    <?php wp_nonce_field(WC_Orderscanner_Packstation_Settings_Network::SAVE_ACTION)?>
                    <input type="hidden" name="action" value="<?php echo WC_Orderscanner_Packstation_Settings_Network::SAVE_ACTION; ?>">
                </form>
            <?php else: ?>
                <form action="<?php echo admin_url('options.php') ?>" method="post">
                    <?php
                        settings_fields( 'redlight_op_settings' );
                        do_settings_sections( 'packing_station_setting' );
                        submit_button( __( 'Save', 'woocommerce-orderscanner-packstations' ) );
                    ?>
                </form>
            <?php endif; ?>
        </div>
    </div>
</div>