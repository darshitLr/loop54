<?php
/**
 * Shows an order item
 *
 * @var object $item The item being displayed
 * @var object $wc_product The product $item->get_product()
 * @var int $item_id The id of the item being displayed
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$packed_product_status = $packed_status['products'][$item_id];

$class = '';
$packstation_product_comment = '';
$packstation_variation_comment = '';
$barcode_meta = apply_filters('orderscanner_packstation_product_barcode_meta_key','_packstation_ean'); 

if($packed_product_status['scanned'] > 0){
    $class = $packed_product_status['scanned'] >= $packed_product_status['quantity'] ? 'op-order-product-item--scanned_full' : 'op-order-product-item--scanned_part';
}
?>
<tr class="item op-order-product-item <?php echo $class; ?>" 
    data-quantity="<?php echo $item->get_quantity()?>"
    data-is-scan-all
    data-done="<?php echo $is_packed; ?>" 
    data-scanned="<?php echo $packed_product_status['scanned']; ?>" 
    data-order-item-id="<?php echo $item->get_id() ?>" 
    data-order-product-id="<?php echo $item->get_product_id() ?>" 
    data-order-variation-id="<?php echo $item->get_variation_id() ?>" 
    data-barcode-trigger="<?php echo $wc_product->get_meta($barcode_meta,true);?>" 
    data-sku="<?php echo $wc_product->get_sku(); ?>" 
    data-product-type="<?php echo $wc_product->get_type(); ?>"
    data-blog-id="<?php echo $blog_id; ?>"
    data-order-product-item
    data-barcode-allow-multiple-scans
    data-barcode-scroll-to-element
>

<td class="thumb">
    <?php $thumbnail = $wc_product ? apply_filters( 'woocommerce_admin_order_item_thumbnail', $wc_product->get_image( 'thumbnail', array( 'title' => '' ), false ), $item_id, $item ) : ''; ?>
    <?php $thumbnail_url = wp_get_attachment_image_src($wc_product->get_image_id(), 'full'); ?>
    <a href="<?php echo $thumbnail_url[0] ?>" class="wc-order-item-thumbnail">
        <?php echo wp_kses_post( $thumbnail ) ?>
    </a>
</td>

<td class="name">
    <span class="wc-order-item-name"><?php echo  $wc_product->get_name(); ?></span>
    <?php do_action( 'orderscanner_packstation_order_before_item_meta_html',$wc_product, $item_id, $item, $order );?>
    <?php require REDLIGHT_OP_PLUGIN_PATH . '/views/admin/html-order-item-meta.php'; ?>   
    <?php do_action( 'orderscanner_packstation_order_after_item_meta_html', $wc_product, $item_id, $item, $order );?>
                                   
</td>

<td class="quantity scanned-times" width="10px">
    <div class="view">
        <small class="times">×</small>
        <span data-scanned-times><?php echo $packed_product_status['scanned']; ?></span>
    </div>
</td>

<td class="quantity" width="1%">
    <div class="view">
        <small class="times">×</small><?php echo $item->get_quantity()?>
    </div>
</td>
</tr>