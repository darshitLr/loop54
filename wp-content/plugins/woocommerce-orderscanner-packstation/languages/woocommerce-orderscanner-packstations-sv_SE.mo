��    K      t      �      �     �     �     �     �          "     )  
   B     M     R     _     f     �     �     �  *   �     �            "   -  Z   P  B   �     �     �                     7     ?  &   N  6   u     �     �  <   �     �               $  	   +  	   5     ?     F     V     d     t     �     �     �     �     �     �     �     �      	     	     	     $	     1	  $   @	     e	  	   n	     x	     }	     �	  '   �	     �	  #   �	     �	     �	  !   
     '
     8
  $   K
  "   p
  �  �
     �  	   �     �     �      �     �       	   %     /  
   5     @  +   Q     }  '   �     �  6   �  !        '     5  2   J  m   }  B   �     .     6     C     T  "   e     �     �  *   �  ;   �  
        %  2   +     ^     p     y     �  
   �  	   �     �     �     �     �     �     �     �       '        B     Q     b     h     o      u     �     �     �     �     �  
   �  	   �          
  '        F     M     l     r  2   �     �     �  #   �  1   �   Add new Barcode Cannot find code Cannot find order: %s Cannot find product with code Change Completed packing status Created by Date Date created Delete Display EAN field on products Display list of orders Display orders status Do not change the status Do you really want to delete this station? Do you want to pack all items? EAN Found several orders High quantity product started from If you enter a EAN/Barcode here it will be used to identify the product at the packstation If you enter a comment here, it will be visible at the packstation Item Logged in as Manage station Manage stations Mark all item as packed Mark as Mark as status No order_scanned_status meta was found No orders was found, did you pack them all? Great job! Not selected Order Order %s have been packed already, do you want to repack it? Order does not exist Order items Orders Packed Packed at Packed by Packer Packing Station Packing began Packing details Packing ended Packing started Packstation Packstation comment Please, enter a code Please, select your station Product comment Quantity SKU Save Scan order to continue Scanned Search order Select station Set order status when packing begins Settings Show/hide Site Station Station does not exist Stations do not exist. Create first one Status This order has been already packed. Total Variant comment What sites to display orders from Wrong input data You can repack it! You don't have access to this action You don't have access to this page Project-Id-Version: WooCommerce Orderscanner Packstation
POT-Creation-Date: 2018-10-23 07:22+0000
PO-Revision-Date: 2018-10-23 07:24+0000
Last-Translator: bb-2w2 <christopher.hedqvist@redlight.se>
Language-Team: Svenska
Language: sv_SE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
Report-Msgid-Bugs-To:  Lägg till packbord Streckkod Kan inte hitta koden Kan inte hitta order med: %s Kan inte hitta produkt med koden Redigera Orderstatus vid färdigpackad Skapad av Datum Skapat den Ta bort packbord Visa fält för EAN/Streckkod på produkter Visa lista med ordrar Vilka orderstatusar ska visas i listan? Ändra inte status Är du säkert på att du vill ta bort detta packbord? Vill du markera alla som packade? EAN/Streckkod Hittade flera ordrar Snabbmarkering av högt antal på en produkt från Om du skriver in en EAN/Streckkod här så kommer den användas till att identifiera produkten på packbordet Om du anger en kommentar här så kommer den visas på packbordet. Artikel Inloggad som Hantera packbord Hantera packbord Markera alla artiklar som plockade Markera order som Markera order som status Inget 'order_scanned_status' meta hittades Inga ordrar hittades, har du packat allihopa? Grymt jobbat! Inget valt Order Order %s är redan packad, vill du packa den igen? Ordern finns inte Artiklar Ordrar Packad Packad vid Packad av Packare Packbord Packningen påbörjad Packningsdetaljer Packningen slutförd Packning påbörjad Packbord Packbordskommentar Ange något för att söka efter ordrar Välj packbord Produktkommentar Antal Art.nr Spara Scanna order för att fortsätta Skannad Sök efter order Välj packbord Status vid påbörjad packning Inställningar Visa/Dölj Webbplats Packbord Packbord finns inte Inget packbord finns skapat, skapa ett. Status Denna order har redan packats. Total Variantkommentar Välj vilka webbplatser vi ska hämta ordrar från Felaktig inmatning Packa om ordern! Du har inte rättigheter för detta Du har inte rättigheter för att visa denna sida 