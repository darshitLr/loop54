<?php

abstract class Abstract_WC_Orderscanner_Packstation_Settings{

    /**
     * WC_Orderscanner_Packstation_Settings constructor.
     */
    public function __construct()
    {
        add_action('admin_init', array($this, 'register'));
    }

    /**
     * Register settings
     */
    public function register()
    {
        register_setting('redlight_op_settings', REDLIGHT_OP_SETTING_PREFIX . 'license_key');
        register_setting('redlight_op_settings', REDLIGHT_OP_SETTING_PREFIX . 'disable_license_key');
        register_setting('redlight_op_settings', REDLIGHT_OP_SETTING_PREFIX . 'packed_status');
        register_setting('redlight_op_settings', REDLIGHT_OP_SETTING_PREFIX . 'display_order_list');
        register_setting('redlight_op_settings', REDLIGHT_OP_SETTING_PREFIX . 'display_order_status');
        register_setting('redlight_op_settings', REDLIGHT_OP_SETTING_PREFIX . 'mark_as_status');
        register_setting('redlight_op_settings', REDLIGHT_OP_SETTING_PREFIX . 'begin_pack_status');
        register_setting('redlight_op_settings', REDLIGHT_OP_SETTING_PREFIX . 'high_quantity_products');
        register_setting('redlight_op_settings', REDLIGHT_OP_SETTING_PREFIX . 'display_ean_field');

        add_settings_section(
            'redlight_op_settings',
            __('Settings', 'woocommerce-orderscanner-packstations'),
            null,
            'packing_station_setting'
        );

        add_settings_field(
            REDLIGHT_OP_SETTING_PREFIX . 'license_key',
            __('License Key', 'woocommerce-orderscanner-packstations'),
            [$this, 'renderLicenseKey'],
            'packing_station_setting',
            'redlight_op_settings'
        );
        add_settings_field(
            REDLIGHT_OP_SETTING_PREFIX . 'disable_license_key',
            '',
            [$this, 'renderDisableLicenseKey'],
            'packing_station_setting',
            'redlight_op_settings'
        );

        add_settings_field(
            REDLIGHT_OP_SETTING_PREFIX . 'packed_status',
            __('Completed packing status', 'woocommerce-orderscanner-packstations'),
            [$this, 'renderCompletedStatus'],
            'packing_station_setting',
            'redlight_op_settings'
        );

        add_settings_field(
            REDLIGHT_OP_SETTING_PREFIX . 'display_order_list',
            __('Display list of orders', 'woocommerce-orderscanner-packstations'),
            [$this, 'renderDisplayOrderList'],
            'packing_station_setting',
            'redlight_op_settings'
        );

        add_settings_field(
            REDLIGHT_OP_SETTING_PREFIX . 'display_order_status',
            __('Display orders status', 'woocommerce-orderscanner-packstations'),
            [$this, 'renderDisplayOrderStatus'],
            'packing_station_setting',
            'redlight_op_settings'
        );

        add_settings_field(
            REDLIGHT_OP_SETTING_PREFIX . 'begin_pack_status',
            __('Set order status when packing begins', 'woocommerce-orderscanner-packstations'),
            [$this, 'renderBeginPackStatus'],
            'packing_station_setting',
            'redlight_op_settings'
        );

        add_settings_field(
            REDLIGHT_OP_SETTING_PREFIX . 'mark_as_status',
            __('Mark as status', 'woocommerce-orderscanner-packstations'),
            [$this, 'renderMarkAsStatus'],
            'packing_station_setting',
            'redlight_op_settings'
        );

        add_settings_field(
            REDLIGHT_OP_SETTING_PREFIX . 'high_quantity_products',
            __('High quantity product started from', 'woocommerce-orderscanner-packstations'),
            [$this, 'renderHighQuantityProduct'],
            'packing_station_setting',
            'redlight_op_settings'
        );
        add_settings_field(
            REDLIGHT_OP_SETTING_PREFIX . 'display_ean_field',
            __('Display EAN field on products', 'woocommerce-orderscanner-packstations'),
            [$this, 'renderDisplayEanField'],
            'packing_station_setting',
            'redlight_op_settings'
        );
    }

    /**
     * @param      $name
     *
     * @param bool $default
     *
     * @return mixed
     */
    public abstract function getOption($name, $default = false);

    /**
     * @param string $option
     *
     * @return string
     */
    public function getOptionName($option)
    {
        return REDLIGHT_OP_SETTING_PREFIX . $option;
    }

    public function renderHighQuantityProduct()
    {
        $option_name = $this->getOptionName('high_quantity_products');
        $option_value = $this->getOption('high_quantity_products');

        echo '<input type="number" min="1" value="'.$option_value.'" name="'.$option_name.'"/>';
    }

    public function renderBeginPackStatus()
    {
        $option_name = $this->getOptionName('begin_pack_status');
        $option_value = $this->getOption('begin_pack_status');

        echo '<select name="'.$option_name.'">';
        echo '<option value="">'.__('Do not change the status', 'woocommerce-orderscanner-packstations').'</option>';
        foreach (wc_get_order_statuses() as $key => $value){
            echo '<option value="'.$key.'" type="text" '.selected($key, $option_value).'>'.$value.'</option>';
        }
        echo '</select>';
    }

    public function renderMarkAsStatus()
    {
        $option_name = $this->getOptionName('mark_as_status');
        $option_value = $this->getOption('mark_as_status');

        echo '<select name="'.$option_name.'">';
        foreach (wc_get_order_statuses() as $key => $value){
            echo '<option value="'.$key.'" type="text" '.selected($key, $option_value).'>'.$value.'</option>';
        }
        echo '</select>';
    }

    /**
     * Render completed status field
     */
    public function renderCompletedStatus()
    {
        $option_name = $this->getOptionName('packed_status');
        $option_value = $this->getOption('packed_status');

        echo '<select name="'.$option_name.'">';
        echo '<option value="">'.__('Do not change the status', 'woocommerce-orderscanner-packstations').'</option>';
        foreach (wc_get_order_statuses() as $key => $value){
            echo '<option value="'.$key.'" type="text" '.selected($key, $option_value).'>'.$value.'</option>';
        }
        echo '</select>';
    }

    /**
     *  render display order status field
     */
    public function renderDisplayOrderStatus()
    {
        $option_name = $this->getOptionName('display_order_status');
        $option_value = $this->getOption('display_order_status');

        echo '<select name="'.$option_name.'[]" multiple>';
        foreach (wc_get_order_statuses() as $key => $value){
            echo '<option value="'.$key.'" type="text" '. selected(in_array($key, $option_value ,true)) . '>'.$value.'</option>';
        }
        echo '</select>';
    }

    public function renderDisplayOrderList()
    {
        $option_name = $this->getOptionName('display_order_list');
        $option_value = $this->getOption('display_order_list');

        $checked = $option_value ? "checked": '';
        echo '<input id='.$option_name.' type=checkbox name="'.$option_name.'" value="1" '.$checked.'>';
        echo '<label for="'.$option_name.'">'.__('Show/hide', 'woocommerce-orderscanner-packstations').'</label>';
    }

    public function renderDisplayEanField()
    {
        $option_name = $this->getOptionName('display_ean_field');
        $option_value = $this->getOption('display_ean_field');

        $checked = ($option_value) ? "checked": '';
        echo '<input id='.$option_name.' type=checkbox name="'.$option_name.'" value="1" '.$checked.'>';
        echo '<label for="'.$option_name.'">'.__('Show/hide', 'woocommerce-orderscanner-packstations').'</label>';
    }

    public function renderLicenseKey()
    {
        $option_name = $this->getOptionName('license_key');
        $option_value = $this->getOption('license_key');

        echo '<input id='.$option_name.' type=text name="'.$option_name.'" value="'.$option_value.'">';
		echo '<p class="description">' . __( 'Enter the license key that you recived when you purchased the plugin', 'woocommerce-orderscanner-packstations' ) . '</p>';
    }
    public function renderDisableLicenseKey()
    {
        $option_name = $this->getOptionName('disable_license_key');
        $option_value = $this->getOption('disable_license_key');
        $checked = ($option_value) ? "checked": '';
        echo '<input id='.$option_name.' type=checkbox name="'.$option_name.'" value="1" '.$checked.'>';
        echo '<label for="'.$option_name.'">'.__('Deactivate this license', 'woocommerce-orderscanner-packstations').'</label>';
		echo '<p class="description">' . __( 'Deactivating this license key will disable updates', 'woocommerce-orderscanner-packstations' ) . '</p>';
    }
}