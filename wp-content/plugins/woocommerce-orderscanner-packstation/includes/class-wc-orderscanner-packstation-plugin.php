<?php

require_once 'Admin/class-wc-orderscanner-packstation-admin.php';

/**
 * Class PackageStationPlugin
 *
 * @package PackageStation
 */
class WC_Orderscanner_Packstation_Plugin {

	/**
	 * PackageStationPlugin constructor.
	 *
	 */
    public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'init_updater' ) );
        add_action( 'plugins_loaded', array( $this, 'loadTextDomain' ) );
	}

	/**
	 * Initialize the plugin updater. Called very early - in the context of the plugins_loaded action
	 *
	 */
	public function init_updater(){
		add_action( 'admin_init', array( $this, 'plugin_updater' ) );
		add_action( 'admin_init', array( $this, 'activate_license' ));
		add_action( 'admin_init', array( $this, 'deactivate_license' ));
		add_action( 'admin_notices', array( $this, 'do_license_check' ) );
	}

	/**
	 * Run plugin part
	 */
	public function run() {
        if(self::isNetworkActivated() && !self::isWooCommerceNetworkActivated()){
            add_action('network_admin_notices', function(){
                echo '<div class="notice notice-error"><p>' . __('WooCommerce Orderscanner Packstation in network mode require WooCommerce active in network mode') . '</p></div>';
            });

            add_action('admin_notices', function(){
                echo '<div class="notice notice-error"><p>' . __('WooCommerce Orderscanner Packstation in network mode require WooCommerce active in network mode') . '</p></div>';
            });
        }else{
            if ( is_admin() ) {
                $WC_Orderscanner_Packstation_Admin = new WC_Orderscanner_Packstation_Admin();
            }
        }
	}

    /**
	 * Load Localisation files.
	 *
	 * Note: the first-loaded translation file overrides any following ones if the same translation is present.
	 *
	 * Locales found in:
	 *      - WP_LANG_DIR/woocommerce-orderscanner-packstations/woocommerce-orderscanner-packstations-LOCALE.mo
	 *      - WP_LANG_DIR/plugins/woocommerce-orderscanner-packstations-LOCALE.mo
	 */
    public function loadTextDomain()
    {	
		$locale = apply_filters('plugin_locale', get_locale(), 'woocommerce-orderscanner-packstations');
		
		load_textdomain('woocommerce-orderscanner-packstations', WP_LANG_DIR . '/woocommerce-orderscanner-packstations/woocommerce-orderscanner-packstations-' . $locale . '.mo');
		load_plugin_textdomain('woocommerce-orderscanner-packstations', false, plugin_basename(REDLIGHT_OP_PLUGIN_PATH) . '/languages');

    }

    public static function isNetworkActivated()
    {
        if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
        }

        $main_file_path = explode(DIRECTORY_SEPARATOR, REDLIGHT_OP_MAIN_FILE);
        $plugin         = $main_file_path[count($main_file_path)-2] . DIRECTORY_SEPARATOR .  $main_file_path[count($main_file_path) - 1];

        return is_plugin_active_for_network($plugin);
    }

    public static function isWooCommerceNetworkActivated()
    {
        if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
        }

        return is_plugin_active_for_network('woocommerce/woocommerce.php');
	}
	
	public function plugin_updater() {
		// retrieve our license key from the DB
		$license = get_option(REDLIGHT_OP_SETTING_PREFIX . 'license_key', '');

		if( !class_exists( 'License_WP_Plugin_Updater' ) ) {
			// load our custom updater
			include_once REDLIGHT_OP_PLUGIN_PATH . '/includes/License_WP_Plugin_Updater.php';
		}
		// setup the updater
		$license_wp_updater = new License_WP_Plugin_Updater( REDLIGHT_OP_STORE_URL, REDLIGHT_OP_MAIN_FILE,
			array(
				'version' => REDLIGHT_OP_VERSION,       // current version number
				'license' => $license,             		// license key (used get_option above to retrieve from DB)
				'item_slug' => REDLIGHT_OP_ITEM_NAME,      // Slug of the product
				'item_id' => REDLIGHT_OP_ITEM_ID,      // ID of the product
				'beta'    => false,
			)
		);
	}
	public function activate_license() {
		// listen for our activate button to be clicked
		if( isset( $_POST['redlight_op_setting_license_key'] ) && !isset( $_POST['redlight_op_setting_disable_license_key'] ) ) {
			// retrieve the license from the database
			//$license = get_option(REDLIGHT_OP_SETTING_PREFIX . 'license_key', '');
			$license = sanitize_text_field(($_POST['redlight_op_setting_license_key']));
			$home_url = str_replace( array( 'http://', 'https://' ), '', trim( home_url() ) );

			$api_params = array(
				'wc-api'         =>'license_wp_api_activation',
				'request'        => 'activate',
				'license_key'    => $license,
				'api_product_id' => urlencode( REDLIGHT_OP_ITEM_NAME ), // Plugin slug from licence-wp,
				'api_item_id' 	 => urlencode( REDLIGHT_OP_ITEM_ID ), // Plugin ID from licence-wp,
				'instance'       => $home_url,
			);
			$response = wp_remote_get(
				REDLIGHT_OP_STORE_URL. '?' . http_build_query( $api_params, '', '&' ),
				array( 
					'timeout' => 15,
					'headers'   => array(
						'Accept' => 'application/json'
					)
				)
			);

			// make sure the response came back okay
			if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
				if ( is_wp_error( $response ) ) {
					$message = $response->get_error_message();
				} else {
					$message = __( 'An error occurred, please try again.' );
				}
		
			} else {
				$license_value = '';
				$license_data = json_decode( wp_remote_retrieve_body( $response ) );
				if ( false === $license_data->success ) {
					$license_value = 'invalid';
				}elseif( $license_data->success ){
					$license_value = 'valid';
					delete_option( REDLIGHT_OP_SETTING_PREFIX . 'license_error' );
				}elseif( isset($license_data->error) ){
					$license_value = 'invalid';
					update_option( REDLIGHT_OP_SETTING_PREFIX . 'license_error', $license_data->error );
				}
			}
			
			// $license_value will be either "valid" or "invalid"
			update_option( REDLIGHT_OP_SETTING_PREFIX . 'license_status', $license_value );

		}
	}
	public function deactivate_license() {
		// listen for our activate button to be clicked
		if( isset( $_POST['redlight_op_setting_license_key'] ) && isset( $_POST['redlight_op_setting_disable_license_key'] ) ) {
			// retrieve the license from the database
			$license = sanitize_text_field(($_POST['redlight_op_setting_license_key']));
			//$license = get_option(REDLIGHT_OP_SETTING_PREFIX . 'license_key', '');
			
			$api_params = array(
				'wc-api'         =>'license_wp_api_activation',
				'request'        => 'deactivate',
				'license_key'    => $license,
				'api_product_id' => urlencode( REDLIGHT_OP_ITEM_NAME ), // Plugin slug from licence-wp,
				'api_item_id' 	 => urlencode( REDLIGHT_OP_ITEM_ID ), // Plugin ID from licence-wp,
				'instance'       => str_replace( array( 'http://', 'https://' ), '', trim( home_url() ) ),
			);

			$response = wp_remote_get(
				REDLIGHT_OP_STORE_URL. '?' . http_build_query( $api_params, '', '&' ),
				array( 
					'timeout' => 15,
					'headers'   => array(
						'Accept' => 'application/json'
					)
				)
			);

			// make sure the response came back okay
			if ( is_wp_error( $response ) )
				return false;

			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );

			// $license_data->success will be either true or false
			if( $license_data->success )
				delete_option( REDLIGHT_OP_SETTING_PREFIX . 'license_status' );
		}
	}
	public function do_license_check() {
        if( get_option( REDLIGHT_OP_SETTING_PREFIX . 'license_status' ) != "valid" ) {
            echo "<div class=\"error\"><p>". sprintf( __( "License key for <strong>%s</strong> is invalid. Please ensure that you have <a href=\"%s\">entered a valid license key.</a>", 'woocommerce-orderscanner-packstation' ), get_plugin_data( REDLIGHT_OP_MAIN_FILE )['Name'], admin_url( 'admin.php?page=packing_station_setting' ) ) ."</p></div>";	
        }		
        if( get_option( REDLIGHT_OP_SETTING_PREFIX . 'license_error' ) ) {
            echo "<div class=\"error\"><p>". get_option( REDLIGHT_OP_SETTING_PREFIX . 'license_error' ) ."</p></div>";	
        }		
    }

	/**
	 * Fired when the plugin is activated
	 */
	public function activate() {

	}

	/**
	 * Fired when the plugin is deactivated
	 */
	public function deactivate() {

	}

	/**
	 * Fired during plugin uninstall
	 */
	public static function uninstall() {

	}
}