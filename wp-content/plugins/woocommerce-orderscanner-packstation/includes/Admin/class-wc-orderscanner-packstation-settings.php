<?php

require_once  dirname( __FILE__ ) . '/../Abstract/abstract-class-wc-orderscanner-packstation-settings.php';

class WC_Orderscanner_Packstation_Settings extends  Abstract_WC_Orderscanner_Packstation_Settings{

    public function getOption($name, $default = false)
    {
       return get_option($this->getOptionName($name), $default);
    }
}