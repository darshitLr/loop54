<?php

class WC_Orderscanner_Packstation_Packstation {

    /**
     * @const string
     */
    const ADD_STATION_ACTION = 'op_add_new_station';

    /**
     * @const string
     */
    const SET_STATION_ACTION = 'op_set_station';

    /**
     * @const string
     */
    const DELETE_STATION_ACTION = 'op_delete_station';

    /**
     * @const string
     */
    const REPACK_ACTION = 'op_repack_order';

    /**
     * @const string
     */
    const USER_STATION_META_KEY = '_op_user_station';

    /**
     * @const string
     */
    const CHANGE_STATION_NAME_ACTION = 'op_change_station_name';

    /**
     * @var \WC_Orderscanner_Packstation_Admin
     */
    private $controller;

    /**
     * WC_Orderscanner_Packstation_Packstation constructor.
     *
     * @param \WC_Orderscanner_Packstation_Admin $controller
     */
    public function __construct(WC_Orderscanner_Packstation_Admin $controller)
    {
        $this->controller = $controller;
        $this->hooks();
    }

    /**
     * Register hooks
     */
    protected function hooks()
    {
        add_action('init', array($this, 'addPackerRole'));

        add_action('admin_post_' . self::ADD_STATION_ACTION, array($this, 'addNewStationHandler'));
        add_action('admin_post_' . self::SET_STATION_ACTION, array($this, 'setStationHandler'));
        add_action('admin_post_' . self::DELETE_STATION_ACTION, array($this, 'deleteStationHandler'));
        add_action('admin_post_' . self::CHANGE_STATION_NAME_ACTION, array($this, 'changeStationNameHandler'));
        add_action('admin_post_' . self::REPACK_ACTION, array($this, 'repackOrderHandler'));
    }

    public function changeStationNameHandler()
    {
        $data = $_REQUEST;

        if(!is_user_logged_in() || empty($data['hash']) || empty($data['name']) || !wp_verify_nonce($data['_wpnonce'], self::CHANGE_STATION_NAME_ACTION)){
            _e('You don\'t have access to this page', 'woocommerce-orderscanner-packstations' );
            wp_die();
        }

        if(!$this->getStationByHash($data['hash'])){
            _e('Station does not exist', 'woocommerce-orderscanner-packstations' );
            wp_die();
        }

        $stations = $this->getStations();

        $station  = array_filter($stations, function($item) use($data){
            if($item['hash'] == $data['hash']) return true;
        });

        $key = key($station);

        $stations[$key]['name'] = $data['name'];

        $this->updateStations($stations);

        do_action('orderscanner_packstation_station_change_name', $station, $stations, get_current_user_id());

        return wp_redirect(wp_get_referer());
    }

    public function repackOrderHandler()
    {
        $data = $_REQUEST;

        if(!is_user_logged_in() || empty($data['order_id']) || !wp_verify_nonce($data['_wpnonce'], self::REPACK_ACTION)){
            _e('You don\'t have access to this page', 'woocommerce-orderscanner-packstations' );
            wp_die();
        }

        $order = wc_get_order($data['order_id']);

        if($order){
            $this->repackOrder($order);
        }

        return wp_redirect(wp_get_referer());
    }

    /**
     * @param  \WC_Order $order
     *
     * @return array
     */
    public function repackOrder($order)
    {
        $this->controller->getScanner()->createOrderScannedStatus($order);

        $status = $this->controller->getSettings()->getOption('begin_pack_status');

        if($status !== ''){
            $this->controller->getScanner()->changeStatus($order, $status);
        }
        return  $this->controller->getScanner()->createOrderScannedStatus($order);
    }

    /**
     * @return bool
     */
    public function deleteStationHandler()
    {
        $data = $_REQUEST;
        if(!is_user_logged_in() || empty($data['hash']) || !wp_verify_nonce($data['_wpnonce'], self::DELETE_STATION_ACTION)){
            _e('You don\'t have access to this page', 'woocommerce-orderscanner-packstations' );
            wp_die();
        }

        if(!$this->getStationByHash($data['hash'])){
            _e('Station does not exist', 'woocommerce-orderscanner-packstations' );
            wp_die();
        }

        $stations = $this->getStations();
        $station  = array_filter($stations, function($item) use($data){
            if($item['hash'] == $data['hash']) return true;
        });

        unset($stations[key($station)]);

        $this->updateStations($stations);

        do_action('orderscanner_packstation_station_deleted', $station, $stations, get_current_user_id());

        return wp_redirect(wp_get_referer());
    }

    /**
     * @return bool
     */
    public function addNewStationHandler()
    {
        $data = $_REQUEST;
        $user = wp_get_current_user();

        if(! wp_verify_nonce($data['_wpnonce'], self::ADD_STATION_ACTION) || get_current_user_id() == 0){
            _e('You don\'t have access to this page', 'woocommerce-orderscanner-packstations' );
            wp_die();
        }

        $stations   = $this->getStations();

        $station    = [
            'name'         => $data['station_name'],
            'user_created' => $user->ID,
            'date_created' => time(),
            'hash'         => sha1(time(). $data['station_name']),
        ];

        $stations[] = $station;

        $this->updateStations($stations);

        do_action('orderscanner_packstation_station_created', $station, $stations, get_current_user_id());

        return wp_redirect($data['_wp_http_referer']);
    }

    /**
     * @return bool
     */
    public function setStationHandler()
    {
        $data = $_REQUEST;

        if(!is_user_logged_in() || empty($data['hash']) || !wp_verify_nonce($data['_wpnonce'], self::SET_STATION_ACTION)){
            _e('You don\'t have access to this page', 'woocommerce-orderscanner-packstations' );
            wp_die();
        }

        update_user_meta(get_current_user_id(), '_op_user_station', $data['hash']);

        return wp_redirect(wp_get_referer());
    }

    /**
     * Add new role packer
     */
    public function addPackerRole()
    {
        add_role(
            'packer',
            __('Packer!', 'package-station'),
            array(
                'read'           => true,
                'pack_orders'    => true,
            )
        );

        add_role(
            'packer_network',
            __('Packer Network', 'package-station'),
            array(
                'read'           => true,
                'pack_orders'    => true,
                'manage_network' => true,
            )
        );

        global $wp_roles;

        $wp_roles->add_cap( 'administrator', 'pack_orders' );

        if(current_user_can('pack_orders')){
            add_filter('woocommerce_prevent_admin_access', '__return_false');
        }

        add_filter('editable_roles', function($roles){

            $roles['packer'] = [
                'name' =>  __('Packer', 'package-station'),
                'capabilities' => array(
                    'read'           => true,
                    'pack_orders'    => true,
                )
            ];

            $roles['packer_network'] = [
                'name' =>  __('Packer Network', 'package-station'),
                'capabilities' => array(
                    'read'           => true,
                    'pack_orders'    => true,
                    'manage_network' => true,
                )
            ];

            return $roles;
        });
    }

    /**
     * @param string $hash
     *
     * @return bool
     */
    public function getStationByHash($hash = '')
    {
        $stations = $this->getStations();

        $station = array_filter($stations, function ($_station) use($hash){
            return $_station['hash'] === $hash;
        });

        if(!empty($station) && is_array($station)){
            return array_values($station)[0];
        }

        return false;
    }

    public function getStations()
    {
        if(WC_Orderscanner_Packstation_Plugin::isNetworkActivated()){
           return  get_site_option(REDLIGHT_OP_STATION_OPTION_KEY, []);
        }else{
           return  get_option(REDLIGHT_OP_STATION_OPTION_KEY, []);
        }
    }

    public function updateStations($stations)
    {
        if(WC_Orderscanner_Packstation_Plugin::isNetworkActivated()){
            return  update_site_option(REDLIGHT_OP_STATION_OPTION_KEY, $stations);
        }else{
            return  update_option(REDLIGHT_OP_STATION_OPTION_KEY, $stations);
        }
    }

    /**
     * Check if user have deleted station.
     */
    public function checkUser()
    {
        if(is_user_logged_in()){
            $hash = get_user_meta(get_current_user_id(), '_op_user_station', true);

            if(!empty($hash) && !$this->getStationByHash($hash)){
                update_user_meta(get_current_user_id(), '_op_user_station', false);
            }
        }
    }
}