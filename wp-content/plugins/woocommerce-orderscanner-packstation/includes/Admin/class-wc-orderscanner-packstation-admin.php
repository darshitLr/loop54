<?php

/**
 * Class Admin
 */
class WC_Orderscanner_Packstation_Admin {

    /**
     * @const string
     */
    const MARK_AS_STATUS_ACTION = 'op_mark_as_status';

    /**
     * @var string
     */
    private $viewPath;

    /**
     * @var string
     */
    private $assetsUrl;

    /**
     * Wordpress plugin screen
     */
    const SCREENS = [
        'toplevel_page_packing_station',
        'packing-station_page_packing_station_manage_station',
        'packing-station_page_packing_station_setting',
    ];


    /**
     * @const string
     */
    const ORDER_PACKED_STATUS_META_KEY = '_op_order_packed_status';

    /**
     * @const string
     */
    const ORDER_BARCODE_META_KEY = '_barcode_text';

    /**
     * @var \WC_Orderscanner_Packstation_Packstation
     */
    private $packing_station;

    /**
     * @var \WC_Orderscanner_Packstation_Settings
     */
    private $settings;

    /**
     * @var \WC_Orderscanner_Packstation_Scanner
     */
    private $scanner;

    /**
	 * Admin constructor.
	 */
	public function __construct()
    {
	    $this->viewPath   = REDLIGHT_OP_PLUGIN_PATH . '/views/admin/';
	    $this->assetsUrl  = REDLIGHT_OP_PLUGIN_URL . '/assets/admin/';

	    if(WC_Orderscanner_Packstation_Plugin::isNetworkActivated()){
            require_once 'class-wc-orderscanner-packstation-settings-network.php';
            $this->settings = new WC_Orderscanner_Packstation_Settings_Network();
        } else {
            require_once 'class-wc-orderscanner-packstation-settings.php';
            $this->settings = new WC_Orderscanner_Packstation_Settings();
        }

        require_once 'class-wc-orderscanner-packstation-packing-station.php';
	    require_once 'class-wc-orderscanner-packstation-scanner.php';

	    $this->packing_station = new WC_Orderscanner_Packstation_Packstation($this);
	    $this->scanner         = new WC_Orderscanner_Packstation_Scanner($this);

        $this->hooks();

        add_action('admin_init', function(){
        });
	}

    /**
     * @return \WC_Orderscanner_Packstation_Scanner
     */
	public function getScanner()
    {
        return $this->scanner;
    }

    /**
     * @return \WC_Orderscanner_Packstation_Settings
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Hooks
     */
	public function hooks()
    {
        if(WC_Orderscanner_Packstation_Plugin::isNetworkActivated()){
            add_action('network_admin_menu', array($this, 'addMenuPage'));
        }

        add_action('admin_menu', array($this, 'addMenuPage'));

        add_action('admin_enqueue_scripts', array($this, 'enqueueAssets'), 11);

        add_action('init', array($this, 'registerPackedOrderStatus') );
        add_filter('wc_order_statuses', array($this, 'addPackedToOrderStatuses') );

        add_filter('woocommerce_order_data_store_cpt_get_orders_query', array($this, 'handleBarcodeVar'), 10, 2 );

        add_action('admin_post_' . self::MARK_AS_STATUS_ACTION, array($this, 'markAsStatus'));

        add_action( 'woocommerce_admin_order_data_after_shipping_address', array( $this, 'admin_order_meta' ), 10, 1 );

        add_action( 'woocommerce_product_options_inventory_product_data', array( $this, 'product_packstation_comment_field' ) );
        add_action( 'woocommerce_process_product_meta', array( $this, 'save_product_packstation_comment_field' ) );

        add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'variation_product_packstation_comment_field' ), 10, 3 );
        add_action( 'woocommerce_save_product_variation', array( $this, 'save_variation_product_packstation_comment_field' ), 10, 2 );

        if( $this->settings->getOption( 'display_ean_field' ) ){
            add_action( 'woocommerce_product_options_sku', array( $this, 'product_ean_field' ) );
            add_action( 'woocommerce_process_product_meta', array( $this, 'save_product_ean_field' ) );

            add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'variation_product_ean_field' ), 10, 3 );
            add_action( 'woocommerce_save_product_variation', array( $this, 'save_variation_product_ean_field' ), 10, 2 );
        }
        
    }

    public function markAsStatus()
    {
        $data = $_REQUEST;

        if(isset($_GET['blog_id']) && WC_Orderscanner_Packstation_Plugin::isNetworkActivated()){
            switch_to_blog(intval($_GET['blog_id']));
            $needRestore = true;
        }

        if(!empty($data['status']) && !empty($data['order_id']) && wp_verify_nonce($data['_wpnonce'], self::MARK_AS_STATUS_ACTION)){
            $order = new WC_Order($data['order_id']);
            if($order){
               $order->update_status($data['status']);
            }
        }

        if(isset($needRestore)){
            restore_current_blog();
        }

        return wp_redirect(wp_get_referer());
    }

    public function handleBarcodeVar($query, $query_vars)
    {
        if ( ! empty( $query_vars[self::ORDER_BARCODE_META_KEY] ) ) {
            $query['meta_query'][] = array(
                'key'   => self::ORDER_BARCODE_META_KEY,
                'value' => esc_attr( $query_vars[self::ORDER_BARCODE_META_KEY] ),
            );
        }
        return $query;
    }

    public function registerPackedOrderStatus()
    {
        register_post_status( 'wc-packed', array(
            'label'                     => __('Packed', 'woocommerce-orderscanner-packstations'),
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop( __('Packed', 'woocommerce-orderscanner-packstations') .  '<span class="count">(%s)</span>', __('Packed', 'woocommerce-orderscanner-packstations') .  '<span class="count">(%s)</span>' )
        ) );
    }

    public function addPackedToOrderStatuses( $order_statuses )
    {
        $order_statuses['wc-packed'] = __('Packed', 'woocommerce-orderscanner-packstations');
        return $order_statuses;
    }

    /**
     * Enqueue plugin assets
     * @param string $screen
     */
    public function enqueueAssets($screen)
    {
        if (in_array($screen, self::SCREENS) || preg_match('/(_page_packing_station_)/i', $screen)) {

            wp_enqueue_style('op-admin-main-css', $this->assetsUrl . 'main.css');
            wp_enqueue_style('woocommerce_admin_styles');
            wp_register_style(
                'op-admin-main-css',
                $this->assetsUrl . 'main.css'
            );

            wp_register_script( 
                'op-admin-main-js', 
                $this->assetsUrl . 'main.js',
                array( 'jquery' ),
                WC()->version
            );

            $orderscanner_packstation_localize_params = array(
                'packing_complete_url' 			=> $this->getAdminUrl('admin.php?page=packing_station'),
                'cannot_find_code' 				=> __('Cannot find code', 'woocommerce-orderscanner-packstations'),
                'confirm_high_quantity_message' => __('Do you want to pack all items?', 'woocommerce-orderscanner-packstations'),
                'found_several_orders_message'  => __('Found several orders', 'woocommerce-orderscanner-packstations'),
            );

            wp_localize_script('op-admin-main-js', 'redlight_op', $orderscanner_packstation_localize_params);
            
            wp_enqueue_style( 'op-admin-main-css' );
            wp_enqueue_script( 'op-admin-main-js' );
            
            wp_enqueue_script('op-admin-scanner-js', $this->assetsUrl . 'BarcodeScanner.js', ['jquery']);

            wp_enqueue_style( 'magnific-popup', $this->assetsUrl . 'magnific-popup.css' );
            wp_enqueue_script('magnific-popup',$this->assetsUrl . 'jquery.magnific-popup.min.js', ['jquery'], '1.0', true );
            wp_enqueue_script('jquery-mobile-events',$this->assetsUrl . 'jquery.mobile.events.js', ['jquery'], '1.4.5', false );
        }
    }

    /**
     * Add menu item to WordPress
     */
    public function addMenuPage()
    {
        add_menu_page(
            __('Packing Station', 'woocommerce-orderscanner-packstations'),
            __('Packing Station', 'woocommerce-orderscanner-packstations'),
            'pack_orders',
            'packing_station',
            [$this, 'renderOrders']
        );

        add_submenu_page(
            'packing_station',
            __('Orders', 'woocommerce-orderscanner-packstations'),
            __('Orders', 'woocommerce-orderscanner-packstations'),
            'pack_orders',
            'packing_station',
            [$this, 'renderOrders']
        );

        if(WC_Orderscanner_Packstation_Plugin::isNetworkActivated()){
            if(is_network_admin()){
                add_submenu_page(
                    'packing_station',
                    __('Manage station', 'woocommerce-orderscanner-packstations'),
                    __('Manage station', 'woocommerce-orderscanner-packstations'),
                    'manage_options',
                    'packing_station_manage_station',
                    [$this, 'renderManageStations']
                );
                add_submenu_page(
                    'packing_station',
                    __('Settings', 'woocommerce-orderscanner-packstations'),
                    __('Settings', 'woocommerce-orderscanner-packstations'),
                    'manage_options',
                    'packing_station_setting',
                    [$this, 'renderSettings']
                );
            }
        }else{
            add_submenu_page(
                'packing_station',
                __('Manage station', 'woocommerce-orderscanner-packstations'),
                __('Manage station', 'woocommerce-orderscanner-packstations'),
                'manage_options',
                'packing_station_manage_station',
                [$this, 'renderManageStations']
            );
            add_submenu_page(
                'packing_station',
                __('Settings', 'woocommerce-orderscanner-packstations'),
                __('Settings', 'woocommerce-orderscanner-packstations'),
                'manage_options',
                'packing_station_setting',
                [$this, 'renderSettings']
            );
        }
    }

    public function renderOrders()
    {
        if(isset($_GET['order_id'])){

            $status               = $this->settings->getOption('begin_pack_status', '');
            $mark_as_status       = $this->settings->getOption('mark_as_status', 'wc-completed');
            $current_user_station = $this->packing_station->getStationByHash(get_user_meta(get_current_user_id(), WC_Orderscanner_Packstation_Packstation::USER_STATION_META_KEY, true));
            $blog_id_url_part     = '';
            if(isset($_GET['blog_id']) && WC_Orderscanner_Packstation_Plugin::isNetworkActivated()){
                switch_to_blog(intval($_GET['blog_id']));
                $blog_id_url_part = '&blog_id=' . $_GET['blog_id'];
            }

            $order = wc_get_order($_GET['order_id']);
            do_action('orderscanner_packstation_pack_started', $order, get_current_user_id() );
            if(isset($_GET['override'])){
                $this->packing_station->repackOrder($order);

                $url = $this->getAdminUrl($_SERVER['REQUEST_URI']);

                $parsed = parse_url($url);
                $query  = $parsed['query'];

                parse_str($query, $params);

                unset($params['override']);

                 wp_safe_redirect($this->getAdminUrl() .'?'. http_build_query($params));
            }

            $packed_status        = $order->get_meta(WC_Orderscanner_Packstation_Scanner::ORDER_PACKED_STATUS_META_KEY);

            if(empty($packed_status)){
                if($status !== ''){
                    $this->scanner->changeStatus($order, $status);
                }

                $packed_status = $this->scanner->createOrderScannedStatus($order);
            }

            $this->view('html-order.php', [
                'controller'                 => $this,
                'blog_id'                    => isset($_GET['blog_id']) ? $_GET['blog_id'] : 0,
                'order'                      => $order,
                'orderDetails'               => $this->getOrderDetails($order),
                'user'                       => wp_get_current_user(),
                'current_user_station'       => $current_user_station,
                'packed_status'              => $packed_status,
                'is_packed'                  => !is_null($packed_status['end_pack']),
                'mark_as'                    => $mark_as_status,
                'mark_as_link'               => wp_nonce_url($this->getAdminUrl('admin-post.php?order_id='.$order->get_id().'&status='.$mark_as_status.'&action=' . self::MARK_AS_STATUS_ACTION . $blog_id_url_part, true), self::MARK_AS_STATUS_ACTION),
                'mark_all_packed_link'       => wp_nonce_url($this->getAdminUrl('admin-post.php?order_id='.$order->get_id().'&action=' . WC_Orderscanner_Packstation_Scanner::MARK_ALL_AS_PACKED_ACTION . $blog_id_url_part, true), WC_Orderscanner_Packstation_Scanner::MARK_ALL_AS_PACKED_ACTION),
                'repack_link'                => wp_nonce_url($this->getAdminUrl('admin-post.php?order_id='.$order->get_id().'&action=' . WC_Orderscanner_Packstation_Packstation::REPACK_ACTION . $blog_id_url_part,true), WC_Orderscanner_Packstation_Packstation::REPACK_ACTION),
                'high_quantity_product'      => $this->settings->getOption('high_quantity_products', 10),
                'scan_product_action'        => WC_Orderscanner_Packstation_Scanner::SCAN_PRODUCT_CODE_ACTION,
                'reset_scan_product_action'  => WC_Orderscanner_Packstation_Scanner::RESET_SCAN_PRODUCT_CODE_ACTION
            ]);

            if(isset($_GET['blog_id']) && WC_Orderscanner_Packstation_Plugin::isNetworkActivated()){
                restore_current_blog();
            }
        } else {
            require_once 'class-wc-orderscanner-packstation-orders-table.php';

            $this->packing_station->checkUser();
            $current_user_station = $this->packing_station->getStationByHash(get_user_meta(get_current_user_id(), WC_Orderscanner_Packstation_Packstation::USER_STATION_META_KEY, true));

            $this->view('orders-list.php', [
                'table'                 => new WC_Orderscanner_Packstation_Table($this),
                'order_search_action'   => WC_Orderscanner_Packstation_Scanner::SEARCH_ORDER_BY_CODE_ACTION,
                'stations'              => $this->packing_station->getStations(),
                'current_user_station'  => $current_user_station['hash'],
                'set_station_action'    => WC_Orderscanner_Packstation_Packstation::SET_STATION_ACTION,
                'display_order_list'    => $this->settings->getOption( 'display_order_list',false),
            ]);
        }
    }

    public function renderManageStations()
    {
        $this->view('stations-list.php',[
            'stations'                   => $this->packing_station->getStations(),
            'add_station_action'         => WC_Orderscanner_Packstation_Packstation::ADD_STATION_ACTION,
            'delete_station_action'      => WC_Orderscanner_Packstation_Packstation::DELETE_STATION_ACTION,
            'change_station_name_action' => WC_Orderscanner_Packstation_Packstation::CHANGE_STATION_NAME_ACTION,
        ]);
    }

    public function renderSettings()
    {
        $this->view('settings.php', [
            'is_network' => WC_Orderscanner_Packstation_Plugin::isNetworkActivated(),
        ]);
    }

    /**
     * @param \WC_Order $order
     * @return string
     */
    public function getOrderDetails($order)
    {
        $meta_list = array();

        if ( WC()->payment_gateways() ) {
            $payment_gateways = WC()->payment_gateways->payment_gateways();
        } else {
            $payment_gateways = array();
        }

        $payment_method = $order->get_payment_method();

        if ( $payment_method ) {

            $payment_method_string = sprintf(
                __( 'Payment via %s', 'woocommerce' ),
                esc_html( isset( $payment_gateways[ $payment_method ] ) ? $payment_gateways[ $payment_method ]->get_title() : $payment_method )
            );


            if ( $transaction_id = $order->get_transaction_id() ) {
                if ( isset( $payment_gateways[ $payment_method ] ) && ( $url = $payment_gateways[ $payment_method ]->get_transaction_url( $order ) ) ) {
                    $payment_method_string .= ' (<a href="' . esc_url( $url ) . '" target="_blank">' . esc_html( $transaction_id ) . '</a>)';
                } else {
                    $payment_method_string .= ' (' . esc_html( $transaction_id ) . ')';
                }
            }

            $meta_list[] = $payment_method_string;
        }

        if ( $order->get_date_paid() ) {

            $meta_list[] = sprintf(
                __( 'Paid on %1$s @ %2$s', 'woocommerce' ),
                wc_format_datetime( $order->get_date_paid() ),
                wc_format_datetime( $order->get_date_paid(), get_option( 'time_format' ) )
            );
        }

        if ( $ip_address = $order->get_customer_ip_address() ) {

            $meta_list[] = sprintf(
                __( 'Customer IP: %s', 'woocommerce' ),
                '<span class="woocommerce-Order-customerIP">' . esc_html( $ip_address ) . '</span>'
            );
        }

        return wp_kses_post( implode( '. ', $meta_list ) );
    }

    /**
     * @param \WC_Order $order
     */
    public function admin_order_meta( $order ){
        $packed_status = $order->get_meta( WC_Orderscanner_Packstation_Scanner::ORDER_PACKED_STATUS_META_KEY, true );

        if(!empty($packed_status)){
            echo "<h3>". __('Packing details', 'woocommerce-orderscanner-packstations') . "</h3>";
            echo "<p>";
            if(!empty($packed_status['begin_pack'])){
                $packing_began = new DateTime();
                $packing_began->setTimestamp($packed_status['begin_pack']);
                echo  "<strong>". __('Packing started', 'woocommerce-orderscanner-packstations') . ":</strong> ".$packing_began->format('Y-m-d H:i:s')."</br>";
            }
            if(!empty($packed_status['end_pack'])){
                $packing_ended = new DateTime();
                $packing_ended->setTimestamp($packed_status['end_pack']);
                echo  "<strong>". __('Packing ended', 'woocommerce-orderscanner-packstations') . ":</strong> ".$packing_ended->format('Y-m-d H:i:s')."</br>";
            }
            if(!empty($packed_status['packer'])){
                $packer = new WP_User($packed_status['packer']);
                echo  "<strong>". __('Packed by', 'woocommerce-orderscanner-packstations') . ":</strong> ".$packer->first_name . ' ' . $packer->last_name."</br>";
            }
            if(!empty($packed_status['station'])){
                echo  "<strong>". __('Packed at', 'woocommerce-orderscanner-packstations') . ":</strong> ".$this->packing_station->getStationByHash($packed_status['station'])['name'];
            }
            echo "</p>";
        }
    }

    /**
     * @global \WP_Post $post
     */
    public function product_packstation_comment_field() {
        
        global $post;

        woocommerce_wp_textarea_input( 
            array(    
                'id'          => '_packstation_product_comment',
                'label'       => __('Packstation comment', 'woocommerce-orderscanner-packstations'),
                'desc_tip'    => 'true',
                'description' => __('If you enter a comment here, it will be visible at the packstation', 'woocommerce-orderscanner-packstations'),
                'value'       => get_post_meta( $post->ID, '_packstation_product_comment', true ),
            )
        );

    }
    
    /**
     * Save Packstation Comment
     *
     * @param  $post_id
     * @return      void
     */
    public function save_product_packstation_comment_field( $post_id ) {

        $packstation_product_comment = $_POST['_packstation_product_comment'];

        if( isset( $packstation_product_comment ) ) {
            update_post_meta( $post_id, '_packstation_product_comment', wp_kses_post(( $packstation_product_comment ) ));
        }

        $packstation_product_comment = get_post_meta( $post_id, '_packstation_product_comment', true );

        if( empty( $packstation_product_comment ) ) {
            delete_post_meta( $post_id, '_packstation_product_comment', '' );
        }

    }

    /**
     * Add Packstation Comment for variations
     *
     * @param int $loop
     * @param $variation_data
     * @param \WC_Product_Variation $variation
     *
     * @return      void
     */
    public function variation_product_packstation_comment_field( $loop, $variation_data, $variation ) {

        woocommerce_wp_textarea_input( 
            array(    
                'id' => '_packstation_variation_comment[' . $variation->ID . ']',
                'label' => __('Packstation comment', 'woocommerce-orderscanner-packstations'),
                'desc_tip' => 'true',
                'description' => __('If you enter a comment here, it will be visible at the packstation', 'woocommerce-orderscanner-packstations'),
                'value'       => get_post_meta( $variation->ID, '_packstation_variation_comment', true ),
            )
        );

    }

    /**
     * Save Packstation Comment for variations
     *
     * @param int $post_id
     *
     * @return      void
     */
    public function save_variation_product_packstation_comment_field( $post_id ) {
        
        $data = $_POST['_packstation_variation_comment'][ $post_id ];

        if( isset( $data ) ) {
            update_post_meta( $post_id, '_packstation_variation_comment', esc_attr( $data ) );
        }

        $packstation_comment = get_post_meta( $post_id,'_packstation_variation_comment', true );

        if ( empty( $packstation_comment ) ) {
            delete_post_meta( $post_id, '_packstation_variation_comment', '' );
        }

    }

    /**
     * @global \WP_Post $post
     */
    public function product_ean_field() {
        
        global $post;

        woocommerce_wp_text_input( 
            array(    
                'id'          => '_packstation_ean',
                'label'       => __('EAN', 'woocommerce-orderscanner-packstations'),
                'desc_tip'    => 'true',
                'description' => __('If you enter a EAN/Barcode here it will be used to identify the product at the packstation', 'woocommerce-orderscanner-packstations'),
                'value'       => get_post_meta( $post->ID, '_packstation_ean', true ),
            )
        );

    }
    
    /**
     * Save Packstation EAN
     *
     * @param  $post_id
     * @return      void
     */
    public function save_product_ean_field( $post_id ) {

        $packstation_ean = $_POST['_packstation_ean'];

        if( isset( $packstation_ean ) ) {
            update_post_meta( $post_id, '_packstation_ean', wp_kses_post(( $packstation_ean ) ));
        }

        $packstation_ean = get_post_meta( $post_id, '_packstation_ean', true );

        if( empty( $packstation_ean ) ) {
            delete_post_meta( $post_id, '_packstation_ean', '' );
        }

    }

    /**
     * Add Packstation EAN for variations
     *
     * @param int $loop
     * @param $variation_data
     * @param \WC_Product_Variation $variation
     *
     * @return      void
     */
    public function variation_product_ean_field( $loop, $variation_data, $variation ) {

        woocommerce_wp_text_input( 
            array(    
                'id' => '_packstation_ean[' . $variation->ID . ']',
                'label' => __('EAN', 'woocommerce-orderscanner-packstations'),
                'desc_tip' => 'true',
                'description' => __('If you enter a EAN/Barcode here it will be used to identify the product at the packstation', 'woocommerce-orderscanner-packstations'),
                'value'       => get_post_meta( $variation->ID, '_packstation_ean', true ),
            )
        );

    }

    /**
     * Save Packstation EAN for variations
     *
     * @param int $post_id
     *
     * @return      void
     */
    public function save_variation_product_ean_field( $post_id ) {
        
        $data = $_POST['_packstation_ean'][ $post_id ];

        if( isset( $data ) ) {
            update_post_meta( $post_id, '_packstation_ean', esc_attr( $data ) );
        }

        $packstation_ean = get_post_meta( $post_id,'_packstation_ean', true );

        if ( empty( $packstation_ean ) ) {
            delete_post_meta( $post_id, '_packstation_ean', '' );
        }

    }

    /**
     * Render admin views
     *
     * @param string $__name
     * @param array $__data
     */
    public function view($__name, $__data = [])
    {
        extract($__data);

        include $this->viewPath . $__name;
    }

    public function getAdminUrl($params = '', $force_simple_admin = false)
    {
        if($force_simple_admin){
            return admin_url($params);
        }
        if(is_network_admin() && WC_Orderscanner_Packstation_Plugin::isNetworkActivated()){
            return network_admin_url($params);
        }

        return admin_url($params);
    }
}