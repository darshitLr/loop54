<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
    require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

class WC_Orderscanner_Packstation_Table extends WP_List_Table {

    /**
     * @var \WC_Orderscanner_Packstation_Admin
     */
    private $controller;

    /**
     * WC_Orderscanner_Packstation_Table constructor.
     *
     * @param \WC_Orderscanner_Packstation_Admin $controller
     */
    public function __construct(WC_Orderscanner_Packstation_Admin $controller)
    {
        parent::__construct( [
            'singular' => __('Orders', 'woocommerce-orderscanner-packstations'),
            'plural'   => __('Orders', 'woocommerce-orderscanner-packstations'),
            'ajax'     => false,
        ] );

        $this->_column_headers = [
            $this->get_columns(),
            [],
            $this->get_sortable_columns(),
        ];

        $this->controller = $controller;
    }

    /**
     * Render data order title
     *
     * @param \WC_Order $order
     * @return string
     */
    protected function column_title( $order )
    {
        if($this->isNetworkAdmin()){
            $blog_id = $order['blog_id'];
            $order   = $order['order'];
            $admin_url = network_admin_url('admin.php?page=packing_station&order_id=' . $order->get_id()) . "&blog_id=" . $blog_id;
        }else{
            $admin_url = admin_url('admin.php?page=packing_station&order_id=' . $order->get_id());
        }

        $is_packed = $order->get_meta(WC_Orderscanner_Packstation_Scanner::ORDER_PACKED_STATUS_META_KEY);

        if($is_packed && is_array($is_packed) && !is_null($is_packed['end_pack'])){
            $is_packed = true;
            $confirm_message = $this->controller->getScanner()->getPackedConfirmMessage($order);
        }else{
            $is_packed = false;
            $confirm_message = '';
        }

        return '<a 
        href="'. $admin_url . '"
        data-order-packed="'. $is_packed  .'"
        data-order-id='. $order->get_id() .'
        data-packed-confirm-message="'. $confirm_message . '"
        data-order-key='. $order->get_order_key() . ' 
        data-order-number='. $order->get_order_number() . ' 
        ><b>' . '#' .  $order->get_order_number() . '</b></a>';
    }

     /**
     * Render order status
     *
     * @param \WC_Order $order
     * @return string
     */
    protected function column_status( $order )
    {
        if($this->isNetworkAdmin()){
            $order   = $order['order'];
        }

        return sprintf(
            '<mark class="order-status %s"><span>%s</span></mark>',
            esc_attr( sanitize_html_class( 'status-' . $order->get_status() ) ),
            esc_html( wc_get_order_status_name( $order->get_status() ) )
        );
    }

    /**
     * Render image column
     *
     * @param \WC_Order $order
     * @return string
     */
    protected function column_date( $order )
    {
        if($this->isNetworkAdmin()){
            $order   = $order['order'];
        }

       return sprintf(
           _x( '%s ago', '%s = human-readable time difference', 'woocommerce' ),
           human_time_diff( $order->get_date_created()->getTimestamp(), current_time( 'timestamp', true ) )
       );
    }

    /**
     * Render total field
     *
     * @param \WC_Order $order
     * @return string
     */
    protected function column_total( $order )
    {
        if($this->isNetworkAdmin()){
            $order   = $order['order'];
        }

        return wc_price($order->get_total());
    }

    /**
     * Render total field
     *
     * @param \WC_Order $order
     * @return string
     */
    protected function column_order_items( $order )
    {
        if($this->isNetworkAdmin()){
            switch_to_blog($order['blog_id']);

            $items_count = 0;

            if($order['order'] instanceof WC_Order) {
                $items_count = count($order['order']->get_items());
            }

            restore_current_blog();
        }else{
            $items_count = count($order->get_items());
        }

        return $items_count;
    }

    /**
     * Render date_created field
     *
     * @param object $order
     * @return string
     */
    protected function column_date_created( $order )
    {
        return date_i18n( get_option( 'date_format' ), strtotime( $order->date_created ) );
    }

    /**
     * Return array with columns titles
     *
     * @return array
     */
    public function get_columns()
    {
        $columns =  [
            'title'       => __( 'Order', 'woocommerce-orderscanner-packstations' ),
            'order_items' => __( 'Order items', 'woocommerce-orderscanner-packstations' ),
            'status'      => __( 'Status', 'woocommerce-orderscanner-packstations' ),
            'date'        => __( 'Date', 'woocommerce-orderscanner-packstations' ),
            'total'       => __('Total', 'woocommerce-orderscanner-packstations' ),
        ];

        if($this->isNetworkAdmin()){
            $columns['site'] = __('Site', 'woocommerce-orderscanner-packstations');
        }

        return $columns;
    }

    protected function column_site($order)
    {
        if(!empty($order['blog_id'])){
            return get_site($order['blog_id'])->blogname;
        }
        return '';
    }

    /**
     * Set items data in table
     */
    public function prepare_items()
    {
        $perPage = 50;
        $paged   = isset($_GET['paged']) ? $_GET['paged'] : 1;
        $display_statuses = $this->controller->getSettings()->getOption('display_order_status', ['processing']);

        if($this->isNetworkAdmin()){

            $perPage   = 20;
            $all_sites = $this->controller->getSettings()->getOption('network_display_sites', []);

            $query = [
                'total'  => 0,
                'orders' => [],
            ];

            foreach ($all_sites as $site_id) {
                switch_to_blog($site_id);

                $_query = wc_get_orders([
                    'status'   => implode($display_statuses, ','),
                    'limit'    => $perPage,
                    'orderby'  => 'date',
                    'order'    => 'ASC',
                    'paged'    => $paged,
                    'paginate' => true,
                ]);

                $query['total']  += $_query->total;

                $orders = array_map(function($item) use($site_id) {
                    return [
                        'blog_id' => $site_id,
                        'order'   => $item,
                    ];
                }, $_query->orders);

                $query['orders'] = array_merge($query['orders'], $orders);

                restore_current_blog();
            }

            $this->set_pagination_args( [
                'total_items' => $query['total'],
                'per_page'    => $perPage,

            ] );

            $this->items = $query['orders'];
        }else{
            $query = wc_get_orders([
                'status'   => implode($display_statuses, ','),
                'limit'    => $perPage,
                'orderby'  => 'date',
                'order'    => 'ASC',
                'paged'    => $paged,
                'paginate' => true,
            ]);

            $this->set_pagination_args( [
                'total_items' => $query->total,
                'per_page'    => $perPage,

            ] );

            $this->items = $query->orders;
        }
    }

    protected function isNetworkAdmin(){
        return WC_Orderscanner_Packstation_Plugin::isNetworkActivated() && is_network_admin();
    }
    /**
     * Render if no items
     */
    public function no_items()
    {
        _e('No orders was found, did you pack them all? Great job!', 'woocommerce-orderscanner-packstations' );
    }

}
