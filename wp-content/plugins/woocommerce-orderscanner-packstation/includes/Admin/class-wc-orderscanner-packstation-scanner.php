<?php

/**
 * Class Scanner
 */
class WC_Orderscanner_Packstation_Scanner
{
    /**
     * @const string
     */
    const ORDER_PACKED_STATUS_META_KEY = '_op_order_packed_status';

    /**
     * @const string
     */
    const SEARCH_ORDER_BY_CODE_ACTION = 'op_search_order_by_code';

    /**
     * @const string
     */
    const SCAN_PRODUCT_CODE_ACTION = 'op_scan_product_code';

    /**
     * @const string
     */
    const RESET_SCAN_PRODUCT_CODE_ACTION = 'op_reset_scan_product_code';

    /**
     * @const string
     */
    const MARK_ALL_AS_PACKED_ACTION = 'op_mark_all_packed';

    /**
     * @const string
     */
    const SET_PARCELS_ACTION = 'op_order_parcels';

    /**
     * @var WC_Orderscanner_Packstation_Admin
     */
    private $controller;

    /**
     * WC_Orderscanner_Packstation_Scanner constructor.
     *
     * @param \WC_Orderscanner_Packstation_Admin $controller
     */
    public function __construct(WC_Orderscanner_Packstation_Admin $controller)
    {
        $this->controller = $controller;
        $this->hooks();
    }

    /**
     * Hooks
     */
    public function hooks()
    {
        add_action('wp_ajax_' . self::SEARCH_ORDER_BY_CODE_ACTION, array($this, 'searchOrderByCode'));
        add_action('wp_ajax_' . self::SCAN_PRODUCT_CODE_ACTION, array($this, 'scanProductCode'));
        add_action('wp_ajax_' . self::RESET_SCAN_PRODUCT_CODE_ACTION, array($this, 'resetScanProduct'));
        add_action('wp_ajax_' . self::SET_PARCELS_ACTION, array($this, 'setOrderParcels'));

        add_action('admin_post_' . self::MARK_ALL_AS_PACKED_ACTION, array($this, 'markAllPacked'));
    }

    public function markAllPacked()
    {
        $data = $_REQUEST;

        if(isset($data['blog_id'])){
            switch_to_blog($data['blog_id']);
        }

        if(!is_user_logged_in() || empty($data['order_id']) ||  !wp_verify_nonce($data['_wpnonce'], self::MARK_ALL_AS_PACKED_ACTION)){
            _e('You don\'t have access to this action', 'woocommerce-orderscanner-packstations' );
            wp_die();
        }
        $order = wc_get_order($data['order_id']);

        if(!$order){
            _e('Order does not exist', 'woocommerce-orderscanner-packstations' );
            wp_die();
        }

        $packed_status = $order->get_meta(self::ORDER_PACKED_STATUS_META_KEY);

        if(!$packed_status){
            $packed_status = $this->createOrderScannedStatus($order);
            $needRestore   = true;
        }

        $packed_status['products'] = array_map(function($item){
                $item['scanned'] = $item['quantity'];
                return $item;
        }, $packed_status['products']);

        $packed_status['end_pack'] = time();

        update_post_meta($order->get_id(), self::ORDER_PACKED_STATUS_META_KEY, $packed_status);

        do_action('orderscanner_packstation_marked_all_scanned', $order, get_current_user_id());

        if(isset($needRestore)){
            restore_current_blog();
        }

        return wp_redirect($this->controller->getAdminUrl('admin.php?page=packing_station'));
    }


    /**
     * Handler when user search order by code
     */
    public function searchOrderByCode()
    {
        $data = $_REQUEST;

        if (isset($data['code'])) {

            if(isset($data['blog_id'])){
                switch_to_blog($data['blog_id']);
            }

            if(WC_Orderscanner_Packstation_Plugin::isNetworkActivated() && !isset($data['blog_id'])){
                $orders = [];

                foreach ($this->controller->getSettings()->getOption('network_display_sites') as $blog_id){
                    switch_to_blog($blog_id);

                    $order = $this->findOrder($data['code']);

                    if($order && is_array($order)){
                        $orders[] = [
                            'order'   => $order[0],
                            'blog_id' => $blog_id,
                        ];
                    }
                }

                if(!empty($orders) && is_array($orders)){
                    if(sizeof($orders)  === 1){
                        $this->sendSuccessFound($orders[0]['order'], true, $orders[0]['blog_id']);
                    } else {
                        $this->sendFoundSeveral($orders);
                    }
                    restore_current_blog();
                }else{

                    restore_current_blog();

                    wp_send_json_error(sprintf(__('Cannot find order: %s',
                        'woocommerce-orderscanner-packstations'), $data['code']));
                }

            }else{
                $order = $this->findOrder($data['code']);

                if ( ! empty($order)) {

                    if(is_array($order)){
                        $order =  $order[0];
                    }

                    $this->sendSuccessFound($order);

                } else {
                    wp_send_json_error(sprintf(__('Cannot find order: %s',
                        'woocommerce-orderscanner-packstations'), $data['code']));
                }
            }

            restore_current_blog();

        } else {
            wp_send_json_error(__('Please, enter a code',
                'woocommerce-orderscanner-packstations'));
        }

        wp_die();
    }

    /**
     * @param \WC_Order $order
     * @param bool      $network
     * @param null|int  $blog_id
     */
    protected function sendSuccessFound($order, $network = false, $blog_id = null)
    {
        $order_id = $order->get_id();

        if($network){
            $link = network_admin_url('admin.php?page=packing_station&order_id='
                                      . $order_id . "&blog_id=". $blog_id );
        }else{
            $link = $this->controller->getAdminUrl('admin.php?page=packing_station&order_id='
                                                   . $order_id);
        }

        $packed_confirm_message = $this->getPackedConfirmMessage($order);

        $packed_status = $order->get_meta(self::ORDER_PACKED_STATUS_META_KEY);

        wp_send_json_success(['link' => $link, 'packed_confirm_message' => $packed_confirm_message, 'packed_status' => $packed_status], 200);
    }

    /**
     * @param \WC_Order $order
     *
     * @return string
     */
    public function getPackedConfirmMessage($order)
    {
        return sprintf(__('Order %s have been packed already, do you want to repack it?', 'woocommerce-orderscanner-packstations'), '#'. $order->get_order_number());
    }

    /**
     * @param string $code
     *
     * @return mixed
     */
    protected function findOrder($code)
    {
        $order = wc_get_orders([
            WC_Orderscanner_Packstation_Admin::ORDER_BARCODE_META_KEY => $code,
        ]);

        // Check if we just scanned order_key
        if(empty($order)){
            $order = wc_get_orders([
                'order_key' => $code,
            ]);
        }

        // Check if we just scanned order_id
        if(empty($order)){
            $order = wc_get_order($code);
        }

        if(!empty($order)){
            if(!is_array($order)){
                return [$order];
            }
            return $order;
        }

        return false;
    }

    /**
     * Handler when user scan product code
     */
    public function scanProductCode()
    {
        $data = $_REQUEST;

        if(isset($data['data']['blogId']) && $data['data']['blogId'] != 0){
            switch_to_blog($data['data']['blogId']);
        }

        if ( ! empty($data['data']) && ! empty($data['order_id'])) {
            $order = wc_get_order($data['order_id']);

            if ($order) {
                $order_scanned_status = get_post_meta($order->get_id(),
                    self::ORDER_PACKED_STATUS_META_KEY, true);

                if ( ! $order_scanned_status) {
                    wp_send_json_error(__('No order_scanned_status meta was found',
                        'woocommerce-orderscanner-packstations'));
                }

                $wc_oi = new WC_Order_Item_Product($data['data']['orderItemId']);
                if ( ! $wc_oi) {
                    wp_send_json_error(__('Cannot find Order_Item_Product with item id' . ': '
                                          . $data['data']['orderItemId'],
                        'woocommerce-orderscanner-packstations'));
                }

                $product_data = $order_scanned_status['products'][$wc_oi->get_id()];

                if ( ! empty($product_data)) {

                    if($data['is_scan_all']){
                        $order_scanned_status['products'][$wc_oi->get_id()]['scanned'] = $order_scanned_status['products'][$wc_oi->get_id()]['quantity'];
                    }else if ($product_data['scanned'] < $product_data['quantity']) {
                        $order_scanned_status['products'][$wc_oi->get_id()]['scanned'] += 1;
                    }

                    do_action('orderscanner_packstation_product_scanned',
                        $order, $order_scanned_status, get_current_user_id());

                    $order_scanned_status = $this->checkIfDone($order_scanned_status, $order);

                    update_post_meta($order->get_id(),
                        self::ORDER_PACKED_STATUS_META_KEY,
                        $order_scanned_status);

                    wp_send_json_success($order_scanned_status);
                } else {
                    wp_send_json_error(__('Cannot find product with code', 'woocommerce-orderscanner-packstations') . ': '
                                          .'<pre>'. print_r($data['data'],true).'</pre>',
                        'woocommerce-orderscanner-packstations');
                }
            }

        } else {
            wp_send_json_error(__('Wrong input data',
                'woocommerce-orderscanner-packstations'));
        }

        restore_current_blog();
        wp_die();
    }

    public function resetScanProduct()
    {
        $data = $_REQUEST;

        if(isset($data['blog_id'])){
            switch_to_blog($data['blog_id']);
        }

        if ( ! empty($data['data']) && ! empty($data['order_id'])) {

            $order = wc_get_order($data['order_id']);
            if ($order) {
                $order_scanned_status = get_post_meta($order->get_id(),
                    self::ORDER_PACKED_STATUS_META_KEY, true);

                if ( ! $order_scanned_status) {
                    wp_send_json_error(__('Wrong input data',
                        'woocommerce-orderscanner-packstations'));
                }

                $wc_oi = new WC_Order_Item_Product($data['data']['orderItemId']);
                if ( ! $wc_oi) {
                    wp_send_json_error(__('Cannot find Order_Item_Product with item id' . ': '
                                          . $data['data']['orderItemId'],
                        'woocommerce-orderscanner-packstations'));
                }

                $product_data = $order_scanned_status['products'][$wc_oi->get_id()];

                if ( ! empty($product_data)) {

                    if ($order_scanned_status['end_pack'] == null) {
                        $order_scanned_status['products'][$wc_oi->get_id()]['scanned'] = 0;
                    }

                    update_post_meta($order->get_id(),
                        self::ORDER_PACKED_STATUS_META_KEY,
                        $order_scanned_status);

                    do_action('orderscanner_packstation_reset_product_scanned',
                        $order, $order_scanned_status, get_current_user_id());

                    wp_send_json_success($order_scanned_status);
                } else {
                    wp_send_json_error(__('Cannot find Order_Item_Product',
                        'woocommerce-orderscanner-packstations'));
                }
            }

        } else {
            wp_send_json_error(__('Wrong input data',
                'woocommerce-orderscanner-packstations'));
        }

        restore_current_blog();
        wp_die();
    }
    
    public function setOrderParcels()
    {
        $data = $_REQUEST;

        if(isset($data['blog_id'])){
            switch_to_blog($data['blog_id']);
        }

        if ( ! empty($data['parcels']) && ! empty($data['order_id'])) {

            $order = wc_get_order($data['order_id']);
            if ($order) {
                $parcels = intval($data['parcels']);
                $order->update_meta_data('_unifaun_parcels',$parcels);
                $order->save();

                wp_send_json_success();
            }

        } else {
            wp_send_json_error(__('Wrong input data',
                'woocommerce-orderscanner-packstations'));
        }

        restore_current_blog();
        wp_die();
    }

    /**
     * @param \WC_Order $order
     *
     * @return array
     */
    public function createOrderScannedStatus($order)
    {
        $data = [
            'begin_pack' => time(),
            'end_pack'   => null,
            'packer'     => get_current_user_id(),
            'station'    => get_user_meta(get_current_user_id(),
                WC_Orderscanner_Packstation_Packstation::USER_STATION_META_KEY,
                true),
        ];

        foreach ($order->get_items() as $item_id => $item) {
            $wc_product = $item->get_product();

            if( $wc_product->is_type('bundle') || $wc_product->is_virtual() ){
                //Skip bundle since it only contains other products
                //Skip virtual products, since they should not be packed.
                continue;
            }

            if ($item instanceof WC_Order_Item_Product) {

                $data['products'][$item_id] = [
                    'order_item_id' => $item->get_id(),
                    'product_id'    => $item->get_product_id(),
                    'variation_id'  => $item->get_variation_id(),
                    'type'          => $wc_product->get_type(),
                    'quantity'      => $item->get_quantity(),
                    'scanned'       => 0,
                ];

            }
        }

        update_post_meta($order->get_id(), self::ORDER_PACKED_STATUS_META_KEY,
            $data);

        do_action('orderscanner_packstation_pack_meta_created', $order,
            get_current_user_id());

        return $data;
    }

    /**
     * If all products are scanned - set end packed time
     *
     * @param array $order_scanned_status
     *
     * @param \WC_Order $order
     *
     * @return array
     */
    public function checkIfDone($order_scanned_status, $order)
    {
        $done = true;

        foreach ($order_scanned_status['products'] as $product_status) {
            if ($product_status['scanned'] < $product_status['quantity']) {
                $done = false;
            }
        }

        if ($done) {
            $order_scanned_status['end_pack'] = time();
            
            $status = get_site_option(REDLIGHT_OP_SETTING_PREFIX . 'packed_status', '');

            if($status !== ''){
                $this->changeStatus($order, $status);
            }

            do_action('orderscanner_packstation_pack_end', $order,
                $order_scanned_status);
        }

        return $order_scanned_status;
    }

    /**
     *
     * @param \WC_Order $order
     * @param bool      $status
     */
    public function changeStatus($order, $status = false)
    {
        $status = !$status ? get_site_option(REDLIGHT_OP_SETTING_PREFIX . 'packed_status', 'wc-packed') : $status;
        $order->update_status($status);
    }

    /**
     * @param array $orders
     */
    protected function sendFoundSeveral($orders)
    {
        $data['find_several'] = true;

        foreach ($orders as $order){
            $blog_id = $order['blog_id'];
            $order   = $order['order'];

            $link = network_admin_url('admin.php?page=packing_station&order_id='
                                      . $order->get_id() . "&blog_id=". $blog_id );
            if($order instanceof WC_Order){
                $data['orders'][] = [
                    'order_name'             => __('Order', 'woocommerce-orderscanner-packstations'). ' #' . $order->get_order_number() . ' (' . get_site($blog_id)->blogname . ')',
                    'blog_id'                => $blog_id,
                    'link'                   => $link,
                    'packed_confirm_message' => $this->getPackedConfirmMessage($order),
                    'packed_status'          => $order->get_meta(self::ORDER_PACKED_STATUS_META_KEY),
                ];
            }
        }

        wp_send_json_success($data, 200);
    }
}