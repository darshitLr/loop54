<?php

require_once dirname( __FILE__ ) . '/../Abstract/abstract-class-wc-orderscanner-packstation-settings.php';

class WC_Orderscanner_Packstation_Settings_Network extends Abstract_WC_Orderscanner_Packstation_Settings{

    const SAVE_ACTION = 'op_save_settings';

    private $handle_options = [
        'network_display_sites',
        'packed_status',
        'display_order_list',
        'display_order_status',
        'begin_pack_status',
        'mark_as_status',
        'high_quantity_products',
        'display_ean_field',
    ];
    /**
     * WC_Orderscanner_Packstation_Settings_Network constructor.
     */
    public function __construct()
    {
        add_action( 'admin_init', array($this, 'registerNetworkSetting') );
        add_action( 'admin_post_' . self::SAVE_ACTION, array($this, 'handleSave') );

        parent::__construct();
    }

    public function handleSave()
    {
        if(!is_user_logged_in() || !wp_verify_nonce($_POST['_wpnonce'], self::SAVE_ACTION)){
            _e('You don\'t have access to this page', 'woocommerce-orderscanner-packstations' );
            wp_die();
        }
        foreach ($this->handle_options as $option){
            $option_name = $this->getOptionName($option);
            $value = isset($_POST[$option_name]) ? $_POST[$option_name] : false;
            $this->updateOption($option, $value);
        }

        wp_redirect($_POST['_wp_http_referer']);
    }

    public function getOption($name, $default = false)
    {
        return get_site_option($this->getOptionName($name), $default);
    }

    /**
     * Register settings
     */
    public function registerNetworkSetting()
    {
        register_setting('redlight_op_settings', REDLIGHT_OP_SETTING_PREFIX . 'network_display_sites');

        add_settings_field(
            REDLIGHT_OP_SETTING_PREFIX . 'network_display_sites',
            __('What sites to display orders from', 'woocommerce-orderscanner-packstations'),
            [$this, 'renderNetworkSitesField'],
            'packing_station_setting',
            'redlight_op_settings'
        );
    }

    public function renderNetworkSitesField()
    {
        $option_name = $this->getOptionName('network_display_sites');
        $option_value = $this->getOption('network_display_sites');

        echo '<select name="'.$option_name.'[]" multiple>';
        foreach (get_sites() as $site){
            echo '<option value="'.$site->blog_id.'" type="text" '. selected(in_array($site->blog_id, $option_value ,true)) .'>'.$site->blogname.'</option>';
        }
        echo '</select>';
    }

    protected function updateOption($name, $value)
    {
        update_site_option($this->getOptionName($name), $value);
    }

}