<?php

namespace PixelYourSite\Bing\Helpers;

use PixelYourSite;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function getWooProductContentId( $product_id ) {

    if ( PixelYourSite\Bing()->getOption( 'woo_content_id' ) == 'product_sku' ) {
        $content_id = get_post_meta( $product_id, '_sku', true );
    } else {
        $content_id = $product_id;
    }

    $prefix = PixelYourSite\Bing()->getOption( 'woo_content_id_prefix' );
    $suffix = PixelYourSite\Bing()->getOption( 'woo_content_id_suffix' );

    $value = $prefix . $content_id . $suffix;

    return $value;
}

function getWooCartItemId( $item ) {

    if ( PixelYourSite\Bing()->getOption( 'woo_variable_as_simple' )
        && isset( $item['parent_id'] )
        && $item['parent_id'] !== 0
    ) {
        $product_id = $item['parent_id'];
    } else {
        $product_id = $item['product_id'];
    }

    return $product_id;

}
function bing_round( $val, $precision = 2, $mode = PHP_ROUND_HALF_UP )  {
    if ( ! is_numeric( $val ) ) {
        $val = floatval( $val );
    }
    return round( $val, $precision, $mode );
}
/**
 * @param PixelYourSite\SingleEvent $event
 */
function getWooEventOrderTotal( $event ) {

    if(PixelYourSite\PYS()->getOption( 'woo_event_value' ) != 'custom') {
        $total = 0;
        // $include_tax = get_option( 'woocommerce_tax_display_cart' ) == 'incl';
        foreach ($event->args['products'] as $product) {
            $total += $product['total'] + $product['total_tax'];
        }
        $total+=$event->args['shipping_cost'] + $event->args['shipping_tax'];
        return bing_round($total);
    }

    $include_tax = PixelYourSite\PYS()->getOption( 'woo_tax_option' ) == 'included' ? true : false;
    $include_shipping = PixelYourSite\PYS()->getOption( 'woo_shipping_option' ) == 'included' ? true : false;


    $total = 0;
    foreach ($event->args['products'] as $product) {
        $total += $product['total'];
        if($include_tax) {
            $total += $product['total_tax'];
        }
    }

    if($include_shipping) {
        $total += $event->args['shipping_cost'];
    }
    if($include_tax) {
        $total += $product['shipping_cost_tax'];
    }

    return bing_round($total );

}

/**
 * EASY DIGITAL DOWNLOADS
 */

function getEddDownloadContentId( $download_id ) {

    if ( PixelYourSite\Bing()->getOption( 'edd_content_id' ) == 'download_sku' ) {
        $content_id = get_post_meta( $download_id, 'edd_sku', true );
    } else {
        $content_id = $download_id;
    }

    $prefix = PixelYourSite\Bing()->getOption( 'edd_content_id_prefix' );
    $suffix = PixelYourSite\Bing()->getOption( 'edd_content_id_suffix' );

    return $prefix . $content_id . $suffix;

}

