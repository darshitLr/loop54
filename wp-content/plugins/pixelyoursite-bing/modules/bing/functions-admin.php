<?php

namespace PixelYourSite\Bing;

use PixelYourSite;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function renderPixelIdField() {

    if ( PixelYourSite\Bing()->enabled() ) : ?>

        <div class="row align-items-center">
            <div class="col-2 py-2">
                <img class="tag-logo" src="<?php echo PYS_BING_URL; ?>/dist/images/microsoft-small-square.png">
            </div>
            <div class="col-6">
                Your Bing Tag
            </div>
            <div class="col-4">
                <label for="bing_settings_switch" class="btn btn-block btn-sm btn-primary btn-settings">Click for settings</label>
            </div>
        </div>
        <input type="checkbox" id="bing_settings_switch" style="display: none">
        <div class="settings_content">
            <div class="plate">
                <div class="row pt-3 pb-3">
                    <div class="col-12">
                        <h4 class="label">Microsoft UET Tag (Bing):</h4>
                        <?php PixelYourSite\Bing()->render_pixel_id( 'pixel_id', 'UET Tag' ); ?>

                        <?php if ( isPysProActive() ) : ?>
                            <small class="form-text"><a
                                        href="https://www.pixelyoursite.com/documentation/add-your-microsoft-uet-tag"
                                        target="_blank">How to get it?</a></small>
                        <?php else : ?>
                            <small class="form-text"><a
                                        href="https://www.pixelyoursite.com/pixelyoursite-free-version/how-to-add-your-microsoft-uet-tag-bing?utm_source=pixelyoursite-free-plugin&utm_medium=plugin&utm_campaign=free-plugin-ids"
                                        target="_blank">How to get it?</a></small>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <hr>
            <?php
            if(function_exists("PixelYourSite\addMetaTagFields")) {
                PixelYourSite\addMetaTagFields(PixelYourSite\Bing(),"");
            }
            ?>
        </div>
        <hr>

    <?php endif;

}

function adminSecondaryNavTabs( $tabs ) {

	$tabs['bing_settings'] = array(
		'url'  => PixelYourSite\buildAdminUrl( 'pixelyoursite', 'bing_settings' ),
		'name' => 'Bing Settings',
	);

	return $tabs;

}

function renderSettingsPage() {

	/** @noinspection PhpIncludeInspection */
    include PYS_BING_PATH . '/modules/bing/views/html-settings.php';

}

function adminNoticePysCoreNotActive() {

	if ( current_user_can( 'manage_options' ) ) : ?>

        <div class="notice notice-error">
            <p>PixelYourSite Bing Add-on needs <a href="https://www.pixelyoursite.com/facebook-pixel-plugin/buy-pixelyoursite-pro"
               target="_blank">PixelYourSite PRO</a> or <a href="https://wordpress.org/plugins/pixelyoursite/"
               target="_blank">Free</a> in order to work. Activate it now.</p>
        </div>

	<?php endif;

}

function adminNoticePysProOutdated() {

	if ( current_user_can( 'manage_options' ) ) : ?>

        <div class="notice notice-error">
            <p>PixelYourSite Bing Add-on requires PixelYourSite PRO version <?php echo
                PYS_BING_PRO_MIN_VERSION; ?> or newer.</p>
        </div>

	<?php endif;

}

function adminNoticePysFreeOutdated() {

    if ( current_user_can( 'manage_options' ) ) : ?>

        <div class="notice notice-error">
            <p>PixelYourSite Bing Add-on requires PixelYourSite Free version <?php echo
                PYS_BING_FREE_MIN_VERSION; ?> or newer.</p>
        </div>

    <?php endif;

}