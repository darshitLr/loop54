<?php

namespace PixelYourSite;

use PixelYourSite\Bing\Helpers;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

class Bing extends Settings implements Pixel, Plugin {

    private static $_instance;

    private $configured;

    private $core_compatible;

    public static function instance() {

        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }

        return self::$_instance;

    }

    public function __construct() {

        // check and cache status
        if ( Bing\isPysProActive() ) {
            $this->core_compatible = Bing\pysProVersionIsCompatible();
        } else {
            $this->core_compatible = Bing\pysFreeVersionIsCompatible();
        }

        parent::__construct( 'bing' );

        $this->locateOptions(
            PYS_BING_PATH . '/modules/bing/options_fields.json',
            PYS_BING_PATH . '/modules/bing/options_defaults.json'
        );

        // migrate after event post type registered
        add_action( 'pys_register_pixels', 'PixelYourSite\Bing\maybeMigrate' );

        add_action( 'pys_register_plugins', function( $core ) {
            /** @var PYS $core */
            $core->registerPlugin( $this );
        } );

        if ( ! $this->core_compatible ) {
            return;
        }

        add_action( 'pys_register_pixels', function( $core ) {
            /** @var PYS $core */
            $core->registerPixel( $this );
        } );

        if ( $this->configured() ) {

            // output debug info
            add_action( 'wp_head', function() {
                echo "<script type='text/javascript'>console.log('PixelYourSite Bing version " . PYS_BING_VERSION . "');</script>\r\n";
            }, 2 );

            // load add-on public JS
            add_action( 'wp_enqueue_scripts', function() {
                wp_enqueue_script( 'pys-bing', PYS_BING_URL . '/dist/scripts/public.js', array( 'pys' ),
                    PYS_BING_VERSION );
            } );

        }

        add_action( 'pys_admin_pixel_ids', 'PixelYourSite\Bing\renderPixelIdField' );
        add_filter( 'pys_admin_secondary_nav_tabs', 'PixelYourSite\Bing\adminSecondaryNavTabs' );
        add_action( 'pys_admin_bing_settings', 'PixelYourSite\Bing\renderSettingsPage' );
        add_action( 'wp_head', array( $this, 'output_meta_tag' ) );
    }

    public function enabled() {
        return $this->getOption( 'enabled' );
    }

    public function configured() {

        $license_status = $this->getOption( 'license_status' );
        $pixel_id = $this->getAllPixels();
        $disabledPixel =  apply_filters( 'pys_pixel_disabled', '', $this->getSlug() );

        $this->configured = $this->enabled()
            && ! empty( $license_status ) // license was activated before
            && ! empty( $pixel_id )
            && $disabledPixel != '1' && $disabledPixel != 'all';

        return $this->configured;

    }

    public function getPixelIDs() {

        $ids = (array) $this->getOption( 'pixel_id' );

        return (array) reset( $ids ); // return first id only

    }

    public function getAllPixels() {
        return $this->getPixelIDs();
    }

    /**
     * @param SingleEvent $event
     */
    public function getAllPixelsForEvent($event) {
        return $this->getPixelIDs();
    }

    public function getPixelOptions() {

        $ids = $this->getPixelIDs();

        return array(
            'pixelId' => reset($ids),
        );
    }

    public function getPluginName() {
        return 'PixelYourSite Bing Add-On';
    }

    public function getPluginFile() {
        return PYS_BING_PLUGIN_FILE;
    }

    public function getPluginVersion() {
        return PYS_BING_VERSION;
    }

    public function adminUpdateLicense() {

        if ( PYS()->adminSecurityCheck() ) {
            updateLicense( $this );
        }

    }

    public function adminRenderPluginOptions() {
        // for backward compatibility with PRO < 7.0.6
    }

    public function updatePlugin() {
        // for backward compatibility with PRO < 7.0.6
    }

    /**
     * @param CustomEvent $event
     */
    public function renderCustomEventOptions( $event ) {
        /** @noinspection PhpIncludeInspection */
        include PYS_BING_PATH . '/modules/bing/views/html-main-events-edit.php';
    }

    /**
     * Create pixel event and fill it
     * @param SingleEvent $event
     */
    public function generateEvents($event) {
        $pixelEvents = [];
        $pixelIds = $this->getAllPixelsForEvent($event);
        $disabledPixel =  apply_filters( 'pys_pixel_disabled', '', $this->getSlug() );


        if($event->getId() == 'custom_event'){
            $preselectedPixel = $event->args->bing_pixel_id;
            if($preselectedPixel != 'all') { $pixelIds = [$preselectedPixel]; }
        }

        // filter disabled pixels
        if(!empty($disabledPixel)) {
            foreach ($pixelIds as $key => $value) {
                if($value == $disabledPixel) {
                    array_splice($pixelIds,$key,1);
                }
            }
        }
        if(count($pixelIds) > 0) {
            $pixelEvent = clone $event;
            if($this->addParamsToEvent($pixelEvent)) {
                $pixelEvent->addPayload([ 'pixel_ids' => $pixelIds ]);
                $pixelEvents[] = $pixelEvent;
            }
        }

        return $pixelEvents;
    }

    /**
     * @param SingleEvent $event
     * @return bool
     */

    public function addParamsToEvent(&$event) {
        if ( ! $this->configured() ) {
            return false;
        }
        $isActive = false;
        switch ($event->getId()) {

            //Automatic events
            case 'automatic_event_signup' : {
                $event->addPayload(["name" => "sign_up"]);
                $isActive = $this->getOption($event->getId().'_enabled');
            } break;
            case 'automatic_event_login' :{
                $event->addPayload(["name" => "Login"]);
                $isActive = $this->getOption($event->getId().'_enabled');
            } break;
            case 'automatic_event_search' :{
                $event->addPayload(["name" => "search"]);
                $isActive = $this->getOption($event->getId().'_enabled');
            } break;
            case 'automatic_event_tel_link' :
            case 'automatic_event_email_link':
            case 'automatic_event_form' :

            case 'automatic_event_download' :
            case 'automatic_event_comment' :
            case 'automatic_event_adsense' :
            case 'automatic_event_scroll' :
            case 'automatic_event_time_on_page' :

            case "automatic_event_video":
            case "automatic_event_outbound_link":
            case "automatic_event_internal_link": {
                $isActive = $this->getOption($event->getId().'_enabled');
            }break;
            //Signal Events
            case "signal_user_signup":
            case "signal_page_scroll":
            case "signal_time_on_page":
            case "signal_tel":
            case "signal_email":
            case "signal_form":
            case "signal_download":
            case "signal_comment":
            case "signal_watch_video":
            case "signal_click" : {
                $isActive = $this->getOption('signal_events_enabled');
            }break;
            case "signal_adsense": {
                $isActive = false;
                // not support
            }break;

            case 'woo_view_content':{
                $eventData = $this->getWooViewContentEventParams();
                if ($eventData) {
                    $isActive = true;
                    $this->addDataToEvent($eventData, $event);
                }
            }break;


            case 'woo_add_to_cart_on_cart_page':
            case 'woo_add_to_cart_on_checkout_page':{
                if($event->args == null) { // deprecate
                    $eventData = $this->getWooAddToCartOnCartEventParams();
                    if ($eventData) {
                        $isActive = true;
                        $this->addDataToEvent($eventData, $event);
                    }
                } else {
                    $isActive = $this->setWooAddToCartOnCartEventParams($event);
                }


            }break;


            case 'woo_view_category':{
                $eventData = $this->getWooViewCategoryEventParams();
                if ($eventData) {
                    $isActive = true;
                    $this->addDataToEvent($eventData, $event);
                }
            }break;


            case 'woo_initiate_checkout':{
                if($event->args == null) { // deprecate
                    $eventData = $this->getWooInitiateCheckoutEventParams();
                    if ($eventData) {
                        $isActive = true;
                        $this->addDataToEvent($eventData, $event);
                    }
                } else {
                    $isActive = $this->setWooInitiateCheckoutEventParams($event);
                }


            }break;


            case 'woo_purchase':{
                if($event->args == null) {
                    $isActive = $this->getWooPurchaseEventParams($event);
                } else {
                    $isActive = $this->addWooPurchaseEventParams($event);
                }
            }break;


            case 'woo_frequent_shopper':
            case 'woo_vip_client':
            case 'woo_big_whale':{
                $eventData = $this->getWooAdvancedMarketingEventParams( $event->getId() );
                if ($eventData) {
                    $isActive = true;
                    $this->addDataToEvent($eventData, $event);
                }
            }break;

            case 'edd_view_content':
            {
                $eventData = $this->getEddViewContentEventParams();
                if ($eventData) {
                    $isActive = true;
                    $this->addDataToEvent($eventData, $event);
                }
            }break;

            // EDD

            case 'edd_view_category':{
                $eventData = $this->getEddViewCategoryEventParams();
                if ($eventData) {
                    $isActive = true;
                    $this->addDataToEvent($eventData, $event);
                }
            }break;


            case 'edd_add_to_cart_on_checkout_page':{
                if($event->args == null) {
                    $eventData = $this->getEddCartEventParams('add_to_cart');
                    if ($eventData) {
                        $isActive = true;
                        $this->addDataToEvent(array(
                            'name' => 'add_to_cart',
                            'data' => $eventData
                        ), $event);
                    }
                } else {
                    $isActive = $this->setEddCartEventParams( $event );
                }
            }break;

            case 'edd_initiate_checkout':{
                if($event->args == null) {
                    $eventData = $this->getEddCartEventParams('begin_checkout');
                    if ($eventData) {
                        $isActive = true;
                        $this->addDataToEvent(array(
                            'name' => 'InitiateCheckout',
                            'data' => $eventData
                        ), $event);
                    }
                } else {
                    $isActive = $this->setEddCartEventParams( $event );
                }

            }break;

            case 'edd_purchase':{
                if($event->args == null) { // deprecate
                    $eventData = $this->getEddCartEventParams( 'purchase' );
                    if ($eventData) {
                        $isActive = true;
                        $this->addDataToEvent(array(
                            'name'  => 'purchase',
                            'data'  => $eventData
                        ), $event);
                    } ;
                } else {
                    $isActive = $this->setEddCartEventParams( $event );//'purchase'
                }

            }break;

            case 'edd_frequent_shopper':
            case 'edd_vip_client':
            case 'edd_big_whale':{
                $eventData = $this->getEddAdvancedMarketingEventParams( $event->getId() );
                if ($eventData) {
                    $isActive = true;
                    $this->addDataToEvent($eventData, $event);
                }
            }break;

            case 'custom_event':{
                $eventData =  $this->getCustomEventParams( $event->args );
                if ($eventData) {
                    $isActive = true;
                    $this->addDataToEvent($eventData, $event);
                }
            }break;

            case 'woo_add_to_cart_on_button_click':{
                if (  $this->getOption( 'woo_add_to_cart_enabled' ) && PYS()->getOption( 'woo_add_to_cart_on_button_click' ) ) {
                    $isActive = true;
                    if(isset($event->args['productId'])) { // use for old main pixel plugin
                        $productId = $event->args['productId'];
                        $quantity = $event->args['quantity'];
                        $eventData = $this->getWooAddToCartOnButtonClickEventParams($productId, $quantity);

                        if ($eventData) {
                            $event->addParams($eventData["params"]);
                            unset($eventData["params"]);
                            $event->addPayload($eventData);
                        }
                    }
                    $event->addPayload(array(
                        'name'=>"add_to_cart"
                    ));
                }
            }break;


            case 'edd_add_to_cart_on_button_click':{
                if (  $this->getOption( 'edd_add_to_cart_enabled' ) && PYS()->getOption( 'edd_add_to_cart_on_button_click' ) ) {
                    $isActive = true;
                    if($event->args != null) {
                        $event->addParams($this->getEddAddToCartOnButtonClickEventParams( $event->args ));
                    }
                    $event->addPayload(array(
                        'name'=>"add_to_cart"
                    ));
                }
            }break;

            case 'wcf_view_content': {
                $isActive =  $this->getWcfViewContentEventParams($event);
            }break;

            case 'wcf_add_to_cart_on_bump_click':
            case 'wcf_add_to_cart_on_next_step_click': {
                $isActive = $this->prepare_wcf_add_to_cart($event);
            }break;


            case 'wcf_lead': {
                $isActive = PYS()->getOption('wcf_lead_enabled');
            }break;

            case 'wcf_step_page': {
                $isActive = $this->getOption('wcf_step_event_enabled');
            }break;

            case 'wcf_bump': {
                $isActive = $this->getOption('wcf_bump_event_enabled');
            }break;

            case 'wcf_page': {
                $isActive = $this->getOption('wcf_cart_flows_event_enabled');
            }break;

        }

        return $isActive;
    }
    private function addDataToEvent($eventData,&$event) {
        $params = $eventData["data"];
        unset($eventData["data"]);
        //unset($eventData["name"]);
        $event->addParams($params);
        $event->addPayload($eventData);
    }
    public function getEventData( $eventType, $args = null ) {

        return false;
    }

    public function outputNoScriptEvents() {
        // not supported
    }

    public function renderAddonNotice() {}

    /**
     * @param CustomEvent $event
     *
     * @return array|bool
     */
    private function getCustomEventParams( $event ) {

        $action = $event->bing_event_action;

        if ( ! $event->isBingEnabled() || empty( $action ) ) {
            return false;
        }

        $params = array(
            'event_category' => $event->bing_event_category,
            'event_label' => $event->bing_event_label,
            'event_value' => $event->bing_event_value,
        );

        // SuperPack Dynamic Params feature
        $params = apply_filters( 'pys_superpack_dynamic_params', $params, 'bing' );

        return array(
            'name'  => $action,
            'data'  => $params,
            'delay' => $event->getDelay(),
        );

    }

    private function getWooViewCategoryEventParams() {
        global $posts;

        if ( ! $this->getOption( 'woo_view_category_enabled' ) ) {
            return false;
        }

        $product_categories = array();
        $term = get_term_by( 'slug', get_query_var( 'term' ), 'product_cat' );

        if ( $term ) {
            $parent_ids = get_ancestors( $term->term_id, 'product_cat', 'taxonomy' );
            $product_categories[] = $term->name;

            foreach ( $parent_ids as $term_id ) {
                $parent_term = get_term_by( 'id', $term_id, 'product_cat' );
                $product_categories[] = $parent_term->name;
            }
        }

        $list_name = implode( '/', array_reverse( $product_categories ) );

        $product_ids = array();

        for ($i = 0; $i < count($posts); $i++) {
            if ($posts[$i]->post_type !== 'product') {
                continue;
            }

            $product_ids[] = Helpers\getWooProductContentId($posts[$i]->ID);
        }

        $params = array(
            'event_category'  => 'ecommerce',
            'event_label'     => $list_name,
            'ecomm_prodid' => $product_ids,
            'ecomm_pagetype' => 'category',
        );

        return array(
            'name'  => 'view_item_list',
            'data'  => $params,
        );
    }

    /**
     * @param SingleEvent $event
     * @return bool
     */
    private function getWcfViewContentEventParams(&$event)  {
        if ( ! $this->getOption( 'woo_view_content_enabled' )
            || empty($event->args['products'])
        ) {
            return false;
        }

        $product_data = $event->args['products'][0];
        $params = array(
            'event_category'  => 'ecommerce',
            'event_label' => $product_data['name'],
            'ecomm_prodid' => Helpers\getWooProductContentId($product_data['id']),
            'ecomm_pagetype' => 'product',
        );

        $event->addParams($params);
        $event->addPayload([
            'name'  => 'view_item',
            'delay' => (int) PYS()->getOption( 'woo_view_content_delay' ),
        ]);
        return true;
    }

    private function getWooViewContentEventParams() {
        global $post;

        if ( ! $this->getOption( 'woo_view_content_enabled' ) ) {
            return false;
        }

        $params = array(
            'event_category'  => 'ecommerce',
            'event_label' => $post->post_title,
            'ecomm_prodid' => Helpers\getWooProductContentId($post->ID),
            'ecomm_pagetype' => 'product',
        );

        return array(
            'name'  => 'view_item',
            'data'  => $params,
            'delay' => (int) PYS()->getOption( 'woo_view_content_delay' ),
        );
    }

    /**
     * @param SingleEvent $event
     * @return false
     */
    private function prepare_wcf_add_to_cart(&$event) {
        if(  !$this->getOption( 'woo_add_to_cart_enabled' )
            || empty($event->args['products'])
        ) {
            return false; // return if args is empty
        }

        $product_ids = array();
        $product_labels = array();

        foreach ($event->args['products'] as $product_data) {
            $product_labels[] = $product_data['name'];
            $product_ids[] = Helpers\getWooProductContentId( $product_data['id'] );
        }

        $params = array(
            'event_category' => 'ecommerce',
            'event_label' => implode(',', $product_labels),
            'ecomm_prodid' => $product_ids,
            'ecomm_pagetype' => 'cart',
        );

        $event->addParams($params);
        $event->addPayload([
            'name'=>"add_to_cart"
        ]);

        return true;
    }

    private function getWooAddToCartOnButtonClickEventParams( $product_id,$quantity ) {

        $product = get_post( $product_id );

        return array(
            'params' => array(
                'event_category' => 'ecommerce',
                'event_label' => $product->post_title,
                'ecomm_prodid' => Helpers\getWooProductContentId($product_id),
                'ecomm_pagetype' => 'cart',
            )
        );
    }

    /**
     * @deprecated
     */
    private function getWooAddToCartOnCartEventParams() {

        if (!$this->getOption('woo_add_to_cart_enabled')) {
            return false;
        }

        return array(
            'name' => 'add_to_cart',
            'data' => $this->getWooCartParamsOld()
        );
    }
    /**
     * @param SingleEvent $event
     * @return boolean
     */
    private function setWooAddToCartOnCartEventParams(&$event) {

        if (!$this->getOption('woo_add_to_cart_enabled')) {
            return false;
        }

        $event->addParams($this->getWooCartParams($event));
        $event->addPayload([ 'name' => 'add_to_cart']);

        return true;
    }

    /**
     * @deprecated
     * @return array|false
     */
    private function getWooInitiateCheckoutEventParams() {

        if (!$this->getOption('woo_initiate_checkout_enabled')) {
            return false;
        }

        return array(
            'name' => 'begin_checkout',
            'data' => $this->getWooCartParamsOld()
        );
    }

    /**
     * @param SingleEvent $event
     * @return boolean
     */
    private function setWooInitiateCheckoutEventParams(&$event) {

        if (!$this->getOption('woo_initiate_checkout_enabled')) {
            return false;
        }
        $event->addParams($this->getWooCartParams($event));
        $event->addPayload(['name' => 'begin_checkout']);

        return true;
    }

    /**
     * @deprecated
     * @return array
     */
    private function getWooCartParamsOld() {

        $product_ids = array();
        $product_labels = array();

        foreach (WC()->cart->cart_contents as $cart_item_key => $cart_item) {

            $product_id = Helpers\getWooCartItemId( $cart_item );
            $content_id = Helpers\getWooProductContentId( $product_id );

            $product = get_post($product_id);

            $product_labels[] = $product->post_title;

            $product_ids[] = $content_id;
        }

        $params = array(
            'event_category' => 'ecommerce',
            'event_label' => implode(',', $product_labels),
            'ecomm_prodid' => $product_ids,
            'ecomm_pagetype' => 'cart',
        );

        return $params;
    }

    /**
     * @param SingleEvent $event
     * @return array
     */
    private function getWooCartParams($event) {

        $product_ids = array();
        $product_labels = array();

        foreach ($event->args['products'] as $product) {

            $product_id = Helpers\getWooCartItemId( $product );
            $content_id = Helpers\getWooProductContentId( $product_id );

            $product_labels[] = $product['name'];

            $product_ids[] = $content_id;
        }

        $params = array(
            'event_category' => 'ecommerce',
            'event_label' => implode(',', $product_labels),
            'ecomm_prodid' => $product_ids,
            'ecomm_pagetype' => 'cart',
        );

        return $params;
    }
    /**
     * @param SingleEvent $event
     * @return bool
     */
    private function getWooPurchaseEventParams(&$event) {
        if ( ! $this->getOption( 'woo_purchase_enabled' ) ) {
            return false;
        }
        if($event->args && !empty($event->args['order_id'])) {
            $order_id = $event->args['order_id'];
        } else {
            $order_key = sanitize_key($_REQUEST['key']);
            $order_id = (int) wc_get_order_id_by_order_key( $order_key );
        }

        $order = new \WC_Order( $order_id );

        $product_ids = array();
        $product_labels = array();

        $order_total = (float) $order->get_total( 'edit' );
        //$order_tax = (float) $order->get_total_tax( 'edit' );

        foreach ($order->get_items('line_item') as $line_item) {

            $product_id = Helpers\getWooCartItemId($line_item);
            $content_id = Helpers\getWooProductContentId($product_id);
            $post = get_post($product_id);
            if (!$post) continue;
            $product_labels[] = $post->post_title;
            $product_ids[] = $content_id;
        }
        $event->addParams(array(
            'event_category' => 'ecommerce',
            'event_label' => implode(',', $product_labels),
            'event_value' => $order_total,
            'currency' => get_woocommerce_currency(),
            'ecomm_prodid' => $product_ids,
            'ecomm_pagetype' => 'purchase',
            'revenue_value' => $order_total
        ));

        $event->addPayload([
            'name' => 'purchase',
        ]);
        return true;
    }
    /**
     * @param SingleEvent $event
     * @return bool
     */
    private function addWooPurchaseEventParams(&$event) {
        if ( ! $this->getOption( 'woo_purchase_enabled' ) ) {
            return false;
        }
        $product_ids = [];
        $product_labels = [];

        foreach ($event->args['products'] as $product_data) {
            $product_id = Helpers\getWooCartItemId( $product_data );
            $content_id = Helpers\getWooProductContentId( $product_id );

            $product_labels[] = $product_data['name'];
            $product_ids[] = $content_id;
        }
        $total = Helpers\getWooEventOrderTotal($event);
        $event->addParams(array(
            'event_category' => 'ecommerce',
            'event_label' => implode(',', $product_labels),
            'event_value' => $total,
            'currency' => $event->args['currency'],
            'ecomm_prodid' => $product_ids,
            'ecomm_pagetype' => 'purchase',
            'revenue_value' => $total
        ));

        $event->addPayload([
            'name' => 'purchase',
        ]);
        return true;
    }

    private function getWooAdvancedMarketingEventParams( $eventType ) {

        if (!$this->getOption($eventType . '_enabled')) {
            return false;
        }

        switch ($eventType) {
            case 'woo_frequent_shopper':
                $eventName = 'FrequentShopper';
                break;

            case 'woo_vip_client':
                $eventName = 'VipClient';
                break;

            default:
                $eventName = 'BigWhale';
        }

        return array(
            'name' => $eventName,
            'data' => array(
                'event_category' => 'marketing'
            ),
        );
    }

    private function getEddViewContentEventParams() {
        global $post;

        if ( ! $this->getOption( 'edd_view_content_enabled' ) ) {
            return false;
        }

        $params = array(
            'event_category'  => 'ecommerce',
            'event_label' => $post->post_title,
            'ecomm_prodid' => Helpers\getEddDownloadContentId($post->ID),
            'ecomm_pagetype' => 'product',
        );

        return array(
            'name'  => 'view_item',
            'data'  => $params,
            'delay' => (int) PYS()->getOption( 'edd_view_content_delay' ),
        );
    }

    private function getEddAddToCartOnButtonClickEventParams( $download_id ) {


        $download_post = get_post( $download_id );

        return array(
                'event_category' => 'ecommerce',
                'event_label' => $download_post->post_title,
                'ecomm_prodid' => Helpers\getEddDownloadContentId($download_id),
                'ecomm_pagetype' => 'cart',
            );
    }

    private function getEddViewCategoryEventParams() {
        global $posts;

        if ( ! $this->getOption( 'edd_view_category_enabled' ) ) {
            return false;
        }

        $term = get_term_by( 'slug', get_query_var( 'term' ), 'download_category' );
        $parent_ids = get_ancestors( $term->term_id, 'download_category', 'taxonomy' );

        $download_categories = array();
        $download_categories[] = $term->name;

        foreach ( $parent_ids as $term_id ) {
            $parent_term = get_term_by( 'id', $term_id, 'download_category' );
            $download_categories[] = $parent_term->name;
        }

        $list_name = implode( '/', array_reverse( $download_categories ) );

        for ($i = 0; $i < count($posts); $i++) {
            $product_ids[] = Helpers\getEddDownloadContentId($posts[$i]->ID);
        }

        $params = array(
            'event_category'  => 'ecommerce',
            'event_label'     => $list_name,
            'ecomm_prodid' => $product_ids,
            'ecomm_pagetype' => 'category',
        );

        return array(
            'name'  => 'view_item_list',
            'data'  => $params,
        );
    }

    /**
     * @param SingleEvent $event
     * @return bool
     */
    private function setEddCartEventParams( &$event ) {

        $params = [];
        $data = [];
        $page_type = 'purchase';
        switch ($event->getId()) {
            case 'edd_add_to_cart_on_checkout_page' : {
                if(! $this->getOption( 'edd_add_to_cart_enabled' )) return false;
                $data['name'] = 'add_to_cart';
                $page_type = 'cart';
            }break;
            case 'edd_initiate_checkout' : {
                if(! $this->getOption( 'edd_initiate_checkout_enabled' )) return false;
                $data['name'] = 'begin_checkout';
            }break;
            case 'edd_purchase' : {
                if(! $this->getOption( 'edd_purchase_enabled' )) return false;
                $data['name'] = 'purchase';
                $params['currency'] = edd_get_currency();
            }break;
        }


        $product_ids = array();
        $product_labels = array();
        $total = 0;
        $total_as_is = 0;

        foreach ($event->args['products'] as $product) {
            $download_id = (int)$product['product_id'];
            $product_labels[] = $product['name'];
            $product_ids[] = Helpers\getEddDownloadContentId($download_id);
            if ( $event->getId() == 'edd_purchase' ) {
                if (PYS()->getOption('edd_tax_option') == 'included') {
                    $total += $product['subtotal'] + $product['tax'] - $product['discount'];
                } else {
                    $total += $product['subtotal'] - $product['discount'];
                }
                $total_as_is += $product['price'];
            }
        }

        //add fee
        $fee = isset($event->args['fee']) ? $event->args['fee'] : 0;
        $feeTax= isset($event->args['fee_tax']) ? $event->args['fee_tax'] : 0;
        if(PYS()->getOption( 'edd_event_value' ) == 'custom') {
            if(PYS()->getOption( 'edd_tax_option' ) == 'included') {
                $total += $fee + $feeTax;
            } else {
                $total += $fee;
            }
        } else {
            if(edd_prices_include_tax()) {
                $total_as_is += $fee + $feeTax;
            } else {
                $total_as_is += $fee;
            }
        }


        $params = array(
            'event_category' => 'ecommerce',
            'event_label' => implode(',', $product_labels),
            'ecomm_prodid' => $product_ids,
            'ecomm_pagetype' => $page_type,
        );

        if($event->getId() == 'edd_purchase') {
            if (PYS()->getOption('edd_event_value') == 'custom') {
                $params['event_value'] = $total;
            } else {
                $params['event_value'] = $total_as_is;
            }
        }
        $event->addParams($params);
        $event->addPayload($data);

        return true;
    }

    /**
     * @deprecated
     * @param string $context
     * @return array|false
     */
    private function getEddCartEventParams( $context = 'add_to_cart' ) {

        if ( $context == 'add_to_cart' && ! $this->getOption( 'edd_add_to_cart_enabled' ) ) {
            return false;
        } elseif ( $context == 'begin_checkout' && ! $this->getOption( 'edd_initiate_checkout_enabled' ) ) {
            return false;
        } elseif ( $context == 'purchase' && ! $this->getOption( 'edd_purchase_enabled' ) ) {
            return false;
        }

        if ( $context == 'add_to_cart' || $context == 'begin_checkout' ) {
            $cart = edd_get_cart_contents();
            $page_type = 'cart';
        } else {
            $cart = edd_get_payment_meta_cart_details( edd_get_purchase_id_by_key( getEddPaymentKey() ), true );
            $page_type = 'purchase';
        }

        $product_ids = array();
        $product_labels = array();

        foreach ($cart as $cart_item_key => $cart_item) {
            $download_id = (int)$cart_item['id'];
            $download_post = get_post($download_id);

            $product_labels[] = $download_post->post_title;
            $product_ids[] = Helpers\getEddDownloadContentId($download_id);
        }

        $params = array(
            'event_category' => 'ecommerce',
            'event_label' => implode(',', $product_labels),
            'ecomm_prodid' => $product_ids,
            'ecomm_pagetype' => $page_type,
        );

        if ($context == 'purchase') {
            $payment_key = getEddPaymentKey();
            $payment_id = (int)edd_get_purchase_id_by_key($payment_key);

            $params['event_value'] = edd_get_payment_amount($payment_id);
            $params['currency'] = edd_get_currency();
        }

        return $params;
    }

    private function getEddAdvancedMarketingEventParams( $eventType ) {

        if ( ! $this->getOption( $eventType . '_enabled' ) ) {
            return false;
        }

        switch ( $eventType ) {
            case 'edd_frequent_shopper':
                $eventName = 'FrequentShopper';
                break;

            case 'edd_vip_client':
                $eventName = 'VipClient';
                break;

            default:
                $eventName = 'BigWhale';
        }

        return array(
            'name' => $eventName,
            'data' => array(
                'event_category' => 'marketing'
            ),
        );
    }
    function output_meta_tag() {
        $metaTags = (array) $this->getOption( 'verify_meta_tag' );
        foreach ($metaTags as $tag) {
            echo $tag;
        }
    }
}

/**
 * @return Bing
 */
function Bing() {
    return Bing::instance();
}

Bing();