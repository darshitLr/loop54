<?php

namespace PixelYourSite\Bing;

use PixelYourSite;
use function PixelYourSite\getObjectTerms;
use function PixelYourSite\isEddActive;
use function PixelYourSite\isWooCommerceActive;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function isPysProActive() {
	
	if ( ! function_exists( 'is_plugin_active' ) ) {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	}
	
	return is_plugin_active( 'pixelyoursite-pro/pixelyoursite-pro.php' );
	
}

function pysProVersionIsCompatible() {
	
	if ( ! function_exists( 'get_plugin_data' ) ) {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	}

	$data = get_plugin_data( WP_PLUGIN_DIR   . '/pixelyoursite-pro/pixelyoursite-pro.php', false, false );

	return version_compare( $data['Version'], PYS_BING_PRO_MIN_VERSION, '>=' );
	
}

function isPysFreeActive() {
    
    if ( ! function_exists( 'is_plugin_active' ) ) {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    }
    
    return is_plugin_active( 'pixelyoursite/facebook-pixel-master.php' );
    
}

function pysFreeVersionIsCompatible() {
    
    if ( ! function_exists( 'get_plugin_data' ) ) {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    }
    
    $data = get_plugin_data( WP_PLUGIN_DIR . '/pixelyoursite/facebook-pixel-master.php', false, false );
    
    return version_compare( $data['Version'], PYS_BING_FREE_MIN_VERSION, '>=' );
    
}
