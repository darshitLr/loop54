<?php

namespace PixelYourSite\Bing;

use PixelYourSite;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function maybeMigrate() {
	
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return;
	}
	
	if ( ! is_admin() || ! current_user_can( 'manage_options' ) ) {
		return;
	}
	
	$bing_version = get_option( 'pys_bing_version', false );

	// first install
    if ( ! $bing_version ) {

        update_option( 'pys_bing_version', PYS_BING_VERSION );

        return;

    }

}
