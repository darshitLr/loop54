<?php

namespace PixelYourSite;

use PixelYourSite\Events;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * @var CustomEvent $event
 */

?>

<div class="card card-static">
    <div class="card-header">
        Bing
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
				<?php Events\renderSwitcherInput( $event, 'bing_enabled' ); ?>
                <h4 class="switcher-label">Enable on Bing</h4>
            </div>
        </div>
        <div id="bing_panel">
            <div class="row mt-3">
                <?php if(function_exists('PixelYourSite\Events\renderBingEventId')) : ?>
                    <label class="col-5 control-label">Fire for:</label>
                    <div class="col-4"><?php Events\renderBingEventId( $event, 'bing_pixel_id' ); ?></div>
                <?php endif; ?>
            </div>
            <div class="row mt-3">
                <div class="col ">
                    <div class="row mb-3">
                        <label class="col-5 control-label">Action</label>
                        <div class="col-4">
                            <?php Events\renderTextInput( $event, 'bing_event_action' ); ?>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-5 control-label">Category</label>
                        <div class="col-4">
                            <?php Events\renderTextInput( $event, 'bing_event_category' ); ?>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-5 control-label">Label</label>
                        <div class="col-4">
                            <?php Events\renderTextInput( $event, 'bing_event_label' ); ?>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-5 control-label">Value</label>
                        <div class="col-4">
                            <?php Events\renderTextInput( $event, 'bing_event_value' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>