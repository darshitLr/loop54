/* global pysOptions */

! function ($) {



    var Bing = function () {


        var initialized = false;

        function getUtils() {
            return window.pys.Utils;
        }

        function getOptions() {
            return window.pysOptions;
        }

        function fireEvent(name, event) {
            if(typeof window.pys_event_data_filter === "function" && window.pys_disable_event_filter(name,'bing')) {
                return;
            }
            var params = getUtils().copyProperties(event.params, {});
            getUtils().copyProperties(getUtils().getRequestParams(), params);
            if (getOptions().debug) {
                console.log('[Bing] ' + name, params);
            }

            window.uetq = window.uetq || [];
            window.uetq.push('event', name, params);

        }

        /**
         * Public API
         */
        return {
            tag: function() {
                return "bing";
            },
            isEnabled: function () {
                return getOptions().hasOwnProperty('bing');
            },

            disable: function () {
                initialized = false;
            },

            /**
             * Load pixel's JS
             *
             * @link: https://developers.pinterest.com/docs/ad-tools/enhanced-match/
             */
            loadPixel: function () {

                if (initialized || !this.isEnabled() || !getUtils().consentGiven('bing')) {
                    return;
                }

                var tagId = getOptions().bing.pixelId;

                // load base tag
                (function (w, d, t, r, u) {
                    var f, n, i;
                    w[u] = w[u] || [], f = function () {
                        var o = { ti: tagId };
                        o.q = w[u], w[u] = new UET(o), w[u].push("pageLoad")
                    }, n = d.createElement(t), n.src = r, n.async = 1, n.onload = n.onreadystatechange = function () {
                        var s = this.readyState;
                        s && s !== "loaded" && s !== "complete" || (f(), n.onload = n.onreadystatechange = null)
                    }, i = d.getElementsByTagName(t)[0], i.parentNode.insertBefore(n, i)
                })(window, document, "script", "//bat.bing.com/bat.js", "uetq");

                initialized = true;

                getUtils().fireStaticEvents('bing');

            },

            fireEvent: function (name, data) {

                if (!initialized || !this.isEnabled()) {
                    return false;
                }

                data.delay = data.delay || 0;
                data.params = data.params || {};

                if (data.delay === 0) {

                    fireEvent(name, data);

                } else {

                    setTimeout(function (name, params) {
                        fireEvent(name, params);
                    }, data.delay * 1000, name, data);

                }

                return true;

            },

            onAdSenseEvent: function () {
                // not supported
            },

            onClickEvent: function (event) {

                if (initialized && this.isEnabled() ) {

                    this.fireEvent(event.name, event);

                }

            },

            onWatchVideo: function (event) {

                if (initialized && this.isEnabled() ) {
                    this.fireEvent(event.name, event);
                }

            },

            onCommentEvent: function (event) {

                if (initialized && this.isEnabled() ) {
                    this.fireEvent(event.name, event);
                }

            },

            onFormEvent: function (event) {

                if (initialized && this.isEnabled() ) {
                    this.fireEvent(event.name, event);
                }

            },

            onDownloadEvent: function (event) {

                if (initialized && this.isEnabled() ) {
                    this.fireEvent(event.name, event);
                }

            },

            onWooAddToCartOnButtonEvent: function (product_id) {
                if(!getOptions().dynamicEvents.woo_add_to_cart_on_button_click.hasOwnProperty(this.tag()))
                    return;

                if (window.pysWooProductData.hasOwnProperty(product_id)) {
                    if (window.pysWooProductData[product_id].hasOwnProperty('bing')) {

                        var event = getUtils().clone(getOptions().dynamicEvents.woo_add_to_cart_on_button_click[this.tag()])
                        getUtils().copyProperties(window.pysWooProductData[product_id][this.tag()].params, event.params)


                        this.fireEvent(event.name, event);
                    }
                }

            },

            onWooAddToCartOnSingleEvent: function (product_id, qty, product_type, is_external, $form) {

                window.pysWooProductData = window.pysWooProductData || [];
                if(!getOptions().dynamicEvents.woo_add_to_cart_on_button_click.hasOwnProperty(this.tag()))
                    return;


                if (window.pysWooProductData.hasOwnProperty(product_id)) {
                    if (window.pysWooProductData[product_id].hasOwnProperty('bing')) {
                        var event = getUtils().clone(getOptions().dynamicEvents.woo_add_to_cart_on_button_click[this.tag()])
                        getUtils().copyProperties(window.pysWooProductData[product_id]['bing'].params, event.params);

                        if(product_type === getUtils().PRODUCT_GROUPED ) {
                            var quantity = 0
                            $form.find(".woocommerce-grouped-product-list .qty").each(function(index){
                                var qty =parseInt($(this).val());
                                if(isNaN(qty)) {
                                    qty = 0;
                                }
                                quantity += qty;
                            });
                            if(quantity == 0) return;// skip if no items selected
                        }

                        this.fireEvent(event.name, event);
                    }
                }

            },

            onWooRemoveFromCartEvent: function (cart_item_hash) {
                // not supported
            },

            onWooAffiliateEvent: function (product_id) {
                // not supported
            },

            onWooPayPalEvent: function (event) {
                // not supported
            },

            onEddAddToCartOnButtonEvent: function (download_id, price_index, qty) {
                if(!getOptions().dynamicEvents.edd_add_to_cart_on_button_click.hasOwnProperty(this.tag()))
                    return;
                var event = getOptions().dynamicEvents.edd_add_to_cart_on_button_click[this.tag()];

                if (window.pysEddProductData.hasOwnProperty(download_id)) {

                    if (window.pysEddProductData[download_id].hasOwnProperty(download_id)) {
                        if (window.pysEddProductData[download_id][download_id].hasOwnProperty('bing')) {
                            event = getUtils().copyProperties(event, {})
                            getUtils().copyProperties(window.pysEddProductData[download_id][download_id]['bing'].params, event.params);

                            this.fireEvent(event.name, event);

                        }
                    }

                }

            },

            onEddRemoveFromCartEvent: function (item) {
                // not supported
            },
            onPageScroll: function (event) {
                if (initialized && this.isEnabled()) {
                    this.fireEvent(event.name, event);
                }
            },
            onTime: function (event) {
                if (initialized && this.isEnabled()) {
                    this.fireEvent(event.name, event);
                }
            },
        };

    }();

    window.pys = window.pys || {};
    window.pys.Bing = Bing;


}(jQuery);