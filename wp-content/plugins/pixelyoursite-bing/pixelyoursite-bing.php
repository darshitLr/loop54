<?php

/**
 * Plugin Name: PixelYourSite Microsoft UET (Bing)
 * Plugin URI: http://www.pixelyoursite.com/
 * Description: Manage your Microsoft UET Tag (Bing): automatically add it to any page, fire global events, integrate with WooCommerce or Easy Digital Downloads. This add-on requires the PixelYourSite plugin.
 * Version: 3.0.0
 * Author: PixelYourSite
 * Author URI: http://www.pixelyoursite.com
 * License URI: http://www.pixelyoursite.com/pixel-your-site-pro-license
 *
 * Requires at least: 4.4
 * Tested up to: 5.9
 *
 * WC requires at least: 2.6.0
 * WC tested up to: 6.4
 *
 * Text Domain: pys
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

use PixelYourSite\Bing;

define( 'PYS_BING_VERSION', '3.0.0' );
define( 'PYS_BING_PRO_MIN_VERSION', '8.0.0' );
define( 'PYS_BING_FREE_MIN_VERSION', '8.0.0' );
define( 'PYS_BING_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'PYS_BING_URL', untrailingslashit( plugin_dir_url( __FILE__ ) ) );
define( 'PYS_BING_PLUGIN_FILE', __FILE__ );

require_once 'modules/bing/functions-common.php';
require_once 'modules/bing/functions-helpers.php';
require_once 'modules/bing/functions-admin.php';
require_once 'modules/bing/functions-migrate.php';

register_activation_hook( __FILE__, 'pysBingActivation' );
function pysBingActivation() {
    if ( Bing\isPysProActive() ) {
        if ( ! Bing\pysProVersionIsCompatible() ) {
            wp_die( 'PixelYourSite Bing requires PixelYourSite PRO version ' . PYS_BING_PRO_MIN_VERSION . ' or newer.',
                'Plugin Activation',
                array(
                    'back_link' => true,
                ) );
        }
    } elseif ( Bing\isPysFreeActive() ) {
        if ( ! Bing\pysFreeVersionIsCompatible() ) {
            wp_die( 'PixelYourSite Bing requires PixelYourSite Free version ' . PYS_BING_FREE_MIN_VERSION . ' or newer.',
                'Plugin Activation',
                array(
                    'back_link' => true,
                ) );
        }
    } else {
        wp_die( '<p>PixelYourSite Bing Add-on needs <a href="https://www.pixelyoursite.com/facebook-pixel-plugin/buy-pixelyoursite-pro"
               target="_blank">PixelYourSite PRO</a> or <a href="https://wordpress.org/plugins/pixelyoursite/"
               target="_blank">Free</a> in order to work. Activate it now.</p>',
            'Plugin Activation',
            array(
                'back_link' => true,
            ) );
    }
}

/**
 * Initialize Bing plugin instance.
 * Should be loaded before PYS core ( init, 9 ) to prevent dummy Bing plugin usage
 */
if ( Bing\isPysProActive() ) {
    if ( Bing\pysProVersionIsCompatible() ) {
        add_action( 'init', function() {
            require_once 'modules/bing/bing.php';
        }, 8 );
    } else {
        add_action( 'admin_notices', 'PixelYourSite\Bing\adminNoticePysProOutdated' );
    }
} elseif ( Bing\isPysFreeActive() ) {
    if ( Bing\pysFreeVersionIsCompatible() ) {
        add_action( 'init', function() {
            require_once 'modules/bing/bing.php';
        }, 8 );
    } else {
        add_action( 'admin_notices', 'PixelYourSite\Bing\adminNoticePysFreeOutdated' );
    }
} else {
    add_action( 'admin_notices', 'PixelYourSite\Bing\adminNoticePysCoreNotActive' );
}
