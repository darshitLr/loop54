<?php
/*
Plugin Name: Enable WooCommerce GPF feed item cache.
Plugin URI: http://docs.woothemes.com/document/google-product-feed/
Description: Enables feed item caching in the Google Product Feed extension.
Version: 1.0
Author: Lee Willis
Author URI: https://plugins.leewillis.co.uk/ 
*/
add_filter( 'woocommerce_gpf_render_cache_enabled', '__return_true' );