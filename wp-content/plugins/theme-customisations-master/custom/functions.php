<?php
/**
 * Functions.php
 *
 * @package  Theme_Customisations
 * @author   WooThemes
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * functions.php
 * Add PHP snippets here
 */
//remove_action( 'woocommerce_after_main_content', 'flatsome_pages_in_search_results', 10 );
// Add Pages and blog posts to top of search results if set.
function relevanssi_pages_in_search_results() {
	if ( ! is_search() || ! get_theme_mod( 'search_result', 1 ) ) {
		return;
	}
	global $post;
	if ( get_search_query() ) {
		$args = array(
			'post_type' => 'post',
			's'         => get_search_query(),
		);
		$query = new WP_Query();
		$query->parse_query( $args );
		relevanssi_do_query( $query );
		$posts = array();
		while ( $query->have_posts() ) {
			$query->the_post();
			array_push( $posts, $post->ID );
		}
		$args = array(
			'post_type' => 'page',
			's'         => get_search_query(),
		);
		$query = new WP_Query();
		$query->parse_query( $args );
		relevanssi_do_query( $query );
		$pages = array();
		while ( $query->have_posts() ) {
			$query->the_post();
			$wc_page = false;
			if ( 'page' === $post->post_type ) {
				foreach ( array( 'shop', 'cart', 'checkout', 'view_order', 'terms' ) as $wc_page_type ) {
					if ( $post->ID === wc_get_page_id( $wc_page_type ) ) {
						$wc_page = true;
					}
				}
			}
			if ( ! $wc_page ) {
				array_push( $pages, $post->ID );
			}
		}
		do_action( 'flatsome_products_page_loader' );
		if ( ! empty( $posts ) || ! empty( $pages ) ) {
			$list_type = get_theme_mod( 'search_result_style', 'slider' );
			if ( ! empty( $posts ) ) {
				echo '<hr/><h4 class="uppercase">' . __( 'Posts found', 'flatsome' ) . '</h4>' . do_shortcode( '[blog_posts columns="3" columns__md="3" columns__sm="2" type="' . $list_type . '" image_height="16-9" ids="' . implode( ',', $posts ) . '"]' );
			}
			if ( ! empty( $pages ) ) {
				echo '<hr/><h4 class="uppercase">' . __( 'Pages found', 'flatsome' ) . '</h4>' . do_shortcode( '[ux_pages columns="3" columns__md="3" columns__sm="2" type="' . $list_type . '" image_height="16-9" ids="' . implode( ',', $pages ) . '"]' );
			}
		}
	}
}
//add_action( 'woocommerce_after_main_content', 'relevanssi_pages_in_search_results', 10 );
  /**
 * Add a standard $ value surcharge to all transactions in cart / checkout
 */
//add_action( 'woocommerce_cart_calculate_fees','wc_add_surcharge' ); 
function wc_add_surcharge() { 

	if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
		return;
	}
    $chosen_shipping_method_id = WC()->session->get( 'chosen_shipping_methods' )[0];
    $chosen_shipping_method    = explode( ':', $chosen_shipping_method_id )[0];
 
    if ( $chosen_shipping_method == 'table_rate' ) {
		// change the $fee to set the surcharge to a value to suit
		$fee = 47.20;

		WC()->cart->add_fee( 'Expeditionsavgift', $fee, true, 'standard' );  
    }
	

}

function redlight_disable_alt_product_thumb( $image ) {
    return "";
}
add_filter('flatsome_woocommerce_get_alt_product_thumbnail', 'redlight_disable_alt_product_thumb', 10, 1 ); 
/**
 * Trim zeros in price decimals
 **/
 add_filter( 'woocommerce_price_trim_zeros', '__return_true' );
/**
 * @snippet       WooCommerce - Adds ADDON to Shipment
 * @author        Redlight Media AB / Christopher Hedqvist
 * @compatible    WooCommerce 3.9.0
 */
function redlight_unifaun_bhp_return_print( $shipment, $order ) {
    // Only Schenker Ombud
  if( 'BHP' === $shipment['service']['id'] ){
    $shipment['service']['addons'][] = array(
      'id' => 'RETPP',
      'misc' => 'PRINT'
    );
  }
  return $shipment;
}
//add_filter('ac_wc_unifaun_shipment_data', 'redlight_unifaun_bhp_return_print', 10, 2 ); 
/**
 * @snippet       WooCommerce - Sets size of shippinglabel to 107x190 (thermo-190)
 * @author        Redlight Media AB / Christopher Hedqvist
 * @compatible    WooCommerce 3.5.3
 */
function redlight_unifaun_pacsoft_pdf_label( $pdf_config, $order ) {

    //$pdf_config['target1Media'] = 'laser-ste';
    
    return $pdf_config;
}
add_filter('ac_wc_unifaun_shipment_pdf_config_data', 'redlight_unifaun_pacsoft_pdf_label', 10, 2 ); 
/**
 * @snippet       WooCommerce - Print to different tray
 * @author        Redlight Media AB / Christopher Hedqvist
 * @compatible    WooCommerce 4.4.0
 */
function redlight_print_labels_to_printer_tray($printjob, $order) {
    if( strpos($printjob['title'], 'Packingslip') !== false) { 
     return $printjob;
    }
    if( strpos($printjob['title'], 'Invoice') !== false) { 
     return $printjob;
    }
    
    //$printjob['options']['bin'] = 'Kassett 3';
    
    return $printjob;
}
add_filter('redlight_wc_automatic_printing_data', 'redlight_print_labels_to_printer_tray', 10, 2 ); 
/**
  * @snippet       WooCommerce - Unifaun plugin by Redlight Media - Add Track & Trace url to completed email
  * @author        Redlight Media AB / Christopher Hedqvist
  * @compatible    WooCommerce 3.8.1
  */
function redlight_add_tracking_url_to_completed_email( $order, $sent_to_admin, $plain_text, $email ) {
   if ( $email->id == 'customer_completed_order' ) {
       $track_and_trace_url = sprintf(
				'<strong>%1$s</strong> <a href="%2$s" target="_blank">%3$s</a>',
				 __( 'För att spåra din försändelse så kan du använda följande länk:'),
				 wc_clean( WC_Unifaun_Shipping::generate_track_and_trace_url( $order->get_id() ) ),
				 __( 'Spårningslänk' )
			);
       echo '<p>' . $track_and_trace_url . '</p>';
   }
}
//add_action( 'woocommerce_email_before_order_table', 'redlight_add_tracking_url_to_completed_email', 20, 4 );
/**
 * Filter the document table headers to add a product thumbnail header
 *
 * @param array $table_headers Table column headers
 * @return array The updated table column headers
 */
function sv_wc_pip_document_table_headers_product_thumbnail( $table_headers ) {

    $thumbnail_header = array( 'product_thumbnail' => '' );

    // add product thumbnail column as the first column
    return array_merge( $thumbnail_header, $table_headers );
}
add_filter( 'wc_pip_document_table_headers', 'sv_wc_pip_document_table_headers_product_thumbnail' );
/**
 * Filter the document table row cells to add product thumbnail column data
 *
 * @param string $table_row_cells The table row cells.
 * @param string $type WC_PIP_Document type
 * @param string $item_id Item id
 * @param array $item Item data
 * @param \WC_Product $product Product object
 * @return array The filtered table row cells.
 */
function sv_wc_pip_document_table_row_cells_product_thumbnail( $table_row_cells, $document_type, $item_id, $item, $product ) {

    // get the product's or variation's thumbnail; you may want to set the size depending on the default shop_thumbnail size
	if($product){
		$thumbnail_content = array( 'product_thumbnail' => $product->get_image( array( 75, 75 ) ) );
	}else{
		$thumbnail_content = array( 'product_thumbnail' =>'');
	}

    // add product thumbnail column as the first column
    return array_merge( $thumbnail_content, $table_row_cells );
}
add_filter( 'wc_pip_document_table_row_cells', 'sv_wc_pip_document_table_row_cells_product_thumbnail', 10, 5 );
//add_action( 'wpo_wcpdf_before_item_meta', 'redlight_shelf_location', 10, 3 );
add_action( 'wpo_wcpdf_after_item_meta', 'redlight_shelf_location', 10, 3 );

function redlight_shelf_location ($template_type, $item, $order) {
    if(!empty($item['variation_id'])){
      $product_id = $item['variation_id'];
    }else{
      $product_id = $item['product_id'];
    }
    $wc_product = wc_get_product($product_id);
    if(!$wc_product){
      return;
    }
    $shelf_location = $wc_product->get_meta('_shelf_location');
    if(!empty($shelf_location)){
      ?>
      <dt class="shelf-location">Lagerplats: <?php echo $shelf_location; ?></dt>
      <?php
    }
}
function redlight_barcode_scanner_wc_barcode( $template_type, $order ) {
	echo '<div class="barcode" style="width:250px;">';
	echo WC_Order_Barcodes()->display_barcode( $order->get_id(), true );
	echo '</div>';
}
add_action( 'wpo_wcpdf_after_document_label', 'redlight_barcode_scanner_wc_barcode', 10, 2 );

//TEMPORARY BUGFIX FOR WOOCOMMERCE "PHONING HOME" - REMOVE AFTER WOOC FIXES
add_filter('woocommerce_admin_features', 'pk_woocommerce_admin_features');
function pk_woocommerce_admin_features($features) {
	if(($key = array_search('remote-inbox-notifications', $features)) !== false) {
		unset($features[$key]);
	}
	return $features ; 
}
function add_ready_to_send_status( $statuses ){
	if(!isset($statuses['wc-redo-att-skickas'])){
		//$statuses['wc-redo-att-skickas'] = 'Redo att skickas';
	}

    
    return $statuses;
}
add_filter( 'wc_order_statuses', 'add_ready_to_send_status',5 );


add_filter('gettext', 'translate_in_stock');
add_filter('ngettext', 'translate_in_stock');
function translate_in_stock($translated) { 
	$translated = str_ireplace('I Lager', 'I webblager', $translated);
	return $translated;
}
/**
 * @snippet       WooCommerce - Adds Google Survey on thankyou-page.
 * @author        Redlight Media AB / Christopher Hedqvist
 * @compatible    WooCommerce 4.7.0
 */
add_action( 'woocommerce_thankyou', 'google_survey_optin' );
function google_survey_optin( $order_id ) {
	// Lets grab the order
	$order = wc_get_order( $order_id );
	$data = array(
		"merchant_id" =>  120500409,
		"order_id" => $order->get_id(),
		"email" => $order->get_billing_email(),
		"delivery_country" => $order->get_billing_country(),
		"estimated_delivery_date" => date('Y-m-d', strtotime('+3 weekday', time())) 
	);
	$gsurvey_data = json_encode($data);
	?>
	<script src="https://apis.google.com/js/platform.js?onload=renderOptIn" async defer></script>
	<script>
		
	window.renderOptIn = function() {
		window.gapi.load('surveyoptin', function() {
			window.gapi.surveyoptin.render(<?php echo $gsurvey_data;?>);
		});
	}
	</script>
	<?php
}
//add_action('woocommerce_admin_order_data_after_order_details','display_link_to_order_thankyou_page');
function display_link_to_order_thankyou_page($order){
	printf(
		'<a href="%s">%s</a>',
		esc_url( $order->get_checkout_order_received_url() ),
		__( 'Customer order confirmation page &rarr;', 'woocommerce' )
	);
}
