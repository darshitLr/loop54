<?php

function processProduct( &$xw, &$product_meta_data, &$product, &$currency, &$profitmetrics_inhertit_cost_price_variation )
{
	$price = $product->get_price();
	$stock_status = $product->get_stock_status() == 'instock' ? 1 : 0;

	xmlwriter_start_element( $xw,'item' );

	xmlwriter_write_element( $xw, 'g:id', $product->get_id() );
	xmlwriter_write_element( $xw, 'title', $product->get_name() );

	$image_link = wp_get_attachment_image_url( $product->get_image_id(), 'full' );
	if( $image_link ) {
			xmlwriter_write_element( $xw, 'g:image_link', $image_link );
	}
	xmlwriter_write_element( $xw, 'g:price', $price );
	xmlwriter_write_element( $xw, 'pm:price_currency', $currency );
	if( array_key_exists( $product->get_id(), $product_meta_data ) ) {
			xmlwriter_write_element( $xw, 'pm:price_buy', $product_meta_data[$product->get_id()] );
			xmlwriter_write_element( $xw, 'pm:price_buy_currency', $currency );
	} else if ( $profitmetrics_inhertit_cost_price_variation == 'yes' ) {
			if( array_key_exists( $product->get_parent_id(), $product_meta_data ) ) {
					xmlwriter_write_element( $xw, 'pm:price_buy', $product_meta_data[$product->get_parent_id()] );
					xmlwriter_write_element( $xw, 'pm:price_buy_currency', $currency );
			}
	}

	xmlwriter_write_element( $xw, 'pm:num_stock', $product->get_stock_quantity() ? $product->get_stock_quantity() : $stock_status );

	xmlwriter_write_element( $xw, 'link', $product->get_permalink() );

	xmlwriter_end_element( $xw ); // item
}

/**
 * Template Name: Custom RSS Template - Product Feed
 */
if ( is_ssl() ) {
	//$num_queries_start = get_num_queries();

	//$start = microtime(true);

	//echo "\nSTART ------------------- " . memory_get_usage() . " | " . memory_get_peak_usage()  . " ---------------------------\n";


	$query = new WC_Product_Query();
	$query_vars = $query->get_query_vars();
	$query_vars['type'][] = 'variation';

	// The user can set her/his own cost meta price key, that stores tha cost price, in woocommerce profitmetrics settings.
	$profitmetrics_cost_price = get_option( 'profitmetrics_cost_price', false );
	$cost_price_key = empty( $profitmetrics_cost_price ) ? '_cost_price' : $profitmetrics_cost_price;

	// The user can set her/his own cost meta price key, that stores tha cost price, in woocommerce profitmetrics settings.
	$profitmetrics_cost_price_variation = get_option( 'profitmetrics_cost_price_variation', false );
	$cost_price_variation_key = empty( $profitmetrics_cost_price_variation ) ? '_cost_price_variation' : $profitmetrics_cost_price_variation;

	$profitmetrics_inhertit_cost_price_variation = get_option( 'profitmetrics_inherit_cost_price_variation', false );

	$char_set = get_option( 'blog_charset' );


	global $wpdb;

	$product_meta_data = $wpdb->get_results(
		"SELECT * FROM $wpdb->postmeta WHERE (meta_key = '$cost_price_key' OR meta_key = '$cost_price_variation_key')"
	);

	$product_meta_data = wp_list_pluck($product_meta_data, 'meta_value', 'post_id');
	//echo "\nGET METADATA ------------------- " . memory_get_usage() . " | " . memory_get_peak_usage()  . " ---------------------------\n";


	//$baseurl = wp_get_upload_dir()['baseurl'] . '/';
	$currency = get_woocommerce_currency();

	$xw = xmlwriter_open_memory();
	xmlwriter_set_indent( $xw, 1 );
	$res = xmlwriter_set_indent_string( $xw, '	' );
	xmlwriter_start_document( $xw, '1.0', $char_set );

	xmlwriter_start_element( $xw,'rss' );

	xmlwriter_start_attribute( $xw,'xmlns:g' );
	xmlwriter_text( $xw, 'http://base.google.com/ns/1.0' );
	xmlwriter_end_attribute( $xw );

	xmlwriter_start_attribute( $xw,'xmlns:pm' );
	xmlwriter_text( $xw, 'https://my.profitmetrics.io/ns/1.0' );
	xmlwriter_end_attribute( $xw );

	xmlwriter_start_element( $xw,'channel' );

	///////////////////
	$pgn_numpage = 0;

	while( true ) {
		$pgn_offset = $pgn_numpage++ * 3000;

		$query = new WC_Product_Query(array('type' => $query_vars['type'], 'posts_per_page' => 3000, 'offset' => $pgn_offset, 'post_status' => 'publish'));

		$products = $query->get_products();
		//echo "\nGOT PRODUCTS ------------------- " . memory_get_usage() . " | " . memory_get_peak_usage()  . " ---------------------------\n";

		if( $products ) {
			foreach( $products as &$product ) {
				if( $product ) {
					processProduct( $xw, $product_meta_data, $product, $currency, $profitmetrics_inhertit_cost_price_variation );
				}
			}
		} else {
			break;
		}

		//wp_suspend_cache_addition( false );
		wp_cache_flush();
	}
	//////////////////////////////////

	xmlwriter_end_element( $xw ); // channel
	xmlwriter_end_element( $xw ); // rss
	xmlwriter_end_document( $xw );


	header( sprintf( 'Content-Type: %s; charset=%s', feed_content_type( 'rss-http' ), $char_set ), true );
	echo xmlwriter_output_memory( $xw );

	//$num_queries_end = get_num_queries();

	//if (PM_DEBUG) error_log('num_queries_start: ' . $num_queries_start . "\nnum_queries_end: " . $num_queries_end);

	//if (PM_DEBUG) error_log('time_elapsed_secs: ' . (microtime(true) - $start));

	//echo "\nEND ------------------- " . memory_get_usage() . " | " . memory_get_peak_usage()  . " ---------------------------\n";
}
