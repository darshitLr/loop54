<?php
/**
 * Plugin Name: WooCommerce Profitmetrics
 * Plugin URI: https://profitmetrics.io/
 * Description: Adds Profitmetrics integration to WooCommerce.
 * Version: 2.18
 * Author: Profitmetrics
 * Author URI: https://profitmetrics.io/
 * License: GPL2
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
* Check if WooCommerce is active
*
**/

if ( ! class_exists( 'WC_Profitmetrics' ) ) :

if (isset($_SERVER['PM_DEBUG']))
	define('PM_DEBUG', true);
else
	define('PM_DEBUG', false);

class WC_Profitmetrics {

	/*
	* Plugin Version.
	*/
	const VERSION = '2.18';

	/**
	* Construct the plugin.
	*/
	public function __construct() {
		add_filter( 'generate_rewrite_rules', array( $this, 'add_endpoints' ), 10, 1 );
		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 10 , 1 );
		add_filter( 'request', array( $this, 'add_query_vars_endpoints'), 10, 1 );
		add_action( 'template_redirect', array( $this, 'load_templates' ), 10, 1 );

		// Save custom site feed unique ID
		register_activation_hook( __FILE__, array( $this, 'save_feed_url_plugin_activation' ) );

		add_action( 'plugins_loaded', array( $this, 'init' ) );
	}

	/**
	* Create custom rewriet endpoint
	*/
	public function add_endpoints ( $wp_rewrite ) {
		$wp_rewrite->rules = array_merge(
			['productsfeed/?$' => 'index.php?id', // 'productsfeed/?$' => 'index.php?pmprodfeed',
			'pmtptrack/?$' => 'index.php?pmtptrack'],
			$wp_rewrite->rules
		);

	}

	/**
	* Add custom url parameter
	*/
	public function add_query_vars ( $query_vars ) {
		$query_vars[] = 'id';
		$query_vars[] = 'pmprodfeed';
		$query_vars[] = 'pmtptrack';
		return $query_vars;
	}

	/*
	* Add query vars endpoints
	*/
	function add_query_vars_endpoints($vars) {
		if (isset($vars['pmprodfeed'])) {
			$vars['pmprodfeed'] = true;
		}

		if (isset($vars['pmtptrack'])) {
			$vars['pmtptrack'] = true;
		}

		return $vars;
	}

	/**
	* Load xml feed template
	*/
	public function load_templates() {
		if (get_query_var('id' /*'pmprodfeed'*/)) {
			if (get_query_var('id') == get_option('profitmetrics_xml_feed_url', false)) {
				ob_start();

				include plugin_dir_path( __FILE__ ) . 'template/product-feed.php';

				$pmprodfeed = trim( ob_get_clean() );
				while( ob_get_level() != 0 ) {
					ob_end_clean();
				}
				die( $pmprodfeed );
			}
		}

		if( get_query_var( 'pmtptrack' ) ) {
			ob_start();

			include plugin_dir_path( __FILE__ ) . 'template/pmtptrack.php';

			$pmtptrack = trim( ob_get_clean() );
			while( ob_get_level() != 0 ) {
				ob_end_clean();
			}
			die( $pmtptrack );
		}
	}

	/**
	* Initialize the plugin.
	*/
	public function init() {
		// add_action( 'init', array( $this, 'load_xml_feed' ) );

		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		// Checks if WooCommerce is installed.
		if ( class_exists( 'WC_Integration' ) && defined( 'WOOCOMMERCE_VERSION' ) && version_compare( WOOCOMMERCE_VERSION, '2.0.0', '>=' ) ) {
			// Include our integration class.
			include_once 'classes/woocommerce-profitmetrics-core.php';
			include_once 'classes/woocommerce-profitmetrics-settings-tab.php';

			// Register the integration.
			add_filter( 'woocommerce_integrations', array( $this, 'add_integration' ) );

		} else {
			// throw an admin error if you like
			add_action( 'admin_notices', array( $this, 'woocommerce_missing_notice' ) );
		}

		add_action( 'wp_enqueue_scripts', array( $this, 'load_scripts_profitmetrics' ) );
	}


	// Set default values here
	function save_feed_url_plugin_activation() {
		// Form settings
		if ( empty( get_option( 'profitmetrics_xml_feed_url', false ) ) ) {
			add_option( 'profitmetrics_xml_feed_url', md5( uniqid( rand(), true ) ) );

			// Flush rewrite rules to add custom endpoint
			flush_rewrite_rules();
		}

	}

	/**
	* Never worry about cache again!
	*/
	function load_scripts_profitmetrics() {
		if (!is_wc_endpoint_url('order-received'))
			wp_enqueue_script( 'pmTPTrack', plugins_url( 'js/pmTPTrack.js', __FILE__ ), array(), self::VERSION );
	}

	/*
	* Load the plugin text domain for translation
	*/
	public function load_plugin_textdomain() {

		$locale = apply_filters( 'plugin_locale', get_locale(), 'woocommerce-profitmetrics-integration' );
		load_textdomain( 'woocommerce-profitmetrics-integration', trailingslashit( WP_LANG_DIR ) . 'woocommerce-profitmetrics-integration-' . $locale . '.mo' );
		load_plugin_textdomain( 'woocommerce-profitmetrics-integration', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

	}

	/**
	* Add a new integration to WooCommerce.
	*/
	public function add_integration( $integrations ) {
	// $integrations[] = 'WC_Profitmetrics_Core';
		return $integrations;
	}

	/*
	* Fallback notice
	*/
	public function woocommerce_missing_notice() {
		echo '<div class="error"><p>'.sprintf( __( 'Profitmetrics integration depends on minimum 2.2.6 version of %s to work!', 'woocommerce-profitmetrics-integration' ), '<a href="http://www.woothemes.com/woocommerce/" target="_blank">' . __( 'WooCommerce', 'woocommerce-profitmetrics-integration' ) . '</a>' ).'</p></div>';
	}
}

$WC_Profitmetrics = new WC_Profitmetrics( __FILE__ );

endif;
