# WooCommerce Profitmetrics #
Contributors: profitmetrics
Tags: woocommerce
Requires at least: 3.8
Tested up to: 4.7
Stable tag: 4.2
Version: 2.18
Requires WooCommerce at least: 2.2.6
Tested WooCommerce up to: 2.3.8
License: GPLv2

Adds ProfitMetrics integration to WooCommerce.

# Installation #

1. Upload the entire 'woocommerce-profitmetrics' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
