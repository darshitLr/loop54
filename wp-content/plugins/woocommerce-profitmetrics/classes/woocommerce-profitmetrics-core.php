<?php

/**
* Exit if accessed directly
*
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'WC_Profitmetrics_Core' ) ):
class WC_Profitmetrics_Core extends WC_Integration {
	protected static $product_type = array( 'simple', 'variable', 'grouped' );

	/*
	* Construct
	*/
	public function __construct() {
		// Handle when an order is paid on order creation.
		add_action('woocommerce_new_order', function ($order_id) {
			if (PM_DEBUG) error_log('woocommerce_new_order');

			$order = new WC_Order($order_id);

			$order->update_meta_data('_pm_order', true);
			$order->update_meta_data('_pm_order_status', 'pending');
			$order->save();

			$this->save_order($order_id);
		}, 10, 1);

		// Handle when an order is not paid on order creation.
		add_action('woocommerce_order_status_changed', function ($order_id, $old_status, $new_status) {
			if (PM_DEBUG) error_log('woocommerce_order_status_changed');
	
			$this->save_order($order_id);
		}, 10, 3);

		// Add cost price input field on edit product
		add_action( 'woocommerce_product_options_pricing', array( $this, 'cost_price_simple_product_field' ) );
		add_action( 'woocommerce_process_product_meta', array( $this, 'cost_price_simple_product_field_save' ) );

		// Add cost price field to variations on edit product
		add_action( 'woocommerce_variation_options_pricing', array( $this, 'cost_price_variation_product_field' ), 10, 3 );
		add_action( 'woocommerce_save_product_variation', array( $this, 'cost_price_variation_product_field_save' ), 10, 2 );

		// pmTPTrack
		//add_action('template_redirect', array($this, 'on_page_load'));
		//add_action('wp_loaded', array($this, 'on_page_load'));
		add_action('wp', array($this, 'on_page_load'));
		add_action('woocommerce_checkout_create_order', array($this, 'on_create_order'), 20, 2);

		/*add_action( 'all', function () {
			if (PM_DEBUG) error_log(current_action());
		});*/

		// Bulk edit
		add_action( 'woocommerce_variable_product_bulk_edit_actions', array($this, 'profitmetrics_variable_product_bulk_edit_actions'), 10);
		add_action( 'woocommerce_bulk_edit_variations_default', array($this, 'profitmetrics_bulk_edit_variations'), 10, 4 );
	}

	function profitmetrics_variable_product_bulk_edit_actions() {
		?>
		<optgroup label="<?php esc_attr_e( 'Profitmetrics', 'woocommerce' ); ?>">
		<option value="variable_cost_price"><?php esc_html_e('Set cost prices', 'woocommerce' ); ?></option>
		</optgroup>
		<script>
		jQuery(function ($) {
			jQuery('.wc-metaboxes-wrapper').on('click', 'a.bulk_edit', function(event) {
				var do_variation_action = jQuery('select.variation_actions').val();

				if ( do_variation_action == 'variable_cost_price' ) {
					var value = window.prompt(woocommerce_admin_meta_boxes_variations.i18n_enter_a_value);

					var data = {};

					if (value != null) {
						data.value = value;
					} else {
						return;
					}

					//wc_meta_boxes_product_variations_ajax.block();
					jQuery( '#woocommerce-product-data' ).block({
						message: null,
						overlayCSS: {
							background: '#fff',
							opacity: 0.6
						}
					});

					$.ajax({
						url: woocommerce_admin_meta_boxes_variations.ajax_url,
						data: {
							action:       'woocommerce_bulk_edit_variations',
							security:     woocommerce_admin_meta_boxes_variations.bulk_edit_variations_nonce,
							product_id:   woocommerce_admin_meta_boxes_variations.post_id,
							product_type: $( '#product-type' ).val(),
							bulk_action:  do_variation_action,
							data:         data
						},
						type: 'POST',
						success: function() {
							//wc_meta_boxes_product_variations_pagenav.go_to_page( 1, changes );
							jQuery('.variations-pagenav .page-selector').val(1).first().change();
						}
					});

					//wc_meta_boxes_product_variations_ajax.unblock();
					jQuery('#woocommerce-product-data').unblock();
				}
			});
		});
		</script>
		<?php
	}

	function profitmetrics_bulk_edit_variations( $bulk_action, $data, $product_id, $variations ) {
		if ( $bulk_action == 'variable_cost_price' ) {
			foreach ( $variations as $variation ) {
				update_post_meta( $variation, '_cost_price_variation', $data['value'] );
			}
		}
	}
	
	/*
	* on_page_load
	*/
	function on_page_load() {
		if (PM_DEBUG) error_log('on_page_load');

		if (is_admin()) {
			if (PM_DEBUG) error_log('on_page_load: is_admin is true');
			return;
		}

		if (!WC()->session) {
			return;
		}

		if (WC()->session && !WC()->session->has_session()) {
			if (PM_DEBUG) error_log('on_page_load: has_session is false');

			WC()->session->set_customer_session_cookie(true);
		}

		// TrackOrigin
		if (!WC()->session->get('pmOriginTrack')) {
			if (PM_DEBUG) error_log('on_page_load: track origin');

			$pmOriginTrack = array();

			if (isset($_GET['utm_source']))
				$pmOriginTrack['utm_source'] = substr($_GET['utm_source'], 0, 100);

			if (isset($_GET['utm_campaign']))
				$pmOriginTrack['utm_campaign'] = substr($_GET['utm_campaign'], 0, 100);

			if (isset($_GET['utm_medium']))
				$pmOriginTrack['utm_medium'] = substr($_GET['utm_medium'], 0, 100);

			if (isset($_SERVER['HTTP_REFERER'])) {
				$pmOriginTrack['http_referrer'] = substr($_SERVER['HTTP_REFERER'], 0, 2000);
				$pmOriginTrack['http_referrer_length'] = strlen($_SERVER['HTTP_REFERER']);
			}

			if (count($pmOriginTrack) != 0)
				$pmOriginTrack['time'] = time();

			$pmOriginTrackJson = json_encode($pmOriginTrack, JSON_FORCE_OBJECT);
			if( strlen( urlencode( $pmOriginTrackJson ) ) > 4000 && isset( $pmOriginTrack['http_referrer'] ) ) {
				$pmOriginTrack['http_referrer'] = substr( $pmOriginTrack['http_referrer'], 0, 500 );
				$pmOriginTrackJson = json_encode($pmOriginTrack, JSON_FORCE_OBJECT);
			}

			if( strlen( urlencode( $pmOriginTrackJson ) ) > 4000 ) {
				$pmOriginTrack = array( 'error' => 'too long', 'time' => time() );
				$pmOriginTrackJson = json_encode($pmOriginTrack, JSON_FORCE_OBJECT);
			}

			WC()->session->set('pmOriginTrack', $pmOriginTrackJson);
		}

		// Track
		if (!WC()->session->get('pmTPTrack') && isset($_COOKIE['pmTPTrack'])) {
			if (PM_DEBUG) error_log('on_page_load: track');

			$pmTPTrack = json_decode(stripslashes($_COOKIE['pmTPTrack']), true);

			WC()->session->set('pmTPTrack', $pmTPTrack);
		} else if (isset($_COOKIE['pmTPTrack'])) {
			if (PM_DEBUG) error_log('on_page_load: track update');

			$pmTPTrackNew = json_decode(stripslashes($_COOKIE['pmTPTrack']), true);
			$pmTPTrackOld = WC()->session->get('pmTPTrack');

			if ($pmTPTrackNew['timestamp'] > $pmTPTrackOld['timestamp']) {
				if ($pmTPTrackNew['gacid'])
					$pmTPTrackOld['gacid'] = $pmTPTrackNew['gacid'];

				if ($pmTPTrackNew['gacid_source'])
					$pmTPTrackOld['gacid_source'] = $pmTPTrackNew['gacid_source'];

				if ($pmTPTrackNew['fbp'])
					$pmTPTrackOld['fbp'] = $pmTPTrackNew['fbp'];

				if ($pmTPTrackNew['fbc'])
					$pmTPTrackOld['fbc'] = $pmTPTrackNew['fbc'];

				if ($pmTPTrackNew['timestamp'])
					$pmTPTrackOld['timestamp'] = $pmTPTrackNew['timestamp'];

				WC()->session->set('pmTPTrack', $pmTPTrackOld);
			}
		}
		
		// fbclid will always get updated with the most recent found
		if (isset($_GET['fbclid'])) {
			$fbclid = 'fb.1.' . time() . '.' . $_GET['fbclid'];
			WC()->session->set('fbclid', $fbclid);
		}

		// gclid will always get updated with the most recent found
		if (isset($_GET['gclid'])) {
			WC()->session->set('gclid', $_GET['gclid']);
		}

		if (PM_DEBUG) {
			error_log('on_page_load: pmOriginTrack = ' . WC()->session->get('pmOriginTrack'));
			error_log('on_page_load: pmTPTrack = ' . json_encode(WC()->session->get('pmTPTrack')));
		}
	}

	/*
	* on_create_order
	*/
	function on_create_order($order, $data) {
		if (PM_DEBUG) error_log('on_create_order');

		if (WC()->session->get('fbclid')) {
			$order->update_meta_data('_fbclid', WC()->session->get('fbclid'));
		}

		if (WC()->session->get('gclid')) {
			$order->update_meta_data('_gclid', WC()->session->get('gclid'));
		}

		$order->update_meta_data('_pmOriginTrack', WC()->session->get('pmOriginTrack'));
		$order->update_meta_data('_pmTPTrack', WC()->session->get('pmTPTrack'));
	}

	/*
	* save_order
	*/
	public function save_order($order_id) {
		if (PM_DEBUG) error_log('save_order');

		$order = new WC_Order($order_id);

		if (PM_DEBUG) {
			error_log('save_order: _pm_order = ' . $order->get_meta('_pm_order'));
			error_log('save_order: _pm_order_status = ' . $order->get_meta('_pm_order_status'));
		}

		if (!$order->get_meta('_pm_order')) {
			if (PM_DEBUG) error_log('save_order: _pm_order is false');
			return;			
		}

		if (strcmp($order->get_meta('_pm_order_status'), 'pending') != 0) {
			if (PM_DEBUG) error_log('save_order: _pm_order_status is not pending');
			return;
		}

		if (!$order->is_paid()) {
			if (PM_DEBUG) error_log('save_order: is_paid is false');
			return;
		}

		$line_items = array();

		foreach ( (array) $order->get_items() as $item_id => $item ) {
			$_product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );

			if (isset( $item['bundled_items']))
				continue;

			$line_item = array(
				'sku'           => $_product->get_id(),
				'qty'           => $item['qty'],
				'priceExVat'    => $item['total'] ? $item['total'] / $item['qty'] : $_product->get_price(),
			);

			$line_items[] = $line_item;
		}

		if (!count($line_items)) {
			if (PM_DEBUG) error_log('save_order: line_items is empty');
			return;
		}

		$orderspec = array(
			'id' => $order->get_order_number(),
			'orderEmail' => $order->get_billing_email(),
			'shippingMethod' => $order->get_shipping_method(),
			'priceShippingExVat' => $order->get_shipping_total(),
			'priceTotalExVat' => $order->get_total() - $order->get_total_tax(),
			'currency' => get_woocommerce_currency(),
			'products' => $line_items,
		);

		$pmTPTrack = $order->get_meta('_pmTPTrack');
		$pmId = urlencode(get_option('vt_api_key', false));

		$o = urlencode(json_encode($orderspec, JSON_NUMERIC_CHECK));
		
		if (PM_DEBUG)
			$u = 'https://testinst1.int.profitmetrics.io/l.php?v=2&pid=' . $pmId . '&o=' . $o;
		else
			$u = 'https://my.profitmetrics.io/l.php?v=2&pid=' . $pmId . '&o=' . $o;

		if (isset($pmTPTrack['gacid']))
			$u .= '&gacid=' . urlencode($pmTPTrack['gacid']);
			
		if (isset($pmTPTrack['gacid_source']))
			$u .= '&gacid_source=' . urlencode($pmTPTrack['gacid_source']);

		if (isset($pmTPTrack['fbp']))
			$u .= '&fbp=' . urlencode($pmTPTrack['fbp']);

		if(isset($pmTPTrack['fbc']))
			$u .= '&fbc=' . urlencode($pmTPTrack['fbc']);
		else if ($order->get_meta('_fbclid'))
			$u .= '&fbc=' . urlencode($order->get_meta('_fbclid'));

		if ($order->get_meta('_gclid'))
			$u .= '&gclid=' . urlencode($order->get_meta('_gclid'));

		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$u .= '&cip=' . urlencode(explode(',', $_SERVER['HTTP_CLIENT_IP'])[0]);
		else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$u .= '&cip=' . urlencode(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])[0]);
		else if (isset($_SERVER['HTTP_X_FORWARDED']))
			$u .= '&cip=' . urlencode(explode(',', $_SERVER['HTTP_X_FORWARDED'])[0]);
		else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
			$u .= '&cip=' . urlencode(explode(',', $_SERVER['HTTP_FORWARDED_FOR'])[0]);
		else if (isset($_SERVER['HTTP_FORWARDED']))
			$u .= '&cip=' . urlencode(explode(',', $_SERVER['HTTP_FORWARDED'])[0]);
		else if (isset($_SERVER['REMOTE_ADDR']))
			$u .= '&cip=' . urlencode(explode(',', $_SERVER['REMOTE_ADDR'])[0]);

		if (isset($_SERVER['HTTP_USER_AGENT']))
			$u .= '&cua=' . urlencode($_SERVER['HTTP_USER_AGENT']);

		// note: might be empty when it's an admin order.
		if (strcmp($order->get_meta('_pmOriginTrack'), '{}') != 0 && strcmp($order->get_meta('_pmOriginTrack'), '') != 0)
			$u .= '&t=' . urlencode($order->get_meta('_pmOriginTrack'));

		$response = file_get_contents($u);

		if ($response) {
			$order->update_meta_data('_pm_order_status', 'completed');
			$order->save();
		}

		if (PM_DEBUG) {
			error_log('save_order: u = ' . $u);
			error_log('save_order: _pm_order = ' . $order->get_meta('_pm_order'));
			error_log('save_order: _pm_order_status = ' . $order->get_meta('_pm_order_status'));
		}
	}
	
	/**
	* Add cost price to edit product page
	* 
	*/
	function cost_price_simple_product_field() {

		/* KASPER 2021-09-13: the user can choose such sure, but that doesnt mean we should be taking over that other field name.
		// The user can set her/his own cost meta price key, that stores tha cost price, in woocommerce profitmetrics settings.
		$profitmetrics_cost_price = get_option( 'profitmetrics_cost_price', false );
		$cost_price_key = empty( $profitmetrics_cost_price ) ? '_cost_price' : $profitmetrics_cost_price;*/
		$cost_price_key = '_cost_price';

		woocommerce_wp_text_input(
			array(
				'id' => $cost_price_key,
				'class' => 'short wc_input_price',
				'label' => __( 'Cost Price', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')',
				'data_type' => 'price',
				'desc_tip' => true,
				'description' => __( "Insert the Cost Price here.", "woocommerce" ),
				'wrapper_class' => 'form-row form-row-full',
			)
		);

	}

	/**
	* Save cost price
	* 
	*/
	function cost_price_simple_product_field_save( $product_id ) {

		// stop the quick edit interferring as this will stop it saving properly, when a user uses quick edit feature
		if ( isset( $_POST['_inline_edit'] ) && wp_verify_nonce( $_POST['_inline_edit'], 'inlineeditnonce' ) ) {
			return;
		}

		// If this is a auto save do nothing, we only save when update button is clicked
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		/* 2021-09-13: see comment in cost_price_simple_product_field()
		// The user can set her/his own cost meta price key, that stores tha cost price, in woocommerce profitmetrics settings.
		$profitmetrics_cost_price = get_option( 'profitmetrics_cost_price', false );
		$cost_price_key = empty( $profitmetrics_cost_price ) ? '_cost_price' : $profitmetrics_cost_price;
		*/
		$cost_price_key = '_cost_price';

		$priceSanitized = null;
		if ( isset( $_POST[ $cost_price_key ] ) && ! empty( $_POST[ $cost_price_key ] ) ) {
			$_price = $_POST[ $cost_price_key ];

			$_price = trim( $_price ) . '';
			$_price = str_replace( ',', '.', $_price );

			if( substr_count( $_price, '.' ) <= 1 ) {
				$priceSanitized = (float) $_price;
			}
		}

		if( null != $priceSanitized ) {
			update_post_meta( (int) $product_id, $cost_price_key, $priceSanitized ); // TODO: should this use esc_attr?
		} else {
			delete_post_meta( $product_id, $cost_price_key );
		}
	}


	/*
	* Add our Custom Fields to variable products
	*/
	function cost_price_variation_product_field( $loop, $variation_data, $variation ) {
		/* 2021-09-13: see comment in cost_price_simple_product_field()
		// The user can set her/his own cost meta price key, that stores tha cost price, in woocommerce profitmetrics settings.
		$profitmetrics_cost_price_variation = get_option( 'profitmetrics_cost_price_variation', false );
		$cost_price_variation_key = empty( $profitmetrics_cost_price_variation ) ? '_cost_price_variation' : $profitmetrics_cost_price_variation;
		*/
		$cost_price_variation_key = '_cost_price_variation';
		
		woocommerce_wp_text_input(
			array(
				'id'        => sprintf( $cost_price_variation_key . '[%s]', (int) $variation->ID ),
				'label'     => __( 'Cost Price', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')',
				'value'     => get_post_meta( (int) $variation->ID, $cost_price_variation_key, true ),
				'class'     => 'short wc_input_price',
				'data_type' => 'price',
				'desc_tip'  => true,
				'description' => __( "Insert the Cost Price here.", "woocommerce" ),
				'wrapper_class' => 'form-row form-row-full',
			)
		);

	}

	/*
	* Save our variable product custom fields
	*/
	function cost_price_variation_product_field_save( $post_id ) {
		/* 2021-09-13: see comment in cost_price_simple_product_field()
		// The user can set her/his own cost meta price key, that stores tha cost price, in woocommerce profitmetrics settings.
		$profitmetrics_cost_price_variation = get_option( 'profitmetrics_cost_price_variation', false );
		$cost_price_variation_key = empty( $profitmetrics_cost_price_variation ) ? '_cost_price_variation' : $profitmetrics_cost_price_variation;
		*/
		$cost_price_variation_key = '_cost_price_variation';

		$priceSanitized = null;
		if( isset( $_POST[ $cost_price_variation_key ][ (int) $post_id ] ) ) {
			$_price = $_POST[ $cost_price_variation_key ][ (int) $post_id ];

			$_price = trim( $_price ) . '';
			$_price = str_replace( ',', '.', $_price );

			if( substr_count( $_price, '.' ) <= 1 ) {
				$priceSanitized = (float) $_price;
			}
		}

		if( null != $priceSanitized ) {
			update_post_meta( $post_id, $cost_price_variation_key, esc_attr( $priceSanitized ) ); // TODO: should this really use esc_attr?
		}
	}

}

new WC_Profitmetrics_Core( __FILE__ );

endif;
