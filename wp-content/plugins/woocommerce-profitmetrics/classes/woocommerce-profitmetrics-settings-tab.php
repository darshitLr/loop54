<?php
/**
 *
 */

class WC_Profitmetrics_Settings_Tab {

    /**
     * Bootstraps the class and hooks required actions & filters.
     *
     */
    public static function init() {
        add_filter( 'woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50 );
        add_action( 'woocommerce_settings_tabs_settings_tab_profitmetrics', __CLASS__ . '::settings_tab' );
        add_action( 'woocommerce_update_options_settings_tab_profitmetrics', __CLASS__ . '::update_settings' );
        //add_action( 'woocommerce_update_options_settings_tab_profitmetrics_test', __CLASS__ . '::generate_text_html' );

        add_action('wp_head', function () {
            if (is_wc_endpoint_url('order-received'))
                return;

            $profitmetrics_script = get_option('profitmetrics_script');
            
            if ($profitmetrics_script)
                echo htmlspecialchars_decode($profitmetrics_script, ENT_QUOTES);
        });
    }
    


    /**
     * Add a new settings tab to the WooCommerce settings tabs array.
     *
     * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding the Subscription tab.
     * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including the Subscription tab.
     */
    public static function add_settings_tab( $settings_tabs ) {
        $settings_tabs['settings_tab_profitmetrics'] = __( 'Profitmetrics', 'profitmetrics' );
        return $settings_tabs;
    }


    /**
     * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
     *
     * @uses woocommerce_admin_fields()
     * @uses self::get_settings()
     */
    public static function settings_tab() {

        woocommerce_admin_fields( self::get_settings() );

        ?>
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row" class="titledesc"><label><?php _e( 'Ny XML Feed URL ID', 'profitmetrics' ) ?></label></th>
                    <td class="forminp forminp-text">
                         <button name="save" class="button-secondary woocommerce-save-button" type="submit" value="new_xml_feed_id"><?php _e( 'Generer ny xml feed url ID', 'profitmetrics' ) ?></button>
                        <p>Her kan man danne ny unik ID til XML feed url'en. Det nuværende ID vil blive slettet permanent.</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php

    }




    /**
     * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
     *
     * @uses woocommerce_update_options()
     * @uses self::get_settings()
     */
    public static function update_settings() {
        if ( isset( $_POST['save'] ) && $_POST['save'] == 'new_xml_feed_id') {
            $_POST['profitmetrics_xml_feed_url'] = md5( uniqid( rand(), true ) );
        }

        if (isset($_POST['profitmetrics_script'])) {
            $_POST['profitmetrics_script'] = htmlspecialchars($_POST['profitmetrics_script'], ENT_QUOTES);
        }

        woocommerce_update_options( self::get_settings() );
    }



    /**
     * Get all the settings for this plugin for @see woocommerce_admin_fields() function.
     *
     * @return array Array of settings for @see woocommerce_admin_fields() function.
     */
    public static function get_settings() {

        $feed_description = sprintf(
            '<p>%s <span style="font-weight: bold; padding: 6px 10px; border: 1px solid #ddd; background: #fff;">%s</span></p>',
            __( 'This is the url for your xml feed: ', 'profitmetrics' ),
            add_query_arg( 'id', get_option( 'profitmetrics_xml_feed_url', false ), esc_url( get_site_url() ) . '/productsfeed/' )
        );

        $profitmetrics_script = get_option('profitmetrics_script');

        $settings = array(
            'section_title' => array(
                //'name'     => __( 'Section Title', 'profitmetrics' ),
                'type'     => 'title',
                'desc'     => '',
                'id'       => 'wc_settings_tab_profitmetrics_section_title'
            ),
            'api_key' => array(
                'name' => __( 'Profitmetrics API KEY', 'profitmetrics' ),
                'type' => 'text',
                'desc' => __( '<p>Insert Profitmetrics API Key found under shop account settings</p>', 'profitmetrics' ),
                'id'   => 'vt_api_key'
            ),
            'cost_price' => array(
                'name' => __( 'Cost Price', 'profitmetrics' ),
                'type' => 'text',
                'desc' => __( '<p>This is the cost price variable, that stores the product cost price. Change this if you have a different cost price variable you want to use.</p>', 'profitmetrics' ),
                'id'   => 'profitmetrics_cost_price',
                'default' => '_cost_price'
            ),
            'cost_price_variation' => array(
                'name' => __( 'Variation Cost Price', 'profitmetrics' ),
                'type' => 'text',
                'desc' => __( '<p>This is the variation cost price variable, that stores the product variation cost price. Change this if you have a different cost price variation variable you want to use.</p>', 'profitmetrics' ),
                'id'   => 'profitmetrics_cost_price_variation',
                'default' => '_cost_price_variation'
            ),
            'inherit_cost_price_variation' => array(
                'name' => __( 'Variation Inherit Cost Price From Parent (Not Recommended).', 'profitmetrics' ),
                'type' => 'checkbox',
                'desc' => __( '<p>Variation Inherit Cost Price From Parent (Not Recommended)</p>', 'profitmetrics' ),
                'id'   => 'profitmetrics_inherit_cost_price_variation',
                'default' => 'no'
            ),
            'script' => array(
                'name' => __( 'Script', 'profitmetrics' ),
                'type' => 'textarea',
                'desc' => __('<p>Script inserted here will be injected on all shop pages except the order confirmation page</p>', 'profitmetrics'),
                'id'   => 'profitmetrics_script',
                'value' => $profitmetrics_script ? htmlspecialchars_decode($profitmetrics_script, ENT_QUOTES) : $profitmetrics_script
            ),
            'feed_url' => array(
                'name' => __( 'XML Feed URL ID', 'profitmetrics' ),
                'type' => 'text',
                'desc' => $feed_description,
                'id'   => 'profitmetrics_xml_feed_url',
                'custom_attributes' => array( 'readonly' => 'readonly' )
            ),
            'section_end' => array(
                 'type' => 'sectionend',
                 'id' => 'wc_settings_tab_profitmetrics_section_end'
            )
        );

        return apply_filters( 'wc_settings_tab_profitmetrics_settings', $settings );
    }

}

WC_Profitmetrics_Settings_Tab::init();
