<?php

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WCOR_Main' ) ) {

	class WCOR_Main {

		private static $instance;

		private $settings;

		public static function instance() {

			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof WCOR_Main ) ) {
				self::$instance = new WCOR_Main();
			}

			return self::$instance;

		}

		public function __construct() {
			$this->includes();
			$this->init_hooks();
			$this->init_updater();
		}

		public function init_updater(){
			add_action( 'admin_init', array( $this, 'plugin_updater' ) );
			add_action( 'admin_init', array( $this, 'activate_license' ));
			add_action( 'admin_init', array( $this, 'deactivate_license' ));
			add_action( 'admin_notices', array( $this, 'do_license_check' ) );
		}

		public function includes() {

			require_once WCOR_PATH . 'includes/class-wc-order-reviews-query.php';
			require_once WCOR_PATH . 'includes/class-wc-order-reviews-reminder-email-scheduler.php';
			require_once WCOR_PATH . 'includes/class-wc-order-reviews-forms.php';
			require_once WCOR_PATH . 'includes/class-wc-order-reviews-block.php';
			require_once WCOR_PATH . 'includes/class-wc-order-reviews-data-store.php';
			require_once WCOR_PATH . 'includes/class-wc-order-reviews-comments.php';
			require_once WCOR_PATH . 'includes/class-wc-order-reviews-rest-api.php';

			$this->query = new WCOR_Query();
			$this->forms = new WCOR_Forms();
			$this->block = new WCOR_Block();
			$this->store = new WCOR_Data_Store();
			$this->comments = new WCOR_Comments();
			$this->rest_api = new WCOR_Rest_API();
			$this->reminder_email_scheduler = new WCOR_Reminder_Email_Scheduler();

		}

		public function includes_copuons() {
			require_once WCOR_PATH . 'includes/class-wc-order-reviews-coupons.php';
			$this->coupons = new WCOR_Coupons();
		}

		public function init_hooks() {
			add_action( 'wp_enqueue_scripts', array( $this, 'register_assets' ) );
			add_action( 'wp_loaded', array( $this, 'retrieve_order' ), 100 );
			add_action( 'wp_loaded', array( $this, 'save_reviews' ), 100 );
			add_filter( 'woocommerce_get_settings_pages', array( $this, 'load_settings_page' ) );
			add_filter( 'admin_comment_types_dropdown', array( $this,'add_comment_type_for_dropdown' ) );
			add_filter( 'get_avatar_comment_types', array( $this, 'add_avatar_for_store_review_comment_type' ) );
			add_filter( 'woocommerce_my_account_my_orders_actions',  array( $this,'my_account_review_action'), 10, 2 );
			add_action( 'woocommerce_view_order', array( $this, 'view_order_review_button' ), 5, 1 );
			add_shortcode( 'wcor_store_reviews', array( $this, 'store_reviews_shortcode' ) );
			add_filter( 'woocommerce_hidden_order_itemmeta', array( $this, 'hide_order_items_meta' ) );
			add_filter( 'woocommerce_email_classes', array( $this, 'add_review_reminder_email' ) );
			add_action( 'add_meta_boxes', array( $this, 'add_order_settings_meta_box' ) );
			add_action( 'save_post', array( $this, 'save_order_settings' ) );
			add_filter( 'woocommerce_email_styles', array( $this, 'add_email_button_styles' ), 10, 2 );
			add_filter( 'woocommerce_order_actions', array( $this, 'add_review_reminder_email_order_action' ) );
			add_filter( 'bulk_actions-edit-shop_order', array( $this, 'add_review_reminder_email_bulk_order_action'), 20, 1 );
			add_filter( 'handle_bulk_actions-edit-shop_order', array($this, 'review_reminder_email_bulk_handler'), 10, 3 );
			add_filter( 'admin_notices', array($this, 'review_reminder_email_bulk_admin_notice'), 10, 3 );
			add_filter( 'comment_text', array( $this, 'add_uploaded_photos_to_reviews' ), 10, 2 );
			add_action( 'woocommerce_order_action_wcor_trigger_email_reminder_action', array( $this, 'review_reminder_email_order_action') );
			add_action( 'body_class', array( $this, 'set_feedback_page_body_class' ));

			//This is needed since Taxonomy is registered at init from WooCommerce
			add_action( 'init', array( $this, 'includes_copuons' ), 200);
			add_action( 'wp_ajax_vote_review', array( $this, 'vote_review' ) );
			add_action( 'wp_ajax_nopriv_vote_review', array( $this, 'vote_review' ) );

			add_action( 'wp_ajax_delete_review_image', array( $this, 'delete_review_image' ) );
			add_action( 'wp_ajax_nopriv_delete_review_image', array( $this, 'delete_review_image' ) );
		}

		public function register_assets() {

			global $wp_query;

			if ( wcor_is_feedback_page() ) {

				wp_register_script( 'wcor-main-js', WCOR_DIR . 'assets/js/main.js', array( 'jquery' ), REDLIGHT_WCOR_VERSION );

				wp_localize_script( 'wcor-main-js', 'wcor_settings', array(

					'i18n_required_rating_text' => esc_attr__( 'Please select a rating', 'wc-order-reviews' ),
					'review_rating_required'    => get_option( 'woocommerce_review_rating_required' ),
					'wcor_delete_review_nonce' => wp_create_nonce( 'wcor_delete_review' ),
					'wcor_order_key' => $wp_query->get( 'order_key' ),
					'wcor_order_id' => $wp_query->get( 'order_id' ),
					'ajax_url' => admin_url( 'admin-ajax.php' )

				) );

				wp_enqueue_script( 'wcor-main-js' );

			}

			wp_enqueue_style( 'wcor-main-css', WCOR_DIR . 'assets/css/main.css', array(), REDLIGHT_WCOR_VERSION );

			wp_enqueue_style( 'fancybox-css', WCOR_DIR . 'assets/css/fancybox.css', array(), REDLIGHT_WCOR_VERSION );

			wp_enqueue_script( 'fancybox-js', WCOR_DIR . 'assets/js/fancybox.js', array(), REDLIGHT_WCOR_VERSION );

			if ( is_product() ) {

				wp_register_script( 'wcor-vote-js', WCOR_DIR . 'assets/js/review_vote.js', array( 'jquery' ),REDLIGHT_WCOR_VERSION );

				wp_localize_script( 'wcor-vote-js', 'wcor_vote_settings', array(

					'nonce' => wp_create_nonce( 'wcor_review_vote' ),
					'ajax_url' => admin_url( 'admin-ajax.php' )

				) );

				wp_enqueue_script( 'wcor-vote-js' );

			}

			wp_enqueue_style( 'fa-css', WCOR_DIR . 'assets/css/fa.css' );

			wp_register_style( 'slick-theme-css', WCOR_DIR . 'assets/css/slick-theme.css' );

			wp_register_style( 'slick-css', WCOR_DIR . 'assets/css/slick.css' );

			wp_register_script( 'slick-js', WCOR_DIR . 'assets/js/slick.min.js', array( 'jquery' ) );

			wp_enqueue_style( 'wcor-block-css', WCOR_DIR . '/includes/blocks/build/style-index.css', array( 'fancybox-css', 'fa-css', 'slick-theme-css', 'slick-css' ) );

			wp_enqueue_script( 'wcor-global-js', WCOR_DIR . 'assets/js/global.js', array( 'slick-js' ) );

		}

		public function load_settings_page( $settings ) {

			$settings[] = include WCOR_PATH . 'includes/class-wc-order-reviews-settings.php';

			return $settings;

		}

		public function add_order_settings_meta_box() {

			add_meta_box(
				'wcor_order_settings',
				'Order Reviews Settings',
				array( $this, 'wcor_order_settings_html' ),
				'shop_order',
				'side',
				'low'
			);

		}

		public function wcor_order_settings_html( $post ) {

			wp_nonce_field( 'wcor_order_settings', 'wcor_order_settings_nonce' );

			$_wcor_disable_reminder = wc_string_to_bool( get_post_meta( $post->ID, '_wcor_disable_reminder', true ) );
			$order = wc_get_order($post->ID);
			?>
			<p>
				<?php
				printf(
					'<a href="%s">%s</a>',
					esc_url( $this->query->get_feedback_url( $order->get_id(), $order->get_order_key() ) ),
					__( 'Customer order review page &rarr;', 'wc-order-reviews' )
				);
				?>
			</p>
			<p class="form-row validate-required" id="_wcor_disable_reminder_field">
				<span class="woocommerce-input-wrapper">
					<label class="checkbox ">
						<input type="checkbox" class="input-checkbox " name="_wcor_disable_reminder" id="_wcor_disable_reminder" value="1" <?php echo $_wcor_disable_reminder ? 'checked="checked"' : '' ?>>
						<?php echo __( 'Disable the review reminder for this order?', 'wc-order-reviews' ) ?>
					</label>
				</span>
			</p>

			<?php
			$args = array(
				'hook'      => 'wcor_trigger_reminder_email',
				'args'      => array( $order->get_id() ),
				'group'     => 'wc-order-reviews',
			);
			$schedule_status = as_get_scheduled_actions( $args );
			if(!empty($schedule_status)){
				printf(
					'<strong>%s</strong>',
					__( 'Emailer reminder schedules', 'wc-order-reviews' )
				);
			}
			foreach ($schedule_status as $action){
				if( $action->is_finished() ){
					printf(
						'<p>%s %s</a>',
						__( 'Email reminder has been sent at', 'wc-order-reviews' ),
						$action->get_schedule()->get_date()->format('Y-m-d H:i:s O')
					);
				}else{
					printf(
						'<p>%s %s</a>',
						__( 'This is scheduled to be sent at', 'wc-order-reviews' ),
						$action->get_schedule()->get_date()->format('Y-m-d H:i:s O')
					);
				}

			}
		}

		public function save_order_settings( $post_id ) {

			global $post;

			if ( ! $post || ! $post->post_type || $post->post_type != 'shop_order' ) return;

			$is_autosave = wp_is_post_autosave( $post_id );

			$is_revision = wp_is_post_revision( $post_id );

			if ( $is_revision || $is_autosave || ! wp_verify_nonce( $_POST[ 'wcor_order_settings_nonce' ], 'wcor_order_settings' ) ) return;

			if ( $_POST['_wcor_disable_reminder'] == '1' ) {

				update_post_meta( $post_id, '_wcor_disable_reminder', 1 );

			} else {

				update_post_meta( $post_id, '_wcor_disable_reminder', 0 );

			}

		}

		public function retrieve_order() {


			if ( 'POST' !== strtoupper( $_SERVER['REQUEST_METHOD'] ) ) {
				return;
			}

			if ( empty( $_POST['action'] ) || 'wcor_retrieve_order' !== $_POST['action'] ) {
				return;
			}

			$nonce = wc_get_var( $_POST['wcor-find-order-nonce'], wc_get_var( $_POST['_wpnonce'], '' ) );
			if ( ! $nonce && ! wp_verify_nonce( $nonce ) ) {
				return;
			}

			$order_id = empty( $_POST['gpm_field_order_id'] ) ? 0 : ltrim( wc_clean( wp_unslash( $_POST['gpm_field_order_id'] ) ), '#' );
			$order_email = empty( $_POST['gpm_field_order_email'] ) ? '' : sanitize_email( wp_unslash( $_POST['gpm_field_order_email'] ) );

			if ( ! $order_id ) {
				wc_add_notice( __( 'Please enter a valid order ID.', 'wc-order-reviews' ), 'error' );
				return;
			}

			if ( ! $order_email ) {
				wc_add_notice( __( 'Please enter a valid email address.', 'wc-order-reviews' ), 'error' );
				return;
			}

			$order = wc_get_order( apply_filters( 'woocommerce_shortcode_order_tracking_order_id', $order_id ) );
			//Allow imported orders without order_key to be found via form, generate missing order_key
			if( empty($order->get_order_key()) ){
				$order->set_order_key( wc_generate_order_key() );
				$order->save();
			}
			if ( $order && $order->get_id() && strtolower( $order->get_billing_email() ) === strtolower( $order_email ) ) {
				wp_redirect( $this->query->get_feedback_url( $order->get_id(), $order->get_order_key() ) );
				exit();

			} else {

				wc_add_notice( __( 'We couldn\' find your order. Please try again.', 'wc-order-reviews' ), 'error' );

			}




		}

		public function save_reviews() {

			if ( 'POST' !== strtoupper( $_SERVER['REQUEST_METHOD'] ) ) {
				return;
			}

			if ( empty( $_POST['action'] ) || 'wcor_save_reviews' !== $_POST['action'] ) {
				return;
			}

			$nonce = wc_get_var( $_POST['wcor-save-reviews-nonce'], wc_get_var( $_POST['_wpnonce'], '' ) );

			if ( ! $nonce && ! wp_verify_nonce( $nonce ) ) {
				return;
			}

			$order_id = empty( $_POST['wcor_order_id'] ) ? '' : sanitize_text_field( $_POST['wcor_order_id'] );

			if ( ! $order_id ) {
				return;
			}

			$order = wc_get_order( $order_id );

			$products_reviews = empty( $_POST['wcor_products_reviews'] ) ? NULL : array_map( function( $review ){

				return sanitize_text_field( $review );

			}, $_POST['wcor_products_reviews'] );


			$store_review = empty( $_POST['wcor_store_review'] ) ? NULL : sanitize_text_field( $_POST['wcor_store_review'] );

			$rating_enabled = wc_review_ratings_enabled();

			$image_upload_enabled = wcor_image_upload_enabled();

			$is_reviewed = false;

			$errors = array();

			if ( $rating_enabled ) {

				$store_rating = empty( $_POST['wcor_store_rating'] ) ? NULL : intval( $_POST['wcor_store_rating'] );

				$products_ratings = empty( $_POST['wcor_products_ratings'] ) ? NULL : array_map( function( $rating ){

					return intval( $rating );

				}, $_POST['wcor_products_ratings'] );

			}


			if ( ( $rating_enabled && ! $products_ratings ) || ! $products_reviews || count( $products_reviews ) == 0 || ( $products_ratings && count( $products_ratings ) == 0 ) ) {

				wc_add_notice( __( 'Please review one or more products.', 'wc-order-reviews' ), 'error' );

				return;

			}

			if ( $image_upload_enabled ) {

				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				require_once( ABSPATH . 'wp-admin/includes/media.php' );

				$__FILES = $_FILES;

				$allowed_file_types = array(
					'image/jpeg', 'image/pjpeg', 'image/jpeg', 'image/pjpeg', 'image/png'
				);

			}

			$order_user_id = $order->get_user_id();

			$order_user_name = $order->get_billing_first_name() . ' ' . $order->get_billing_last_name();

			$order_user_email = $order->get_billing_email();

			$review_data = array(
				'comment_author' => $order_user_name,
				'comment_author_email' => $order_user_email,
				'comment_approved' => 0,
				'user_id' => $order_user_id
			);

			$update_existing_review = false;

			if ( ! empty( $_SERVER['REMOTE_ADDR'] ) && rest_is_ip_address( wp_unslash( $_SERVER['REMOTE_ADDR'] ) ) ) {
				$review_data['comment_author_IP'] = wc_clean( wp_unslash( $_SERVER['REMOTE_ADDR'] ) );
			} else {
				$review_data['comment_author_IP'] = '127.0.0.1';
			}

			if ( $_SERVER['HTTP_USER_AGENT'] ) {
				$review_data['comment_agent'] = $_SERVER['HTTP_USER_AGENT'];
			} else {
				$review_data['comment_agent'] = '';
			}

			$review_data['comment_agent'] = substr( $review_data['comment_agent'], 0, 254 );

			foreach ( $products_reviews as $line_item_id => $review ) {

				$line_item = $order->get_item( $line_item_id );

				$is_line_item_reviewed = $line_item->get_meta( '_wcor_reviewed' );

				$rating = $products_ratings[$line_item_id];

				if ( $is_line_item_reviewed ) {

					$review_id = $line_item->get_meta( '_wcor_review_id' );

					$existing_review = get_comment( $review_id );

					if ( $existing_review->comment_content != $review && ! empty( $review_id ) ) {
						$update_existing_review = true;
						$comment_meta = array(

							'rating' => $rating,
							'_wcor_modified_date' => date( 'Y-m-d H:i:s' ),

						);

						wp_update_comment( array_merge( $review_data, array(

							'comment_ID' => $review_id,
							'comment_content' => $review,
							'comment_meta' => $comment_meta,
							'comment_approved' => 'hold',

						) ) );
						$is_reviewed =true;

					}

				} else {

					$line_item_data = $line_item->get_data();

					$product_id = $line_item_data['product_id'];

					$comment_meta = array();

					$comment_meta['order_id'] = $order->get_id();

					$comment_meta['_wcor_review'] = 'yes';

					if ( $rating_enabled ) {

						$comment_meta['rating'] = $rating;

					}

					$review_id = wp_insert_comment( array_merge( $review_data, array(
						'comment_type'    => 'review',
						'comment_post_ID' => $product_id,
						'comment_content' => $review,
						'comment_meta' => $comment_meta,

					) ) );

					if ( $review_id ) {

						$is_reviewed = true;
						$line_item->update_meta_data( '_wcor_reviewed', true );
						$line_item->update_meta_data( '_wcor_review_id', $review_id );
						$line_item->save_meta_data();

					}

				}

				if ( $image_upload_enabled ) {

					$files = $__FILES['wcor_products_photos_' . $line_item_id];

					foreach ( $files['name'] as $key => $value ) {

						if ( $files['name'][$key] ) {

							$file = array(
								'name'     => $files['name'][$key],
								'type'     => $files['type'][$key],
								'tmp_name' => $files['tmp_name'][$key],
								'error'    => $files['error'][$key],
								'size'     => $files['size'][$key]
							);

							if ( ! in_array( $file['type'], $allowed_file_types ) ) {

								$errors[] = __( 'Image format not supported.', 'wc-order-reviews');

								continue;

							}

							$_FILES = array( 'wcor_products_photos_' . $line_item_id . $key => $file );

							$uploaded_image = media_handle_upload( 'wcor_products_photos_' . $line_item_id . $key, 0 );

							if ( ! is_wp_error( $uploaded_image ) ) {

								update_post_meta( $uploaded_image, '_review_id', $review_id );

							}

							wp_update_comment( array(
								'comment_ID' => $review_id,
								'comment_approved' => 'hold',
							) );
						}
					}

				}

			}

			if ( $store_review ) {

				$is_store_reviewed = wc_string_to_bool( $order->get_meta( '_wcor_store_reviewed' ) );

				if ( $is_store_reviewed ) {

					$store_review_id = $order->get_meta( '_wcor_store_review_id' );

					$store_existing_review = get_comment( $store_review_id );

					$update_existing_review = true;

					if ( ! empty( $store_existing_review ) && $store_existing_review->comment_content != $store_review ) {

						$comment_meta = array(

							'rating' => $store_rating,
							'_wcor_modified_date' => date( 'Y-m-d H:i:s' ),

						);

						wp_update_comment( array_merge( $review_data, array(

							'comment_ID' => $store_review_id,
							'comment_content' => $store_review,
							'comment_meta' => $comment_meta,
							'comment_approved' => 'hold',

						) ) );

					}

					$is_reviewed = true;

				} else {

					$comment_meta = array();

					if ( $rating_enabled ) {

						$comment_meta['rating'] = $store_rating;
						$comment_meta['order_id'] = $order->get_id();

					}


					$store_review_id = wp_insert_comment( array_merge( $review_data, array(
						'comment_post_ID' => 0,
						'comment_type'    => 'store_review',
						'comment_content' => $store_review,
						'comment_meta' => $comment_meta,

					) ) );

					if ( $store_review_id ) {
						$is_reviewed = true;
						$order->update_meta_data( '_wcor_store_reviewed', true );
						$order->update_meta_data( '_wcor_store_review_id', $store_review_id );
						$order->save_meta_data();

					}

				}

			}

			if($is_reviewed){
				$order->update_meta_data( '_wcor_reviewed', true );
				$order->save_meta_data();

				$post = array(
                    'ID' => $order->get_id(),
                    'post_modified' => current_time( 'Y:m:d H:i:s' ),
                    'post_modified_gmt' => current_time( 'Y:m:d H:i:s', 1 ),
                );
				wp_update_post( $post );
				
				if ( count( $errors ) > 0 ) {

					foreach ( $errors as $error ) {

						wc_add_notice( $error, 'error' );

					}

				} elseif( $update_existing_review ){
					wc_add_notice( __( 'You have successfully updated your review. Thank you! ', 'wc-order-reviews' ), 'success' );
					do_action('wcor_successfully_order_review_updated',$order);
				}else{
					wc_add_notice( __( 'You have successfully reviewd your order. Thank you! ', 'wc-order-reviews' ), 'success' );
					do_action('wcor_successfully_order_review',$order);
				}
			}else{
				wc_add_notice( __( 'Something went wrong when sending the review. Please try again. ', 'wc-order-reviews' ), 'error' );
				do_action('wcor_failed_order_review',$order);
			}

		}

		public function vote_review() {

			if ( ! wp_verify_nonce( $_POST[ 'nonce' ], 'wcor_review_vote' ) ) return;

			$type = isset( $_POST['type'] ) ? sanitize_text_field( $_POST['type'] ) : false;

			if ( ! $type ) return;

			$review_id = isset( $_POST['review_id'] ) ? sanitize_text_field( $_POST['review_id'] ) : false;

			if ( ! $review_id ) return;

			$user_id = get_current_user_id();

			if ( $user_id == 0 ) {

				$meta_name = array(
					'up' => 'wcor_nopriv_upvotes',
					'down' => 'wcor_nopriv_downvotes'
				);

				$opposite_meta_name = array(
					'up' => 'wcor_nopriv_downvotes',
					'down' => 'wcor_nopriv_upvotes'
				);

				$voted_cookie_name = $meta_name[$type] . $review_id;

				$opposite_voted_cookie_name = $opposite_meta_name[$type] . $review_id;

				$voted = ( isset( $_COOKIE[$voted_cookie_name] ) && $_COOKIE[$voted_cookie_name] == 'yes' ) ? true : false;

				$opposite_voted = ( isset( $_COOKIE[$opposite_voted_cookie_name] ) && $_COOKIE[$opposite_voted_cookie_name] == 'yes' ) ? true : false;


				if ( ! $voted ) {

					$review_votes = intval( get_comment_meta( $review_id, $meta_name[$type], true ) ) ?? 0;

					update_comment_meta( $review_id, $meta_name[$type], ++$review_votes );

					setcookie( $voted_cookie_name, 'yes', time() + 360000, '/' );

					if ( $opposite_voted ) {

						$opposite_review_votes = intval( get_comment_meta( $review_id, $opposite_meta_name[$type], true ) ) ?? 0;

						update_comment_meta( $review_id, $opposite_meta_name[$type], --$opposite_review_votes );

						setcookie( $opposite_voted_cookie_name, 'no', time() + 360000, '/' );

					}

				}


			} else {

				$meta_name = array(
					'up' => 'wcor_upvotes',
					'down' => 'wcor_downvotes'
				);

				$opposite_meta_name = array(
					'up' => 'wcor_downvotes',
					'down' => 'wcor_upvotes'
				);

				$review_votes_meta = get_comment_meta( $review_id, $meta_name[$type], true );

				$review_votes = ! empty( $review_votes_meta ) ? $review_votes_meta : array();

				if ( ! in_array( $user_id, $review_votes ) ) {

					$review_votes[] = $user_id;

					update_comment_meta( $review_id, $meta_name[$type], $review_votes );

				}

				$opposite_review_votes_meta = get_comment_meta( $review_id, $opposite_meta_name[$type], true );

				$opposite_review_votes = ! empty( $opposite_review_votes_meta ) ? $opposite_review_votes_meta : array();

				if ( in_array( $user_id, $opposite_review_votes ) ) {

					$opposite_review_votes = array_diff( $opposite_review_votes, array( $user_id ) );

					update_comment_meta( $review_id, $opposite_meta_name[$type], $opposite_review_votes );

				}

			}

			$total_upvotes = wcor_get_total_upvotes_html( $review_id );

			echo $total_upvotes;

			wp_die();

		}

		function delete_review_image() {

			if ( ! wp_verify_nonce( $_POST[ 'nonce' ], 'wcor_delete_review' ) ) return;

			$image_id = sanitize_text_field( $_POST['image_id'] );

			$order_key = sanitize_text_field( $_POST['order_key'] );

			$order_id = sanitize_text_field( $_POST['order_id'] );

			if ( empty( $image_id ) || empty( $order_key ) || empty( $order_id ) ) return;

			if ( wc_get_order_id_by_order_key( $order_key ) != $order_id ) return;

			wp_delete_post( $image_id );

			wp_die();

		}

		public function store_reviews_shortcode( $attrs ) {

			$store_reviews = get_comments( array(
				'post_id' => 0,
				'type'    => 'store_review',
				'posts_per_page' => ($attrs && $attrs['count']) ? $attrs['count'] : -1
			) );

			$store_reviews = array_filter( $store_reviews, function( $review ){

				return $review->comment_approved == '1';

			} );

			ob_start();

			wc_get_template(
				'store-reviews.php',
				array(
					'store_reviews' => $store_reviews
				),
				'',
				WCOR_PATH . 'includes/templates/'
			);

			return ob_get_clean();

		}

		public function add_review_reminder_email( $email_classes ) {

			require_once WCOR_PATH . 'includes/emails/class-wc-order-reviews-reminder-email.php';
			$email_classes['WCOR_Reminder_Email'] = new WCOR_Reminder_Email();
			return $email_classes;

		}

		public function add_review_reminder_email_order_action( $actions ) {

			global $theorder;

			if ( wcor_email_reminder_disabled( $theorder->ID ) ) {

				return $actions;

			}

			$actions['wcor_trigger_email_reminder_action'] = __( 'Send review email', 'wc-order-reviews' );

			return $actions;

		}
		public function add_review_reminder_email_bulk_order_action( $actions ){
			$actions['wcor_trigger_email_reminder_action'] = __( 'Send review email', 'wc-order-reviews' );

			return $actions;
		}
		public function review_reminder_email_bulk_handler( $redirect_to, $doaction, $post_ids ){
			if ( !in_array($doaction, array('wcor_trigger_email_reminder_action')) ) {
				return $redirect_to;
			}

			$processed_ids = array();

			foreach ( $post_ids as $post_id ) {
				$order = wc_get_order( $post_id );
				$this->reminder_email_scheduler->trigger_email( $order->get_id() );
				// Note the event.
				$order->add_order_note( __( 'Order reviews reminder sent via bulk actions.', 'wc-order-reviews' ), false, true );
				$processed_ids[] = $post_id;
			}

			return $redirect_to = add_query_arg(
				array(
					'wcor_bulk_email'       => '1',
					'wcor_processed_count'  => count( $processed_ids ),
					'wcor_processed_ids'    => implode( ',', $processed_ids ),
				),
				$redirect_to );
			}
			public function review_reminder_email_bulk_admin_notice(){
				if ( empty( $_REQUEST['wcor_bulk_email'] ) ) return; // Exit

				$count = intval( $_REQUEST['wcor_processed_count'] );

				printf( '<div id="message" class="updated fade"><p>' .
				_n( 'Sent reminder for %s Order.',
				'Sent reminders for %s Orders.',
				$count,
				'wc-order-reviews'
				) . '</p></div>', $count
			);
		}
		public function review_reminder_email_order_action( $order ) {

			if ( wcor_email_reminder_disabled( $order->get_id() ) ) {

				return;

			}

			$this->reminder_email_scheduler->trigger_email( $order->get_id() );
			// Note the event.
			$order->add_order_note( __( 'Order reviews reminder manually sent to customer.', 'wc-order-reviews' ), false, true );
		}

		public function hide_order_items_meta( $keys ) {

			$hide = array( '_wcor_reviewed', '_wcor_review_id' );

			return array_merge( $keys, $hide );

		}

		public function add_comment_type_for_dropdown( $comment_types ) {
			$comment_types['store_review'] = esc_html__( 'Store Reviews','wc-order-reviews' );
			return $comment_types;
		}
		/**
		* Make sure WP displays avatars for comments with the `store_review` type.
		* @param  array $comment_types Comment types.
		* @return array
		*/
		public static function add_avatar_for_store_review_comment_type( $comment_types ) {
			return array_merge( $comment_types, array( 'store_review' ) );
		}
		public function my_account_review_action( $actions, $order ) {
			$is_order_reviewed = $order->get_meta( '_wcor_reviewed', true );
			if ( !$is_order_reviewed ) {
				$actions['review'] = array(
					'url'  => $this->query->get_feedback_url( $order->get_id(), $order->get_order_key() ),
					'name' => __( 'Review order', 'wc-order-reviews' ),
				);
			}
			return $actions;
		}
		public function set_feedback_page_body_class( $classes ) {
			if ( wcor_is_feedback_page() ) {
				$classes[] = 'feedback-page';
			}
			return $classes;
		}

		public function view_order_review_button( $order_id ){
			$order = wc_get_order( $order_id );
			$is_order_reviewed = $order->get_meta( '_wcor_reviewed', true );
			if ( !$is_order_reviewed ) {
				echo '<a href="'.$this->query->get_feedback_url( $order->get_id(), $order->get_order_key() ).'" class="button alt review-order" id="review-order" data-order-id="'.$order_id.'">'. __('Review Order', 'wc-order-reviews') .'</a>';
			}
		}

		public function add_email_button_styles( $css, $email ) {

			if ( $email->id != 'wcor_reminder_email' ) return $css;
			$base      = apply_filters( 'wcor_email_review_button_bg_color', get_option( 'woocommerce_email_base_color' ) );
			$body      = get_option( 'woocommerce_email_body_background_color' );
			$base_text = apply_filters( 'wcor_email_review_button_color', wc_light_or_dark( $base, '#202020', '#ffffff' ));

			$css .= '
			.button {
				padding: 0.8em 1em;
				font-size: 0.875em;
				margin-right: 0.236em;
				line-height: 3em;
				background-color: '.$base.';
				color: '.$base_text.';
				cursor: pointer;
				text-decoration: none;
			}
			';

			return $css;

		}

		public function add_uploaded_photos_to_reviews( $comment_text, $comment ) {

			if ( get_post_type( $comment->comment_post_ID ) != 'product' ) return $comment_text;

			$output = $comment_text . wcor_review_photos_html( $comment->comment_ID );

			if ( ! is_admin() ) {

				$output .=  wcor_review_vote_html( $comment->comment_ID );

			}

			return $output;

		}

		public function plugin_updater() {
			// retrieve our license key from the DB
			$license_key = get_option( 'wcor_redlight_license_key');

			if( !class_exists( 'License_WP_Plugin_Updater' ) ) {
				// load our custom updater
				include_once WCOR_PATH . 'includes/License_WP_Plugin_Updater.php';
			}
			// setup the updater
			$license_wp_updater = new License_WP_Plugin_Updater( REDLIGHT_WCOR_STORE_URL, WCOR_FILE,
			array(
				'version'   => REDLIGHT_WCOR_VERSION,      // current version number
				'license'   => $license_key,             // license key (used get_option above to retrieve from DB)
				'item_slug' => urlencode( REDLIGHT_WCOR_ITEM_NAME ), // Plugin slug from licence-wp,
				'item_id'   => urlencode( REDLIGHT_WCOR_ITEM_ID ), // Plugin ID from licence-wp,
				'beta'      => false,
			)
		);
	}

	public function activate_license(){

		if( isset( $_POST['wcor_redlight_license_key'] ) && !isset( $_POST['wcor_redlight_license_deactivate'] ) ) {
			// retrieve the license from the database
			$license_key = sanitize_text_field($_POST['wcor_redlight_license_key']);
			$home_url = str_replace( array( 'http://', 'https://' ), '', trim( home_url() ) );

			$api_params = array(
				'wc-api'         =>'license_wp_api_activation',
				'request'        => 'activate',
				'license_key'    => $license_key,
				'api_product_id' => urlencode( REDLIGHT_WCOR_ITEM_NAME ), // Plugin slug from licence-wp,
				'api_item_id'    => urlencode( REDLIGHT_WCOR_ITEM_ID ), // Plugin ID from licence-wp,
				'instance'       => $home_url,
			);
			$response = wp_remote_get(
				REDLIGHT_WCOR_STORE_URL. '?' . http_build_query( $api_params, '', '&' ),
				array(
					'timeout' => 15,
					'headers'   => array(
						'Accept' => 'application/json'
					)
				)
			);

			// make sure the response came back okay
			if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
				if ( is_wp_error( $response ) ) {
					$message = $response->get_error_message();
				} else {
					$message = __( 'An error occurred, please try again.' );
				}

			} else {
				$license_value = '';
				$license_data = json_decode( wp_remote_retrieve_body( $response ) );
				if( isset($license_data->error) ){
					$license_value = 'invalid';
					update_option( 'redlight_wcor_license_error', $license_data->error );
				}elseif ( false === $license_data->success ) {
					$license_value = 'invalid';
				}elseif( $license_data->success ){
					$license_value = 'valid';
					delete_option( 'redlight_wcor_license_error' );
				}
			}

			// $license_value will be either "valid" or "invalid"
			update_option( 'redlight_wcor_license_status', $license_value );

		}
	}

	public function deactivate_license() {

		if( isset( $_POST['wcor_redlight_license_key'] ) && isset( $_POST['wcor_redlight_license_deactivate'] ) ) {
			// retrieve the license from the database
			$license_key = sanitize_text_field($_POST['wcor_redlight_license_key']);

			$api_params = array(
				'wc-api'         => 'license_wp_api_activation',
				'request'        => 'deactivate',
				'license_key'    => $license_key,
				'api_product_id' => urlencode( REDLIGHT_WCOR_ITEM_NAME ), // Plugin slug from licence-wp,
				'api_item_id' 	 => urlencode( REDLIGHT_WCOR_ITEM_ID ), // Plugin ID from licence-wp,
				'instance'       => str_replace( array( 'http://', 'https://' ), '', trim( home_url() ) ),
			);

			$response = wp_remote_get(
				REDLIGHT_WCOR_STORE_URL. '?' . http_build_query( $api_params, '', '&' ),
				array(
					'timeout' => 15,
					'headers'   => array(
						'Accept' => 'application/json'
					)
				)
			);

			// make sure the response came back okay
			if ( is_wp_error( $response ) )
			return false;

			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );

			// $license_data->success will be either true or false
			if( $license_data->success )
			delete_option( 'redlight_wcor_license_status' );
		}
	}

	public function do_license_check() {
		if( get_option( 'redlight_wcor_license_status' ) != "valid" ) {
			echo "<div class=\"error\"><p>". sprintf( __( "License key for <strong>%s</strong> is invalid. Please ensure that you have <a href=\"%s\">entered a valid license key.</a>", 'wc-order-reviews' ), get_plugin_data( WCOR_FILE )['Name'], admin_url( 'admin.php?page=wc-settings&tab=wcor' ) ) ."</p></div>";
		}
		if( get_option( 'redlight_wcor_license_error' ) ) {
			echo "<div class=\"error\"><p>". get_option( 'redlight_wcor_license_error' ) ."</p></div>";
		}
	}

}

}

function wcor() {
	return WCOR_Main::instance();
}

wcor();
