<?php

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WCOR_Query' ) ) {

	class WCOR_Query {

		private $endpoint;

		public function __construct() {

			$this->endpoint = apply_filters( 'wcor_endpoint', get_option( 'wcor_endpoint', 'feedback' ) );

			add_action( 'init', array( $this, 'create_feedback_endpoint' ) ); 
			add_filter( 'template_include', array( $this, 'include_feedback_template' ) );

		}

		public function is_feedback() {

			global $wp_query;

			if ( isset( $wp_query->query_vars[$this->endpoint] ) ) {

				return $wp_query->query_vars[$this->endpoint] !== NULL;

			}

			return false;

		}

		public function create_feedback_endpoint() {

            add_rewrite_tag('%order_id%', '([0-9]+)');
            add_rewrite_tag('%order_key%', '([^/]*)');

            add_rewrite_rule('^' . $this->endpoint . '/([0-9]+)/([^/]*)/?', 'index.php?' . $this->endpoint . '&order_id=$matches[1]&order_key=$matches[2]', 'top');

            add_rewrite_endpoint( $this->endpoint, EP_ROOT | EP_PAGES );

		}

		public function include_feedback_template( $template ) {

			global $wp_query, $wpdb;

			if ( $this->is_feedback() ) {

                if ( $wp_query->get( $this->endpoint ) !== '' ) {

                    $wp_query->set_404();
                    return get_404_template();

                }

                if ( ( $wp_query->get( 'order_id' ) && ! $wp_query->get( 'order_key' ) ) || 
                ( $wp_query->get( 'order_key' ) && ! $wp_query->get( 'order_id' ) ) ) {


                    $wp_query->set_404();
                    return get_404_template();

                }

                if ( $wp_query->get( 'order_id' ) && $wp_query->get( 'order_key' ) ) {

                    $order_key = $wp_query->get( 'order_key' );

                    $order_id = $wp_query->get( 'order_id' );

					$order = wc_get_order( $order_id );
					

                    if ( ! $order || $order->get_id() != $order_id || $order->get_order_key() != $order_key ) {

                        $wp_query->set_404();
                        return get_404_template();

                    }

                    $is_order_reviewed = wc_string_to_bool( $order->get_meta( '_wcor_reviewed' ) );

					return wc_get_template(
						'feedback.php',
						array(
							'order' => $order,
							'site_name' => get_bloginfo( 'name' ),
							'is_order_reviewed' => $is_order_reviewed
						),
						'',
						WCOR_PATH . 'includes/templates/'
					);

                }
				
				return wc_get_template(
					'feedback-form.php',
					array(),
					'',
					WCOR_PATH . 'includes/templates/'
				);

			}


			return $template;

		}

		public function get_endpoint() {

			return $this->endpoint;

		}

		public function get_feedback_url( $order_id, $order_key ) {

			$permalink_structure = get_option( 'permalink_structure' );

			if ( '' == $permalink_structure ) {

				return add_query_arg( array(
					$this->endpoint => '',
					'order_id' => $order_id,
					'order_key' => $order_key
				), site_url() );

			} else {

				return site_url() . '/' . $this->endpoint . '/' . $order_id . '/' . $order_key. '/';

			}

		}

	}

}