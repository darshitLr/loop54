<?php

defined( 'ABSPATH' ) || exit;

function wcor_is_feedback_page() {

	return wcor()->query->is_feedback();

}


function wcor_image_upload_enabled() {

	return wcor()->forms->is_image_upload_enabled();

}

function wcor_non_logged_in_review_voting_enabled() {

	return wc_string_to_bool( apply_filters( 'wcor_allow_non_logged_in_review_voting', get_option( 'wcor_review_non_logged_in_vote', 'no' ) ) );

}

function wcor_email_reminder_disabled( $order_id ) {

	return wcor()->reminder_email_scheduler->is_order_reminder_disabled( $order_id );

}

function wcor_get_review_photos( $review_id ) {

	$attached_media = get_posts( array(
		'post_type' => 'attachment',
		'posts_per_page' => 999,
		'meta_query' => array(
			array(
				'key' => '_review_id',
				'value' => $review_id,
				'compare' => '='
			)
		)
	) );

	if ( empty( $attached_media ) ) {

		return null;

	}

	$photos = array_map( function( $item ) {

		return array(
			'thumbnail' => wp_get_attachment_image_src( $item->ID, 'thumbnail' )[0],
			'full' => $item->guid,
			'ID' => $item->ID
		);

	}, $attached_media );

	return $photos;

}

function wcor_review_photos_html( $review_id, $line_item_id = 0 ) {

	$photos = wcor_get_review_photos( $review_id );

	if ( ! $photos ) return;

	$output = '';

	$output .= '<div class="wcor_feedback__review__uploaded_photos">';

	foreach ( $photos as $photo ) {

		$output .= '<div class="wcor_feedback__review__uploaded_photo">';
		$output .= '<a href="' . $photo['full'] . '" data-fancybox="' . $line_item_id . '">';
		$output .= '<img src="' . $photo['thumbnail'] . '" ' . ( is_admin() ? 'width="100" height="100"' : '' ) . '>';
		$output .= '</a>';
		if ( wcor_is_feedback_page() ) {
			$output .= '<a class="wcor_feedback__review__uploaded_photo_remove" data-image-id="' . $photo['ID'] . '" href="#">' . __( 'Delete image', 'wc-order-reviews' ) . '</a>';
		}

		$output .= '</div>';

	}

	$output .= '</div>';

	return $output;

}

function wcor_review_vote_html( $review_id ) {

	$output = '<div class="wcor_feedback_review__vote">';

	$output .= '<p>' . __( 'Was this review helpful?', 'wc-order-reviews' ) . '</p>';

	$user_id = get_current_user_id();

	if ( $user_id == 0 && ! wcor_non_logged_in_review_voting_enabled() ) {

		$output .= '<p class="wcor_feedback_review__vote__login">' . __( 'You need to be logged in to vote', 'wc-order-reviews' ) . '</p>';
		$output .= '</div>';

		return $output;

	}

	$upvotes_meta = get_comment_meta( $review_id, 'wcor_upvotes', true );

	$downvotes_meta = get_comment_meta( $review_id, 'wcor_downvotes', true );

	$upvotes = ! empty( $upvotes_meta ) ? $upvotes_meta : array();

	$downvotes = ! empty( $downvotes_meta ) ? $downvotes_meta : array();

	$nopriv_upvotes = intval( get_comment_meta( $review_id, 'wcor_nopriv_upvotes', true ) ) ?? 0;

	$total_upvotes = count( $upvotes ) + $nopriv_upvotes;

	if ( $user_id == 0 ) {

		$upvote_cookie_name = 'wcor_nopriv_upvotes' . $review_id;

		$downvote_cookie_name = 'wcor_nopriv_downvotes' . $review_id;

		$upvoted = isset( $_COOKIE[$upvote_cookie_name] ) && $_COOKIE[$upvote_cookie_name] == 'yes';

		$downvoted = isset( $_COOKIE[$downvote_cookie_name] ) && $_COOKIE[$downvote_cookie_name] == 'yes';

	} else {

		$upvoted = in_array( $user_id, $upvotes);

		$downvoted = in_array( $user_id, $downvotes);

	}

	$output .= '<div class="wcor_feedback_review__vote__buttons">';

	$output .= '<div class="wcor_feedback_review__vote__button' . ( $upvoted ? ' selected' : '' ) .'" data-vote="up" data-review-id="' . $review_id . '"><i class="fas fa-thumbs-up"></i> ' . __( 'Yes', 'wc-order-reviews' ) . '</div>';

	$output .= '<div class="wcor_feedback_review__vote__button' . ( $downvoted ? ' selected' : '' ) .'" data-vote="down" data-review-id="' . $review_id . '"><i class="fas fa-thumbs-down"></i> ' . __( 'No', 'wc-order-reviews' ) . '</div>';

	$output .= '</div>';

	$output .= '<div class="wcor_feedback_review__vote__helpful ' . ( $total_upvotes == 0 ? 'hidden' : '' ) . '">';

	$output .= sprintf( _n( '%s person found this review helpful', '%s people found this review helpful',  $total_upvotes, 'wc-order-reviews' ),  $total_upvotes );

	$output .= '</div>';

	$output .= '</div>';

	return $output;

}

function wcor_get_total_upvotes( $review_id ) {

	$upvotes_meta = get_comment_meta( $review_id, 'wcor_upvotes', true );

	$upvotes = ! empty( $upvotes_meta ) ? $upvotes_meta : array();

	$nopriv_upvotes = intval( get_comment_meta( $review_id, 'wcor_nopriv_upvotes', true ) ) ?? 0;

	$total_upvotes = count( $upvotes ) + $nopriv_upvotes;

	return $total_upvotes;

}

function wcor_get_total_upvotes_html( $review_id ) {

	$upvotes = wcor_get_total_upvotes( $review_id );

	return sprintf( _n( '%s person found this review helpful', '%s people found this review helpful',  $upvotes, 'wc-order-reviews' ),  $upvotes );

}

function wcor_generate_rating_html( $score, $size, $space, $color ) {

	$output = '<div class="wcorb_reviews__review__rating" style="font-size: ' . $size . 'px; color: ' . $color . '">';

	$classes = array();

	$fill = floor( $score );

	$left = 5 - $fill;

	for ( $i = 0; $i < $fill; $i++ ) {

		$classes[] = 'fas fa-star';

	}

	if ( $fill != $score ) {

		$classes[] = 'fas fa-star-half-alt';


	}

	for ( $i = 0; $i < $left; $i++ ) {

		$classes[] = 'far fa-star';

	}

	for ( $i = 0; $i < 5; $i++ ) {

		$output .= '<div class="wcor_review__rating__star" style="margin: 0 ' . $space . 'px"><i class="' . $classes[$i] . '"></i></div>';

	}

	$output .= '</div>';

	echo $output;

}


function wcor_get_justify( $justify ) {

	if ( $justify == 'center' ) return 'center';
	if ( $justify == 'left' ) return 'flex-start';
	if ( $justify == 'right' ) return 'flex-end';

}

function wcor_get_styles( $styles, $is_flex = false, $is_container = false ) {

	$output = '';

	foreach ( $styles as $style => $value ) {

		switch ( $style ) {
			case 'fontSize':
				$output .= 'font-size: ' . $value . 'px;';
			break;
			case 'fontStyle':
				$output .= 'font-style: ' . $value . ';';
			break;
			case 'fontWeight':
				$output .= 'font-weight: ' . $value . ';';
			break;
			case 'color':
				$output .= 'color: ' . $value . ';';
			break;
			case 'alignment':
				if ( !$is_container ) break;
				$output .= ($is_flex ? 'justify-content: ' : 'text-align: ') . $value . ';';
			break;
			case 'backgroundColor':
				if ( !$is_container ) break;
				$output .= 'background-color: ' . $value . ';';
			break;
		}

	}

	return $output;

}

function wcor_moment_date_to_php( $moment ) {

	switch ( $moment ) {
		case 'L':
			return 'm/d/Y';
		break;
		case 'l':
			return 'n/j/Y';
		break;
		case 'LL':
			return 'F j, Y';
		break;
		case 'll':
			return 'M j, Y';
		break;
		case 'LLL':
			return 'F j, Y g:i A';
		break;
		case 'lll':
			return 'M j, Y g:i A';
		break;
		case 'LLLL':
			return 'l, F j, Y g:i A';
		break;
		case 'llll':
			return 'D, M j, Y g:i A';
		case 'Y-m-d':
			return 'D, M j, Y g:i A';
		case 'YYYY-MM-DD HH:mm:ss':
			return 'Y-m-d H:i:s';
		break;
	}

	return '';

}

function wcor_get_time_ago( $date ) {

		$time = strtotime( $date );

    $time_difference = time() - $time;

		$year = 12 * 30 * 24 * 60 * 60;


    $condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
    );

    foreach( $condition as $secs => $str ) {
        $d = $time_difference / $secs;

        if( $d >= 1 ) {

						$t = round( $d );
						$output = '';

						switch ( $str ) {
							case 'year':
								$output = sprintf( _n( '%s year', '%s years', $t, 'wc-order-reviews' ), $t );
							break;
							case 'month':
								$output = sprintf( _n( '%s month', '%s months', $t, 'wc-order-reviews' ), $t );
							break;
							case 'day':
								$output = sprintf( _n( '%s day', '%s days', $t, 'wc-order-reviews' ), $t );
							break;
							case 'hour':
								$output = sprintf( _n( '%s hour', '%s hours', $t, 'wc-order-reviews' ), $t );
							break;
							case 'minute':
								$output = sprintf( _n( '%s minute', '%s minutes', $t, 'wc-order-reviews' ), $t );
							break;
							case 'second':
								$output = sprintf( _n( '%s second', '%s seconds', $t, 'wc-order-reviews' ), $t );
							break;
						}

            return $output . ' ' . __( 'ago', 'wc-order-reviews' );
        }
    }

}

function wcor_get_attribute_name( $name ) {

	$parts = explode( '_', $name );

	$output = $parts[0];
	unset($parts[0]);

	foreach ( $parts as $part ) {
		$output .= ucfirst( $part );
	}

	return $output;

}

function wcor_get_attribute_value( $value ) {

	if ( $value == 'true' ) return true;
	if ( $value == 'false' ) return false;
	if ( is_numeric( $value ) ) return floatval( $value );
	return $value;

}

function wcor_add_layout_attribute( &$attributes, $prefix, $attribute_name, $value ) {



	$name = 'wc-order-reviews/reviews-block-' . $prefix;

	foreach ( $attributes['reviewLayout'] as $key => $element ) {
		$element_name = $element[0];
		if ( $element_name == $name ) {
			if ( !$attribute_name && $value == false ) {
				unset( $attributes['reviewLayout'][$key] );
				break;
			}
			$attributes['reviewLayout'][$key][1][$attribute_name] = $value;
			break;
		}
	}

}
