<?php

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WCOR_Reminder_Email' ) ) {

    if ( ! class_exists( 'WC_Email' ) ) {

        include_once(WC_ABSPATH . '/includes/emails/class-wc-email.php' );

    }

    class WCOR_Reminder_Email extends WC_Email {

        public function __construct() {

            $this->id          = 'wcor_reminder_email';
            $this->title       = __( 'Order Reviews Reminder', 'wc-order-reviews' );
            $this->description = __( 'WooCommerce Order Reviews Reminder', 'wc-order-reviews' );
            $this->heading     = __( 'Your opinion matters! (Order {order_number})', 'wc-order-reviews' );
            $this->subject     = __( 'Regarding your recent order({order_number}) from {site_title}', 'wc-order-reviews' );

            $this->template_base  = WCOR_PATH . 'includes/templates/email/';
            $this->template_html  = 'review-reminder-email.php';
            $this->template_plain = 'review-reminder-email-plain.php';
            $this->customer_email = true;
            $this->init_settings();           
            
            // Call parent constructor.
            parent::__construct();

            // Manual trigger is not required
            $this->manual = false;

        }
        /**
		 * Get email subject.
		 *
		 * @return string
		 */
		public function get_default_subject() {
			return __( 'Regarding your recent order({order_number}) from {site_title}', 'wc-order-reviews' );
		}

		/**
		 * Get email heading.
		 *
		 * @return string
		 */
		public function get_default_heading() {
			return __( 'Your opinion matters! (Order {order_number})', 'wc-order-reviews' );
		}
        /**
         * get_content_html function.
         *
         * @return string
         */
        public function get_content_html() {
            return wc_get_template_html(
                $this->template_html,
                array(
                    'order'         => $this->object,
                    'email_heading' => $this->get_heading(),
                    'sent_to_admin' => false,
                    'email'         => $this,
                    'email_body'    => $this->format_string( $this->get_option( 'body' ) ),
                    'plain_text'    => false,
                ),
                '',
                $this->template_base
            );
        }

        /**
         * Get content plain.
         *
         * @return string
         */
        public function get_content_plain() {
		   return wc_get_template_html(
                $this->template_plain,
                array(
                    'order'         => $this->object,
                    'email_heading' => $this->get_heading(),
                    'sent_to_admin' => false,
                    'email'         => $this,
                    'email_body'    => $this->format_string( $this->get_option( 'body' ) ),
                    'plain_text'    => true,
                ),
                '',
                $this->template_base
            );
        }

        /**
         * Initialize Settings Form Fields
         *
         */
        public function init_form_fields() {

            $form_fields = array(
                'enabled'    => array(
                    'title'   => __( 'Enable/Disable', 'woocommerce' ),
                    'type'    => 'checkbox',
                    'label'   => __( 'Enable this email notification', 'woocommerce' ),
                    'default' => 'yes',
                ),
                'subject'    => array(
                    'title'       => __( 'Subject', 'woocommerce' ),
                    'type'        => 'text',
                    'desc_tip'    => true,
                    'description' => sprintf( __( 'Available placeholders: %s', 'woocommerce' ), '<code>{site_title}, {order_date}, {order_number}</code>' ),
                    'default'     => __( 'Regarding your recent order({order_number}) from {site_title}', 'wc-order-reviews' ),
                ),
                'heading'    => array(
                    'title'       => __( 'Email heading', 'woocommerce' ),
                    'type'        => 'text',
                    'placeholder' => '',
                    'desc_tip'    => true,
                    'description' => sprintf( __( 'Available placeholders: %s', 'woocommerce' ), '<code>{site_title}, {order_date}, {order_number}</code>' ),
                    'default'     => __( 'Your opinion matters! (Order {order_number})', 'wc-order-reviews' ),
                ),
                'body'    => array(
                    'title'       => __( 'Email Body', 'wc-order-reviews' ),
                    'type'        => 'textarea',
                    'placeholder' => '',
                    'description' => sprintf( __( 'Available placeholders: %s', 'woocommerce' ), '<code>{site_title}, {order_date}, {order_number}, {order_firstname}, {review_url}, {review_link}, {review_button}</code>' ),
                    'default'     => __( "Hi!\nWe hope that you are satisfied with your purchase with us at and that you have already started using your products. We would be incredibly grateful if you would help other customers by leaving feedback on your most recent purchase.\n{review_button}", 'wc-order-reviews' ),
                ),
                'email_type' => array(
                    'title'       => __( 'Email type', 'woocommerce' ),
                    'type'        => 'select',
                    'description' => __( 'Choose which format of email to send.', 'woocommerce' ),
                    'default'     => 'html',
                    'class'       => 'email_type',
                    'options'     => $this->get_email_type_options(),
                ),
            );

            $this->form_fields =  $form_fields;
        }

        /**
         * Send email
         */
        public function trigger( $order_id, $attachment = array() ) {

            //error_log( 'Sendin email reminder for order #' . $order_id );

            $order = wc_get_order( $order_id );
            $this->send_to_order_customer( $order, $attachment );
        }

        /**
         * Send email to order customer
         *
         * @param WC_Order $order
         */
        public function send_to_order_customer( $order, $attachment ) {
            $this->setup_locale();
		
            $this->object    = $order;
            $this->review_url = wcor()->query->get_feedback_url( $order->get_id(), $order->get_order_key() );
            $customer_email = $this->object->get_billing_email();
            $result         = false;

            if ( $customer_email ) {

                $this->placeholders['{order_date}']   = wc_format_datetime( $order->get_date_created() );
                $this->placeholders['{order_number}'] =  $this->object->get_order_number();
                $this->placeholders['{order_firstname}'] = $this->object->get_billing_first_name();
                $this->placeholders['{review_url}'] = $this->review_url;
                $this->placeholders['{review_link}'] = sprintf( '<a href="%s">%s</a>', $this->review_url, __( 'Review order', 'wc-order-reviews' ) );
                $this->placeholders['{review_button}'] = sprintf( '<a class="button" href="%s">%s</a>', $this->review_url, __( 'Review order', 'wc-order-reviews' ) );
                if ( $this->is_enabled() ) {
                    $result = $this->send( $customer_email, $this->get_subject(), $this->get_content(), $this->get_headers(), $attachment );
                }
		    
		        $this->restore_locale();
		    
                if ( $result ) {

                    $this->log( 'Review reminder has been sent for Order id #' . $this->object->get_order_number(), 'info' );
                } else {
                    $this->log( 'Review reminder has been sent for Order id #' . $this->object->get_order_number(), 'warning' );
                }
                return $result;

            } else {
                $this->log( 'Order id #' . $this->object->get_order_number() . 'has no customer email, Review reminder could not be sent', 'warning' );
                return false;
            }

        }

        protected function log( $message, $type ) {

            if ( $this->get_option( 'debug', false ) ) {
                $context = array( 'source' => 'wc-order-reviews' );
                wc_get_logger()->log( $type, $message, $context );
            }

        }

    }

}
