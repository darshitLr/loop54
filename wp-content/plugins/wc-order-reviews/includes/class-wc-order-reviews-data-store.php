<?php

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WCOR_Data_Store' ) ) {

	class WCOR_Data_Store {

		function __construct() {
			add_action('init', array($this, 'test'));
		}

		function test() {
			//var_dump($this->get_reviews( array('include_photos' => true)));
		}

		public function get_reviews( $user_args = array() ) {

			$default_args = array(
				'type' => array( 'review', 'store_review' ),
				'meta_query' => array(),
				'status' => 'approve',
				'include_photos' => true,
				'number' => 30,
				'random_order' => false,
			);

			global $args, $average;

			$args = array_merge( $default_args, $user_args );

			$reviews = get_comments( $args );
			
			$average = 0;

			$reviews = array_map( function( $review_data ) {

				global $args, $average;

				$rating = floatval( get_comment_meta( $review_data->comment_ID, 'rating', true ) );

				$average += $rating;

				$photos = null;

				if ( $args['include_photos'] == true ) {

					$photos = wcor_get_review_photos( $review_data->comment_ID );

				}

				$avatar = 'https://www.gravatar.com/avatar/' . md5( strtolower( trim( $review_data->comment_author_email ) ) ) . '?d=blank';

				$product = null;

				if ( $review_data->comment_post_ID != 0 ) {

					$product = array(
						'name' => get_the_title( $review_data->comment_post_ID ),
						'link' => get_permalink( $review_data->comment_post_ID ),
						'image' => get_the_post_thumbnail_url( $review_data->comment_post_ID )
					);

				}

				return array(
					'id' => $review_data->comment_ID,
					'type' => $review_data->comment_type,
					'customer_name' => $review_data->comment_author,
					'content' => $review_data->comment_content,
					'date' => $review_data->comment_date,
					'helpful' => wcor_get_total_upvotes( $review_data->comment_ID ),
					'rating' => $rating,
					'photos' => $photos,
					'product' => $product,
					'avatar' => $avatar
				);

			}, $reviews );
			if ( $args['random_order'] == true ) {
				shuffle($reviews);
			}
			$total_average = (object) array(
				'count' => 0,
				'average_rating' => 0
			);
			if (count($args['type']) === 1) {
				$type = reset($args['type']);
				if( in_array( $type,array('review','store_review') ) ){
					$total_average = $this->get_reviews_total_average( $type );
				}
			}elseif (count($args['type']) === 2) {
				$total = (object) array();
				//Get the two different objects
				foreach($args['type'] as $review_type){
					if( in_array( $review_type,array('review','store_review') ) ){
						$total->$review_type = $this->get_reviews_total_average( $review_type );
					}
				}
				$review_value = $total->review->count * $total->review->average_rating;
				$store_review_value = $total->store_review->count * $total->store_review->average_rating;

				$total_amount_of_review = $total->store_review->count +  $total->review->count;
				$average_value = ($store_review_value + $review_value ) / $total_amount_of_review;
				//Create the total final object
				$total_average = (object) array(
					'count' => $total_amount_of_review,
					'average_rating' => number_format( $average_value, 2 )
				);
			}
			return array(
				'count' => $total_average->count,
				'average' => $total_average->average_rating,
				'reviews' => $reviews
			);


		}

		public function get_reviews_total_average($review_type){
			$total_reviews_transient = get_transient('wcor_total_reviews_'.$review_type);
			//wc_get_logger()->log('info','Our review type: ' . print_r( $review_type, true ), array( 'source' => 'wc-order-reviews' ) );
			if ($total_reviews_transient) {
				//wc_get_logger()->log('info','We had this info in our transient: ' . wp_json_encode( $total_reviews_transient ), array( 'source' => 'wc-order-reviews' ) );
				return $total_reviews_transient;
			}
			//wc_get_logger()->log('info','regenerating transient with totals: ' . wp_json_encode( $total_reviews_transient ), array( 'source' => 'wc-order-reviews' ) );


			//Get total count and average
			$reviews = get_comments(
				array(
					'type' => $review_type,
					'status' => 'approve',
					'meta_key'=> 'rating'
				)
			);

			$average = 0;
			$total_reviews = count($reviews);
			foreach($reviews as $review){
				$rating = floatval( get_comment_meta( $review->comment_ID, 'rating', true ) );
				$average += $rating;
			}
			$results = (object) array(
				'count' => $total_reviews,
				'average_rating' => number_format( $average / $total_reviews, 2 )
			);
			set_transient('wcor_total_reviews_'.$review_type, $results, WEEK_IN_SECONDS ); //Save this info for a week
			return $results;
		}


	}

}
