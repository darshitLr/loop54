<?php

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WCOR_Forms' ) ) {

	class WCOR_Forms {

		private $image_upload_enabled;

		public function __construct() {

			$this->image_upload_enabled = wc_string_to_bool( apply_filters( 'wcor_allow_reviews_image_upload', get_option( 'wcor_review_image_upload', 'no' ) ) );

		}

		public function is_image_upload_enabled() {

			return $this->image_upload_enabled;

		}

		public function build_form( $attrs ) { 

			$default_attrs = array(
				'name' => null,
				'image' => null,
				'review_rating' => 5,
				'review_content' => '',
				'line_item_id' => 0,
				'store_review' => false
			);

			$attrs = array_replace_recursive( $default_attrs, $attrs );

			extract( $attrs );

			if ( $store_review ) {

				$rating_field_name = 'wcor_store_rating';
				$review_field_name = 'wcor_store_review';

			} else {

				$rating_field_name = 'wcor_products_ratings[' . $line_item_id . ']';
				$review_field_name = 'wcor_products_reviews[' . $line_item_id . ']';
				$photos_field_name = 'wcor_products_photos_' . $line_item_id . '[]';

			}

			?>

			<div class="wcor_feedback__review">
				<?php if ( ! empty( $image ) ) : ?>
					<div class="wcor_feedback__review__image">
						<?php echo $image ?>
					</div>
				<?php endif; ?>
				<div class="wcor_feedback__review__details">
					<div class="wcor_feedback__review__name">
						<?php printf( __( 'What do you think about <strong>%s</strong>?', 'wc-order-reviews' ), $name ) ?>
						<?php if ( $store_review ) : ?>
							<br><i><?php _e( 'Help us get better by leaving feedback on how you experienced our customer service and the company as a whole.', 'wc-order-reviews' ) ?></i>
						<?php endif; ?>
					</div>
					<?php if ( wc_review_ratings_enabled() ) : ?>
						<div class="wcor_feedback__review__rating">
							<label for="<?php echo $rating_field_name ?>">
								<?php echo esc_html__( 'Your rating', 'woocommerce' ) ?><?php echo wc_review_ratings_required() ? '&nbsp;<span class="required">*</span>' : '' ?>
							</label>
							<select name="<?php echo $rating_field_name ?>" id="<?php echo $rating_field_name ?>" <?php echo wc_review_ratings_required() ? 'required' : '' ?> value="<?php echo $review_rating ?>">
								<option value=""><?php echo esc_html__( 'Rate&hellip;', 'woocommerce' ) ?></option>
								<option <?php echo $review_rating == 5 ? 'selected="selected"' : '' ?> value="5"><?php echo esc_html__( 'Perfect', 'woocommerce' ) ?></option>
								<option <?php echo $review_rating == 4 ? 'selected="selected"' : '' ?> value="4"><?php echo esc_html__( 'Good', 'woocommerce' ) ?></option>
								<option <?php echo $review_rating == 3 ? 'selected="selected"' : '' ?> value="3"><?php echo esc_html__( 'Average', 'woocommerce' ) ?></option>
								<option <?php echo $review_rating == 2 ? 'selected="selected"' : '' ?> value="2"><?php echo esc_html__( 'Not that bad', 'woocommerce' ) ?></option>
								<option <?php echo $review_rating == 1 ? 'selected="selected"' : '' ?> value="1"><?php echo esc_html__( 'Very poor', 'woocommerce' ) ?></option>
							</select>
							<div class="wcor_feedback__review__rating__error"></div>
						</div>
						<label for="<?php echo $review_field_name ?>"><?php echo __( 'Your review', 'wc-order-reviews' ) ?>&nbsp;<span class="required">*</span></label>
					<?php endif; ?>
					<textarea class="wcor_feedback__review__content" name="<?php echo $review_field_name ?>" id="<?php echo $review_field_name ?>" required="required"><?php echo $review_content ?></textarea>
					<?php if ( $this->image_upload_enabled && ! $store_review ) : ?>
						<div class="wcor_feedback__review__photos">
							<label for="<?php echo $photos_field_name ?>"><?php echo __( 'Upload pictures of the product', 'wc-order-reviews' ) ?></label>
							<?php echo isset( $review_id ) ? wcor_review_photos_html( $review_id, $line_item_id ) : ''  ?>
							<input type="file" accept="image/*" id="<?php echo $photos_field_name ?>" name="<?php echo $photos_field_name ?>" multiple>
						</div>
					<?php endif; ?>
				</div>
			</div>

		<?php }

	}

}