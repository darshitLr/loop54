<?php

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WCOR_Coupons' ) ) {

	class WCOR_Coupons {

		public function __construct() {
            if ( wc_string_to_bool( get_option( 'wcor_generate_coupon' ) ) ) {
                add_action('wcor_successfully_order_review',array($this,'add_coupon'),10,1);
                add_action('wcor_review_intro_update',array($this,'display_previously_generated_coupon'),10,1);
            }

        }
        public function add_coupon($order){
            
            if( !wc_coupons_enabled() ){
                return;
            }
            //Dont generate if we already have coupon for this order
            $order->get_meta( '_wcor_coupon_generated', true );
            if( !empty($order->get_meta( '_wcor_coupon_generated', true ) ) ){
                //Is Coupon valid?
                wc_add_notice( sprintf( __( 'Dont forget your Coupon Code <code>%s</code>', 'wc-order-reviews' ), strtoupper( $order->get_meta( '_wcor_coupon_code', true ) ) ), 'success' );
                return;
            }
            $coupon = $this->generate_coupon($order);
            if( is_null( $coupon->get_date_expires() ) ){
                $message = sprintf( __( '&nbsp;Your discount code is: <code>%s</code>', 'wc-order-reviews' ), strtoupper( $coupon->get_code() ) );
            }else{
                $message = sprintf( __( '&nbsp;Your discount code is: <code>%s</code> and is valid until %s', 'wc-order-reviews' ), strtoupper( $coupon->get_code() ), $coupon->get_date_expires()->date( 'Y-m-d' ) );

            }
            wc_add_notice( apply_filters( 'wcor_generated_coupon_message', get_option( 'wcor_generate_coupon_message' ) ) . $message , apply_filters( 'wcor_generated_coupon_message_type', 'success' ) );
        }
        public function display_previously_generated_coupon($order){
            $order->get_meta( '_wcor_coupon_generated', true );
            if( !empty($order->get_meta( '_wcor_coupon_generated', true ) ) ){
                //Is Coupon valid?
                wc_add_notice( sprintf( __( 'Dont forget your Coupon Code <code>%s</code>', 'wc-order-reviews' ), strtoupper( $order->get_meta( '_wcor_coupon_code', true ) ) ), 'success' );
            }
        }
        public function generate_coupon_code() {
            global $wpdb;
            
            // Get an array of all existing coupon codes
            $coupon_codes = $wpdb->get_col("SELECT post_name FROM $wpdb->posts WHERE post_type = 'shop_coupon'");
            
            for ( $i = 0; $i < 1; $i++ ) {
                $generated_code = strtolower( wp_generate_password( apply_filters( 'wcor_generate_coupon_length', 9 ), false ) );
                
                // Check if the generated code doesn't exist yet
                if( in_array( $generated_code, $coupon_codes ) ) {
                    $i--; // continue the loop and generate a new code
                } else {
                    break; // stop the loop: The generated coupon code doesn't exist already
                }
            }
            return $generated_code;
        }

        public function generate_coupon($order) {

            $coupon_type = apply_filters( 'wcor_generate_coupon_type', get_option( 'wcor_coupon_type' ) ); 
            $coupon_value = apply_filters( 'wco_generater_coupon_value', get_option( 'wcor_coupon_value' ) ); 
            $free_shipping = apply_filters( 'wcor_generate_coupon_free_shipping', get_option( 'wcor_coupon_free_shipping' ) ); 
            $minimum_amount = apply_filters( 'wcor_generate_coupon_minimum_amount', get_option( 'wcor_coupon_minimum_amount' ) ); 
            $maximum_amount = apply_filters( 'wcor_generate_coupon_maximum_amount', get_option( 'wcor_coupon_maximum_amount' ) ); 
            $individual_use = apply_filters( 'wcor_generate_coupon_individual_use', get_option( 'wcor_coupon_individual_use' ) ); 

            $exclude_sale_items = apply_filters( 'wcor_generate_coupon_exclude_sale_items', get_option( 'wcor_coupon_exclude_sale_items' ) ); 
            $product_ids = apply_filters( 'wcor_generate_coupon_product_ids', get_option( 'wcor_coupon_product_ids' ) ); 
            $exclude_product_ids = apply_filters( 'wcor_generate_coupon_exclude_product_ids', get_option( 'wcor_coupon_exclude_product_ids' ) ); 
            //Our categories are stored as slugs, get the IDS
            $categories = array();
            foreach(get_option( 'wcor_coupon_product_categories' ) as $category_slug){
                $term = get_term_by( 'slug', $category_slug, 'product_cat' );
                $categories[] = $term->term_id;
            }
            $product_categories = apply_filters( 'wcor_generate_coupon_product_categories', $categories ); 

            $excl_categories = array();
            foreach(get_option( 'wcor_coupon_exclude_product_categories' ) as $category_slug){
                $term = get_term_by( 'slug', $category_slug, 'product_cat' );
                $excl_categories[] = $term->term_id;
            }
            $excluded_product_categories = apply_filters( 'wcor_generate_coupon_exclude_product_categories', $excl_categories ); 
            
            $valid_days = apply_filters( 'wcor_generate_coupon_valid_days', get_option( 'wcor_coupon_valid_days' ) ); 
            $usage_limit = apply_filters( 'wcor_generate_coupon_usage_limit', get_option( 'wcor_coupon_usage_limit' ) ); 
            $personal = apply_filters( 'wcor_generate_coupon_personal', get_option( 'wcor_coupon_personal' ) ); 

            $date_expires     = date('Y-m-d', strtotime('+'.$valid_days.' days'));
            $coupon_code  = $this->generate_coupon_code();

            $coupon = new WC_Coupon($coupon_code);
            
            //$coupon->set_code( $coupon_code );
            $coupon->set_discount_type( $coupon_type );
            $coupon->set_amount( $coupon_value );
            $coupon->set_description( sprintf( __( 'Automatically generated for order #%s via Order Reviews ', 'wc-order-reviews' ), $order->get_order_number() ) );
            $coupon->set_free_shipping( wc_string_to_bool($free_shipping) );
            $coupon->set_minimum_amount( $minimum_amount );
            $coupon->set_maximum_amount( $maximum_amount );
            $coupon->set_individual_use( wc_string_to_bool($individual_use) );
            $coupon->set_exclude_sale_items( wc_string_to_bool($exclude_sale_items) );

            $coupon->set_product_ids( $product_ids );
            $coupon->set_excluded_product_ids( $exclude_product_ids );
            if(is_array($product_categories)){
                $coupon->set_product_categories( $product_categories );
            }
            if(is_array($excluded_product_categories)){
                $coupon->set_excluded_product_categories( $excluded_product_categories );

            }

            if($valid_days > 0){
                $coupon->set_date_expires( $date_expires );
            }

            if($usage_limit > 0){
                $coupon->set_usage_limit( $usage_limit );
            }
            
            if( wc_string_to_bool($personal) ){
                $email_array = array();
                $email_array[] = $order->get_billing_email();
                $coupon->set_email_restrictions( $email_array );
            }
            

            // Create, publish and save coupon (data)
            $coupon->save();
            //Save coupon to ORDER meta
            $order->update_meta_data( '_wcor_coupon_generated', true );
            $order->update_meta_data( '_wcor_coupon_code', $coupon->get_code() );
            $order->save();
            //Add meta to coupon, "generated via order"
            return $coupon;
        }



	}

}