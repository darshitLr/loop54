<div id="reviews" class="woocommerce-Reviews">
	<div id="comments">
		<?php if ( count( $store_reviews ) > 0 ) : ?>

			<ol class="commentlist">
				<?php wp_list_comments(  array( 'type' => 'all', 'callback' => 'woocommerce_comments' ) , $store_reviews ); ?>
			</ol>

		<?php else : ?>

			<p class="woocommerce-noreviews"><?php _e( 'There are no reviews yet.', 'woocommerce' ); ?></p>

		<?php endif; ?>
	</div>
	<div class="clear"></div>
</div>
