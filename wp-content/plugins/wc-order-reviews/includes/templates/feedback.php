<?php 

// Feedback page template
get_header(); 

$first_name = $order->get_billing_first_name();

$line_items = $order->get_items();

$store_review = array(
	'rating' => apply_filters( 'wcor_default_store_review_rating', '' ),
	'content' => ''
);

$is_store_reviewed = wc_string_to_bool( $order->get_meta( '_wcor_store_reviewed' ) );

if ( $is_store_reviewed ) {

	$review_id = $order->get_meta( '_wcor_store_review_id' );

	$review = get_comment( $review_id );

	if ( ! empty( $review ) ) {

		$store_review['rating'] = get_comment_meta( $review_id, 'rating', true );

		$store_review['content'] = $review->comment_content;

	}

}

$products = array();

foreach ( $line_items as $line_item ) {

	$line_item_id = $line_item->get_id();

	$line_item_data = $line_item->get_data();

	$refunded_qty = $order->get_qty_refunded_for_item( $line_item_id );

	if ( $refunded_qty ) {
	
		if ( $line_item->get_quantity() - abs($refunded_qty) <= 0 ){
	
			continue;
	
		}
	
	}

	$product_id = $line_item_data['variation_id'] ? $line_item_data['variation_id'] : $line_item_data['product_id'];

	$product = wc_get_product( $product_id );
	
	if( apply_filters( 'wcor_do_not_ask_for_review', false, $product, $order, $line_item ) ){
		//This filter can be used to skip certains products and not ask for review
		continue;
	}
	
	$image = $product->get_image( 'full' );

	$name = $product->get_name();

	$products[$line_item_id] = array(
		'image' => $image,
		'name' => $name,
		'review_content' => '',
		'review_rating' => apply_filters( 'wcor_default_product_review_rating', '' ),
		'line_item_id' => $line_item_id
	);

	$is_reviewed = $line_item->get_meta( '_wcor_reviewed' );

	if ( $is_reviewed == true ) {

		$review_id = $line_item->get_meta('_wcor_review_id');

		$review = get_comment( $review_id );

		if ( ! empty( $review ) ) {

			$products[$line_item_id]['review_id'] = $review_id;
			$products[$line_item_id]['review_content'] = $review->comment_content;

		}

		$review_rating = get_comment_meta( $review_id, 'rating', true );

		if ( ! empty( $review_rating ) ) {

			$products[$line_item_id]['review_rating'] = $review_rating;

		}


	}

}

?>

<div class="wcor_feedback_container woocommerce">
	<?php wc_print_notices();?>
	<h2 class="wcor_feedback__title">
		<?php if ( $is_order_reviewed ) : ?>
			<?php echo __( 'Update your review', 'wc-order-reviews' ) ?>
		<?php else: ?>
			<?php echo __( 'Review your order', 'wc-order-reviews' ) ?>
		<?php endif; ?>			
	</h2>
	<?php do_action('wcor_before_review_intro',$order); ?>
	<div class="wcor_feedback__intro">
		<?php if ( $is_order_reviewed ) : ?>
			<?php printf( __( 'Hi %s! We hope that you are satisfied with your purchase with us at %s. We would to know if you would like to update your review.', 'wc-order-reviews' ), $first_name, $site_name ) ?>
			<?php do_action('wcor_review_intro_update',$order); ?>
		<?php else: ?>
			<?php printf( __( 'Hi %s! We hope that you are satisfied with your purchase with us at %s and that you have already started using your products. We would be incredibly grateful if you would help other customers by leaving feedback on your most recent purchase in the form below.', 'wc-order-reviews' ), $first_name, $site_name ) ?>
			<?php do_action('wcor_review_intro',$order); ?>
		<?php endif; ?>
		
	</div>
	<?php do_action('wcor_after_review_intro',$order); ?>
	<form id="wcor_feedback_form" action="" method="POST" <?php echo wcor_image_upload_enabled() ? 'enctype="multipart/form-data"' : '' ?>>
		<div class="wcor_feedback__reviews">
			<?php foreach ( $products as $line_item_id => $product ) : ?>
				<?php wcor()->forms->build_form( $product ) ?>
			<?php endforeach; ?>
			<?php 
			$display_store_review = apply_filters('wcor_display_store_review_in_form', true, $order);
			if ($display_store_review) {
				wcor()->forms->build_form( array(
					'name' => $site_name,
					'image' => null,
					'review_rating' => $store_review['rating'],
					'review_content' => $store_review['content'],
					'store_review' => true
				) ); 
			}
			?>
		</div>
		<?php wp_nonce_field( 'wcor-save-reviews-nonce' ) ?>
		<input type="hidden" name="action" value="wcor_save_reviews">
		<input type="hidden" name="wcor_order_id" value="<?php echo $order->get_id() ?>">
		<input class="button" type="submit" value="<?php _e('Submit','wc-order-reviews')?>">
	</form>

</div>

<?php get_footer() ?>
