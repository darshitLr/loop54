<?php get_header() ?>
	<div class="wcor_feedback_container">
		<?php wc_print_notices();?>
		<h2 class="wcor_feedback__title">
			<?php echo __( 'Review your order', 'wc-order-reviews' ) ?>
		</h2>
		<form action="" method="POST" class="wcor_feedback_container__form">
			<p><?php _e('To give feedback about your order, please enter your ordernumber and your email address.','wc-order-reviews') ?></p>
			<div class="wcor_feedback_container__form__fields">
				<div class="wcor_feedback_container__form__field__group">
					<?php woocommerce_form_field( 'gpm_field_order_id', array(
						'type' => 'text',
						'label' => __( 'Order Number','wc-order-reviews' ),
						'required' => true,
						'custom_attributes' => [ 'required' => 'required' ]
					), isset( $_POST['gpm_field_order_id'] ) ? sanitize_text_field( $_POST['gpm_field_order_id'] ) : '' ) ?>
				</div>
				<div class="wcor_feedback_container__form__field__group">
					<?php woocommerce_form_field( 'gpm_field_order_email', array(
						'type' => 'email',
						'label' => __( 'Billing email','wc-order-reviews' ),
						'required' => true,
						'custom_attributes' => [ 'required' => 'required' ]
					), isset( $_POST['gpm_field_order_email'] ) ? sanitize_text_field( $_POST['gpm_field_order_email'] ) : '' ) ?>
				</div>
			</div>
			<?php wp_nonce_field( 'wcor-find-order-nonce' ) ?>
			<input type="hidden" name="action" value="wcor_retrieve_order">
			<input type="submit" class="woocommerce-Button button" value="<?php _e('Submit','wc-order-reviews')?>">
		</form>
	</div>
<?php get_footer() ?>
