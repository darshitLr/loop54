<div class="wcorb_reviews__review__helpful" style="<?= wcor_get_styles( $ea, false, true ) ?>">
  <?= sprintf( _n( '%s person found this helpful', '%s people found this helpful', $review['helpful'], 'wc-order-reviews' ), $review['helpful'] ); ?>
</div>
