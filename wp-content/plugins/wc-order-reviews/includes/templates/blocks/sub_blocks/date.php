<div class="wcorb_reviews__review__date" style="<?= wcor_get_styles( $ea, false, true ) ?>">
  <?php

  if ( $ea['format'] == 'ago' ) {

    echo wcor_get_time_ago( $review['date'] );

  } else {

    $date = date_create( $review['date'] );
    echo date_format( $date, wcor_moment_date_to_php( $ea['format'] ) );

  }

  ?>
</div>
