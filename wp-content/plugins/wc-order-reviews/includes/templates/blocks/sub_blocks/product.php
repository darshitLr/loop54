<a href="<?= $review['product']['link'] ?>" class="wcorb_reviews__review__product" style="<?= wcor_get_styles( $ea, true, true ) ?>">
  <?php if ( $ea['showImage'] ) : ?>
    <img src="<?= $review['product']['image'] ?>" alt="<?= $review['product']['name'] ?>">
  <?php else: ?>
    <?= $review['product']['name'] ?>
  <?php endif; ?>
</a>
