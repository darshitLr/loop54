<div class="wcorb_reviews__review__verified" style="<?= wcor_get_styles( $ea, true, true ) ?>">
  <div class="wcorb_reviews__review__verified__icon">
    <svg viewBox="0 0 100 125" style="fill: <?= $ea['iconColor'] ?>;">
      <path d="M93.4 56c-1.5-3.7-1.5-7.8 0-11.4l.5-1.2c3.2-7.6-.5-16.4-8.1-19.6l-1.1-.5c-3.7-1.5-6.6-4.4-8.1-8.1l-.4-1C72.9 6.6 64.2 3 56.5 6.1l-1 .4C51.8 8 47.7 8 44 6.5l-.9-.4c-7.5-3.1-16.3.6-19.5 8.2l-.4.8c-1.5 3.7-4.4 6.6-8.1 8.1l-.9.4c-7.5 3.2-11.2 12-8 19.6l.4.9c1.5 3.7 1.5 7.8 0 11.4l-.4 1.1C3 64.2 6.6 73 14.3 76.1l1 .4c3.7 1.5 6.6 4.4 8.1 8.1l.5 1.1C27 93.4 35.8 97 43.4 93.9l1.1-.5c3.7-1.5 7.8-1.5 11.5 0l.9.4c7.6 3.2 16.4-.5 19.6-8.1l.4-.8c1.5-3.7 4.4-6.6 8.1-8.1l.8-.3c7.7-3.2 11.3-11.9 8.1-19.6l-.5-.9zM45.8 69.5L26.3 53.1l6-7.1 12.4 10.5 21.9-26 7.1 6-27.9 33z" />
    </svg>
  </div>
  <div class="wcorb_reviews__review__verified__label" style="<?= wcor_get_styles( $ea, false, false ) ?>">
    <?= $ea['text'] ?>
  </div>
</div>
