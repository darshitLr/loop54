<div class="wcorb_reviews__review__photos" style="<?= wcor_get_styles( $ea, true, true ) ?>">
  <?php foreach( $review['photos'] as $photo ) : ?>
    <a href="<?= $photo['full'] ?>" data-fancybox="<?= $review['id'] ?>">
      <img src="<?= $photo['thumbnail'] ?>">
    </a>
  <?php endforeach; ?>
</div>
