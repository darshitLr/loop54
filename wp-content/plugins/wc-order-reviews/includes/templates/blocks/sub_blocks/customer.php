<div class="wcorb_reviews__review__author" style="<?= wcor_get_styles( $ea, true, true ) ?>">
  <?php if ( $ea['showAvatar'] ) : ?>
    <div class="wcorb_reviews__review__avatar" style="<?= $ea['showCustomerName'] ? 'marginRight: 10px;' : '' ?>">
      <div class="wcorb_reviews__review__avatar__default">
        <?= substr( $review['customer_name'], 0, 1 ) ?>
      </div>
      <img src="<?= $review['avatar'] ?>" class="wcorb_reviews__review__avatar__image" />
    </div>
  <?php endif; ?>
  <?php if ( $ea['showCustomerName'] ) : ?>
    <div class="wcorb_reviews__review__author__name" style="<?= wcor_get_styles( $ea, false, false ) ?>">
      <?= $review['customer_name'] ?>
    </div>
  <?php endif; ?>
</div>
