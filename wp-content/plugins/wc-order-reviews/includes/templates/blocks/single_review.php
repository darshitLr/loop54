<div class="wcorb_reviews__review">
  <?php foreach( $attributes['reviewLayout'] as $element ) {

  $e = $element[0]; $ea = $element[1];

    switch ( $e ) {
      case 'wc-order-reviews/reviews-block-rating': ?>
        <?php include WCOR_PATH . '/includes/templates/blocks/sub_blocks/rating.php' ?>
      <?php break;
      case 'wc-order-reviews/reviews-block-customer': ?>
        <?php include WCOR_PATH . '/includes/templates/blocks/sub_blocks/customer.php' ?>
      <?php break;
      case 'wc-order-reviews/reviews-block-content': ?>
        <?php include WCOR_PATH . '/includes/templates/blocks/sub_blocks/content.php' ?>
      <?php break;
      case 'wc-order-reviews/reviews-block-photos': ?>
        <?php if ( !$review['photos'] ) break ?>
        <?php include WCOR_PATH . '/includes/templates/blocks/sub_blocks/photos.php' ?>
      <?php break;
      case 'wc-order-reviews/reviews-block-product': ?>
        <?php if ( $review['type'] == 'store_review' ) break; ?>
        <?php include WCOR_PATH . '/includes/templates/blocks/sub_blocks/product.php' ?>
      <?php break;
      case 'wc-order-reviews/reviews-block-helpful': ?>
      <?php if ( $review['type'] == 'store_review' || $review['helpful'] == 0 ) break; ?>
        <?php include WCOR_PATH . '/includes/templates/blocks/sub_blocks/helpful.php' ?>
      <?php break;
      case 'wc-order-reviews/reviews-block-verified': ?>
        <?php include WCOR_PATH . '/includes/templates/blocks/sub_blocks/verified.php' ?>
      <?php break;
      case 'wc-order-reviews/reviews-block-date': ?>
        <?php include WCOR_PATH . '/includes/templates/blocks/sub_blocks/date.php' ?>
      <?php break;
    }

  } ?>
</div>
