<div class="wcorb_reviews <?php echo $attributes['overallRatingPosition'] ?>">
  <div class="wcorb_reviews__overall">
    <div class="wcorb_reviews__overall__label" style="color: <?= $attributes['orTextColor']; ?>; font-size: <?= $attributes['orTextSize']; ?>px;"><?php echo $attributes['orText'] ?></div>
      <?php wcor_generate_rating_html( $reviews['average'], $attributes['orStarSize'], $attributes['orStarSpace'], $attributes['orStarColor'] ); ?>
    <div class="wcorb_reviews__overall__number" style="color: <?= $attributes['orTextColor']; ?>;">
      <?php echo __( 'Based on', 'wc-order-reviews' ) ?> <strong><?= sprintf( _n( '%s review', '%s reviews', $reviews['count'], 'wc-order-reviews' ) , $reviews['count'] ) ?></strong>
    </div>
  </div>
  <div class="wcorb_reviews__reviews_list">
    <div class="wcorb_reviews__reviews_slider" data-autoplay="<?= $attributes['autoPlay'] ? 'true' : 'false' ?>">
      <?php foreach( $reviews['reviews'] as $review ) : ?>
        <?php include WCOR_PATH . '/includes/templates/blocks/single_review.php' ?>
      <?php endforeach; ?>
    </div>
  </div>
</div>
