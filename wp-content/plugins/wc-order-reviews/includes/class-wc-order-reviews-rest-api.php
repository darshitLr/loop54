<?php

if ( !defined( 'ABSPATH' ) ) {
  exit;
}

if ( !class_exists( 'WCOR_Rest_API' ) ) {

  class WCOR_Rest_API {

    private $namespace;
    private $routes;

    function __construct() {

      $this->namespace = 'wcor';

      $this->routes = array(
        array(
          'path' => 'reviews',
          'method' => 'GET',
          'handler' => 'show_reviews',
        )
      );

      $this->init();

    }

    function init() {

      add_action( 'rest_api_init', function () {

        foreach ($this->routes as $route) {

          register_rest_route( $this->namespace, $route['path'], array(
            'methods' => $route['method'],
            'callback' => array( $this, $route['handler'] ),
            'args' => $route['args'] ?? array(),
            'permission_callback' => $route['permission_callback'] ?? null,
          ) );

        }

      } );

    }

    public function show_reviews( $request ) {

      $params = $request->get_params();

      $args = array(
				'type' => $params['types'],
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => 'rating',
						'value' => $params['mininmumRating'],
						'compare' => '>='
					)
				),
				'number' => $params['limit'],
				'random_order' => $params['randomOrder']
			);


      $response = new WP_REST_Response();

      $response->set_data( wcor()->store->get_reviews( $args ) );

      return $response;

    }

  }

}
