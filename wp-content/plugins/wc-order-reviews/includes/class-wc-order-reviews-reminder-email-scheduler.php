<?php

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WCOR_Reminder_Email_Scheduler' ) ) {

	class WCOR_Reminder_Email_Scheduler {

		private $reminder_status;

		private $reminder_delay;

		public function __construct() {

			$this->reminder_status = apply_filters( 'wcor_email_reminder_status', str_replace( 'wc-', '', get_option( 'wcor_email_reminder_status', 'completed' ) ) );
            $this->reminder_delay = apply_filters( 'wcor_email_reminder_delay', get_option( 'wcor_email_reminder_delay' ) ); 

            add_action( 'woocommerce_order_status_changed', array( $this, 'schedule_email' ), 10, 3 );
            add_action( 'wcor_trigger_reminder_email', array( $this, 'trigger_email' ), 10, 2 );

            if ( function_exists( 'PLLWC' ) ) {
                add_action( 'wcor_trigger_reminder_email', array( PLLWC()->emails, 'before_order_email' ), 1 ); // Switch the language for the email.
                add_action( 'wcor_trigger_reminder_email', array( PLLWC()->emails, 'after_email' ), 999 ); // Switch the language back after the email has been sent.
            }

		}

		public function trigger_email( $order_id, $attachment = array() ) {

			//error_log( 'Triggering email class' );

			if ( $this->is_order_reminder_disabled( $order_id ) ) {

                //error_log( 'Won\'t send reminder for order #' . $order_id );

                return;

            }

            if( apply_filters( 'wcor_email_trigger_dont_send', false, $order_id ) ){
                return;
            }

            WC()->mailer();
            $email = WC()->mailer()->emails['WCOR_Reminder_Email'];
            $email->trigger( $order_id, $attachment);


		}

		public function schedule_email( $order_id, $old_status, $new_status ) {

            //error_log('Status changed to ' . $new_status);

            //error_log('Required status is ' . $this->reminder_status);

            if ( $new_status == $this->reminder_status ) {

                if ( $this->is_order_reminder_disabled( $order_id ) ) {

                    //error_log( 'Won\'t send reminder for order #' . $order_id );

                    return;

                }

                //error_log( 'Scheduling reminder for order #' . $order_id );

                $delay = floatval( $this->reminder_delay ) * 86400;

                //error_log('Delay is ' . $delay );

                $schedule_id = as_schedule_single_action( time() + $delay, 'wcor_trigger_reminder_email', array( $order_id ), 'wc-order-reviews' );

                //error_log('schedule_id is ' . $schedule_id);

            }

        }

        public function is_order_reminder_disabled( $order_id ) {

			return wc_string_to_bool( get_post_meta( $order_id, '_wcor_disable_reminder', true ) );

		}

	}

}
