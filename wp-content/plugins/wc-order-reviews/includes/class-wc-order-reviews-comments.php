<?php

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WCOR_Comments' ) ) {

	class WCOR_Comments {

		function __construct() {
			add_action('transition_comment_status', array($this, 'comment_rating_average'),10,3 );
		}

		public function comment_rating_average($new_status, $old_status, $comment){
			if('review' == $comment->comment_type || 'store_review' == $comment->comment_type){
				if($old_status != $new_status) {
					if($new_status == 'approved') {
						//Get total count and average
						$reviews = get_comments(
							array(
								'type' => $comment->comment_type,
								'status' => 'approve',
								'meta_key'=> 'rating'
							)
						);
						//wc_get_logger()->log('info','Getting reviews that are approved and has rating: ' . wp_json_encode( $reviews ), array( 'source' => 'wc-order-reviews' ) );
						$average = 0;
						$total_reviews = count($reviews);
						foreach($reviews as $review){
							$rating = floatval( get_comment_meta( $review->comment_ID, 'rating', true ) );
							$average += $rating;
						}
						$results = (object) array(
							'count' => $total_reviews,
							'average_rating' => number_format( $average / $total_reviews, 2 )
						);
						set_transient('wcor_total_reviews_'.$comment->comment_type, $results, WEEK_IN_SECONDS ); //Save this info for a week
						//wc_get_logger()->log('info','This is our result count and average: ' . wp_json_encode( $results ), array( 'source' => 'wc-order-reviews' ) );


					}
					if($new_status == 'unapproved') {
						delete_transient('wcor_total_reviews_'.$comment->comment_type ); //Remove any old cache
						//wc_get_logger()->log('info','A Comment was unapproved, remove transient', array( 'source' => 'wc-order-reviews' ) );
					}
				}
			}

		}
	}

}
