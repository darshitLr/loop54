<?php

defined( 'ABSPATH' ) || exit;

if ( class_exists( 'WCOR_Settings' ) ) {

	return new WCOR_Settings();

}

class WCOR_Settings extends WC_Settings_Page {

	public function __construct() {

		$this->id    = 'wcor';

	    $this->label = __( 'Order Reviews', 'wc-order-reviews' );
	            
	    add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_page' ), 20 );
	    //add_action( 'woocommerce_settings_' . $this->id, array( $this, 'render_settings' ) );
	    add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'save_settings' ) );
		parent::__construct();

	}

	public function add_settings_page( $pages ) {

		$pages[ $this->id ] = $this->label;
		return $pages;

	}
	
	public function get_sections(){
		$sections = array(
			''			=> __( 'General', 'wc-order-reviews' ),
		);
		if( wc_coupons_enabled() ){
			$sections['discount'] = __( 'Discount', 'wc-order-reviews' );
		}
		return apply_filters('woocommerce_get_sections_' . $this->id, $sections);
	}
	
	/**
     * Output the settings.
     */
	public function output()
	{
		global $current_section;
		$settings = $this->get_settings($current_section);
		WC_Admin_Settings::output_fields($settings);
	}
	public function get_settings( $current_section = '' ) {

		if('discount' === $current_section) {
			//Products
			$exclude_products = array();
			if(get_option('wcor_coupon_exclude_product_ids')){
				foreach ( get_option('wcor_coupon_exclude_product_ids') as $product_id ) {
					$product = wc_get_product( $product_id );
					if ( is_object( $product ) ) {
						$exclude_products[$product_id] = wp_kses($product->get_formatted_name(),[]);
					}
				}
			}
			$include_products = array();
			if(get_option('wcor_coupon_product_ids')){
				foreach ( get_option('wcor_coupon_product_ids') as $product_id ) {
					$product = wc_get_product( $product_id );
					if ( is_object( $product ) ) {
						$include_products[$product_id] = wp_kses($product->get_formatted_name(),[]);
					}
				}
			}
			//Categories
			$exclude_categories = array();
			if(get_option('wcor_coupon_exclude_product_categories')){
				$categories   = get_terms( 'product_cat', 'orderby=name&hide_empty=0' );
				if ( $categories ) {
					foreach ( $categories as $cat ) {
						if(in_array($cat->slug, get_option('wcor_coupon_exclude_product_categories'))){
							$exclude_categories[$cat->slug] = $cat->name;
						}
					}
				}
			}
			$include_categories = array();
			if(get_option('wcor_coupon_product_categories')){
				$categories   = get_terms( 'product_cat', 'orderby=name&hide_empty=0' );
				if ( $categories ) {
					foreach ( $categories as $cat ) {
						if(in_array($cat->slug, get_option('wcor_coupon_product_categories'))){
							$include_categories[$cat->slug] = $cat->name;
						}
					}
				}
			}
			
			$settings = apply_filters( 'wcor_discount_settings', array(
				array(
					'title'    => __( 'Discount', 'wc-order-reviews' ),
					'desc' 	   => '',
					'id'       => 'wcor_discount_options',
					'type'     => 'title',
				),
				array(
					'title'    => __( 'Generate Coupon Code', 'wc-order-reviews' ),
					'desc' => __( 'Enabling this will generate a Coupon Code to the customer after submitting review', 'wc-order-reviews' ),
					'id'       => 'wcor_generate_coupon',
					'type'     => 'checkbox',
					'desc_tip' => true,
				),
				array(
					'title'     => __( 'Message', 'wc-order-reviews' ),
					'id'       	=> 'wcor_generate_coupon_message',
					'desc_tip'  => __( 'Enter the description you want shown to the user when they recive the discount code.', 'wc-order-reviews' ),
					'default'   => __( 'As a thank you to get a discount code from us to use on your next purchase', 'wc-order-reviews' ),
					'type'      => 'textarea',
					'custom_attributes' => array(
						'rows' => 4,
					)
				),
				array(
					'title'   => __( 'Coupon Type', 'wc-order-reviews' ),
					'id'      => 'wcor_coupon_type',
					'type'    => 'select',
					'options' => wc_get_coupon_types(),
				),
				array(
					'title'   => __( 'Coupon Value', 'wc-order-reviews' ),
					'desc' => __( 'The value of the coupon', 'wc-order-reviews' ),
					'placeholder' => wc_format_localized_price( 0 ),
					'id'      => 'wcor_coupon_value',
					'type'    => 'text',
				),
				array(
					'title'    => __( 'Free Shipping Coupon Code', 'wc-order-reviews' ),
					'desc' => __( 'Enabling this will generate a Coupon Code with free shipping', 'wc-order-reviews' ),
					'id'       => 'wcor_coupon_free_shipping',
					'type'     => 'checkbox',
					'desc_tip' => true,
				),
				array(
					'id'          => 'wcor_coupon_minimum_amount',
					'title'       => __( 'Minimum spend', 'woocommerce' ),
					'placeholder' => __( 'No minimum', 'woocommerce' ),
					'desc'        => __( 'This field allows you to set the minimum spend (subtotal, including taxes) allowed to use the coupon.', 'woocommerce' ),
					'type'        => 'text',
					'class'		  => 'wc_input_price',
					'desc_tip'    => true,
				),
				array(
					'id'          => 'wcor_coupon_maximum_amount',
					'title'       => __( 'Maximum spend', 'woocommerce' ),
					'placeholder' => __( 'No maximum', 'woocommerce' ),
					'desc'        => __( 'This field allows you to set the maximum spend (subtotal, including taxes) allowed when using the coupon.', 'woocommerce' ),
					'type'        => 'text',
					'class'		  => 'wc_input_price',
					'desc_tip'    => true,
				),
				array(
					'id'      => 'wcor_coupon_individual_use',
					'title'   => __( 'Individual use only', 'woocommerce' ),
					'desc'    => __( 'Check this box if the coupon cannot be used in conjunction with other coupons.', 'woocommerce' ),
					'default' => 'no',
					'type'    => 'checkbox'
				),
				array(
					'id'      => 'wcor_coupon_exclude_sale_items',
					'title'   => __( 'Exclude sale items', 'woocommerce' ),
					'desc'    => __( 'Check this box if the coupon should not apply to items on sale. Per-item coupons will only work if the item is not on sale. Per-cart coupons will only work if there are items in the cart that are not on sale.', 'woocommerce' ),
					'default' => 'no',
					'type'    => 'checkbox'
				),
				array(
					'id'          => 'wcor_coupon_product_ids',
					'title'       => __( 'Products', 'woocommerce' ),
					'desc'        => __( 'Products which need to be in the cart to use this coupon or, for "Product Discounts", which products are discounted.', 'woocommerce' ),
					'placeholder' => __( 'Search for a product&hellip;', 'woocommerce' ),
					'type'        => 'multiselect',
					'class'		  => 'wc-product-search',
					'options' 	  => $include_products,
					'custom_attributes' => array(
						'data-action' => 'woocommerce_json_search_products_and_variations',
					)
				),
				array(
					'id'          => 'wcor_coupon_exclude_product_ids',
					'title'       => __( 'Exclude products', 'woocommerce' ),
					'desc'        => __( 'Products which must not be in the cart to use this coupon or, for "Product Discounts", which products are not discounted.', 'woocommerce' ),
					'placeholder' => __( 'Search for a product&hellip;', 'woocommerce' ),
					'type'        => 'multiselect',
					'class'		  => 'wc-product-search',
					'options' 	  => $exclude_products,
					'custom_attributes' => array(
						'data-action' => 'woocommerce_json_search_products_and_variations',
					)
				),
				array(
					'title'    => __( 'Product categories', 'wc-order-reviews' ),
					'type'     => 'multiselect',
					'desc'     => __( 'A product must be in this category for the coupon to remain valid or, for "Product Discounts", products in these categories will be discounted.', 'wc-order-reviews' ),
					'id'       => 'wcor_coupon_product_categories',
					'desc_tip' => true,
					'class'    => 'wc-category-search',
					'options'  => $include_categories,
					'css'      => 'min-width:400px;'
				),
				array(
					'title'    => __( 'Exclude categories', 'wc-order-reviews' ),
					'type'     => 'multiselect',
					'desc'     => __( 'Product must not be in this category for the coupon to remain valid or, for "Product Discounts", products in these categories will not be discounted.', 'wc-order-reviews' ),
					'id'       => 'wcor_coupon_exclude_product_categories',
					'desc_tip' => true,
					'class'    => 'wc-category-search',
					'options'  => $exclude_categories,
					'css'      => 'min-width:300px;'
				),
				array(
					'title'    => __( 'Period of validity', 'wc-order-reviews' ),
					'desc' => __( 'Amount of days that the coupon will be valid', 'wc-order-reviews' ),
					'id'       => 'wcor_coupon_valid_days',
					'type'     => 'number',
					'desc_tip' => true,
					'custom_attributes' => array(
						'step' => 1,
						'min'  => 0,
					)
				),
				array(
					'id'                => 'wcor_coupon_usage_limit',
					'title'             => __( 'Usage limits', 'woocommerce' ),
					'desc'              => __( 'How many times this coupon can be used before it is void. Set it to 0 for unlimited usage.', 'woocommerce' ),
					'type'              => 'number',
					'desc_tip'          => true,
					'default'           => 0,
					'placeholder'       => 0,
					'class'             => 'short',
					'custom_attributes' => array(
						'step' => 1,
						'min'  => 0,
					)
				),
				array(
					'title'    => __( 'Personal usage', 'wc-order-reviews' ),
					'desc' => __( 'Should the coupon be personal?', 'wc-order-reviews' ),
					'id'       => 'wcor_coupon_personal',
					'type'     => 'checkbox',
					'desc_tip' => true,
				),
				array(
					'id'       => 'wcor_discount_options',
					'type'     => 'sectionend',
				),
			));
		}else{
			$no_status  = array(
				'none' => __( 'Dont send email reminder','wc-order-reviews' ),
			);
			$statuses   = array_merge( $no_status, wc_get_order_statuses() );
			$settings = apply_filters( 'wcor_general_settings', array(
				array(
					'title'    => __( 'General', 'wc-order-reviews' ),
					'desc' 	   => '',
					'id'       => 'wcor_general_options',
					'type'     => 'title',
				),
				array(
					'title'    => __( 'License-Key', 'wc-order-reviews' ),
					'desc' => __( 'License-Key that you received when buying the plugin from redlight.se', 'wc-order-reviews' ),
					'id'       => 'wcor_redlight_license_key',
					'type'     => 'text',
					'desc_tip' => true,
				),
				array(
					'title'     => __( 'Deactivate this license', 'wc-order-reviews' ),
					'type'      => 'checkbox',
					'desc'   => __( 'Deactivating this license key will disable updates', 'wc-order-reviews' ),
					'desc_tip' => true,
					'id'    => 'wcor_redlight_license_deactivate',
				),
				array(
					'title'   => __( 'Email Reminder Delay (Days)', 'wc-order-reviews' ),
					'id'      => 'wcor_email_reminder_delay',
					'desc'   => __( 'Email reminders will be sent X days after an order reaches the status selected below', 'wc-order-reviews' ),
					'desc_tip' => true,
					'type'    => 'number',
					'default' => 10,
				),
				array(
					'title'   => __( 'Email Reminder Order Status', 'wc-order-reviews' ),
					'id'      => 'wcor_email_reminder_status',
					'desc'   => __( 'Email reminders will be sent X days after an order reaches this status. We recommend that you use \'Completed\'', 'wc-order-reviews' ),
					'desc_tip' => true,
					'type'    => 'select',
					'default' => 'wc-completed',
					'desc_tip' => true,
					'class'    => 'wc-enhanced-select',
					'css'      => 'min-width:300px;',
					'options'  => $statuses
				),
				array(
					'title'       => __( 'Feedback page URL', 'wc-order-reviews' ),
					'id'          => 'wcor_endpoint',
					'type'        => 'text',
					'default'     => 'feedback',
					'class'       => '',
					'css'         => '',
					'placeholder' => __( 'Enter the feedback page endpoint', 'woocommerce' ),
					'desc_tip'    => __( 'This is the link customers will visit to access the feedback page.' ),
				),
				array(
					'title'     => __( 'Allow reviews image upload', 'wc-order-reviews' ),
					'type'      => 'checkbox',
					'desc_tip' => true,
					'id'    => 'wcor_review_image_upload',
				),
				array(
					'title'     => __( 'Allow non-logged in users to vote on reviews', 'wc-order-reviews' ),
					'type'      => 'checkbox',
					'desc_tip' => true,
					'id'    => 'wcor_review_non_logged_in_vote',
				),
				array(
					'id'       => 'wcor_general_options',
					'type'     => 'sectionend',
				),
			));
		}
		return apply_filters( 'woocommerce_get_settings_' . $this->id, $settings, $current_section);
	}

	public function render_settings() {

		$settings = $this->get_settings();
		WC_Admin_Settings::output_fields( $settings );

	}

	public function save_settings() {


		$endpoint = wcor()->query->get_endpoint();

		$endpoint_field_value = $_POST['wcor_endpoint'];

		if ( $endpoint != $endpoint_field_value ) {

			flush_rewrite_rules();

		}
		global $current_section;
		$settings = $this->get_settings($current_section);
    	WC_Admin_Settings::save_fields( $settings );

	}

}
	
return new WCOR_Settings();
