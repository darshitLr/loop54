<?php

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WCOR_Block' ) ) {

	class WCOR_Block {


		public function __construct() {

			add_action( 'init', array( $this, 'register_gutenberg_block' ) );
			add_action( 'init', array( $this, 'set_up_translation' ) );
			add_shortcode( 'wcor_reviews', array( $this, 'reviews_shortcode' ) );

		}

		public function register_gutenberg_block() {

			$asset_file = include( WCOR_PATH . '/includes/blocks/build/index.asset.php');

			wp_register_script( 'fancybox-js', WCOR_DIR . 'assets/js/fancybox.js', array(), $asset_file['version'] );

			// Add slick js here too

			wp_register_script(
				'wcor-block',
				WCOR_DIR . '/includes/blocks/build/index.js',
				array_merge( $asset_file['dependencies'] , array( 'fancybox-js' ) ),
				$asset_file['version']
			);

			wp_localize_script( 'wcor-block', 'wcor_block_settings', array(
				'api_url' => get_rest_url(),
				'wcor_dir' => WCOR_DIR
			) );

			wp_register_style( 'fancybox-css', WCOR_DIR . 'assets/css/fancybox.css', array(), $asset_file['version'] );

			wp_register_style( 'fa-css', WCOR_DIR . 'assets/css/fa.css', array(), $asset_file['version'] );

			wp_register_style( 'slick-theme-css', WCOR_DIR . 'assets/css/slick-theme.css', array(), $asset_file['version'] );

			wp_register_style( 'slick-css', WCOR_DIR . 'assets/css/slick.css', array(), $asset_file['version'] );

			wp_register_style(
				'wcor-block-css',
				WCOR_DIR . '/includes/blocks/build/style-index.css',
				array( 'fancybox-css', 'fa-css', 'slick-theme-css', 'slick-css' )
			);

			// Add slick css here too

			register_block_type( 'wc-order-reviews/reviews-block', array(
				'editor_script' => 'wcor-block',
				'editor_style' => 'wcor-block-css',
				'render_callback' => array( $this, 'render_block' )
			) );


		}

		public function render_block( $block_attributes ) {

			unset( $block_attributes['reviewData'] );

			$default_attribtues = $this->default_attribtues();

			$attributes = array_merge( $default_attribtues, $block_attributes );

			$types = array( 'review', 'store_review' );

			if ( $attributes['excludeStoreReviews'] ) {
				unset( $types[array_search( 'store_review', $types )] );
			}

			if ( $attributes['excludeProductReviews'] ) {
				unset( $types[array_search( 'review', $types )] );
			}

			$args = array(
				'type' => $types,
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => 'rating',
						'value' => $attributes['mininmumRating'],
						'compare' => '>='
					)
				),
				'number' => $attributes['reviewsLimit'],
				'random_order' => $attributes['randomOrder'],
			);

			$reviews = wcor()->store->get_reviews( $args );

			ob_start();

			include WCOR_PATH . '/includes/templates/blocks/reviews.php';

			return ob_get_clean();



		}

		function reviews_shortcode( $attrs ) {

			$attributes = $this->default_attribtues();
			$blocks_names = array( 'customer', 'rating', 'content', 'photos', 'product', 'helpful', 'verified', 'date' );

			foreach ( $attrs as $attr => $value ) {

				$name_parts = explode( '_', $attr );

				$prefix = $name_parts[0];

				if ( in_array( $prefix, $blocks_names ) ) {

					unset( $name_parts[0] );
					$attribute_name = wcor_get_attribute_name( implode( '_', $name_parts ) );
					wcor_add_layout_attribute( $attributes, $prefix, $attribute_name, wcor_get_attribute_value( $value ) );

				} else {
					$attributes[wcor_get_attribute_name( $attr )] = wcor_get_attribute_value( $value );
				}


			}

			return $this->render_block( $attributes );


		}

		private function default_attribtues() {

			return array(
				'orText' => 'Excellent',
				'orTextColor' => '#000000',
				'orTextSize' => 24,
				'orStarColor' => '#000000',
				'orStarSize' => 24,
				'orStarSpace' => 2,
				'reviewsLimit' => 30,
				'randomOrder' => false,
				'mininmumRating' => 4,
				'autoPlay' => false,
				'overallRatingPosition' => 'Top',
				'excludeStoreReviews' => false,
				'excludeProductReviews' => false,
				'randomOrder' => false,
				'reviewLayout' => [
				  [
				    'wc-order-reviews/reviews-block-customer',
				    [
				      'showAvatar' => true,
				      'showCustomerName' => true,
				      'color' => '#000000',
				      'fontSize' => 16,
				      'fontStyle' => 'normal',
				      'fontWeight' => 'normal',
				      'alignment' => 'center',
				      'backgroundColor' => '#FFFFFF',
				    ],
				  ],
				  [
				    'wc-order-reviews/reviews-block-rating',
				    [
				      'space' => 1,
				      'color' => '#000000',
				      'size' => 22,
				      'rating' => 5,
				      'alignment' => 'center',
				      'backgroundColor' => '#FFFFFF',
				    ],
				  ],
				  [
				    'wc-order-reviews/reviews-block-content',
				    [
				      'color' => '#000000',
				      'fontSize' => 15,
				      'fontStyle' => 'normal',
				      'fontWeight' => 'normal',
				      'alignment' => 'center',
				      'backgroundColor' => '#FFFFFF',
				    ],
				  ],
				  [
				    'wc-order-reviews/reviews-block-photos',
				    [
				      'alignment' => 'center',
				    ],
				  ],
				  [
				    'wc-order-reviews/reviews-block-product',
				    [
				      'color' => '#000000',
				      'fontSize' => 15,
				      'fontStyle' => 'normal',
				      'fontWeight' => 'normal',
				      'alignment' => 'center',
				      'backgroundColor' => '#FFFFFF',
							'showImage' => false
				    ],
				  ],
				  [
				    'wc-order-reviews/reviews-block-helpful',
				    [
				      'color' => '#cccccc',
				      'fontSize' => 14,
				      'fontStyle' => 'italic',
				      'fontWeight' => 'normal',
				      'alignment' => 'center',
				      'backgroundColor' => '#FFFFFF',
				    ],
				  ],
				  [
				    'wc-order-reviews/reviews-block-verified',
				    [
				      'text' => 'verified buyer',
				      'color' => '#000000',
				      'iconColor' => '#4a9232',
				      'fontSize' => 15,
				      'fontStyle' => 'normal',
				      'fontWeight' => 'normal',
				      'alignment' => 'center',
				      'backgroundColor' => '#FFFFFF',
				    ],
				  ],
				  [
				    'wc-order-reviews/reviews-block-date',
				    [
				      'color' => '#949494',
				      'fontSize' => 14,
				      'fontStyle' => 'italic',
				      'fontWeight' => 'normal',
				      'alignment' => 'center',
				      'date' => '2020-11-12T00:19:53+01:00',
				      'backgroundColor' => '#FFFFFF',
				      'format' => 'ago',
				    ],
				  ],
				]
			);
		}

		function set_up_translation() {

			wp_set_script_translations( 'wcor-block', 'wc-order-reviews', WCOR_PATH . '/languages' );

		}



	}

}
