jQuery(document).ready(function($){

	$('.wcor_feedback_review__vote__button').click(function(){

		let type = $(this).data('vote');

		if ( type != 'up' && type != 'down' ) return;

		if ($(this).hasClass('selected')) return;

		$(this).parent().find('.wcor_feedback_review__vote__button').removeClass('selected');

		$(this).addClass('selected');

		let review_id = $(this).data('review-id')
		helpful = $(this).parent().parent().find('.wcor_feedback_review__vote__helpful');

		$.ajax({
			url: wcor_vote_settings.ajax_url,
			method: 'POST',
			data: {
				action: 'vote_review',
				nonce: wcor_vote_settings.nonce,
				type,
				review_id
			},
			complete: function(e) {
				let votes = e.responseText;
				if (votes[0] == 0) {
					helpful.hide();
				} else {
					helpful.show();
				}
				helpful.text(e.responseText);
			}
		});

	});

	

});