jQuery(function($){

	let productReviews = $( '.wcor_feedback__review' ),
	reviewForm = $( '#wcor_feedback_form' );

	productReviews.each(function(){

		let review = $(this),
		starsRating = review.find( '.wcor_feedback__review__rating' ),
		starsRatingSelect = starsRating.find( 'select' ),
		starsCurrentRating = starsRatingSelect.val(),
		starsError = review.find( '.wcor_feedback__review__rating__error' );

		review
		.on( 'init', function(){

			starsError.hide();
			starsRatingSelect.removeAttr('required')
			starsRatingSelect.hide()
			.before(
				'<p class="stars">\
					<span>\
						<a class="star-1" href="#">1</a>\
						<a class="star-2" href="#">2</a>\
						<a class="star-3" href="#">3</a>\
						<a class="star-4" href="#">4</a>\
						<a class="star-5" href="#">5</a>\
					</span>\
				</p>'
			);
			starsRating.find( 'a.star-' + starsCurrentRating ).addClass( 'active' )
			.closest(  '.stars'  ).addClass( 'selected' );

		} )
		.on( 'click', '.stars a', function(e) {

			e.preventDefault();

			let star = $(this),
			container = star.closest( '.stars' );


			starsError.hide();
			starsRatingSelect.val( star.text() );
			star.siblings( 'a' ).removeClass( 'active' );
			star.addClass( 'active' );
			container.addClass( 'selected' );

		} );

		review.trigger( 'init' );

	});

	if ( wcor_settings.review_rating_required == 'yes' ) {

		reviewForm.on( 'submit', function(e) {

			e.preventDefault();

			let errors = 0;

			productReviews.each(function(i, e){

				let rating = $(e).find('.wcor_feedback__review__rating select').val();

				if ( rating == '' ) {

					errors++;
					$(e).find( '.wcor_feedback__review__rating__error' ).text( wcor_settings.i18n_required_rating_text ).show();

				}

			});

			if ( errors == 0 ) {

				$(this).unbind('submit').submit();

			}


		} );

	}

	$('.wcor_feedback__review__uploaded_photo_remove').click(function(e){

		e.preventDefault();

		let image_id = $(this).data('image-id');

		let order_id = wcor_settings.wcor_order_id;

		let order_key = wcor_settings.wcor_order_key;

		if (!image_id) return;

		if ($(this).hasClass('disabled')) return;

		$(this).addClass('disabled');

		$(this).parent().remove();

		$.ajax({
			url: wcor_settings.ajax_url,
			method: 'POST',
			data: {
				action: 'delete_review_image',
				nonce: wcor_settings.wcor_delete_review_nonce,
				image_id,
				order_id,
				order_key
			}
		});

	});

	


});
