jQuery(document).ready(function($){
  $('.wcorb_reviews__reviews_slider').each(function(i, e){
    var sliderWidth = $(e).parent().width();
    $(e).css('width', sliderWidth + 'px');
    var autoPlay = $(e).data('autoplay');
    $(e).slick({
      infinite: false,
  		speed: 500,
  		slidesToShow: 3,
  		slidesToScroll: 1,
  		arrows: true,
  		autoplay: autoPlay,
      nextArrow: '<div class="wcorb_reviews__arrow next slick-arrow slick-next"><svg viewBox="0 0 12.7 15.875"><path d="M6.35 1.058A5.295 5.295 0 001.059 6.35a5.296 5.296 0 005.291 5.293 5.296 5.296 0 005.291-5.293A5.295 5.295 0 006.35 1.06zm0 .53a4.757 4.757 0 014.761 4.761 4.759 4.759 0 01-4.761 4.764 4.759 4.759 0 01-4.762-4.764A4.758 4.758 0 016.35 1.587zm2.118 3.436a.265.265 0 00-.19.084l-1.93 1.93-1.93-1.93a.265.265 0 00-.454.191.265.265 0 00.08.182l2.117 2.117a.265.265 0 00.375 0l2.117-2.117a.265.265 0 00-.185-.457z" color="#000" font-weight="400" font-family="sans-serif" overflow="visible" style="line-height: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-feature-settings: normal; text-indent: 0px; text-align: start; text-decoration-line: none; text-decoration-style: solid; text-decoration-color: rgb(0, 0, 0); text-transform: none; text-orientation: mixed; white-space: normal; isolation: auto; mix-blend-mode: normal;"></path></svg></div>',
  		prevArrow: '<div class="wcorb_reviews__arrow prev slick-arrow slick-prev"><svg viewBox="0 0 12.7 15.875"><path d="M6.35 1.058A5.295 5.295 0 001.059 6.35a5.296 5.296 0 005.291 5.293 5.296 5.296 0 005.291-5.293A5.295 5.295 0 006.35 1.06zm0 .53a4.757 4.757 0 014.761 4.761 4.759 4.759 0 01-4.761 4.764 4.759 4.759 0 01-4.762-4.764A4.758 4.758 0 016.35 1.587zm2.118 3.436a.265.265 0 00-.19.084l-1.93 1.93-1.93-1.93a.265.265 0 00-.454.191.265.265 0 00.08.182l2.117 2.117a.265.265 0 00.375 0l2.117-2.117a.265.265 0 00-.185-.457z" color="#000" font-weight="400" font-family="sans-serif" overflow="visible" style="line-height: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: normal; font-feature-settings: normal; text-indent: 0px; text-align: start; text-decoration-line: none; text-decoration-style: solid; text-decoration-color: rgb(0, 0, 0); text-transform: none; text-orientation: mixed; white-space: normal; isolation: auto; mix-blend-mode: normal;"></path></svg></div>',
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            adaptiveHeight: true
          }
        }
      ]
    });
  });
});
