<?php

/**
 * Plugin Name: WooCommerce Order Reviews
 * Plugin URI: https://redlight.se/produkter
 * Description: Extends WooCommerce. Allows you to collect product reviews for an entire order within the same form. Sends email reminders to customers and allow to collect 'store reviews'.
 * Version: 1.3.2
 * Author: Redlight Media
 * Author URI: https://redlight.se/
 * Developer: Christopher Hedqvist
 * Developer URI: https://redlight.se/
 * Text Domain: wc-order-reviews
 * Domain Path: /languages
 * WC requires at least: 4.0.0
 * WC tested up to: 6.2.0
 *
 */

defined( 'ABSPATH' ) || exit;


define( 'WCOR_PATH',  plugin_dir_path( __FILE__ ) );
define( 'WCOR_DIR', plugin_dir_url( __FILE__ ) );
define( 'WCOR_FILE', __FILE__ );

define( 'REDLIGHT_WCOR_STORE_URL', 'https://redlight.se' );
define( 'REDLIGHT_WCOR_ITEM_NAME', 'wc-order-reviews' );
define( 'REDLIGHT_WCOR_ITEM_ID', '53358' );
define( 'REDLIGHT_WCOR_VERSION', '1.3.2' );

require_once( WCOR_PATH . 'vendor/autoload.php' );

function wcor_init() {

	if ( !class_exists( 'woocommerce' ) ) {
		return;
   }
	if ( ! wc_reviews_enabled() ) {
		return;
	}
	
	load_plugin_textdomain(
		'wc-order-reviews',
		false,
		basename( dirname( __FILE__ ) ) . '/languages/'
	);

	require_once WCOR_PATH . 'includes/wc-order-reviews-utils.php';
	require_once WCOR_PATH . 'includes/class-wc-order-reviews.php';

}


add_action( 'plugins_loaded', 'wcor_init', 200 );
