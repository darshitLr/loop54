<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;

$current_url = home_url( $wp->request );
$term = basename($current_url);

$parts = parse_url(home_url( $wp->request ));
$fragments = explode('/', $parts['path']);
$fragments = array_filter($fragments);
$fragments0 = $fragments[3];
$fragments1 = $fragments[4];
$fragments2 = $fragments[5];
$fragments3 = $fragments[6];

if (in_array("varumarken", $fragments))
{
    $termBrand = get_term_by('slug', $fragments0, 'pwb-brand'); 
    $nameBrand = $termBrand->name;
  }
if($fragments1 == 'page' ){

    $term = get_term_by('slug', $fragments0, 'product_cat'); 
    $name = $term->name;

} 
else if($fragments2 == 'page'){
    $term = get_term_by('slug', $fragments1, 'product_cat'); 
    $name = $term->name;

}
else if($fragments3 == 'page'){
    $term = get_term_by('slug', $fragments2, 'product_cat'); 
    $name = $term->name;

}
else{

    $term = get_term_by('slug', $term, 'product_cat'); 
    $name = $term->name;
}
?>
<input type="hidden" name="catName" id="catName" value="<?php echo $name; ?>">
<input type="hidden" name="current_prod_id" id="current_prod_id" value="<?php echo get_the_ID(); ?>">
<input type="hidden" name="brand_name" id="brand_name" value="<?php echo $nameBrand; ?>">
</main>

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>

</footer>
</div>
<script>

jQuery('#shop-sidebar select option').each(function(){
if (!jQuery('#shop-sidebar select').hasClass("pwb-dropdown-widget")) {
    var $this = jQuery(this); 
	$this.val($this.text());
}
});

</script>
<script>

jQuery( document ).ready(function() {
 setTimeout(function () {
 jQuery('.complementary_slider .ui.doubling.padded.four.column.grid.product-grid').flickity({
    // options
    wrapAround: true,
    autoPlay: false,
    prevNextButtons: true,
    percentPosition: true,
    imagesLoaded: true,
    pageDots: false,
    rightToLeft: false,
    groupCells: 4,
    contain: true
});
 },3000);

 setTimeout(function () {
 jQuery('.related_slider .ui.doubling.padded.four.column.grid.product-grid').flickity({
    // options
    wrapAround: true,
    autoPlay: false,
    prevNextButtons: true,
    percentPosition: true,
    imagesLoaded: true,
    pageDots: false,
    rightToLeft: false,
    groupCells: 4,
    contain: true
});
 }, 3000);

});

</script>

<?php wp_footer(); ?>

</body>
</html>
