<?php
/*
Template name: Page - Category listning
*/
get_header(); ?>

<script src="https://static.loop54.com/lib/js/loop54-js-connector.js"></script>

<script type="text/javascript">

var client = Loop54.getClient('https://rorfokus-se.54proxy.com/v3');


// Below is an example of a request - response cycle of a category listing request

var categoryName = 'Fyndhörna';

var response = client.getEntitiesByAttribute("Category", categoryName, {skip: 0, take:9}).then((r) => {
    var data = r.data
        var results = data["results"].items;
        
        if (!results || results.length == 0)
        {
        console.log("There were no items in this category.");
        }
        else
        {
        console.log("Total number of items: " + data["results"].count);
        for (var i in results)
        {
            var productId = results[i].id;
            var productTitle = results[i].attributes ? results[i].attributes.find(function(a){return a.name=="Title"}).values[0] : "";
            console.log(productId + " " + productTitle); //render a product on the category listing page
        }
        }    
}
);


</script>

<?php do_action( 'flatsome_before_page' ); ?>

<div id="content" role="main" class="content-area">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>
		
		<?php endwhile; // end of the loop. ?>
		
</div>

<?php do_action( 'flatsome_after_page' ); ?>

<?php get_footer(); ?>
