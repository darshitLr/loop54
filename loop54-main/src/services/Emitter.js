import EventEmitter from "events";
import { isEmpty, get, groupBy } from "lodash";
import getClient from "loop54-js-connector";

import {
  getCategoryObj,
  getcategoryUrl,
  getSearchObj,
  getSearchUrl,
} from "../utils/url";
// import { getConfigKey,  getHostanameStr, getLocationStr, decipher} from '../utils/secret';
import { mapProducts } from "../utils/product";
import {
  EVENTS,
  SEARCH_FIELDS,
  DEFAULT_AUTOCOMPLETE_PRODUCTS_COUNT,
  SORT_NAMES,
  CATEGORY_EVENTS,
  CATEGORY_FIELDS,
  CATEGORY_SORT_NAMES,
  COMLEMENTARY_CATEGORY,
  RELATED_CATEGORY,
} from "../constants";

let config = {};
let client = {};

const eventEmitter = new EventEmitter();

const updateQuery = () => {
  eventEmitter.emit(EVENTS.QUERY_UPDATED, getSearchObj());
};
const categoryUpdateQuery = () => {
  eventEmitter.emit(CATEGORY_EVENTS.QUERY_UPDATED, getCategoryObj());
};

window.onpopstate = () => {
  updateQuery();
  categoryUpdateQuery();
};

eventEmitter.on(EVENTS.AUTOCOMPLETE, async (searchTerm) => {
  const autocompleteProductsCount =
    config.autocompleteProductsCount || DEFAULT_AUTOCOMPLETE_PRODUCTS_COUNT;
  const [autocompleteResponse, searchResponse] = await Promise.all([
    client.autoComplete(searchTerm, {
      skip: 0,
      take: config.autocompleteCount,
    }),
    client.search(searchTerm, {
      skip: 0,
      take: autocompleteProductsCount,
      customData: { directSearch: true },
    }),
  ]);

  const productItems = searchResponse.data
    ? mapProducts(searchResponse.data.results.items)
    : [];
  const productsTotal = searchResponse.data
    ? searchResponse.data.results.count
    : 0;

  if (autocompleteResponse.data) {
    eventEmitter.emit(EVENTS.AUTOCOMPLETE_ITEMS_LOADED, {
      autocompleteItems: autocompleteResponse.data,
      productItems,
      productsTotal,
    });
  }
});

eventEmitter.on(EVENTS.SEARCH, (nextSearchObj) => {
  const searchObj = getSearchObj();

  nextSearchObj = {
    ...nextSearchObj,
    [SEARCH_FIELDS.PAGE]: 1,
    [SEARCH_FIELDS.SORT]: searchObj[SEARCH_FIELDS.SORT],
  };
  if (
    !isEmpty(config) &&
    config.resultsPage === window.location.pathname.replace(/\//g, "")
  ) {
    window.history.pushState(nextSearchObj, "", getSearchUrl(nextSearchObj));
    updateQuery();
  } else {
    window.location.href = getSearchUrl(nextSearchObj);
  }
});

eventEmitter.on(EVENTS.LOAD_MORE, (nextPageNum) => {
  const searchObj = getSearchObj();
  const nextSearchObj = { ...searchObj, [SEARCH_FIELDS.PAGE]: nextPageNum };
  window.history.pushState(nextSearchObj, "", getSearchUrl(nextSearchObj));
  updateQuery();
});

eventEmitter.on(EVENTS.SORT, (sortObj) => {
  const searchObj = getSearchObj();
  const nextSearchObj = {
    ...searchObj,
    [SEARCH_FIELDS.PAGE]: 1,
    [SEARCH_FIELDS.SORT]: sortObj,
  };
  window.history.pushState(nextSearchObj, "", getSearchUrl(nextSearchObj));
  updateQuery();
});

eventEmitter.on(EVENTS.ADD_FACET, (newFacet) => {
  const searchObj = getSearchObj();

  const prevFacets = searchObj[SEARCH_FIELDS.FACET] || [];
  const nextSearchObj = {
    ...searchObj,
    [SEARCH_FIELDS.FACET]: [...prevFacets, newFacet],
    [SEARCH_FIELDS.PAGE]: 1,
  };
  window.history.pushState(nextSearchObj, "", getSearchUrl(nextSearchObj));
  updateQuery();
});

eventEmitter.on(EVENTS.CHANGE_PRICE_FACET, (newFacet) => {
  const searchObj = getSearchObj();
  const prevFacets = (searchObj[SEARCH_FIELDS.FACET] || []).filter(
    (facet) => facet.attributeName !== "Price"
  );
  const nextSearchObj = {
    ...searchObj,
    [SEARCH_FIELDS.FACET]: [...prevFacets, newFacet],
    [SEARCH_FIELDS.PAGE]: 1,
  };
  window.history.pushState(nextSearchObj, "", getSearchUrl(nextSearchObj));
  updateQuery();
});

eventEmitter.on(EVENTS.REMOVE_FACET, (facetToRemove) => {
  const searchObj = getSearchObj();

  const prevFacets = searchObj[SEARCH_FIELDS.FACET] || [];
  const nextFacets = prevFacets.filter(
    (facet) =>
      facet.attributeName !== facetToRemove.attributeName ||
      facet.value !== facetToRemove.value
  );
  const nextSearchObj = {
    ...searchObj,
    [SEARCH_FIELDS.FACET]: nextFacets,
    [SEARCH_FIELDS.PAGE]: 1,
  };
  window.history.pushState(nextSearchObj, "", getSearchUrl(nextSearchObj));
  updateQuery();
});

eventEmitter.on(EVENTS.REMOVE_ALL_FACETS, () => {
  const searchObj = getSearchObj();
  const nextSearchObj = { ...searchObj, [SEARCH_FIELDS.FACET]: [] };
  window.history.pushState(nextSearchObj, "", getSearchUrl(nextSearchObj));
  updateQuery();
});

// ------------category-----------
eventEmitter.on(CATEGORY_EVENTS.LOAD_MORE, (nextPageNum) => {
  const categoryObj = getCategoryObj();
  const nextCategoryObj = {
    ...categoryObj,
    [CATEGORY_FIELDS.PAGE]: nextPageNum,
  };
  window.history.pushState(
    nextCategoryObj,
    "",
    getcategoryUrl(nextCategoryObj)
  );
  categoryUpdateQuery();
});

eventEmitter.on(CATEGORY_EVENTS.SORT, (sortObj) => {
  const categoryObj = getCategoryObj();
  const nextCategoryObj = {
    ...categoryObj,
    [CATEGORY_FIELDS.PAGE]: 1,
    [CATEGORY_FIELDS.SORT]: sortObj,
  };
  window.history.pushState(
    nextCategoryObj,
    "",
    getcategoryUrl(nextCategoryObj)
  );
  categoryUpdateQuery();
});

eventEmitter.on(CATEGORY_EVENTS.ADD_FACET, (newFacet) => {
  const categoryObj = getCategoryObj();

  const prevFacets = categoryObj[CATEGORY_FIELDS.FACET] || [];
  const nextCategoryObj = {
    ...categoryObj,
    [CATEGORY_FIELDS.FACET]: [...prevFacets, newFacet],
    [CATEGORY_FIELDS.PAGE]: 1,
  };
  window.history.pushState(
    nextCategoryObj,
    "",
    getcategoryUrl(nextCategoryObj)
  );
  categoryUpdateQuery();
});

eventEmitter.on(CATEGORY_EVENTS.CHANGE_PRICE_FACET, (newFacet) => {
  const categoryObj = getCategoryObj();

  const prevFacets = (categoryObj[CATEGORY_FIELDS.FACET] || []).filter(
    (facet) => facet.attributeName !== "Price"
  );
  const nextCategoryObj = {
    ...categoryObj,
    [CATEGORY_FIELDS.FACET]: [...prevFacets, newFacet],
    [CATEGORY_FIELDS.PAGE]: 1,
  };
  window.history.pushState(
    nextCategoryObj,
    "",
    getcategoryUrl(nextCategoryObj)
  );
  categoryUpdateQuery();
});

eventEmitter.on(CATEGORY_EVENTS.REMOVE_FACET, (facetToRemove) => {
  const categoryObj = getCategoryObj();

  const prevFacets = categoryObj[CATEGORY_FIELDS.FACET] || [];
  const nextFacets = prevFacets.filter(
    (facet) =>
      facet.attributeName !== facetToRemove.attributeName ||
      facet.value !== facetToRemove.value
  );
  const nextCategoryObj = {
    ...categoryObj,
    [CATEGORY_FIELDS.FACET]: nextFacets,
    [CATEGORY_FIELDS.PAGE]: 1,
  };
  window.history.pushState(
    nextCategoryObj,
    "",
    getcategoryUrl(nextCategoryObj)
  );
  categoryUpdateQuery();
});

eventEmitter.on(CATEGORY_EVENTS.REMOVE_ALL_FACETS, () => {
  const categoryObj = getCategoryObj();
  const nextCategoryObj = { ...categoryObj, [CATEGORY_FIELDS.FACET]: [] };
  window.history.pushState(
    nextCategoryObj,
    "",
    getcategoryUrl(nextCategoryObj)
  );
  categoryUpdateQuery();
});

// const loadConfig = () => fetch(`/buildloop54/config-${window.location.hostname.replace('www.', '')}.json`)
const loadConfig = () =>
  fetch(`/loop54/buildloop54/config.json`)
    .then((res) => res.json())
    .then((config) => {
      // const sk = config[getConfigKey()];
      // const wl = window[getLocationStr()]
      // if(decipher(sk) !== wl[getHostanameStr()]) {
      //   return;
      // }
      eventEmitter.emit(EVENTS.CONFIG_LOADED, config);
    });

const results = {};

export const getResultsAndSubscribe = (cb) => {
  cb(results);
  eventEmitter.on(EVENTS.RESULTS_LOADED, (results) => {
    cb(results);
  });
};

let query = {};

const getQuery = () => {
  query = getSearchObj();
};

export const getQueryAndSubscribe = (cb) => {
  cb(query);
  eventEmitter.on(EVENTS.QUERY_UPDATED, (query) => {
    cb(query);
  });
};

let wasOnceLoaded = false;
const loadResults = (searchObj) => {
  if (isEmpty(searchObj)) {
    return;
  }
  const page = searchObj[SEARCH_FIELDS.PAGE] || 1;

  const take = wasOnceLoaded ? config.pageSize : page * config.pageSize;
  const skip = wasOnceLoaded ? (page - 1) * config.pageSize : 0;

  let options = {
    take,
    skip,
    relatedResultsOptions: {
      skip: 0,
      take: config.relatedResultsCount || 0,
    },
  };

  const searchTerm = searchObj[SEARCH_FIELDS.SEARCH];
  const facets = searchObj[SEARCH_FIELDS.FACET];
  const sort = searchObj[SEARCH_FIELDS.SORT];

  if (!isEmpty(facets)) {
    const groupedFacets = groupBy(facets, "attributeName");
    const preparedFacets = Object.keys(groupedFacets).map((attributeName) => {
      const type = attributeName !== "Price" ? "distinct" : "range";
      const selected =
        attributeName !== "Price"
          ? groupedFacets[attributeName].map((attr) => attr.value)
          : groupedFacets[attributeName][0].value;
      return { type, attributeName, selected };
    });
    options.facets = preparedFacets;
  }

  if (!isEmpty(sort)) {
    options.sortBy =
      sort.attributeName === SORT_NAMES.RELEVANCE
        ? [{ type: SORT_NAMES.RELEVANCE, order: sort.value }]
        : [
            {
              type: "attribute",
              attributeName: sort.attributeName,
              order: sort.value,
            },
          ];
  }

  client.search(searchTerm, options).then((response) => {
    wasOnceLoaded = true;
    const data = response.data;
    const makesSense = data.makesSense;
    const items = data.results.items;
    const relatedQueries = get(data, "relatedQueries.items", []).map(
      (q) => q.query
    );
    const relatedResults = get(data, "relatedResults.items", []);
    const spellingSuggestions = get(data, "spellingSuggestions.items", []).map(
      (q) => q.query
    );

    eventEmitter.emit(EVENTS.RESULTS_LOADED, {
      page,
      results: mapProducts(items),
      relatedResults: mapProducts(relatedResults),
      facets: data.results.facets,
      total: data.results.count,
      relatedQueries,
      spellingSuggestions,
      makesSense,
      searchTerm,
    });
  });
};

eventEmitter.on(EVENTS.QUERY_UPDATED, loadResults);

const addToCart = (productId) => {
  const addToCartEntity = { type: "Product", id: productId };

  client.createEvent("addtocart", addToCartEntity);
  const $formKey = document.getElementsByName("form_key");
  const formKey = $formKey && $formKey.length > 0 ? $formKey[0].value : "";
  if (window.setLocation && formKey) {
    window.setLocation(
      `/checkout/cart/add/product/${productId}/form_key/${formKey}/`
    );
  } else {
    window.location.href = `/cart/?add-to-cart=${productId}`;
  }
};

eventEmitter.on(EVENTS.ADD_TO_CART, addToCart);
// ---category------
const categoryResults = {};

export const getResultsAndCategory = (cb) => {
  cb(categoryResults);

  eventEmitter.on(CATEGORY_EVENTS.RESULTS_LOADED, (categoryResults) => {
    cb(categoryResults);
  });
};

let cateogyQuery = {};

const getCategoryQuery = () => {
  cateogyQuery = getCategoryObj();
};

export const getCategoryQueryAndSubscribe = (cb) => {
  cb(cateogyQuery);
  eventEmitter.on(CATEGORY_EVENTS.QUERY_UPDATED, (cateogyQuery) => {
    cb(cateogyQuery);
  });
};

let wasOnceLoad = false;
const categoryloadResults = (objectObj) => {
  let categoryaValue = document.getElementById("catName").value;
  let categoryName = categoryaValue || objectObj[CATEGORY_FIELDS.CATEGORY];
  if (!categoryName) {
    return;
  }

  const page = objectObj[CATEGORY_FIELDS.PAGE] || 1;

  const take = wasOnceLoad ? config.pageSize + 1 : page * (config.pageSize + 1);

  const skip = wasOnceLoad ? (page - 1) * (config.pageSize + 1) : 0;

  let options = {
    take,
    skip,
  };
  const facets = objectObj[CATEGORY_FIELDS.FACET];
  const sort = objectObj[CATEGORY_FIELDS.SORT];

  if (!isEmpty(facets)) {
    const groupedFacets = groupBy(facets, "attributeName");
    const preparedFacets = Object.keys(groupedFacets).map((attributeName) => {
      const type = attributeName !== "Price" ? "distinct" : "range";
      const selected =
        attributeName !== "Price"
          ? groupedFacets[attributeName].map((attr) => attr.value)
          : groupedFacets[attributeName][0].value;
      return { type, attributeName, selected };
    });
    options.facets = preparedFacets;
  }

  if (!isEmpty(sort)) {
    options.sortBy =
      sort.attributeName === CATEGORY_SORT_NAMES.RELEVANCE
        ? [{ type: CATEGORY_SORT_NAMES.RELEVANCE, order: sort.value }]
        : [
            {
              type: "attribute",
              attributeName: sort.attributeName,
              order: sort.value,
            },
          ];
  }

  client
    .getEntitiesByAttribute("categoryPath", categoryName, options)
    .then((r) => {
      const data = r.data;
      wasOnceLoad = true;
      let items = data["results"].items;
      let total = data["results"].count;
      eventEmitter.emit(CATEGORY_EVENTS.RESULTS_LOADED, {
        facets: data.results.facets,
        results: mapProducts(items),
        page: page,
        total: total,
      });
    });
};

eventEmitter.on(CATEGORY_EVENTS.QUERY_UPDATED, categoryloadResults);

// ---RelatedProduct---

const getRelatedProduct = (RelatedProduct) => {
  const { productId } = RelatedProduct;

  const take = config.relatedResultsCount + 4;
  const skip = 0;
  let options = {
    take,
    skip,
  };

  client
    .getRelatedEntities(
      {
        type: "Product",
        id: productId,
      },
      options
    )
    .then((r) => {
      const data = r.data;

      let items = data["results"].items;
      let total = data["results"].count;
      eventEmitter.emit(RELATED_CATEGORY.RESULTS_LOADED, {
        facets: data.results.facets,
        results: mapProducts(items),
        total: total,
      });
    });
};

eventEmitter.on(RELATED_CATEGORY.REQUEST_RESULTS, getRelatedProduct);

export const getResultsAndRelatedProduct = (cb) => {
  eventEmitter.on(RELATED_CATEGORY.RESULTS_LOADED, (catergoryData) => {
    cb(catergoryData);
  });
};

// ----ComplementaryEntities---

const getComplementaryProduct = (ComplementaryProduct) => {
  const { productId } = ComplementaryProduct;

  const take = config.relatedResultsCount + 4;
  const skip = 0;
  let options = {
    take,
    skip,
  };

  client
    .getComplementaryEntities(
      {
        type: "Product",
        id: productId,
      },
      options
    )
    .then((r) => {
      const data = r.data;
      let items = data["results"].items;
      let total = data["results"].count;
      eventEmitter.emit(COMLEMENTARY_CATEGORY.RESULTS_LOADED, {
        facets: data.results.facets,
        results: mapProducts(items),
        total: total,
      });
    });
};

eventEmitter.on(COMLEMENTARY_CATEGORY.REQUEST_RESULTS, getComplementaryProduct);

export const getResultsAndComplementaryProduct = (cb) => {
  eventEmitter.on(COMLEMENTARY_CATEGORY.RESULTS_LOADED, (catergoryData) => {
    cb(catergoryData);
  });
};

eventEmitter.on(EVENTS.CONFIG_LOADED, (nextConfig) => {
  config = nextConfig;
  client = getClient(config.DOMAIN);
  window.loop54Client = client;
  categoryUpdateQuery();
  getCategoryQuery();
  updateQuery();
  getQuery();
});

export const getConfig = () => {
  return config;
};

export const initData = () => {
  loadConfig();
};

export default eventEmitter;
