import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import React from "react";
import ReactDOM from "react-dom";
import { I18nextProvider } from "react-i18next";
import i18next from "i18next";

import EE, { initData } from "./services/Emitter";
import { EVENTS } from "./constants/index";
import Search from "./components/Containers/AutocompleteContainer";
import SearchResultsContainer from "./components/Containers/SearchResultsContainer";
import CategoryResultsContainer from "./components/Category/CategoryResultsContainer";
import RelatedProductContainer from "./components/Category/RelatedProductContainer";
import "semantic-ui-css/semantic.min.css";
import ComplementaryProductContainer from "./components/Category/ComplementaryProductContainer";

// window.dealers_is_logged = true;
// window.dealers_customer_group = 60;

const loop54SearchEl = document.getElementById("loop54-search");
const loop54CategoryResultsEl = document.getElementById("category-page-result");
const loop54SearchResultsEl = document.getElementById("loop54-search-results");
const loop54RelatedResultsEl = document.getElementById(
  "related-products-section"
);
const loop54ComplementaryResultsEl = document.getElementById(
  "you-may-like-section"
);

if (
  loop54SearchEl ||
  loop54CategoryResultsEl ||
  loop54SearchResultsEl ||
  loop54RelatedResultsEl ||
  loop54ComplementaryResultsEl
) {
  initData();
}

const renderReact = (config) => {
  if (loop54SearchEl) {
    ReactDOM.render(
      <I18nextProvider i18n={i18next}>
        <Search config={config} />
      </I18nextProvider>,
      loop54SearchEl
    );
  }

  if (loop54SearchResultsEl) {
    ReactDOM.render(
      <I18nextProvider i18n={i18next}>
        <SearchResultsContainer config={config} />
      </I18nextProvider>,
      loop54SearchResultsEl
    );
  }
  if (loop54CategoryResultsEl) {
    ReactDOM.render(
      <I18nextProvider i18n={i18next}>
        <div>
          <CategoryResultsContainer config={config} />
        </div>
      </I18nextProvider>,
      loop54CategoryResultsEl
    );
  }
  if (loop54RelatedResultsEl) {
    ReactDOM.render(
      <I18nextProvider i18n={i18next}>
        <div>
          <RelatedProductContainer config={config} />
        </div>
      </I18nextProvider>,
      loop54RelatedResultsEl
    );
  }
  if (loop54ComplementaryResultsEl) {
    ReactDOM.render(
      <I18nextProvider i18n={i18next}>
        <div>
          <ComplementaryProductContainer config={config} />
        </div>
      </I18nextProvider>,
      loop54ComplementaryResultsEl
    );
  }
};

const setLanguage = (config) => {
  const resources = Object.keys(config.translations).reduce((acc, key) => {
    acc[key] = { common: config.translations[key] };
    return acc;
  }, {});

  i18next.init({
    interpolation: { escapeValue: false },
    lng: config.language,
    fallbackLng: config.language,
    resources,
  });
};

EE.on(EVENTS.CONFIG_LOADED, (config) => {
  setLanguage(config);
  renderReact(config);
});
