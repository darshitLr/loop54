import { invert } from "lodash";

export const ATTRIBUTES_MAP = {
  id: "Id",
  image_link: "ImageURL",
  title: "Name",
  link: "ProductURL",
  price: "Price",
  brancd: "Brand",
};

export const INVERT_ATTRIBUTES_MAP = invert(ATTRIBUTES_MAP);

export const SORT_SEPARATOR = "__SORT_SEPARATOR__";

export const SEARCH_FIELDS = {
  SEARCH: "q",
  FACET: "f",
  PAGE: "page",
  SORT: "sort",
};

export const EVENTS = {
  SEARCH: "search",
  AUTOCOMPLETE: "autocomplete",
  AUTOCOMPLETE_ITEMS_LOADED: "autocomplete items loaded",
  QUERY_UPDATED: "query updated",
  LOAD_MORE: "load more",
  SORT: "sort",
  CHANGE_PRICE_FACET: "change price facet",
  ADD_FACET: "add facet",
  REMOVE_FACET: "remove facet",
  REMOVE_ALL_FACETS: "remove all facets",
  CONFIG_LOADED: "config loaded",
  RESULTS_LOADED: "results loaded",
  ADD_TO_CART: "add to cart",
};

export const SORT_NAMES = {
  PRICE: "Price",
  NAME: "Name",
  RELEVANCE: "relevance",
};

export const SORT_TYPES = {
  DESC: "desc",
  ASC: "asc",
};

export const ROUNDING_TYPES = {
  UP: "up",
  DOWN: "down",
  NORMAL: "normal",
};

export const CATEGORY_SORT_SEPARATOR = "__SORT_SEPARATOR__";

export const CATEGORY_FIELDS = {
  CATEGORY: "k",
  PAGE: "page",
  FACET: "f",
  SORT: "sort",
};

export const CATEGORY_EVENTS = {
  CATEGORY: "category",
  QUERY_UPDATED: "category query updated",
  LOAD_MORE: "category load more",
  SORT: "category sort",
  CHANGE_PRICE_FACET: "category change price facet",
  ADD_FACET: "category add facet",
  REMOVE_FACET: "category remove facet",
  REMOVE_ALL_FACETS: "category remove all facets",
  CONFIG_LOADED: "category config loaded",
  REQUEST_RESULTS: "category request loaded",
  RESULTS_LOADED: "category results loaded",
};

export const CATEGORY_SORT_NAMES = {
  PRICE: "Price",
  NAME: "Name",
  RELEVANCE: "relevance",
};

export const CATEGORY_SORT_TYPES = {
  DESC: "desc",
  ASC: "asc",
};

export const RELATED_CATEGORY = {
  REQUEST_RESULTS: " relatedproduct request ",
  RESULTS_LOADED: " relatedproductresults loaded",
};
export const COMLEMENTARY_CATEGORY = {
  REQUEST_RESULTS: "complementryproduct request loaded",
  RESULTS_LOADED: "results loaded",
};

export const MIN_AUTOSEARCH_COUNT = 2;

export const DEFAULT_AUTOCOMPLETE_PRODUCTS_COUNT = 5;
