import { isEmpty } from "lodash";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { COMLEMENTARY_CATEGORY } from "../../constants";
import EE, { getResultsAndComplementaryProduct } from "../../services/Emitter";
import { isOptionEnabled } from "../../utils/common";
import CategoryList from "../categoryResult/CategoryList";
import "../Containers/index.css";

class ComplementaryProductContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: document.getElementById("current_prod_id").value,
      total: 0,
      results: null,
      facets: [],
    };
  }

  getComplementaryProduct = () => {
    EE.emit(COMLEMENTARY_CATEGORY.REQUEST_RESULTS, {
      productId: this.state.id,
    });
    getResultsAndComplementaryProduct(({ results, facets, total }) => {
      this.setState((prevState) => {
        const nextResults = [].concat(prevState.results || [], results || []);

        return {
          results: nextResults,
          facets: facets,
          total: total,
        };
      });
    });
  };

  componentDidMount() {
    this.getComplementaryProduct();
  }

  render() {
    const { results } = this.state;
    const { t, config } = this.props;

    return (
      <div id="category-product-result">
        {results && (
          <>
            {!isEmpty(results) ? (
              <div className="complementary_slider slider">
                {
                  <CategoryList
                    results={results}
                    columns={config.relatedResultsColumnCount}
                    withDealers={isOptionEnabled(config.isDealers)}
                    showDiscountIcon={isOptionEnabled(config.showDiscountIcon)}
                    discountRoundingType={config.discountRoundingType}
                    thousandsSeparator={config.thousandsSeparator}
                  />
                }
              </div>
            ) : (
              <div>
                <p>Inga produkter hittades som motsvarar ditt val.</p>
              </div>
            )}
          </>
        )}
      </div>
    );
  }
}

export default withTranslation("common")(ComplementaryProductContainer);
