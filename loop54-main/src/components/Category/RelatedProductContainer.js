import { isEmpty } from "lodash";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { RELATED_CATEGORY } from "../../constants";
import EE, { getResultsAndRelatedProduct } from "../../services/Emitter";
import { isOptionEnabled } from "../../utils/common";
import CategoryList from "../categoryResult/CategoryList";
import "../Containers/index.css";

class RelatedProductContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: document.getElementById("current_prod_id").value,
      total: 0,
      results: null,
      facets: [],
    };
  }

  getRelatedProduct = () => {
    EE.emit(RELATED_CATEGORY.REQUEST_RESULTS, {
      productId: this.state.id,
    });
    getResultsAndRelatedProduct(({ results, facets, total }) => {
      this.setState((prevState) => {
        const nextResults = [].concat(prevState.results || [], results || []);

        return {
          results: nextResults,
          facets: facets,
          total: total,
        };
      });
    });
  };

  componentDidMount() {
    this.getRelatedProduct();
  }

  render() {
    const { results } = this.state;

    const { t, config } = this.props;

    return (
      <div id="category-product-result">
        {results && (
          <>
            {!isEmpty(results) ? (
              <div className="related_slider slider">
                {
                  <CategoryList
                    results={results}
                    columns={config.relatedResultsColumnCount}
                    withDealers={isOptionEnabled(config.isDealers)}
                    showDiscountIcon={isOptionEnabled(config.showDiscountIcon)}
                    discountRoundingType={config.discountRoundingType}
                    thousandsSeparator={config.thousandsSeparator}
                  />
                }
              </div>
            ) : (
              <div>
                <p>Inga produkter hittades som motsvarar ditt val.</p>
              </div>
            )}
          </>
        )}
      </div>
    );
  }
}

export default withTranslation("common")(RelatedProductContainer);
