import {
  groupBy,
  isEmpty,
  isEqual,
  keyBy,
  orderBy,
  reverse,
  sortBy,
} from "lodash";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import EE, {
  getCategoryQueryAndSubscribe,
  getResultsAndCategory,
} from "../../services/Emitter";
import CategoryList from "../categoryResult/CategoryList";
import FacetList from "../Filter/List";
import { isOptionEnabled } from "../../utils/common";
import { Button } from "semantic-ui-react";
import {
  CATEGORY_EVENTS,
  CATEGORY_FIELDS,
  CATEGORY_SORT_NAMES,
  CATEGORY_SORT_SEPARATOR,
  CATEGORY_SORT_TYPES,
} from "../../constants";
import CategorySort from "../Sort/CategorySort";

class CategoryResultsContainer extends Component {
  initialState = {
    location: window.location.pathname,
    categoryName: document.getElementById("catName").value,
    brandName: document.getElementById("brand_name").value,
    total: 0,
    results: null,
    selectedFacets: [],
    facets: [],
    page: 1,
    sortAttribute: CATEGORY_SORT_NAMES.RELEVANCE,
    sortDescending: CATEGORY_SORT_TYPES.ASC,
  };
  constructor(props) {
    super(props);
    this.state = {
      ...this.initialState,
    };
  }

  componentDidMount() {
    // this.categoryResults();
    this.getCategory();
    this.getCategoryQuery();
  }

  getCategoryQuery() {
    getCategoryQueryAndSubscribe((categoryObj) => {
      const sort = categoryObj[CATEGORY_FIELDS.SORT] || {};
      const selectedFacets =
        categoryObj[CATEGORY_FIELDS.FACET] || this.initialState.selectedFacets;

      this.setState({
        selectedFacets: selectedFacets.filter(
          (selectedFacet) => selectedFacet.attributeName !== "Price"
        ),
        sortAttribute: sort.attributeName || this.initialState.sortAttribute,
        sortDescending: sort.value || this.initialState.sortDescending,
      });
    });
  }

  loadMore = () => {
    EE.emit(CATEGORY_EVENTS.LOAD_MORE, this.state.page + 1);
  };

  getCategory = () => {
    getResultsAndCategory(({ results, facets, total, page }) => {
      this.setState((prevState) => {
        const nextResults =
          isEqual(prevState.facets, facets) && prevState.page + 1 === page
            ? [...prevState.results, ...results]
            : results;
        return {
          results: nextResults,
          page: page,
          facets: facets,
          total: total,
        };
      });
    });
  };

  resetState() {
    this.setState({ ...this.initialState });
  }

  prepareFacets(facets, selectedFacets) {
    const { attributes = [] } = this.props.config;
    const sortFacetsOptions = (
      this.props.config.sortFacetsOptions || ""
    ).toLowerCase();
    const mappedFacets = keyBy(facets, (facet) => facet.name);
    const groupedFacets = groupBy(selectedFacets, "attributeName");

    return attributes
      .filter(
        (attribute) =>
          isOptionEnabled(attribute.enabled) && mappedFacets[attribute.name]
      )
      .map((attribute) => {
        const obj = attribute.label ? { label: attribute.label } : {};
        const type = (attribute.type || "").toLowerCase();
        const attrSort = (attribute.sort || "").toLowerCase();
        const items = mappedFacets[attribute.name].items || [];
        const itemsGroup = groupedFacets[attribute.name];

        let sortedItems = items;
        if (type === "number") {
          let sort = attrSort || sortFacetsOptions;
          sort = sort === "desc" ? sort : "asc";
          sortedItems = sortedItems.map((i) => ({
            ...i,
            item: parseFloat(i.item),
          }));
          sortedItems = orderBy(sortedItems, "item", sort);
        } else if (attrSort) {
          const sort = attrSort === "desc" ? attrSort : "asc";
          sortedItems = orderBy(sortedItems, "item", sort);
        } else if (sortFacetsOptions) {
          const sort = sortFacetsOptions === "desc" ? sortFacetsOptions : "asc";
          sortedItems = orderBy(items, "item", sortFacetsOptions);
        }

        if (itemsGroup) {
          sortedItems = sortBy(
            sortedItems,
            (item) =>
              !reverse(itemsGroup.map((i) => i.value)).includes(item.item)
          );
        }

        return { ...mappedFacets[attribute.name], items: sortedItems, ...obj };
      });
  }

  handleSortChange = (e, data) => {
    const [sortAttribute, sortDescending] = data.value.split(
      CATEGORY_SORT_SEPARATOR
    );
    EE.emit(CATEGORY_EVENTS.SORT, {
      attributeName: sortAttribute,
      value: sortDescending || CATEGORY_SORT_TYPES.DESC,
    });
  };

  handleRangeFacetsChange = (facet) => {
    EE.emit(CATEGORY_EVENTS.CHANGE_PRICE_FACET, facet);
  };

  handleSelectedFacetsChange = (facet) => {
    const isSelected = this.state.selectedFacets.some((selectedFacet) => {
      return (
        selectedFacet.attributeName === facet.attributeName &&
        selectedFacet.value === facet.value
      );
    });

    if (isSelected) {
      EE.emit(CATEGORY_EVENTS.REMOVE_FACET, facet);
    } else {
      EE.emit(CATEGORY_EVENTS.ADD_FACET, facet);
    }
  };

  handleClearFacets = () => {
    EE.emit(CATEGORY_EVENTS.REMOVE_ALL_FACETS);
  };

  render() {
    const {
      results,
      total,
      page,
      facets,
      selectedFacets,
      sortAttribute,
      sortDescending,
    } = this.state;

    const { t, config } = this.props;

    const maxPage = Math.ceil(total / 25);
    const sortedFacets = this.prepareFacets(facets, selectedFacets);

    const loadedResultsCount = page * 25 > total ? total : page * 25;

    return (
      <div id="category-result">
        {!isEmpty(results) && (
          <div className="search-results__label-container">
            <h6 className="search-results__label">
              {t(`Showing 1–${page * 25 > total ? total : page * 25} of ${total}
              results`)}
            </h6>
            <CategorySort
              onSortChange={this.handleSortChange}
              sortAttribute={sortAttribute}
              sortDescending={sortDescending}
            />
          </div>
        )}
        {!isEmpty(sortedFacets) && (
          <FacetList
            onSelectedFacetsChange={this.handleSelectedFacetsChange}
            onRangeFacetsChange={this.handleRangeFacetsChange}
            onClearFacets={this.handleClearFacets}
            selectedFacets={selectedFacets}
            facets={sortedFacets}
            columns={config.filtersColumnCount}
            visibleFacetsCount={config.visibleFacetsCount}
          />
        )}

        {results && (
          <>
            {!isEmpty(results) ? (
              <div>
                <CategoryList
                  results={results}
                  columns={config.resultsColumnCount}
                  withDealers={isOptionEnabled(config.isDealers)}
                  showDiscountIcon={isOptionEnabled(config.showDiscountIcon)}
                  discountRoundingType={config.discountRoundingType}
                  thousandsSeparator={config.thousandsSeparator}
                />
              </div>
            ) : (
              <div>
                <p>Inga produkter hittades som motsvarar ditt val.</p>
              </div>
            )}
          </>
        )}
        {!isEmpty(results) && (
          <div className="search-results__showing">
            {t("Showing", { current: loadedResultsCount })}
          </div>
        )}
        <div className="show-more__container">
          {page < maxPage ? (
            <Button
              className="show-more__button"
              basic
              color="black"
              onClick={this.loadMore}
            >
              {t("Show more")}
            </Button>
          ) : null}
        </div>
      </div>
    );
  }
}

export default withTranslation("common")(CategoryResultsContainer);
