import React, { Component } from "react";
import { Dropdown } from "semantic-ui-react";
import { withTranslation } from "react-i18next";

import {
  CATEGORY_SORT_NAMES,
  CATEGORY_SORT_SEPARATOR,
  CATEGORY_SORT_TYPES,
} from "../../constants";

class CategorySort extends Component {
  constructor(props) {
    super(props);

    this.state = {
      defaultSort: null,
    };
  }

  componentWillMount() {
    let { sortAttribute, sortDescending } = this.props;
    let defaultSort;

    if (sortAttribute !== CATEGORY_SORT_NAMES.RELEVANCE) {
      defaultSort = sortAttribute;
      defaultSort += sortDescending
        ? CATEGORY_SORT_SEPARATOR + sortDescending
        : CATEGORY_SORT_SEPARATOR + CATEGORY_SORT_TYPES.ASC;
    }

    this.setState({
      defaultSort: defaultSort,
    });
  }

  componentWillReceiveProps(nextProps) {
    let { sortAttribute, sortDescending } = nextProps;
    let defaultSort;

    if (sortAttribute !== CATEGORY_SORT_NAMES.RELEVANCE) {
      defaultSort = sortAttribute;
      defaultSort += sortDescending
        ? CATEGORY_SORT_SEPARATOR + sortDescending
        : CATEGORY_SORT_SEPARATOR + CATEGORY_SORT_TYPES.ASC;
    }

    this.setState({
      defaultSort: defaultSort,
    });
  }

  render() {
    const { t } = this.props;

    let options = [
      {
        text: t("Relevance"),
        value: CATEGORY_SORT_NAMES.RELEVANCE,
      },
      {
        text: t("Name A-Z"),
        value:
          CATEGORY_SORT_NAMES.NAME +
          CATEGORY_SORT_SEPARATOR +
          CATEGORY_SORT_TYPES.ASC,
      },
      {
        text: t("Name Z-A"),
        value:
          CATEGORY_SORT_NAMES.NAME +
          CATEGORY_SORT_SEPARATOR +
          CATEGORY_SORT_TYPES.DESC,
      },
    ];

    options.push(
      {
        text: t("Price 0-99"),
        value:
          CATEGORY_SORT_NAMES.PRICE +
          CATEGORY_SORT_SEPARATOR +
          CATEGORY_SORT_TYPES.ASC,
      },
      {
        text: t("Price 99-0"),
        value:
          CATEGORY_SORT_NAMES.PRICE +
          CATEGORY_SORT_SEPARATOR +
          CATEGORY_SORT_TYPES.DESC,
      }
    );

    return (
      <Dropdown
        defaultValue={this.state.defaultSort}
        className="search-results__sorting"
        button
        placeholder={t("Sort by")}
        basic
        options={options}
        onChange={this.props.onSortChange}
      />
    );
  }
}

export default withTranslation("common")(CategorySort);
