import React, { Component } from "react";
import { Dropdown } from "semantic-ui-react";
import { withTranslation } from "react-i18next";

import { SORT_SEPARATOR, SORT_TYPES, SORT_NAMES } from "../../constants";

class Sort extends Component {
  constructor(props) {
    super(props);

    this.state = {
      defaultSort: null,
    };
  }

  componentWillMount() {
    let { sortAttribute, sortDescending } = this.props;
    let defaultSort;

    if (sortAttribute !== SORT_NAMES.RELEVANCE) {
      defaultSort = sortAttribute;
      defaultSort += sortDescending
        ? SORT_SEPARATOR + sortDescending
        : SORT_SEPARATOR + SORT_TYPES.ASC;
    }

    this.setState({
      defaultSort: defaultSort,
    });
  }

  componentWillReceiveProps(nextProps) {
    let { sortAttribute, sortDescending } = nextProps;
    let defaultSort;

    if (sortAttribute !== SORT_NAMES.RELEVANCE) {
      defaultSort = sortAttribute;
      defaultSort += sortDescending
        ? SORT_SEPARATOR + sortDescending
        : SORT_SEPARATOR + SORT_TYPES.ASC;
    }

    this.setState({
      defaultSort: defaultSort,
    });
  }

  render() {
    const { t } = this.props;

    let options = [
      {
        text: t("Relevance"),
        value: SORT_NAMES.RELEVANCE,
      },
      {
        text: t("Name A-Z"),
        value: SORT_NAMES.NAME + SORT_SEPARATOR + SORT_TYPES.ASC,
      },
      {
        text: t("Name Z-A"),
        value: SORT_NAMES.NAME + SORT_SEPARATOR + SORT_TYPES.DESC,
      },
    ];

    options.push(
      {
        text: t("Price 0-99"),
        value: SORT_NAMES.PRICE + SORT_SEPARATOR + SORT_TYPES.ASC,
      },
      {
        text: t("Price 99-0"),
        value: SORT_NAMES.PRICE + SORT_SEPARATOR + SORT_TYPES.DESC,
      }
    );

    return (
      <Dropdown
        defaultValue={this.state.defaultSort}
        className="search-results__sorting"
        button
        placeholder={t("Sort by")}
        basic
        options={options}
        onChange={this.props.onSortChange}
      />
    );
  }
}

export default withTranslation("common")(Sort);
