import React from 'react';
import { Input, Icon, Button} from 'semantic-ui-react';
import { Img } from 'react-image';
import Downshift from 'downshift';
import classnames from 'classnames';
import { withTranslation } from 'react-i18next';

import {formatPrice, isPriceNormalBiggerThanPrice, getDiscountText} from '../../utils/product';
import { EVENTS } from '../../constants';
import EE from '../../services/Emitter';
import PlaceholderImage from '../../assets/images/placeholder.png';

import './index.css';

export const NOTHING_SPLAT = 'N_O_T_H_IN_G__S_P_L_A_T';

var Autocomplete = ({items, productItems, productsTotal, onSearchClick, value, selectedValue, submitted, showDiscountIcon, discountRoundingType, thousandsSeparator, t, ...props}) => {
  const handleClickAddToCart = (productId) => (event) => {
    event.stopPropagation();
    EE.emit(EVENTS.ADD_TO_CART, productId);
  }

  return (
    <Downshift
      itemToString={item => item ? item.value : NOTHING_SPLAT}
      defaultSelectedItem={selectedValue ? {value: selectedValue, label: selectedValue} : null}
      {...props}>
      {({
        getInputProps,
        getItemProps,
        isOpen,
        inputValue,
        selectedItem,
        selectItem,
        highlightedIndex,
        clearSelection,
        reset,
        toggleMenu,
      }) => {
        const facetItems = items.filter(i => !inputValue || Boolean(i.facet));
        const facetItemsLength = facetItems.length;
        const pureItems = items.filter(i => !inputValue || !Boolean(i.facet));
        const pureItemsLength = pureItems.length;

        return (
          <div className='searchfield-container'>
            <Input
              id='searchfield-input'
              fluid={true}
              icon={<Icon name='search' inverted circular link onClick={onSearchClick} title={t("Search")}/>}
              onClick={() => {if(!isOpen && inputValue && items.length > 0){toggleMenu()}} }
              {...getInputProps({
                value: value,
                onKeyUp: (e) => {
                  if(e.keyCode === 13) {
                    selectItem({value: value, label: value});
                  }
                },
                placeholder: `${t("Search for items here …")}`
              })}
            />
            {isOpen && inputValue && items.length > 0 ? (
              <div className='autocomplete-container'>
                <div className='autocomplete-items'>
                  {pureItems.map((item, index) => {
                      return (
                      <div {...getItemProps({item})} key={item.label} className={classnames('downshift-item', 'item-row', highlightedIndex === index ? 'highlighted' : null)}>
                        {item.label}
                      </div>
                      )
                    })}
                </div>
                {facetItemsLength > 0 && (
                  <div className='autocomplete__facets'>
                    <div className='autocomplete__facets-label'>{t('Categories')}</div>
                    {facetItems.map((item, index) => {
                        const commonIndex = pureItemsLength + index;
                        return (
                          <div {...getItemProps({item})}  key={item.facet.value} className={classnames('downshift-item', highlightedIndex === commonIndex ? 'highlighted' : null, item.facet.value ? 'facet-row' : null)}>
                            {item.label} <span>{t('in')}<span className='facet'> <Icon name='folder open outline' />{item.facet.value.replace(/&amp;/g, '&')}</span></span>
                          </div>
                        )
                      })}
                  </div>
                )}
                <div className='autocomplete-products'>
                  {productItems.map((item, index) => {
                      const commonIndex = pureItemsLength + facetItemsLength + index;
                      const placeholderImage = <img src={PlaceholderImage} className="ui image autocomplete-products__image autocomplete-products__image--placeholder" alt={item.Name} title={item.Name} />

                      return (
                        <div {...getItemProps({item})}  key={item.Id} className={classnames('downshift-item', 'product-row', highlightedIndex === commonIndex ? 'highlighted' : null)}>
                          <div className="autocomplete-products__item-container">
                            <div className="autocomplete-products__image-container">
                              <Img className="ui image autocomplete-products__image" src={item.ImageSmallURL} alt={item.Name} title={item.Name} loader={placeholderImage} unloader={placeholderImage} />
                              {showDiscountIcon && item.Price && item.PriceNormal && item.Price !== item.PriceNormal && (
                                <div className="product-card__discount-image">{getDiscountText(item, discountRoundingType, thousandsSeparator)}</div>
                              )}
                            </div>
                            <div className="autocomplete-products__info">
                              <div className="autocomplete-products__name">{item.Name}</div>
                              <div className="autocomplete-products__brand">{item.Brand}</div>
                            </div>
                            <div className="product-card__price-container">
                              { item.PriceNormal && isPriceNormalBiggerThanPrice(item, thousandsSeparator) && (
                              <div className="product-card__price-normal-wrapper">
                                  <span className="product-card__price-normal">{item.PriceNormal}</span>
                                  <span className="product-card__currency">{item.PriceCurrency}</span>
                              </div>
                              )}
                              <div className="product-card__price-wrapper">
                                <span className="product-card__price">{formatPrice(item.Price, thousandsSeparator)}</span>
                                <span className="product-card__currency">{item.PriceCurrency}</span>
                              </div>
                            </div>
                            <div className="autocomplete-products__button-container">
                            <Button onClick={handleClickAddToCart(item.Id)} className='autocomplete-products__button'>
                              {t('Buy')}
                            </Button>
                            </div>
                          </div>
                        </div>
                      )
                    })}
                </div>
                <div className='autocomplete__total'>{t('Show product with', {total: productsTotal, text: inputValue}) }</div>
              </div>
            ) : null}
          </div>
        )
      }
      }
    </Downshift>
  )
}

export default withTranslation('common')(Autocomplete);