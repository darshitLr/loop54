import React, { Component } from 'react';
import {isEmpty} from 'lodash';
import {List} from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

import EE from '../../services/Emitter';
import {EVENTS, SEARCH_FIELDS} from '../../constants';

import './index.css';

class RelatedQueries extends Component {
  constructor(props) {
    super(props);

    this.state = {
      defaultSort: null
    }
  }

  handleRelatedClick = (e, data) => {
    EE.emit(EVENTS.SEARCH, { [SEARCH_FIELDS.SEARCH]: data.value })
  }

  render() {
    const {t, relatedQueries = []} = this.props;

    if(isEmpty(relatedQueries)){
        return null;
    }

    return (
      <div className="related-queries__wrapper">
        <div className="related-queries__label">{t('Related queries')}: </div>
        <List link horizontal>
          {
            relatedQueries.map(query => {
              return <List.Item className="related-query" value={query} key={query} onClick={this.handleRelatedClick}>{query}</List.Item>
            })
          }
        </List>
      </div>
    )
  }
}

export default withTranslation('common')(RelatedQueries);