import React, { Component } from 'react';
import debounce from 'lodash.debounce';
import {isEmpty} from 'lodash';

import EE, {getQueryAndSubscribe} from '../../services/Emitter';
import { formatResponse } from '../../utils/searchResult'
import {isOptionEnabled} from '../../utils/common';
import { SEARCH_FIELDS, EVENTS, MIN_AUTOSEARCH_COUNT } from '../../constants'
import Autocomplete, { NOTHING_SPLAT } from '../Autocomplete';

class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      productItems: [],
      productsTotal: 0,
      value: '',
      submitted: false
    }

    this.getAutocompleteListDebounced = debounce(this.getAutocompleteList, 100)
  }

  componentDidMount(){
    this.getQuery();
    this.getAutocompleteResults();
  }

  getQuery(){
    getQueryAndSubscribe((searchObj) => {
      this.setSearchValue(searchObj);
    })
  }

  getAutocompleteResults(){
    EE.on(EVENTS.AUTOCOMPLETE_ITEMS_LOADED, ({autocompleteItems, productItems, productsTotal}) => {
      const formatedAtocompleteItems = formatResponse(autocompleteItems);
      this.setState({
        items: formatedAtocompleteItems.slice(0, this.props.config.autocompleteCount),
        productItems, 
        productsTotal
      })
    })
  }

  setSearchValue = (searchObj) => {
    const search = searchObj[SEARCH_FIELDS.SEARCH] || '';
    if (this.state.value !== search){
      this.setState({value: search})
    }
  }

  getAutocompleteList = (value) => {
    if(value === ''){
      return
    }
    EE.emit(EVENTS.AUTOCOMPLETE, value);
  }

  onSelect = (selectedItem) => {
    if(selectedItem.ProductURL){
      window.location.href = selectedItem.ProductURL;
      return;
    }
    
    if(selectedItem.value !== '' && this.state.submitted === false) {
      this.setState({
        submitted: true
      })
      EE.emit(EVENTS.SEARCH, { [SEARCH_FIELDS.SEARCH]: selectedItem.value, [SEARCH_FIELDS.FACET]: selectedItem.facet ? [selectedItem.facet] : [] })
    }
  }

  onInputValueChange = (inputValue) => {
    if(inputValue === NOTHING_SPLAT){
      return ;
    }
    const { value } = this.state;
    if(value !== inputValue && inputValue !== undefined) {
      this.setState({
        value: inputValue,
        submitted: false
      })
      
      if(inputValue && inputValue.length >= MIN_AUTOSEARCH_COUNT) {
        this.getAutocompleteListDebounced(inputValue);
      } else {
        this.setState({
          items: [],
        })
      }
    }
  }

  handleSearchClick = () => {
    if(this.state.value && this.state.value !== '') {
      EE.emit(EVENTS.SEARCH, { [SEARCH_FIELDS.SEARCH]: this.state.value })
    }
  }
  
  render() {
    const {config} = this.props;
    
    if(isEmpty(config)){
      return null;
    }

    const {items, productItems, productsTotal} = this.state;

    return (
      <Autocomplete
        items={items}
        productItems={productItems}
        productsTotal={productsTotal}
        onChange={this.onChange}
        onSelect={this.onSelect}
        selectedValue={this.props.searchTerm}
        onSearchClick={this.handleSearchClick}
        onInputValueChange={this.onInputValueChange}
        value={this.state.value}
        showDiscountIcon={isOptionEnabled(config.showDiscountIcon)}
        discountRoundingType={config.discountRoundingType}
        thousandsSeparator={config.thousandsSeparator}
      />
    )
  }
}

export default Search;