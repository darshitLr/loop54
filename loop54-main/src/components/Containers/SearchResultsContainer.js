import React, { Component } from "react";
import { Button } from "semantic-ui-react";
import {
  isEmpty,
  keyBy,
  groupBy,
  sortBy,
  reverse,
  isEqual,
  orderBy,
} from "lodash";
import { withTranslation } from "react-i18next";

import {
  SORT_SEPARATOR,
  SEARCH_FIELDS,
  EVENTS,
  SORT_TYPES,
  SORT_NAMES,
} from "../../constants";
import { isOptionEnabled } from "../../utils/common";
import SearchResultList from "../SearchResult/List";
import Sort from "../Sort";
import EE, {
  getResultsAndSubscribe,
  getQueryAndSubscribe,
} from "../../services/Emitter";
import FacetList from "../Filter/List";
import SpellingSuggestion from "../SpellingSuggestions";
import RelatedQueries from "../RelatedQueries";

import "./index.css";

class SearchResultsContainer extends Component {
  initialState = {
    searchTerm: "",
    results: [],
    relatedResults: [],
    facets: [],
    selectedFacets: [],
    relatedQueries: [],
    spellingSuggestions: [],
    total: 0,
    page: 1,
    sortAttribute: SORT_NAMES.RELEVANCE,
    sortDescending: SORT_TYPES.ASC,
    makesSense: true,
  };

  constructor(props) {
    super(props);

    this.state = {
      ...this.initialState,
    };

    this.relatedResultsRef = React.createRef();
  }

  componentDidMount() {
    this.getQuery();
    this.getResults();
  }

  getQuery() {
    getQueryAndSubscribe((searchObj) => {
      const sort = searchObj[SEARCH_FIELDS.SORT] || {};
      const selectedFacets =
        searchObj[SEARCH_FIELDS.FACET] || this.initialState.selectedFacets;

      this.setState({
        selectedFacets: selectedFacets.filter(
          (selectedFacet) => selectedFacet.attributeName !== "Price"
        ),
        sortAttribute: sort.attributeName || this.initialState.sortAttribute,
        sortDescending: sort.value || this.initialState.sortDescending,
      });
    });
  }

  getResults() {
    getResultsAndSubscribe(
      ({
        results,
        facets,
        total,
        spellingSuggestions,
        relatedQueries,
        relatedResults,
        page,
        makesSense,
        searchTerm,
      }) => {
        this.setState((prevState) => {
          const nextSearchTerm = searchTerm || this.initialState.searchTerm;
          const nextResults =
            isEqual(prevState.facets, facets) && prevState.page + 1 === page
              ? [...prevState.results, ...results]
              : results;
          return {
            results: nextResults,
            searchTerm: nextSearchTerm,
            facets,
            total,
            spellingSuggestions,
            relatedQueries,
            relatedResults,
            page,
            makesSense,
          };
        });
      }
    );
  }

  resetState() {
    this.setState({ ...this.initialState });
  }

  prepareFacets(facets, selectedFacets) {
    const { attributes = [] } = this.props.config;
    const sortFacetsOptions = (
      this.props.config.sortFacetsOptions || ""
    ).toLowerCase();
    const mappedFacets = keyBy(facets, (facet) => facet.name);
    const groupedFacets = groupBy(selectedFacets, "attributeName");

    return attributes
      .filter(
        (attribute) =>
          isOptionEnabled(attribute.enabled) && mappedFacets[attribute.name]
      )
      .map((attribute) => {
        const obj = attribute.label ? { label: attribute.label } : {};
        const type = (attribute.type || "").toLowerCase();
        const attrSort = (attribute.sort || "").toLowerCase();
        const items = mappedFacets[attribute.name].items || [];
        const itemsGroup = groupedFacets[attribute.name];

        let sortedItems = items;
        if (type === "number") {
          let sort = attrSort || sortFacetsOptions;
          sort = sort === "desc" ? sort : "asc";
          sortedItems = sortedItems.map((i) => ({
            ...i,
            item: parseFloat(i.item),
          }));
          sortedItems = orderBy(sortedItems, "item", sort);
        } else if (attrSort) {
          const sort = attrSort === "desc" ? attrSort : "asc";
          sortedItems = orderBy(sortedItems, "item", sort);
        } else if (sortFacetsOptions) {
          const sort = sortFacetsOptions === "desc" ? sortFacetsOptions : "asc";
          sortedItems = orderBy(items, "item", sortFacetsOptions);
        }

        if (itemsGroup) {
          sortedItems = sortBy(
            sortedItems,
            (item) =>
              !reverse(itemsGroup.map((i) => i.value)).includes(item.item)
          );
        }

        return { ...mappedFacets[attribute.name], items: sortedItems, ...obj };
      });
  }

  loadMore = () => {
    EE.emit(EVENTS.LOAD_MORE, this.state.page + 1);
  };

  handleSortChange = (e, data) => {
    const [sortAttribute, sortDescending] = data.value.split(SORT_SEPARATOR);
    EE.emit(EVENTS.SORT, {
      attributeName: sortAttribute,
      value: sortDescending || SORT_TYPES.DESC,
    });
  };

  handleRangeFacetsChange = (facet) => {
    EE.emit(EVENTS.CHANGE_PRICE_FACET, facet);
  };

  handleSelectedFacetsChange = (facet) => {
    const isSelected = this.state.selectedFacets.some((selectedFacet) => {
      return (
        selectedFacet.attributeName === facet.attributeName &&
        selectedFacet.value === facet.value
      );
    });

    if (isSelected) {
      EE.emit(EVENTS.REMOVE_FACET, facet);
    } else {
      EE.emit(EVENTS.ADD_FACET, facet);
    }
  };

  handleClearFacets = () => {
    EE.emit(EVENTS.REMOVE_ALL_FACETS);
  };

  scrollToRelatedResults = () => {
    const $headerWrapper = document.querySelector("#header .header-wrapper");

    if ($headerWrapper) {
      let { top } = this.relatedResultsRef.current.getBoundingClientRect();
      const $masthead = $headerWrapper.querySelector("#masthead");
      if ($masthead) {
        top = top - $masthead.offsetHeight;
      }

      const $wideNav = $headerWrapper.querySelector("#wide-nav");
      if ($wideNav) {
        top = top - $masthead.offsetHeight;
      }

      if (window.innerWidth < 768) {
        top = top - document.querySelector("#loop54-search").offsetHeight;
      }

      window.scrollTo({
        top,
        behavior: "smooth",
      });
    } else {
      this.relatedResultsRef.current.scrollIntoView({
        block: "start",
        behavior: "smooth",
      });
    }
  };

  render() {
    const {
      results,
      relatedResults,
      searchTerm,
      total,
      page,
      selectedFacets,
      facets,
      spellingSuggestions,
      relatedQueries,
      sortAttribute,
      sortDescending,
      makesSense,
    } = this.state;
    const { t, config } = this.props;
    const { pageSize } = config;
    const maxPage = Math.ceil(total / pageSize);
    const sortedFacets = this.prepareFacets(facets, selectedFacets);

    const loadedResultsCount =
      page * pageSize > total ? total : page * pageSize;

    return (
      <>
        <div className="search-results__container">
          <RelatedQueries relatedQueries={relatedQueries} />
          <SpellingSuggestion spellingSuggestions={spellingSuggestions} />
          {!isEmpty(relatedResults) && (
            <div
              className="show-me-related__label"
              onClick={this.scrollToRelatedResults}
            >
              {t("Show me related results")}
            </div>
          )}
          {searchTerm && (
            <div className="search-results__label-container">
              <h3 className="search-results__label">
                {makesSense
                  ? t("Found", { total, searchTerm })
                  : t("Found 0 results that match your search.")}
              </h3>
              <Sort
                onSortChange={this.handleSortChange}
                sortAttribute={sortAttribute}
                sortDescending={sortDescending}
              />
            </div>
          )}
          {searchTerm && !makesSense && (
            <span className="search-results__not-make-sense">
              {t("NOTE")}: {t("Other found", { total })}
            </span>
          )}
          {!isEmpty(sortedFacets) && (
            <FacetList
              onSelectedFacetsChange={this.handleSelectedFacetsChange}
              onRangeFacetsChange={this.handleRangeFacetsChange}
              onClearFacets={this.handleClearFacets}
              selectedFacets={selectedFacets}
              facets={sortedFacets}
              columns={config.filtersColumnCount}
              visibleFacetsCount={config.visibleFacetsCount}
            />
          )}

          {!isEmpty(results) && (
            <div className="results-container">
              <SearchResultList
                results={results}
                columns={config.resultsColumnCount}
                withDealers={isOptionEnabled(config.isDealers)}
                showDiscountIcon={isOptionEnabled(config.showDiscountIcon)}
                discountRoundingType={config.discountRoundingType}
                thousandsSeparator={config.thousandsSeparator}
              />
            </div>
          )}

          {!isEmpty(results) && (
            <div className="search-results__showing">
              {t("Showing", { current: loadedResultsCount, total })}
            </div>
          )}

          <div className="show-more__container">
            {page < maxPage ? (
              <Button
                className="show-more__button"
                basic
                color="black"
                onClick={this.loadMore}
              >
                {t("Show more")}
              </Button>
            ) : null}
          </div>
        </div>

        {!isEmpty(relatedResults) && (
          <div
            className="related-results__container"
            ref={this.relatedResultsRef}
          >
            <div className="related-results__label">
              <h2>{t("Related products")}</h2>
            </div>
            <SearchResultList
              results={relatedResults}
              columns={config.relatedResultsColumnCount}
              withDealers={isOptionEnabled(config.isDealers)}
              showDiscountIcon={isOptionEnabled(config.showDiscountIcon)}
              discountRoundingType={config.discountRoundingType}
              thousandsSeparator={config.thousandsSeparator}
            />
          </div>
        )}
      </>
    );
  }
}

export default withTranslation("common")(SearchResultsContainer);
