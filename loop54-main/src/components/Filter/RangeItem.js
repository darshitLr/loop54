import React, { Component } from 'react';
import { List } from 'semantic-ui-react';
import InputRange from 'react-input-range';
import { withTranslation } from 'react-i18next';

import { prepareAttributeName } from '../../utils/attributes';

import 'react-input-range/lib/css/index.css';

class FacetItem extends Component {
  constructor(props) {
    super(props);

    const {selectedMin, selectedMax, min, max} = this.props.facet;
    let nextMin = Math.floor(selectedMin || min);
    let nextMax = Math.ceil(selectedMax || max);
    const nextMinValue = Math.floor(min);
    const nextMaxValue = Math.ceil(max);
    nextMin = nextMin > nextMinValue ? nextMin : nextMinValue;
    nextMax = nextMax > nextMaxValue ? nextMaxValue : nextMax;

    this.state = {
      value: {min: nextMin, max: nextMax},
      minValue: nextMinValue,
      maxValue: nextMaxValue
    }
  }

  componentWillReceiveProps(nextProps) {
    const {value, minValue, maxValue} = this.state;
    const {selectedMin, selectedMax, min, max} = nextProps.facet;
    let nextMin = Math.floor(selectedMin || min);
    let nextMax = Math.ceil(selectedMax || max);
    const nextMinValue = Math.floor(min);
    const nextMaxValue = Math.ceil(max);
    nextMin = nextMin > nextMinValue ? nextMin : nextMinValue;
    nextMax = nextMax > nextMaxValue ? nextMaxValue : nextMax;
    
    if (value.min !== nextMin || value.max !== nextMax) {
      this.setState({
        value: {min: nextMin, max: nextMax}
      })
    }
    
    if (minValue !== nextMinValue || maxValue !== nextMaxValue) {
      this.setState({
        minValue: nextMinValue,
        maxValue: nextMaxValue
      })
    }
  }

  handleRangeChange = (value) => {
    const {name} = this.props.facet;
    this.props.onRangeFacet({ attributeName: name, value: value });
    this.setState({value})
  }

  onChange = (value) => {
    const {min: prevMin, max: prevMax} = this.props.facet;
    const {min, max} = value;
    const nextMin = min < prevMin ? Math.floor(prevMin) : min;
    const nextMax = max > prevMax ? Math.ceil(prevMax) : max;
    this.setState({ value: {min: nextMin, max: nextMax}});
  }

  renderRange = () => {
    const {name, label} = this.props.facet;
    const {minValue, maxValue, value} = this.state;

    return (
      <List>
        <List.Header>{label || prepareAttributeName(name)}</List.Header>
        <List.Content className="facet-range-content">
          <InputRange
            minValue={minValue}
            maxValue={minValue >= maxValue ? maxValue + 1 : maxValue}
            value={value}
            onChangeComplete={this.handleRangeChange}
            onChange={this.onChange}
          />
        </List.Content>
      </List>
    )
  }

  render() {
    return (
      <div>
        {this.renderRange()}
      </div>
    );
  }
}

export default withTranslation('common')(FacetItem);