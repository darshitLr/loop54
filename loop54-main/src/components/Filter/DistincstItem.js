import React, { Component } from "react";
import ClassNames from "classnames";
import { List, Checkbox, Button } from "semantic-ui-react";
import { withTranslation } from "react-i18next";

import { prepareAttributeName } from "../../utils/attributes";

import "react-input-range/lib/css/index.css";

class FacetItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };
  }

  handleShowMore = (action) => {
    if (action === "open") {
      this.setState({ open: true });
    } else {
      this.setState({ open: false });
    }
  };

  renderList = () => {
    const {
      facet,
      selectedFacets = [],
      onSelectedFacets,
      visibleFacetsCount,
      t,
    } = this.props;
    const { name, items, label } = facet;
    const filtredItems = items.filter((item) => {
      const name = item.item;
      return name;
    });
    const itemsToRender = this.state.open
      ? filtredItems
      : filtredItems.slice(0, visibleFacetsCount);

    const listItems = itemsToRender.map((item) => {
      const name = item.item;
      const isSelected = selectedFacets.some((selectedFacet) => {
        return (
          selectedFacet.attributeName === facet.name &&
          selectedFacet.value === name
        );
      });

      return (
        <List.Item key={name}>
          <List.Content
            className={ClassNames(
              "list-item",
              isSelected ? "list-checked" : null
            )}
          >
            <Checkbox
              checked={isSelected}
              value={name}
              name={facet.name}
              onChange={onSelectedFacets}
              label={name.replace(/&amp;/g, "&")}
            />{" "}
            <span className="list-value">({item.count})</span>
          </List.Content>
        </List.Item>
      );
    });

    if (listItems.length === 0) {
      return null;
    }

    return (
      <List>
        <List.Header>{label || prepareAttributeName(name)}</List.Header>
        {listItems}
        {filtredItems.length > visibleFacetsCount && !this.state.open ? (
          <Button
            basic
            className="show-more-button"
            onClick={() => {
              this.handleShowMore("open");
            }}
          >
            {t("Show more")}
          </Button>
        ) : null}
        {filtredItems.length > visibleFacetsCount && this.state.open ? (
          <Button
            className="show-more-button"
            onClick={() => {
              this.handleShowMore("close");
            }}
          >
            {t("Show less")}
          </Button>
        ) : null}
      </List>
    );
  };

  render() {
    return <div>{this.renderList()}</div>;
  }
}

export default withTranslation("common")(FacetItem);
