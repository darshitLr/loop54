import React, { Component } from "react";
import { Grid, Button, Icon } from "semantic-ui-react";
import { isEmpty } from "lodash";
import { withTranslation } from "react-i18next";

import DistincstItem from "./DistincstItem";
import RangeItem from "./RangeItem";

class FacetList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
    };
  }

  handleFacetClick = (event, data) => {
    const { name, value } = data;
    this.props.onSelectedFacetsChange({ attributeName: name, value: value });
  };

  clearFilters = (e) => {
    e.preventDefault();
    this.props.onClearFacets({});
  };

  renderShowFacetsButton = () => {
    const { t } = this.props;
    return (
      <Grid.Row className="facets__show-facets-button-container">
        <Button fluid basic onClick={() => this.setState({ show: true })}>
          {t("Show filters")}
        </Button>
      </Grid.Row>
    );
  };

  renderFacets = (facets, selectedFacets) => {
    const { t, onRangeFacetsChange, visibleFacetsCount } = this.props;
    const { columns } = this.props;
    return (
      <Grid columns={columns} className="facets" doubling stackable>
        {facets.map((facet) => {
          return (
            <Grid.Column key={facet.name}>
              {facet.type === "distinct" && (
                <DistincstItem
                  facet={facet}
                  visibleFacetsCount={visibleFacetsCount}
                  selectedFacets={selectedFacets}
                  onSelectedFacets={this.handleFacetClick}
                />
              )}
              {facet.type === "range" && (
                <RangeItem facet={facet} onRangeFacet={onRangeFacetsChange} />
              )}
            </Grid.Column>
          );
        })}
        <Grid.Row>
          <Button fluid onClick={() => this.setState({ show: false })}>
            {t("Hide filters")}
          </Button>
        </Grid.Row>
      </Grid>
    );
  };

  renderActiveList = () => {
    const { selectedFacets, t } = this.props;
    return (
      <div className="facets__active-list">
        <div className="facets__active-list-label">{t("Active filters")}:</div>
        {selectedFacets.map(({ attributeName, value }) => {
          return (
            <div
              className="facets__active-list-item"
              key={`${attributeName}_${value}`}
            >
              {value.replace(/&amp;/g, "&")}
            </div>
          );
        })}
      </div>
    );
  };

  renderClearFilters = () => {
    const { t } = this.props;
    return (
      <div className="facets__remove-filters">
        <a href="#" onClick={this.clearFilters}>
          {t("Clear filters")} <Icon name="remove" />
        </a>
      </div>
    );
  };

  render() {
    const { facets, selectedFacets } = this.props;

    return (
      <Grid.Column className="facets-container">
        {!isEmpty(selectedFacets) && (
          <div className="facets__active-list-container">
            {this.renderActiveList()}
            {this.renderClearFilters()}
          </div>
        )}
        {this.state.show
          ? this.renderFacets(facets, selectedFacets)
          : this.renderShowFacetsButton()}
      </Grid.Column>
    );
  }
}

FacetList.defaultProps = {
  columns: 4,
};

export default withTranslation("common")(FacetList);
