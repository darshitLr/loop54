import React, { Component } from 'react';
import {isEmpty} from 'lodash';
import {List} from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

import EE from '../../services/Emitter';
import {EVENTS, SEARCH_FIELDS} from '../../constants';

import './index.css';

class SpellingSuggestion extends Component {
  constructor(props) {
    super(props);

    this.state = {
      defaultSort: null
    }
  }

  handleClick = (e, data) => {
    EE.emit(EVENTS.SEARCH, { [SEARCH_FIELDS.SEARCH]: data.value })
  }

  render() {
    const {t, spellingSuggestions = []} = this.props;

    if(isEmpty(spellingSuggestions)){
      return null;
    }

    return (
      <div className="suggestions__wrapper">
        <div className="suggestions__label">{t('Suggested searches')}: </div>
        <List link horizontal>
          {
            spellingSuggestions.map(query => {
              return <List.Item className="suggestion-query" value={query} key={query} onClick={this.handleClick}>{query}</List.Item>
            })
          }
        </List>
      </div>
    )
  }
}

export default withTranslation('common')(SpellingSuggestion);