import React, { Component } from "react";
import { Grid } from "semantic-ui-react";
import CategoryItem from "./CategoryItem";

class CategoryList extends Component {
  render() {
    const {
      results,
      columns,
      withDealers,
      showDiscountIcon,
      discountRoundingType,
      thousandsSeparator,
    } = this.props;

    return (
      <Grid className="product-grid" doubling padded columns={columns}>
        {results &&
          results.map((item) => {
            return (
              <Grid.Column className="product-grid-column" key={item.Id || ""}>
                <CategoryItem
                  item={item}
                  withDealers={withDealers}
                  showDiscountIcon={showDiscountIcon}
                  discountRoundingType={discountRoundingType}
                  thousandsSeparator={thousandsSeparator}
                />
              </Grid.Column>
            );
          })}
      </Grid>
    );
  }
}

CategoryList.defaultProps = {
  columns: 4,
};

export default CategoryList;
