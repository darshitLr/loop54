import React, { Component } from "react";
// import { Link } from 'react-router-dom';
import { Card, Button } from "semantic-ui-react";
import { Img } from "react-image";
import { withTranslation } from "react-i18next";
// import PropTypes from 'prop-types';

// import BasketTool from '../../utils/basketTool';
import {
  formatPrice,
  getDiscountText,
  isPriceNormalBiggerThanPrice,
} from "../../utils/product";

import PlaceholderImage from "../../assets/images/placeholder.png";
import { EVENTS } from "../../constants";
import EE from "../../services/Emitter";

// import {initializeProduct} from '../../utils/productInitialize'

class CategoryItem extends Component {
  // constructor(props) {
  //   super(props);

  //   this.state = {
  //     item: {}
  //   }
  // }

  //   componentWillMount() {
  //     const { item, config } = this.props;
  //     let stateItem = initializeProduct(item, config);

  //     this.setState({
  //       item: stateItem
  //     })
  //   }

  handleClickReadMore = () => {
    window.location.href = this.props.item.ProductURL;
    // BasketTool.addToBasket(this.state.item, this.props.config.name, this.props.onBasketChange)
    // Events.send(this.state.item.id, this.props.config.purchaseEventType)
    // this.props.onAddNotification('Purchased \'' + this.state.item.title + '\'', 'info')
  };

  handleClickAddToCart = () => {
    const productId = this.props.item.Id;
    EE.emit(EVENTS.ADD_TO_CART, productId);
    // BasketTool.addToBasket(this.state.item, this.props.config.name, this.props.onBasketChange)
    // Events.send(this.state.item.id, this.props.config.purchaseEventType)
    // this.props.onAddNotification('Purchased \'' + this.state.item.title + '\'', 'info')
  };

  handleProductClick = (e) => {};

  get price() {
    const { item, withDealers, thousandsSeparator } = this.props;

    if (!withDealers) {
      return formatPrice(item.Price, thousandsSeparator);
    }

    if (!window.dealers_is_logged || !window.dealers_customer_group) {
      return <span className="hidden-price">***</span>;
    }

    if (
      !item.PriceCustomerGroup ||
      (item.PriceCustomerGroup &&
        !JSON.parse(item.PriceCustomerGroup)[window.dealers_customer_group])
    ) {
      return formatPrice(item.Price, thousandsSeparator);
    }

    return JSON.parse(item.PriceCustomerGroup)[window.dealers_customer_group];
  }

  render() {
    const {
      item,
      withDealers,
      showDiscountIcon,
      discountRoundingType,
      thousandsSeparator,
      t,
    } = this.props;

    const placeholderImage = (
      <img
        src={PlaceholderImage}
        className="ui image product-card__image product-card__image--placeholder"
        alt={item.Name}
        title={item.Name}
        onClick={this.handleProductClick}
      />
    );

    return (
      <Card className="product-card" fluid>
        <div className="product-card__image-container">
          <a href={item.ProductURL}>
            <Img
              className="ui image product-card__image"
              src={item.ImageURL}
              alt={item.Name}
              title={item.Name}
              onClick={this.handleProductClick}
              loader={placeholderImage}
              unloader={placeholderImage}
            />
            {showDiscountIcon &&
              item.PriceSale &&
              item.PriceNormal &&
              item.PriceSale !== item.PriceNormal && (
                <div className="product-card__discount-image">
                  {getDiscountText(
                    item,
                    discountRoundingType,
                    thousandsSeparator
                  )}
                </div>
              )}
          </a>
        </div>
        <Card.Content className="product-card__content custom-class">
          <Card.Header className="product-card__name">
            <a className="product-card__name-link" href={item.ProductURL}>
              {item.Name}
            </a>
          </Card.Header>
          <Card.Meta className="product-card__meta">
            {item.PriceNormal &&
              isPriceNormalBiggerThanPrice(item, thousandsSeparator) &&
              !withDealers && (
                <div className="product-card__price-normal-wrapper">
                  <span className="product-card__price-normal">
                    {item.PriceNormal}
                  </span>
                  <span className="product-card__currency">
                    {item.PriceCurrency}
                  </span>
                </div>
              )}
            {item.PriceSale && (
              <div className="product-card__price-wrapper">
                <span className="product-card__price">{item.PriceSale}</span>
                <span className="product-card__currency">
                  {item.PriceCurrency}
                </span>
              </div>
            )}
            {/* {item.PriceSale && !withDealers && (
              <div className="product-card__price-tier-wrapper">
                <span className="product-card__price-tier-label">
                  {t("As low as")}{" "}
                </span>
                <span className="product-card__price-tier">
                  {formatPrice(item.PriceSale, thousandsSeparator)}
                </span>
                <span className="product-card__currency">
                  {item.PriceCurrency}
                </span>
              </div>
            )} */}
          </Card.Meta>
          {/* <div className="product-card__sku">
            <span className="product-card__sku-label">{t("Sku")}: </span>
            {item.SKU}
          </div>
          <div className="product-card__stock-wrapper">
            <div
              className="product-card__stock"
              dangerouslySetInnerHTML={{ __html: item.Stock }}
            ></div>
          </div> */}
          <div className="product-card__actions-wrapper">
            <Button
              onClick={this.handleClickAddToCart}
              className="product-card__purchase-button"
            >
              {t("Add to cart")}
            </Button>
          </div>
        </Card.Content>
      </Card>
    );
  }
}

export default withTranslation("common")(CategoryItem);
