import React, { Component } from "react";
import { Card, Button } from "semantic-ui-react";
import { Img } from "react-image";
import { withTranslation } from "react-i18next";

import {
  formatPrice,
  isPriceNormalBiggerThanPrice,
  getDiscountText,
} from "../../utils/product";
import PlaceholderImage from "../../assets/images/placeholder.png";
import { EVENTS } from "../../constants";
import EE from "../../services/Emitter";

class SerchResultItem extends Component {
  handleClickReadMore = () => {
    window.location.href = this.props.item.ProductURL;
  };

  handleClickAddToCart = () => {
    const productId = this.props.item.Id;
    EE.emit(EVENTS.ADD_TO_CART, productId);
  };

  handleProductClick = (e) => {};

  get price() {
    const { item, withDealers, thousandsSeparator } = this.props;

    if (!withDealers) {
      return formatPrice(item.Price, thousandsSeparator);
    }

    if (!window.dealers_is_logged || !window.dealers_customer_group) {
      return <span className="hidden-price">***</span>;
    }

    if (
      !item.PriceCustomerGroup ||
      (item.PriceCustomerGroup &&
        !JSON.parse(item.PriceCustomerGroup)[window.dealers_customer_group])
    ) {
      return formatPrice(item.Price, thousandsSeparator);
    }

    return JSON.parse(item.PriceCustomerGroup)[window.dealers_customer_group];
  }

  render() {
    const {
      item,
      withDealers,
      showDiscountIcon,
      discountRoundingType,
      thousandsSeparator,
      t,
    } = this.props;

    const placeholderImage = (
      <img
        src={PlaceholderImage}
        className="ui image product-card__image product-card__image--placeholder"
        alt={item.Name}
        title={item.Name}
        onClick={this.handleProductClick}
      />
    );

    return (
      <Card className="product-card" fluid>
        <div className="product-card__image-container">
          <a href={item.ProductURL}>
            <Img
              className="ui image product-card__image"
              src={item.ImageURL}
              alt={item.Name}
              title={item.Name}
              onClick={this.handleProductClick}
              loader={placeholderImage}
              unloader={placeholderImage}
            />
            {showDiscountIcon &&
              item.Price &&
              item.PriceNormal &&
              item.Price !== item.PriceNormal && (
                <div className="product-card__discount-image">
                  {getDiscountText(
                    item,
                    discountRoundingType,
                    thousandsSeparator
                  )}
                </div>
              )}
          </a>
        </div>
        <Card.Content className="product-card__content">
          <Card.Header className="product-card__name">
            <a className="product-card__name-link" href={item.ProductURL}>
              {item.Name}
            </a>
          </Card.Header>
          <Card.Meta className="product-card__meta">
            {item.PriceNormal &&
              isPriceNormalBiggerThanPrice(item, thousandsSeparator) &&
              !withDealers && (
                <div className="product-card__price-normal-wrapper">
                  <span className="product-card__price-normal">
                    {item.PriceNormal}
                  </span>
                  <span className="product-card__currency">
                    {item.PriceCurrency}
                  </span>
                </div>
              )}
            <div className="product-card__price-wrapper">
              <span className="product-card__price">{this.price}</span>
              <span className="product-card__currency">
                {item.PriceCurrency}
              </span>
            </div>
            {item.PriceTier && !withDealers && (
              <div className="product-card__price-tier-wrapper">
                <span className="product-card__price-tier-label">
                  {t("As low as")}{" "}
                </span>
                <span className="product-card__price-tier">
                  {formatPrice(item.PriceTier, thousandsSeparator)}
                </span>
                <span className="product-card__currency">
                  {item.PriceCurrency}
                </span>
              </div>
            )}
          </Card.Meta>
          <div className="product-card__sku">
            <span className="product-card__sku-label">{t("Sku")}: </span>
            {item.SKU}
          </div>
          <div className="product-card__stock-wrapper">
            <div
              className="product-card__stock"
              dangerouslySetInnerHTML={{ __html: item.Stock }}
            ></div>
          </div>
          <div className="product-card__actions-wrapper">
            <Button
              onClick={this.handleClickReadMore}
              className="product-card__read-more-button"
            >
              {t("Read more")}
            </Button>

            <Button
              onClick={this.handleClickAddToCart}
              className="product-card__purchase-button"
            >
              {t("Add to cart")}
            </Button>
          </div>
        </Card.Content>
      </Card>
    );
  }
}

export default withTranslation("common")(SerchResultItem);
