import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';

import SerchResultItem from './Item';

class SearchResultList extends Component {
  render() {
    const {results,  columns, withDealers, showDiscountIcon, discountRoundingType, thousandsSeparator} = this.props;
    
    return (
      <Grid className="product-grid" doubling padded columns={columns}>
        {results && results.map((item) => {
          return ((
            <Grid.Column className="product-grid-column" key={item.Id}>
              <SerchResultItem 
                item={item} 
                withDealers={withDealers}
                showDiscountIcon={showDiscountIcon}
                discountRoundingType={discountRoundingType}
                thousandsSeparator={thousandsSeparator}
              />
            </Grid.Column>
          ))
        })}
      </Grid>
    );
  }
}

SearchResultList.defaultProps = {
  columns: 4
}

export default SearchResultList;