import { prepareAttribute } from "./attributes";
import { ROUNDING_TYPES } from "../constants";

export const formatPrice = (price, thousandsSeparator = ".") => {
  return Number.parseFloat(price)
    .toFixed(2)
    .toString()
    .replace(".", ",")
    .replace(/\B(?=(\d{3})+(?!\d))/g, thousandsSeparator);
};

export const convertPriceStrToNumber = (price, thousandsSeparator = ".") => {
  return typeof price === "string"
    ? Number(price.replace(thousandsSeparator, "").replace(",", "."))
    : price;
};

export const mapProducts = (products) => {
  if (!products) {
    return [];
  }

  const result = products.map((product) =>
    product.attributes.reduce((acc, curAttr) => {
      const [name, value] = prepareAttribute(curAttr);
      acc[name] = value;
      return acc;
    }, {})
  );

  return result;
};

export const isPriceNormalBiggerThanPrice = (item, thousandsSeparator) => {
  const priceNormal = convertPriceStrToNumber(
    item.PriceNormal,
    thousandsSeparator
  );
  const price = convertPriceStrToNumber(item.Price, thousandsSeparator);
  return priceNormal > price;
};

export const getDiscountText = (
  item,
  roundingType = "",
  thousandsSeparator
) => {
  const priceNormal = convertPriceStrToNumber(
    item.PriceNormal,
    thousandsSeparator
  );
  const price = convertPriceStrToNumber(item.Price, thousandsSeparator);
  let results = ((priceNormal - price) / priceNormal) * 100;

  switch (roundingType.toLowerCase()) {
    case ROUNDING_TYPES.UP:
      results = Math.ceil(results);
      break;
    case ROUNDING_TYPES.DOWN:
      results = Math.floor(results);
      break;
    default:
      results = Math.round(results);
      break;
  }

  return "-" + results + "%";
};
