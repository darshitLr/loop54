// https://jsfiddle.net/ew5ug4mn/

const splitter = 'v';
const secretKet = 'yvevKvtvevrvcvevs';
const location = 'nvovivtvavcvovl';
const hostname = 'evmvavnvtvsvovh';
const salt = 'ovrvBvevcvrvevmvmvovc';

const secretReverse = (str) => str.split(splitter).reverse().join('');

export const getConfigKey = () => secretReverse(secretKet);

export const getLocationStr = () => secretReverse(location);

export const getHostanameStr = () => secretReverse(hostname);

export const decipher = encoded => {
    if(!encoded){
        return
    }
    const textToChars = text => text.split('').map(c => c.charCodeAt(0));
    const applySaltToChar = code => textToChars(secretReverse(salt)).reduce((a,b) => a ^ b, code);
    return encoded.match(/.{1,2}/g)
        .map(hex => parseInt(hex, 16))
        .map(applySaltToChar)
        .map(charCode => String.fromCharCode(charCode))
        .join('');
}
