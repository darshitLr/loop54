import { ATTRIBUTES_MAP, INVERT_ATTRIBUTES_MAP } from "../constants";

const capitalize = (s) => {
  if (typeof s !== "string") return "";
  return s.charAt(0).toUpperCase() + s.slice(1);
};

export const prepareAttribute = (attribute) => {
  const preparedName = attribute.name;
  const nextName = ATTRIBUTES_MAP[preparedName]
    ? ATTRIBUTES_MAP[preparedName]
    : preparedName;
  return [nextName, attribute.values[0]];
};

export const prepareAttributeName = (attributeName = "") => {
  return capitalize(attributeName.replace("attributes_", "").replace("_", " "));
};
