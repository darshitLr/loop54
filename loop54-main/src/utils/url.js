import { isEmpty } from "lodash";
import queryString from "query-string";

import { SEARCH_FIELDS } from "../constants";
import { CATEGORY_EVENTS, CATEGORY_FIELDS } from "../constants";
import { getConfig } from "../services/Emitter";

export const getSearchUrl = (searcObj) => {
  const search = searcObj[SEARCH_FIELDS.SEARCH];
  const facets = searcObj[SEARCH_FIELDS.FACET];
  const page = searcObj[SEARCH_FIELDS.PAGE];
  const sort = searcObj[SEARCH_FIELDS.SORT];
  const resultsPage = getConfig().resultsPage || "";

  let url = `/${resultsPage}?${SEARCH_FIELDS.SEARCH}=${search}`;

  if (!isEmpty(facets)) {
    facets.forEach((facet) => {
      url =
        url +
        `&${SEARCH_FIELDS.FACET}=${encodeURIComponent(JSON.stringify(facet))}`;
    });
  }
  if (page) {
    url = url + `&${SEARCH_FIELDS.PAGE}=${page}`;
  }
  if (sort) {
    url =
      url +
      `&${SEARCH_FIELDS.SORT}=${encodeURIComponent(JSON.stringify(sort))}`;
  }

  return url;
};

export const getcategoryUrl = (categoryObj) => {
  let category = document.getElementById("catName").value;
  const facets = categoryObj[CATEGORY_FIELDS.FACET];
  const page = categoryObj[CATEGORY_FIELDS.PAGE] || 1;
  const sort = categoryObj[CATEGORY_FIELDS.SORT];
  let url = `?${[CATEGORY_FIELDS.CATEGORY]}=${category}`;

  if (!isEmpty(facets)) {
    facets.forEach((facet) => {
      url =
        url +
        `&${CATEGORY_FIELDS.FACET}=${encodeURIComponent(
          JSON.stringify(facet)
        )}`;
    });
  }
  if (page) {
    url = url + `&${CATEGORY_FIELDS.PAGE}=${page}`;
  }
  if (sort) {
    url =
      url +
      `&${CATEGORY_FIELDS.SORT}=${encodeURIComponent(JSON.stringify(sort))}`;
  }

  return url;
};

export const getSearchObj = () => {
  const parsedQuery = queryString.parse(window.location.search);

  return Object.keys(parsedQuery).reduce((acc, key) => {
    let value = parsedQuery[key];

    if (key === SEARCH_FIELDS.FACET) {
      value = Array.isArray(value) ? value : [value];
      value = value.map(JSON.parse);
    } else if (key === SEARCH_FIELDS.SORT) {
      value = JSON.parse(value);
    } else if (key === SEARCH_FIELDS.PAGE) {
      value = Number(value);
    }
    acc[key] = value;

    return acc;
  }, {});
};

export const getCategoryObj = () => {
  const parsedQuery = queryString.parse(window.location.search);

  return Object.keys(parsedQuery).reduce((acc, key) => {
    let value = parsedQuery[key];
    if (key === CATEGORY_FIELDS.FACET) {
      value = Array.isArray(value) ? value : [value];
      value = value.map(JSON.parse);
    } else if (key === CATEGORY_FIELDS.SORT) {
      value = JSON.parse(value);
    } else if (key === CATEGORY_FIELDS.PAGE) {
      value = Number(value);
    }
    acc[key] = value;

    return acc;
  }, {});
};
