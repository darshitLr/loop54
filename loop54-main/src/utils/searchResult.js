export const formatResponse = (data) => {
  if(!data ||  !data.queries ||  !data.queries.count){
      return [];
  }

  const queries = data.queries.items.map(({query}) => ({value: query, label: query}));
  const facets = data.scopedQuery && data.scopedQuery.scopes  
    ? data.scopedQuery.scopes.map( scope => ({
        label: data.scopedQuery.query,
        value: data.scopedQuery.query,
        facet: {attributeName: data.scopedQuery.scopeAttributeName, value: scope},
      }))
    : [];

  return [...facets, ...queries];
}