export const isOptionEnabled = (option, i) => {
  return option && (option === true || option === "true");
}