<?php
/**
 * Export WordPress post data to CSV
 */
ini_set('memory_limit', '-1');
set_time_limit(0);

define( 'PATH_TO_WORDPRESS', '' ); # Default WordPress install location
//define( 'PATH_TO_WORDPRESS', './wordpress/' ); # Default WordPress sub-directory install location

$export_query = array(
    'posts_per_page' => 1000,
    'post_status' => 'publish',
    'post_type' => 'product',
);

// Require WordPress environment
require PATH_TO_WORDPRESS . 'wp-load.php';


$xmlArr = array();
$currency_symbol = 'kr';

$attributesList = array();

$all = 0;
for ($paged = 1; $paged < 30; $paged ++) {

    $args = array(
        'post_type'      => 'product',
        'posts_per_page' => 1000,
        'paged' => $paged,

    );

    $posts = new WP_Query( $args );
    $posts = $posts->posts;

    if($posts) {

        // Output data to file
        foreach ( $posts as $post ) {

            $itemArr = array();

            //echo $post->ID.' - ';

            $product = wc_get_product( $post->ID );
            if($product) {
                $itemArr['Id'] = $product->get_id();
                $itemArr['SKU'] = $product->get_sku();
                $itemArr['Name'] = html_entity_decode($product->get_name());
                //$itemArr['Ean'] = $product->get_packstation_ean();
                $itemArr['Description'] = html_entity_decode($product->get_description());
                $itemArr['ProductURL'] = get_permalink( $product->get_id() );
                $itemArr['Weight'] = sprintf("%.2f", ($product->get_weight() / 1000)). ' Kg';

                $image_id  = $product->get_image_id();
                $image_url = wp_get_attachment_image_url( $image_id, 'medium'); //, array('width'=>400,'height'=>400)
                $itemArr['ImageURL'] = wp_get_attachment_image_url( $image_id, 'woocommerce_thumbnail');
                $itemArr['ImageSmallURL'] = wp_get_attachment_image_url( $image_id, 'woocommerce_gallery_thumbnail');

                // Get the brand name
                $brand_names = wp_get_post_terms( $product->get_id(), 'pwb-brand', array( 'fields' => 'names' ) );
                $brand_name = reset( $brand_names );
                if($brand_name) {
                    $itemArr['Brand'] = $brand_name;
                }

                $allCategories = array();
                $cat_args = array(
                    'orderby'    => 'name',
                    'order'      => 'ASC',
                    'hide_empty' => false
                );

                $product_categories = get_the_terms( $product->get_id(), 'product_cat' );
                $product_categories_ids = array();
                if( !empty($product_categories) ) {
                    foreach($product_categories as $p_cat) {

                        $parentId = $p_cat->parent;
                        if($parentId) {
                            // check all parents till root
                            do {
                                if( $term = get_term_by( 'id', $parentId, 'product_cat' ) ){
                                    $allCategories[] = html_entity_decode($term->name);
                                    $parentId = $term->parent;
                                }
                            } while ($parentId);
                        }
                        $allCategories[] = html_entity_decode($p_cat->name);
                    }

                    $itemArr['CategoryPath'] = array_unique($allCategories);
                }
                /*
                    $product->get_date_created();
                    $product->get_price();
                    $product->get_regular_price();
                    $product->get_sale_price();
                    $product->get_date_on_sale_from();
                    $product->get_date_on_sale_to();
                    $product->get_total_sales();
                */

                if($product->is_type('simple')) {
                    $itemArr['PriceSale'] = number_format($product->get_sale_price(), 2, ',', ' ');
                    $itemArr['PriceNormal'] = number_format($product->get_regular_price(), 2, ',', ' ');
                    $itemArr['Price'] = number_format($product->get_price(), 2, ',', ' ');
                    $itemArr['PriceCurrency'] = $currency_symbol;
                } elseif ($product->is_type('variable')) {

                    $variation_min_price = $product->get_variation_price();

                    $itemArr['PriceSale'] = number_format($variation_min_price, 2, ',', ' ');
                    $itemArr['PriceNormal'] = number_format($variation_min_price, 2, ',', ' ');
                    $itemArr['Price'] = number_format($variation_min_price, 2, ',', ' ');

                    $itemArr['PriceCurrency'] = $currency_symbol;
                } else {
                    continue;
                }

                // stock
                $isInStock = $product->get_stock_status() == 'instock';
                $itemArr['InStock'] = $isInStock ? 'true' : 'false';
                $itemArr['Stock'] = $isInStock ? '<span class="in_stock">I webblager</span>' : '<span class="outof_stock">Utan butik</span>';
                $itemArr['Stock'].= '<br />Skickas inom 1-2 dagar';

                //$itemArr['HiddenKeywords'] = xmlAllowedStringSize($product->get_name().' '.trim($product->get_sku()), 0, 80, true);

                // get all attributes
                $attributes = $product->get_attributes();
                if(!empty($attributes)) {
                    $itemArr['Attributes'] = array();
                    $specificValues = array();
                    foreach( $attributes as $attribute_code => $attribute ) {

                        $attribute_terms = $attribute->get_terms();
                        $term_id = $attribute_terms[0]->term_id;
                        $termName = '';
                        $term = get_term( $term_id );
                        if ( false !== $term ) {
                            $termName = trim($term->name);
                        }

                        if($termName) {
                            $specificValues[$attribute_code] = html_entity_decode($termName);
                        }
                    }
                    if(!empty($specificValues)) {
                        $itemArr['Attributes'] = $specificValues;
                    }
                }

                $xmlArr[] = $itemArr;
            }

            $product = null;
            $all++;
        }

    } else {
        break;
    }

    $posts = null;
    wp_reset_query();
}

// creating object of SimpleXMLElement
$xml_items = new SimpleXMLElementExtended("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root></root>");
//$xml_items = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"? ><root xmlns=\"http://www.w3.org/2005/Atom\" xmlns:g=\"http://base.google.com/ns/1.0\"></root>"); //encoding=\"utf-8\"

// function call to convert array to xml
array_to_xml(
    array(
        'title' => 'Rörfokus SE',
        'link' => 'https://rorfokus.se/'
    ),
    $xml_items);

// function call to convert array to xml
array_to_xml($xmlArr,$xml_items);

//saving generated xml file
$xml_items->asXML('loop54feed-rorfokus-se.xml');

/**
 * @param $feed_info
 * @param $xml_feed_info
 */
function array_to_xml($feed_info, &$xml_feed_info) {
    foreach($feed_info as $key => $value) {

        if(is_array($value)) {

            if(preg_match('/Product/', $key)) {
                $key = 'Product';
            } else {
                $key = is_numeric($key) ? "element" : $key;
            }
            /*
            if($key == 'additional_image_link') {
                foreach($value as $v1) {
                    $xml_feed_info->addChild($key, htmlspecialchars(trim($v1)));
                }
            } else {
                $subnode = $xml_feed_info->addChild($key);
                array_to_xml($value, $subnode);
            }
            */
            $subnode = $xml_feed_info->addChild($key);
            array_to_xml($value, $subnode);

        }
        else {
            $key = is_numeric($key) ? "element" : $key;
            $value = trim($value);

            $xml_feed_info->addChildWithCDATA($key, $value);
            //$xml_feed_info->addChild($key, '<![CDATA['.html_entity_decode(trim($value), ENT_XML1, 'UTF-8').']]>');
        }
    }
}

/**
 * @param $str
 * @param $sub
 * @param int $max
 * @return mixed
 */
function xmlAllowedStringSize($str, $sub, $max=35, $keyword = false, $headline = false) {

    $str = html_entity_decode(trim($str), ENT_QUOTES, 'UTF-8');

    if($keyword) {

        $str = preg_replace('/[^\w\d\p{L}\s]/u', '', $str);

        //$replaceFrom = array('~','+','-','_','.','[',']',',','-->','->','*','|',':',';','(',')','<','>','&','\'','"');
        //$replaceTo = array(' ',' ','','','','','','','','','','','','','','','','','');
        //$str = str_replace($replaceFrom, $replaceTo, $str);

        $newArr = array();
        //$str = preg_replace( '/[^a-zA-Z0-9\såæéø]/', ' ', $str);
        $str = explodeByStringLength($str, 120);
        foreach(explode(' ', $str) as $v) {
            $v = strtolower(trim($v));
            if(strlen($v) > 1) {
                $newArr[] = $v;
            }
        }
        $newArr = array_slice(array_unique($newArr), 0, 10);
        $ret = implode(' ', $newArr);
        //$ret = preg_replace('/\s+?(\S+)?$/','', substr($ret, 0, $max-($sub+1)));
        $ret = explodeByStringLength($ret, $max-($sub+1));

    } else {

        $replaceFrom = array(',','-->','->','*','|',':',';','(',')','<','>','&','\'','"');
        $replaceTo = array('','-','-','','','','',' ',' ','','','','','');
        $str = str_replace($replaceFrom, $replaceTo, $str);

        //$ret = preg_replace('/\s+?(\S+)?$/','', substr(htmlspecialchars_decode($str), 0, $max-($sub+1)));
        $ret = explodeByStringLength($str, $max-($sub+1));

        // if headline - remove 'til' from the end
        if($headline) {
            $ret = preg_replace('/til$/', '', $ret);
        }
    }

    $ret = trim($ret);
    foreach(array('-',',','/','+') as $v) {
        if($v == substr($ret, -1)) {
            $ret = substr($ret, 0, -1);
            $ret = trim($ret);
        }
    }

    $ret = trim($ret);

    return $ret;
}

Class SimpleXMLElementExtended extends SimpleXMLElement {

    /**
     * Adds a child with $value inside CDATA
     * @param unknown $name
     * @param unknown $value
     */
    public function addChildWithCDATA($name, $value = NULL) {
        $new_child = $this->addChild($name);

        if ($new_child !== NULL) {
            $node = dom_import_simplexml($new_child);
            $no   = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($value));
        }

        return $new_child;
    }
}

?>